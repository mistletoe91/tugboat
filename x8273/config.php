<?php
$configMaster = array (
   'local.tugboat.cc' => array(
                        'http'=>'http',
                        'https'=>'http',
                        'base_url'=>'local.tugboat.cc',
                        'base_dir'=>'/var/www/html/tugboat',
                        'db'=>'tugboat_new'
                        ),
    'local.shop.tugboat.cc' => array(
        'http'=>'http',
        'https'=>'http',
        'base_url'=>'local.shop.tugboat.cc',
        'base_dir'=>'/var/www/html/tugboat',
        'db'=>'tugboat_new'
    ),

    'dev.tugboat.cc' => array(
                          'http'=>'http',
                          'https'=>'http',
                          'base_url'=>'dev.tugboat.cc',
                          'base_dir'=>'/var/www/html/dev.tugboat.cc',
                          'db'=>'tugboat_dev_new'
                        ),
    'shopdev.tugboat.cc' => array(
        'http'=>'http',
        'https'=>'http',
        'base_url'=>'shopdev.tugboat.cc',
        'base_dir'=>'/var/www/html/dev.tugboat.cc',
        'db'=>'tugboat_dev_new'
    ),

    'www.tugboat.cc' => array(
                          'http'=>'http',
                          'https'=>'https',
                          'base_url'=>'www.tugboat.cc',
                          'base_dir'=>'/var/www/html/tugboat.cc',
                          'db'=>'tugboat'
                        ),
    'tugboat.cc' => array(
                          'http'=>'http',
                          'https'=>'https',
                          'base_url'=>'www.tugboat.cc',
                          'base_dir'=>'/var/www/html/tugboat.cc',
                           'db'=>'tugboat'
                        ),
    'shop.tugboat.cc' => array(
        'http'=>'http',
        'https'=>'https',
        'base_url'=>'shop.tugboat.cc',
        'base_dir'=>'/var/www/html/tugboat.cc',
        'db'=>'tugboat'
    )
);

// HTTP
define('HTTP_SERVER', $configMaster [$_SERVER['SERVER_NAME']]['http'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/x8273/');
define('HTTP_CATALOG', $configMaster [$_SERVER['SERVER_NAME']]['http'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/');

// HTTPS
define('HTTPS_SERVER', $configMaster [$_SERVER['SERVER_NAME']]['https'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/x8273/');
define('HTTPS_CATALOG', $configMaster [$_SERVER['SERVER_NAME']]['https'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/');

// DIR
define('DIR_APPLICATION', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/x8273/');
define('DIR_SYSTEM', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/');
define('DIR_IMAGE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/image/');
define('DIR_LANGUAGE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/x8273/language/');
define('DIR_TEMPLATE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/x8273/view/template/');
define('DIR_CONFIG', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/config/');
define('DIR_CACHE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/cache/');
define('DIR_DOWNLOAD', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/download/');
define('DIR_LOGS', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/logs/');
define('DIR_MODIFICATION', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/modification/');
define('DIR_UPLOAD', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/upload/');
define('DIR_CATALOG', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'tugboat');
define('DB_PASSWORD', 'Qwert1357!');
define('DB_DATABASE', $configMaster [$_SERVER['SERVER_NAME']]['db']);
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');


