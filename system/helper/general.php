<?php
function token($length = 32) {
	// Create random token
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
	$max = strlen($string) - 1;
	
	$token = '';
	
	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, $max)];
	}	
	
	return $token;
}

if(!function_exists('mailInfo')) {
	function mailInfo($obj) {
		$mailInfo = array();
		$mailInfo['protocol'] = $obj->config->get('config_mail_protocol');
		$mailInfo['parameter'] = $obj->config->get('config_mail_parameter');
		$mailInfo['smtp_hostname'] = $obj->config->get('config_mail_smtp_hostname');
		$mailInfo['smtp_username'] = $obj->config->get('config_mail_smtp_username');
		$mailInfo['smtp_password'] = html_entity_decode($obj->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mailInfo['smtp_port'] = $obj->config->get('config_mail_smtp_port');
		$mailInfo['smtp_timeout'] = $obj->config->get('config_mail_smtp_timeout');
		return $mailInfo;
	}
}

/**
 * Backwards support for timing safe hash string comparisons
 * 
 * http://php.net/manual/en/function.hash-equals.php
 */

if(!function_exists('hash_equals')) {
	function hash_equals($known_string, $user_string) {
		$known_string = (string)$known_string;
		$user_string = (string)$user_string;

		if(strlen($known_string) != strlen($user_string)) {
			return false;
		} else {
			$res = $known_string ^ $user_string;
			$ret = 0;

			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);

			return !$ret;
		}
	}
}