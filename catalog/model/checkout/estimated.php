<?php

class ModelCheckoutEstimated extends Model {
    public function getProductById($product) {

        $whereClause = "";
        foreach($product as $i=>$v) {
            if($whereClause){
                $whereClause .= ",$v";
            } else {
                $whereClause .= "$v";
            }            
        }#end foreach 

        if($whereClause){
            $whereClause  = " IN ($whereClause) ";
        }
 

        $query = $this->db->query("SELECT text FROM ".DB_PREFIX."product_attribute INNER JOIN ".DB_PREFIX."attribute_description ON ".DB_PREFIX."product_attribute.attribute_id = ".DB_PREFIX."attribute_description.attribute_id WHERE ".DB_PREFIX."attribute_description.name = 'Turnaround' AND ".DB_PREFIX."attribute_description.language_id = 1 AND ".DB_PREFIX."product_attribute.product_id  $whereClause" ); 
        return $query->row;
    }
}