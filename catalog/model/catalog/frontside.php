<?php

class ModelCatalogFrontside extends Model {
  public function getFrontsides($product_id) {
    // $sql = "SELECT product_id, model, image, price FROM `".DB_PREFIX."product` WHERE status = 1";

    //$sql = "SELECT oc_product.product_id, model, image, keyword, category_id FROM ".DB_PREFIX."product LEFT JOIN ".DB_PREFIX."url_alias ON CONCAT('product_id=', oc_product.product_id) INNER JOIN ".DB_PREFIX."product_to_category ON ".DB_PREFIX."product_to_category.product_id = oc_product.product_id";

     // Edited by Rajah

    if($product_id > 0){

           $query = "SELECT category_id FROM oc_product_to_category WHERE product_id = ".$product_id ;
           $data  = $this->db->query($query)->row;

          /*
           $sql = "SELECT oc_product.product_id, model, image, keyword, category_id FROM oc_product
           LEFT JOIN oc_url_alias ON CONCAT('product_id=', oc_product.product_id) = oc_url_alias.query
           INNER JOIN oc_product_to_category ON oc_product_to_category.product_id = oc_product.product_id
           WHERE oc_product.product_id IN (SELECT product_id FROM oc_product_to_category WHERE category_id =".$data['category_id'].")";
          */

          $sql = "SELECT oc_product.product_id, model, oc_product.image, keyword, oc_product_to_category.category_id, oc_category.json_back_option FROM oc_product
          LEFT JOIN oc_url_alias ON CONCAT('product_id=', oc_product.product_id) = oc_url_alias.query
          INNER JOIN oc_product_to_category ON oc_product_to_category.product_id = oc_product.product_id
          LEFT JOIN oc_category ON oc_category.category_id = oc_product_to_category.category_id
          WHERE oc_product.product_id IN (SELECT product_id FROM oc_product_to_category WHERE category_id =".$data['category_id'].")";

    }else{
          $sql = "SELECT oc_product.product_id, model, image, keyword, category_id FROM ".DB_PREFIX."product LEFT JOIN ".DB_PREFIX."url_alias ON CONCAT('product_id=', oc_product.product_id) = oc_url_alias.query INNER JOIN ".DB_PREFIX."product_to_category ON ".DB_PREFIX."product_to_category.product_id = oc_product.product_id";
    }

    $front_data = array();

    $query = $this->db->query($sql);

    foreach ($query->rows as $result) {
      //$front_data[$result['frontside_id']] = $result;
      $front_data[$result['product_id']] = $result;
    }

    return $front_data;
  }
}
