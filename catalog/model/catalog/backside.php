<?php
class ModelCatalogBackside extends Model {
	 
	public function getBacksides($json_default_back) {
		
		// $sql = "SELECT backside_id,name,code,'' as code,price FROM `".DB_PREFIX."backside` WHERE status = 1";
		$sql = "SELECT backside_id,name,code,price FROM ".DB_PREFIX."backside WHERE status = 1";	

		$re_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			if($json_default_back){
				if($json_default_back != $result['backside_id'] ){
					$result['code'] =  ""; // don`t send code when we don`t need it .ie we only need code for the default one
				}
			}
			$re_data[$result['backside_id']] = $result;
		}

		return $re_data;
	}

	public function getBacksideById($json_default_back) {
		
		$sql = "SELECT backside_id,name,code as code,price FROM `".DB_PREFIX."backside` WHERE status = 1 and backside_id=". $json_default_back;
 
		$re_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			if($json_default_back){
				if($json_default_back != $result['backside_id'] ){
					$result['code'] =  ""; // don`t send code when we don`t need it .ie we only need code for the default one
				}
			}
			$re_data[$result['backside_id']] = $result;
		}

		return $re_data;
	}

    
}
