<?php
class ControllerProductlandingPosters extends Controller {
  private $error  = array();
  private $record = array();


  public function index() {
      $this->load->language('information/productbanner');
      $data['action'] = $this->url->link('productlanding/posters', '', true);

      $data['heading_title']   = $this->language->get('heading_title');
      $data['entry_name']      = $this->language->get('entry_name');
      $data['entry_email']     = $this->language->get('entry_email');
      $data['text_telephone']  = $this->language->get('text_telephone');
      $data['entry_pdf']       = $this->language->get('entry_pdf');


			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

      $data['priceImage']      = '/image/priceList.png';

	    $this->response->setOutput($this->load->view('productlanding/posters', $data));
	}
}
