<?php
class ControllerCommonSidebar extends Controller {
	public function index() {

		$this->load->model('catalog/information');
        $this->load->model('catalog/category');

        $data = array();

        foreach ($this->model_catalog_category->getCategories(array()) as $category) {
            $name     = $category['name'];
            $category_id = $category['category_id'];
            $data['catList'][] = $this -> getCategories($name, $category_id);
        }

		return $this->load->view('common/sidebar', $data);
	}

    protected function getCategories($name, $parent_id) {

        $results = $this->model_catalog_category->getCategories($parent_id);


        $outPut = "";

        $outPut .= '<li>'.$name;
        if($results) {
            $outPut .= '<ul>';
            foreach ($results as $result) {
                $outPut .= '<li> <a href="index.php?_route_=' . $result['meta_title'] . '"> ' .substr(ucfirst($result['name']), 0,22) . '</a>';
                $product = $this->getProduct($result["category_id"]);
                if ($product) {
                    $outPut .= '<ul>';
                    foreach ($product as $resultPro) {
                        $outPut .= '<li> <a href="index.php?_route_=' . $resultPro['meta_title'] . '"> ' . substr(ucfirst($resultPro["name"]),0, 22) . '</a></li>';
                    }
                    $outPut .= '</ul>';
                }
                $outPut .= '</li>';
            }

            $outPut .= '</ul>';
        }
        $outPut .= '</li>';

        return $outPut;
    }

    protected function getProduct($category_id)
    {

        $this->load->model('catalog/product');
        $data['products'] = array();

        $filter_data = array(
            'filter_category_id' => $category_id
        );


        return $results = $this->model_catalog_product->getProducts($filter_data);

    }

}
