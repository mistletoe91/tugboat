<?php
class ControllerCommonShopHeader extends Controller {
	public function index() {
		$this->load->model('catalog/information');

		$data = array();

		return $this->load->view('common/shop_header', $data);
	}
}
