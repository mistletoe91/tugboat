<?php
class ControllerCommonHome extends Controller
{
    public function index()
    {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }
        
        if ($this->config->get('config_name') == 'Print Store') {
            $data['shop_header'] = $this->load->controller('common/shop_header');
            $data['sidebar'] = $this->load->controller('common/sidebar');
        }
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/home', $data));
    }

    public function productInquiry()
    {
        $subject = 'Tugboat -- Product Inquiry';
        $responseSubject = 'We got your message!';
        $data = array();
        $data['name'] = 'Sailor';
        $data['email'] = $_POST['email'];
        $data['form_name'] = 'Product Inquiry';
        $data['userInput'] = array();
        $data['userInput']['product'] = array('Product', $_POST['product']);
        $data['userInput']['quantity'] = array('Quantity', $_POST['quantity']);
        $data['userInput']['finish'] = array('Finish', $_POST['finish']);
        $data['userInput']['size'] = array('Size', $_POST['size']);
        $data['userInput']['comment'] = array('Comment', $_POST['comment']);
        $data['mail_style'] = $this->load->view('mail/mailStyle');

        $mailInfo = mailInfo($this);

        $mail = new Mail();
        foreach ($mailInfo as $key => $value) {
            $mail->$key = $value;
        }

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->request->post['email']);
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setHtml($this->load->view('mail/toAdmin', $data));
        $mailSuccess = $mail->send();

        $responseMail = new Mail();
        foreach ($mailInfo as $key => $value) {
            $responseMail->$key = $value;
        }

        $responseMail->setTo($_POST['email']);
        $responseMail->setFrom($this->config->get('config_email'));
        $responseMail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $responseMail->setSubject($responseSubject);
        $responseMail->setHtml($this->load->view('mail/response', $data));
        $responseMail->send();
    }

    public function businessInquiry()
    {
        $subject = 'Tugboat -- Business Inquiry';
        $responseSubject = 'We got your message!';
        $data = array();
        $data['name'] = $_POST['name'];
        $data['email'] = $_POST['email'];
        $data['form_name'] = 'Business Inquiry';
        $data['userInput'] = array();
        $data['userInput']['businessName'] = array('Business Name', $_POST['businessName']);
        $data['userInput']['position'] = array('Position', $_POST['position']);
        $data['userInput']['phone'] = array('Phone', "{$_POST['phone']} x{$_POST['ext']}");
        $data['tugboat_phone'] = $this->config->get('config_telephone');
        $data['mail_style'] = $this->load->view('mail/mailStyle');

        $mailInfo = mailInfo($this);

        $mail = new Mail();
        foreach ($mailInfo as $key => $value) {
            $mail->$key = $value;
        }

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($_POST['email']);
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($this->load->view('mail/toAdmin', $data));
        $mail->send();

        $responseMail = new Mail();
        foreach ($mailInfo as $key => $value) {
            $responseMail->$key = $value;
        }

        $responseMail->setTo($_POST['email']);
        $responseMail->setFrom($this->config->get('config_email'));
        $responseMail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $responseMail->setSubject(html_entity_decode($responseSubject, ENT_QUOTES, 'UTF-8'));
        $responseMail->setHtml($this->load->view('mail/response', $data));
        $responseMail->send();
    }    
}
