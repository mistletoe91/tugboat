<?php
/**
 * Created by PhpStorm.
 * User: tugboat
 * Date: 24/01/18
 * Time: 12:49 PM
 */


class ControllerPrintshopHome extends Controller
{
    public function index()
    {

        $this->load->model('catalog/category');


        foreach ($this->model_catalog_category->getCategories(array()) as $category) {
            $name     = $category['name'];
            $category_id = $category['category_id'];
            if($name == "printshop") {
                $data['categories'] = $this->getCategories($category_id);
            }
        }

        $data['logo']            = "";
        $data['header']          = $this->load->controller('common/header');
        $data['footer']          = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('printshop/home',$data));

    }


    protected function getCategories($parent_id, $current_path = '') {
        $output = '';

        $results = $this->model_catalog_category->getCategories($parent_id);

        $output .= '<div class="dropdownPrintShoptitile">';
        $output .= '<span> Shop By Department</span>';
        $output .= '</div>';

        foreach ($results as $result) {

            $output .= '<div class="dropdownPrintShop">';
            $output .= '<span>'.$result['name'].'</span>';
            $output .= '<div class="dropdownPrintShop-content">';

            $children = $this->model_catalog_category->getCategories($result['category_id'], $current_path);

            foreach ($children as $child){
                if (!$current_path) {
                    $new_path = $result['category_id'].'_'.$child['category_id'];
                } else {
                    $new_path = $current_path . '_'.$result['category_id'].'_'.$child['category_id'];
                }

                $output .= ' <a href="index.php?route=product/category&path='.$new_path.'"> <p>'.$child['name'].'</p> </a>
                         ';

            }

            if ($this->category_id == $result['category_id']) {
               // $output .= '<a href="' . $this->model_tool_seo_url->rewrite($this->url->http('product/category&path=' . $new_path))  . '"><b>' . $result['name'] . '</b></a>';
            } else {
                //$output .= '<a href="' . $this->model_tool_seo_url->rewrite($this->url->http('product/category&path=' . $new_path))  . '">' . $result['name'] . '</a>';
            }


            $output .= '</div></div>';
        }

        return $output;
    }

}