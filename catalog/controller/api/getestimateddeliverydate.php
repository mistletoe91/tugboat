<?php

class Controllerapigetestimateddeliverydate extends Controller {
    public function estimate () {
        $json = array();

        $this->load->model("checkout/estimated");
        if($this->request->post['product_id']) {

            $data = explode(',', $this->request->post['product_id']);

            $estimates = $this->model_checkout_estimated->getProductById($data);


            $val = max($estimates);

            $json['success']['estimates'] = $val + 1;
        }
        
    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }
 
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));

    }
}