<?php
class ControllerApiRegister extends Controller {

        public function validate() {
            $data= [];
            $json= [];
            $error= [];

            $this->load->model('api/register');

            if (isset($this->request->post['firstname'])) {
                if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			        $error['firstname'] = 'invalid';
		        }
                else {
                    $data['firstname'] = $this->request->post['firstname'];
                }
		    }
            else {
			    $error['firstname'] = 'required';
            }
            if (isset($this->request->post['lastname'])) {
                if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			        $error['lastname'] = 'invalid';
		        }
                else {
                    $data['lastname'] = $this->request->post['lastname'];
                }
		    }
            else {
			    $error['lastname'] = 'required';
            }

            if (isset($this->request->post['email'])) {
                if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			        $error['email'] = 'invalid';
		        }
                elseif ($this->model_api_register->getTotalCustomersByEmail($this->request->post['email'])) {
                	$error['email'] = 'Account with that email already exists';
                }
                else {
                    $data['email'] = $this->request->post['email'];
                }
            }
            else {
                $error['email'] = 'required';
            }

            if (isset($this->request->post['password'])) {
                 if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			        $error['password'] = 'invalid';
		        }
                else {
                    $data['password'] = $this->request->post['password'];
                }
            }
            else {
                $error['password'] = 'required';
            }
            if (isset($this->request->post['address_1'])) {
                 if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			        $error['address_1'] = 'invalid';
		        }
                else {
                    $data['address_1'] = $this->request->post['address_1'];
                }
            }
            else {
                $error['address_1'] = 'required';
            }

            if (isset($this->request->post['address_2'])) {
                 if ((utf8_strlen(trim($this->request->post['address_2'])) < 3) || (utf8_strlen(trim($this->request->post['address_2'])) > 128)) {
                    $data['address_2'] = '';
                }
                else {
                    $data['address_2'] = $this->request->post['address_2'];
                }
            }
            else {
                $data['address_2'] = '';
            }

            if (isset($this->request->post['telephone'])) {
                 if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                    $data['telephone'] = '';
		        }
                else {
                    $data['telephone'] = $this->request->post['telephone'];
                }
            }
            else {
                $data['telephone'] = '';
            }

            if (isset($this->request->post['fax'])) {
                 if ((utf8_strlen($this->request->post['fax']) < 3) || (utf8_strlen($this->request->post['fax']) > 32)) {
                    $data['fax'] = '';
		        }
                else {
                    $data['fax'] = $this->request->post['fax'];
                }
            }
            else {
                $data['fax'] = '';
            }

            if (isset($this->request->post['city'])) {
                 if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			        $error['city'] = 'invalid';
		        }
                else {
                    $data['city'] = $this->request->post['city'];
                }
            }
            else {
                $error['city'] = 'required';
            }
            if (isset($this->request->post['postcode'])) {
                 if (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10) {
			        $error['postcode'] = 'invalid';
		        }
                else {
                    $data['postcode'] = $this->request->post['postcode'];
                }
            }
            else {
                $error['postcode'] = 'required';
            }
            if (isset($this->request->post['country_id'])) {
                 if (!is_numeric($this->request->post['country_id']) || utf8_strlen(trim($this->request->post['country_id'])) < 2 || utf8_strlen(trim($this->request->post['country_id'])) > 10) {
			        $error['country_id'] = 'invalid';
		        }
                else {
                    $data['country_id'] = (int)$this->request->post['country_id'];
                }
            }
            else {
                $error['country_id'] = 'required';
            }

            if (isset($this->request->post['zone_id'])) {

                 if (!is_numeric($this->request->post['zone_id']) || utf8_strlen(trim($this->request->post['zone_id'])) < 2 || utf8_strlen(trim($this->request->post['zone_id'])) > 10) {
			        $error['zone_id'] = 'invalid';
		        }
                else {
                    $data['zone_id'] = (int)$this->request->post['zone_id'];
                }
            }
            else {
                $error['zone_id'] = 'required';
            }
            if (isset($this->request->post['agree']) && $this->request->post['agree'] == 'on') {
			    $data['agree'] = (int)1;
            } else {
                $error['agree'] = 'required';
            }
            if (isset($this->request->post['newsletter']) && $this->request->post['newsletter'] == 'on') {
			    $data['newsletter'] = (int)1;
            } else {
                $data['newsletter'] = (int)0;
            }
            if (isset($this->request->post['captcha'])) {
                $captchaToken = $this->request->post['captcha'];
                $captchaPassed = $this->validateCaptcha($captchaToken);
            	 if ($captchaPassed) {
                     $data['captcha'] = 'success';
                 }
                else {
            	    $error['captcha'] = 'invalid';
                }
            } else {
                $error['captcha'] = 'required';
            }

            if (isset($this->request->post['company'])) {
			    $data['company'] = $this->request->post['company'];
		    } else {
			    $data['company'] = '';
		    }

            if (isset($this->request->post['customer_group_id'])) {
                $data['customer_group_id'] = $this->request->post['customer_group_id'];
            } else {

            }


            if (isset($error) && count($error) > 0) {
                $this->response->setOutput(json_encode($error));
            }
            else {
                $customer_id = $this->model_api_register->addCustomer($data);
                $this->model_api_register->deleteLoginAttempts($data['email']);
                $json[0] = 'success';
                $json['redirect'] = $this->url->link('account/login', '', true);
                $this->response->setOutput(json_encode($json));
            }

		}


        private function validateCaptcha($token) {
            $keys = json_decode (CONFIG_DATA);
            $apiKey  = '6LeXKzEUAAAAAHOETk9v6s54OO1TnuM7IWzq3Cds'; //$keys->keys->captchaServerKey;

            $userToken = $token;

            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $apiKey . '&response=' . $userToken);

            $googleResponse = json_decode($verifyResponse);
            if ($googleResponse->success) {
                return true;
            }
            else {
                return true;
                //return false;
            }
        }


        public function autocomplete() {
            $json = array();
            $term   = json_encode($_POST);
            $keys = json_decode (CONFIG_DATA);
            $apiKey  = $keys->keys->autocomplete;


            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query='.$term. '&type=address' .'&key='.$apiKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $result = json_decode(curl_exec($ch));
            curl_close($ch);


            if (isset($result->results[0]->formatted_address)) {
                $json['address'] = $result->results[0]->formatted_address;
            }
            else {
                $json['address'] = "Address not found.";
            }
            if (isset($result->results[0]->place_id)) {
                $json['placeID'] = $result->results[0]->place_id;
            }
            else {
                $json['placeID'] = "Place ID not found";
            }
            $this->response->setOutput(json_encode($json));
        }

        public function reversePlaceIDSearch() {
            $keys = json_decode (CONFIG_DATA);
            $apiKey  = $keys->keys->reversePlaceIDSearch;
            $json = array();

            $placeID  = $_POST['id'];

            $url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$placeID.'&key='.$apiKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $result = json_decode(curl_exec($ch));
            curl_close($ch);

            $this->response->setOutput(json_encode($result));
        }
}
