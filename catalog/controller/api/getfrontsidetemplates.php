<?php
// catalog/controller/api/getfrontsidetemplatesbyproductid.php
class Controllerapigetfrontsidetemplates extends Controller {
  public function frontside() {
    $this->load->language('api/getfrontsidetemplatesbyproductid');
    $json = array();

    //Disabled login
    if (!isset($this->session->data['api_id']) && 00) {
      $json['error']['warning'] = $this->language->get('error_permission');
    } else {

    // load model
    $this->load->model('catalog/frontside');
    if(isset($this->request->get['product_id'])){

      $product_id = $this->request->get['product_id'];
      $frontsides = $this->model_catalog_frontside->getFrontsides($product_id);

    } else {
      
        $frontsides = $this->model_catalog_frontside->getFrontsides();

    }//endif

      $json['success']['frontsides'] = $frontsides;

    }#endif

    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
