<?php
class ControllerApiSetting extends Controller {

  public function edit(){
    $error = array();
		$this->load->language('account/edit');
		$this->load->model('account/customer');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

      if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
  			$error['firstname'] = $this->language->get('error_firstname');
  		}

  		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
  			$error['lastname'] = $this->language->get('error_lastname');
  		}

  		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
  			$error['email'] = $this->language->get('error_email');
  		}

  		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
  			$error['warning'] = $this->language->get('error_exists');
  		}

  		if ((utf8_strlen($this->request->post['telephone']) < 10) || (utf8_strlen($this->request->post['telephone']) > 15)) {
  			$error['telephone'] = $this->language->get('error_telephone');
  		}

      if(COUNT($error) > 0){
        $this->response->setOutput(json_encode($error));
      }else{
        $this->model_account_customer->editCustomer($this->request->post);

  			$this->session->data['success'] = $this->language->get('text_success');

        $this->response->setOutput(json_encode('1'));
      }
    }else{
      	$this->response->setOutput(json_encode('Some thing went wrong!'));
    }

	}

  public function changePassword()
  {
    $error  = array();
    $this->load->language('account/password');

    if ($this->request->server['REQUEST_METHOD'] == 'POST'){
      if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
  			$error['password'] = $this->language->get('error_password');
  		}

  		if ($this->request->post['confirm'] != $this->request->post['password']) {
  			$error['confirm'] = $this->language->get('error_confirm');
  		}

      if(COUNT($error) > 0){
        $this->response->setOutput(json_encode($error));
      }else{
        $this->load->model('account/customer');
        $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
  			$this->session->data['success'] = $this->language->get('text_success');
        $this->response->setOutput(json_encode('1'));
      }
    }else{
      	$this->response->setOutput(json_encode('Some thing went wrong!'));
    }
  }

  public function newsLetter()
  {
    $this->load->language('account/newsletter');

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->load->model('account/customer');

			$this->model_account_customer->editNewsletter($this->request->post['newsletter']);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->setOutput(json_encode('1'));
		}else{
      $this->response->setOutput(json_encode('Some thing went wrong!'));
    }
  }

}
