<?php
// catalog/controller/api/getbacksidetemplatesbyproductid.php
class ControllerApiGetbacksidetemplatesbyproductid extends Controller {
  public function backside() {
    $this->load->language('api/getbacksidetemplatesbyproductid');
    $json = array();

    //Disabled login
    if (!isset($this->session->data['api_id']) && 00) {
      $json['error']['warning'] = $this->language->get('error_permission');
    } else {

    // load model
    $this->load->model('catalog/backside');           
    if(isset($this->request->post['backsideid']) && $this->request->post['backsideid']){
      // get backside by Id
      $data = array ( 'backsideid'=> $this->request->post['backsideid']);
      $backsides = $this->model_catalog_backside->getBacksideById($data);
    } else {
      // get backside
      $json_default_back = '';
      if(isset($this->request->get['json_default_back'])){
        $json_default_back = $this->request->get['json_default_back'];
      }
      $backsides = $this->model_catalog_backside->getBacksides($json_default_back);
    }//endif



      $json['success']['backsides'] = $backsides;

    }#endif

    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
