
var backside;
var backBleedGroup3;
var backBleedGroup;
var finalBack;
var backBleedGroup2;

$("#designer-wrapper").hide();

$(document).ready(function() {
	// setCanvasSizeBack();
	var backcanvas;
	var frontcanvas_again = document.getElementById("canvas-product-front");
	var productid = 10;

	// backcanvas = new fabric.Canvas("canvas-product-back", {
	// 	width: $("#ctl_canvas_width").html(),
	// 	height: $("#ctl_canvas_height").html(),
	// 	preserveObjectStacking: true,
	// 	selection: false
	// });


function setBackCanvasProperty() {
		backcanvas = new fabric.Canvas("canvas-product-back", {
		 width: $("#ctl_canvas_width").html(),
		 height: $("#ctl_canvas_height").html(),
		 bleed: $("#ctl_canvas_bleed").html(),
		 saveZoon: $("#ctl_canvas_savezoon").html(),
		 preserveObjectStacking: true,
		 selection: false
	  });
}

		setBackCanvasProperty();

		const bCh = backcanvas.height;
		const bCw = backcanvas.width;
		const bl  = backcanvas.bleed ? parseInt(canvas.bleed) : 18.5;
		const sz  = backcanvas.saveZoon ? parseInt(canvas.saveZoon) : 37.50;

    //Click event
		$("#backsavebtn").click(function() {
			$("ul.setup-panel li:eq(2)").removeClass("disabled");
			$('ul.setup-panel li a[href="#step-3"]').trigger("click");
			 findGetParameter("designermode");
		});

		$("ul.setup-panel li:eq(1)").on("click", function() {
			backcanvas.remove(backBleedGroup3);
			backcanvas.remove(backBleedGroup2);
			backcanvas.remove(finalBack);
			setTimeout(function() {
				backBleedGroup3.selectable = false;
				backBleedGroup3.evented = false;
				backcanvas.add(backBleedGroup3);
				backcanvas.add(backBleedGroup2);
			}, 50);
		});

		$('ul.setup-panel li a[href="#step-3"]').click(function() {
			fabric.util
				.toArray(document.querySelectorAll("input[type='checkbox']"))
				.forEach(function(el) {
					el.disabled = false;
				});
			backcanvas.remove(backBleedGroup3);
			backcanvas.remove(backBleedGroup2);
			backcanvas.add(finalBack);
			//finalFront.bringToFront();
			sendStringRepresentation(0);
			sendStringRepresentation(1);
		});





	// backcanvas.setWidth($("#ctl_canvas_width").html());
	// backcanvas.setHeight($("#ctl_canvas_height").html());
	// backcanvas.calcOffset();

	fabric.Object.prototype.set({
		transparentCorners: false,
		cornerColor: "#C40005",
		borderColor: "#10009B"
	});

	/***
  * ---------------------RESPONSIVE CANVAS---------------------------
  * This function resizes the canvas so that it maintains aspect ratio
  * across all different mobile devices. First it takes the canvas sizing element
  * (in this case it's col-md-8) and creates a width offset. Then the width and height
  * properties also get offsets based on the canvas sizing element. Next, the ratio gets
  * calculated by getting the canvas width and dividing it by the height.
  * Finally, a check is performed to see if the width divided by height is greater
  * than the ratio set up earlier. If so, we need to set the width equal to the height
  * multiplied by the ratio. Otherwise, we set the height equal to the width divided by the ratio.
  * Zoom is also used here to ensure that nothing looks too small on a smaller device.
  * ***/

	// function backResize() {
	// var backCanvasSizer = document.getElementById("back-resp-canvas-elem");
	// var backCanvasScaleFactor = backCanvasSizer.offsetWidth/525;
	// var backWidth = backCanvasSizer.offsetWidth;
	// var backHeight = backCanvasSizer.offsetHeight;
	// console.log('height: ' + backHeight + 'width: ' + backWidth);
	// var backRatio = backcanvas.getWidth() / backcanvas.getHeight();
	//    if((backWidth/backHeight)>backRatio){
	//      backWidth = backHeight*backRatio;
	//    } else {
	//      backHeight = backWidth / backRatio;
	//    }
	// var backScale = backWidth / backcanvas.getWidth();
	// var backZoom = backcanvas.getZoom();
	// backZoom *= backScale;
	//
	// backcanvas.setDimensions({ width: backWidth, height: backHeight });
	// backcanvas.setViewportTransform([backZoom , 0, 0, backZoom , 0, 0]);
	// }
	//
	// window.addEventListener('load', backResize, false);

	//No actually ya this first one
	$.ajax({
		type: "GET",
		url: "/index.php?route=api/getbacksidetemplatesbyproductid/backside",
		data: {
			productid: productid,
			json_default_back: getElementValueByPlaceholder("json_default_back")
		},
		dataType: "json",
		success: function(data) {
			backside = data;
			doint(getElementValueByPlaceholder("json_default_back"), 1);
		}
	});

	function getElementValueByPlaceholder(placeholderName) {
		var ctlId = "";
		var inputs = document.querySelectorAll("textarea");
		Array.prototype.forEach.call(inputs, function(i) {
			if (!String.prototype.startsWith) {
				String.prototype.startsWith = function(searchString, position) {
					position = position || 0;
					return this.substr(searchString, position) === position;
				};
			}
			if (i.name.startsWith("option[")) {
				if (i.getAttribute("placeholder") === placeholderName) {
					ctlId = i.id;
				}
			}
		});
		return $("#" + ctlId).val();
	} //end function

	function loadbacktemplate(id) {
		$.ajax({
			type: "GET",
			url: "/index.php?route=api/getbacksidetemplatesbyproductid/backside",
			data: { backsideid: id },
			dataType: "json",
			success: function(data) {
                backside = data;
                doint(getElementValueByPlaceholder("json_default_back"), 1);
			}
		});
	} //end function

	function findGetParameter(parameterName) {
				var result = null,
					tmp = [];
				location.search.substr(1).split("&").forEach(function(item) {
					tmp = item.split("=");
					if (tmp[0] === parameterName) {
						result = decodeURIComponent(tmp[1]);
						$("#designer-wrapper").show();
						// $("#json_stuff").show();
						// $("#json_stuff").append();

						// $("#json_back_stuff").show();
						// $("#json_back_stuff").append();
						// $("#json_stuff").val(jsondefault);
					}
				});
				return result;
    }

	function observeBoolean(property) {
		document.getElementById(property).onclick = function() {
			backcanvas.getActiveObject()[property] = this.checked;
			backcanvas.renderAll();
		};
	}

	function observeNumeric(backProperty) {
		document.getElementById(backProperty).onchange = function() {
			backcanvas.getActiveObject()[backProperty] = this.value;
			backcanvas.renderAll();
		};
	}

	function observeOptionsList(property) {
		var list = document.querySelectorAll("#" + property + ' [type="checkbox"]');
		for (var i = 0, len = list.length; i < len; i++) {
			list[i].onchange = function() {
				backcanvas.getActiveObject()[property](this.name, this.checked);
				backcanvas.renderAll();
			};
		}
	}



	function sendStringRepresentation(front) {

      var multipler = 1;
      var quality = 1;
      var postData;
			var strDataURI;

			canvas.deactivateAll().renderAll();
			backcanvas.deactivateAll().renderAll();



		var json = backcanvas.toJSON(["id"]);
		var forDesignerBack = backcanvas.toJSON(["id"]);

		$("#input-option229").val(JSON.stringify(json));
		$("#json_back_stuff").val(JSON.stringify(forDesignerBack));

		if (front > 0) {
                strDataURI = canvas.toDataURL();
			    //strDataURI = canvas.toDataURLWithMultiplier("jpg", multipler, quality);

					postData = {
					    json: canvas.toJSON (),
					    width: canvas.getWidth(),
					    height: canvas.getHeight(),
					    multiplier: 4
					};

		} else {

					postData = {
					    json: backcanvas.toJSON (),
					    width: backcanvas.getWidth(),
					    height: backcanvas.getHeight(),
					    multiplier: 4
					};

			 strDataURI = backcanvas.toDataURL();
			//strDataURI = backcanvas.toDataURLWithMultiplier("jpg", multipler, quality);
        jsonData = backcanvas.toJSON ();
			  var json = backcanvas.toJSON(["id"]);
			  $("#input-option229").val(JSON.stringify(json));
		}

        $('#divloadingmodal_generic').modal('show');
		strDataURI = strDataURI.substr(22, strDataURI.length);

			var myport = '9001';
			var myprotocol = 'https';
			if((window.location.hostname === "dev.tugboat.cc") || (window.location.hostname === "local.tugboat.cc")){
			    myport = '9000';
			    myprotocol = 'http';
			}

			//create printable image
			$.ajax({
			    url: myprotocol+'://'+window.location.hostname+':'+myport,
			    type: 'POST',
			    contentType: 'application/json; charset=utf-8',
			    data: JSON.stringify(postData),
			    dataType: 'json',
			    front: front,
			    success: function(data) {
			        //console.log(data);
			        //console.log($id_of_fileoutput_back);
			        if (front > 0) {
			            $("#" + $id_of_fileoutput_front).val(data.success);
			        } else {
			            $("#" + $id_of_fileoutput_back).val(data.success);
			        }//endif
			    },
			    error: function(err) {
			        //console.log(err);
			    }
			});

		$.post("/saveimage.php", { str: strDataURI, front: front }, function(data) {
            $('#divloadingmodal_generic').modal('hide');

			var obj = jQuery.parseJSON(data);
			if (obj[0]) {
				if (front > 0) {
					//$("#" + $id_of_fileoutput_front).val(obj[0]);

					//Add image for proof
					// $('#clsproof_front > img').attr('src','/image/u/'+obj [0]+'.png');
					$("#clsproof_front > img").attr({
						crossOrigin: "Anonymous",
						src: "/image/u/" + obj[0] + ".jpg"
					});
				} else {

					//Add image for proof
					// $('#clsproof_back > img').attr('src','/image/u/'+obj [0]+'.png');
					$("#clsproof_back > img").attr({
						crossOrigin: "Anonymous",
						src: "/image/u/" + obj[0] + ".jpg"
					});
				} //endif
			} //endif
		});
	} //end function

	function generatePdf() {
		if (
			$("#clsproof_front > img").attr("src") !== "" &&
			$("#clsproof_back > img").attr("src") !== ""
		) {
			$.post(
				"/makepdf.php",
				{
					imageOne: $("#clsproof_back > img").attr("src"),
					imageTwo: $("#clsproof_back > img").attr("src")
				},
				function(data) {
					var obj = jQuery.parseJSON(data);
					$("#downloadArea").append(
						'<a href="image/u/"' + obj + " download>Download PDF</a>"
					);
				}
			);
		}
	}

	$("#button-cart").on("click", function() {
		generatePdf();
	});

	// select all objects
	function deleteObjects() {
		var activeObject = backcanvas.getActiveObject(),
			activeGroup = backcanvas.getActiveGroup();
		if (activeObject) {
			if (confirm("Are you sure? This action can't be undone")) {
				backcanvas.remove(activeObject);
			}
		} else if (activeGroup) {
			if (confirm("Are you sure? This action can't be undone")) {
				var objectsInGroup = activeGroup.getObjects();
				backcanvas.discardActiveGroup();
				objectsInGroup.forEach(function(object) {
					backcanvas.remove(object);
				});
			}
		}
	}

	//Start over from scratch with a blank canvas after clicking on the clear canvas button (Erase)
	function emptyCanvas() {
		/* Possible refactor needed: Map over the objects currently on the canvas
    /* and assign them to a new group that can be set active all at once so
    /* that when you click the clear button the entire canvas gets cleared
    /* for anyone who might want to start over from scratch on their design
    */
		backcanvas.selection = true;
		if (confirm("Reset Canvas to blank?")) {
			var objs = backcanvas.getObjects().map(function(obj) {
				return obj.set("active", true);
			});
			var backGroup = new fabric.Group(objs, {
				originX: "center",
				originY: "center"
			});

			backcanvas._activeObject = null;
			backcanvas.setActiveGroup(backGroup.setCoords()).renderAll();

			//actually empty the canvas here
			var oldBackCanvas = backcanvas.toJSON(["id"]);
			backcanvas.clear();
		}

		if ($("#hidden-counter-back").val() > 0) {
			$("#hidden-counter-back").val(0);
			$("#back-editable-field-wrapper").html("");
		}
	}

	$("#textcolorback").spectrum({
		color: "#000",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdateBack = color.toHexString();
			var obj = backcanvas.getActiveObject();
			if (obj.get("type") === "textbox") {
				obj.set("fill", colToUpdateBack);
			}

			backcanvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {
			// var colToUpdateBack = color.toHexString();
			// var obj = backcanvas.getActiveObject();
			// obj.set('fill', colToUpdateBack);
			// backcanvas.renderAll();
		},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});

	$("#textbgcolorback").spectrum({
		color: "#000",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdateBack = color.toHexString();
			var obj = backcanvas.getActiveObject();
			obj.setBackgroundColor(colToUpdateBack);
			backcanvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {
			// var colToUpdateBack = color.toHexString();
			// var obj = backcanvas.getActiveObject();
			// obj.setBackgroundColor(colToUpdateBack);
			// backcanvas.renderAll();
		},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});

	document.getElementById("back-font-family").onchange = function() {
		backcanvas.getActiveObject().setFontFamily(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("back-text-font-size").onchange = function() {
		backcanvas.getActiveObject().setFontSize(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("back-text-lines-bg-color").onchange = function() {
		backcanvas.getActiveObject().setTextBackgroundColor(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("back-text-stroke-color").onchange = function() {
		backcanvas.getActiveObject().setStroke(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("back-text-stroke-width").onchange = function() {
		backcanvas.getActiveObject().setStrokeWidth(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("back-text-line-height").onchange = function() {
		backcanvas.getActiveObject().setLineHeight(this.value);
		backcanvas.renderAll();
	};

	document.getElementById("backtext-align").onchange = function() {
		backcanvas.getActiveObject().setTextAlign(this.value);
		backcanvas.renderAll();
	};

	$("#backbtnbold").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			if (backActiveTextObj.get("fontWeight", "bold")) {
				$("#backbtnbold").removeClass("active");
				backActiveTextObj.set("fontWeight", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnbold").addClass("active");
				backActiveTextObj.set("fontWeight", "bold");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			if (backActiveTextObj.get("fontWeight", "bold")) {
				$("#backbtnbold").removeClass("active");
				backActiveTextObj.set("fontWeight", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnbold").addClass("active");
				backActiveTextObj.set("fontWeight", "bold");
				backcanvas.renderAll();
			}
		}
	});

	$("#backbtnitalic").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			if (backActiveTextObj.get("fontStyle", "italic")) {
				$("#backbtnitalic").removeClass("active");
				backActiveTextObj.set("fontStyle", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnitalic").addClass("active");
				backActiveTextObj.set("fontStyle", "italic");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			if (backActiveTextObj.get("fontStyle", "italic")) {
				$("#backbtnitalic").removeClass("active");
				backActiveTextObj.set("fontStyle", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnitalic").addClass("active");
				backActiveTextObj.set("fontStyle", "italic");
				backcanvas.renderAll();
			}
		}
	});

	$("#backbtnunderline").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			if (backActiveTextObj.get("textDecoration", "underline")) {
				$("#backbtnunderline").removeClass("active");
				backActiveTextObj.set("textDecoration", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnunderline").addClass("active");
				backActiveTextObj.set("textDecoration", "underline");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			if (backActiveTextObj.get("textDecoration", "underline")) {
				$("#backbtnunderline").removeClass("active");
				backActiveTextObj.set("textDecoration", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnunderline").addClass("active");
				backActiveTextObj.set("textDecoration", "underline");
				backcanvas.renderAll();
			}
		}
	});
	$("#backbtnstrikethrough").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			if (backActiveTextObj.get("textDecoration", "line-through")) {
				$("#backbtnstrikethrough").removeClass("active");
				backActiveTextObj.set("textDecoration", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnstrikethrough").addClass("active");
				backActiveTextObj.set("textDecoration", "line-through");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			if (backActiveTextObj.get("textDecoration", "line-through")) {
				$("#backbtnstrikethrough").removeClass("active");
				backActiveTextObj.set("textDecoration", "");
				backcanvas.renderAll();
			} else {
				$("#backbtnstrikethrough").addClass("active");
				backActiveTextObj.set("textDecoration", "line-through");
				backcanvas.renderAll();
			}
		}
	});

	$("#backbtnleftalign").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "left") {
				$("#backbtnleftalign").addClass("active");
				backActiveTextObj.set("textAlign", "left");
				backcanvas.renderAll();
			}
			if (backAlignment === "left") {
				$("#backbtnleftalign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "left") {
				$("#backbtnleftalign").addClass("active");
				backActiveTextObj.set("textAlign", "left");
				backcanvas.renderAll();
			}
			if (backAlignment === "left") {
				$("#backbtnleftalign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}
	});

	$("#backbtnrightalign").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "right") {
				$("#backbtnrightalign").addClass("active");
				backActiveTextObj.set("textAlign", "right");
				backcanvas.renderAll();
			}
			if (backAlignment === "right") {
				$("#backbtnrightalign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "right") {
				$("#backbtnrightalign").addClass("active");
				backActiveTextObj.set("textAlign", "right");
				backcanvas.renderAll();
			}
			if (backAlignment === "right") {
				$("#backbtnrightalign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}
	});

	$("#backbtncenteralign").click(function() {
		var backActiveTextObj = backcanvas.getActiveObject();
		if (backActiveTextObj.get("type") === "textbox") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "center") {
				$("#backbtncenteralign").addClass("active");
				backActiveTextObj.set("textAlign", "center");
				backcanvas.renderAll();
			}
			if (backAlignment === "center") {
				$("#backbtncenteralign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}

		if (backActiveTextObj.get("type") === "i-text") {
			var backAlignment = backActiveTextObj.getTextAlign();
			if (backAlignment !== "center") {
				$("#backbtncenteralign").addClass("active");
				backActiveTextObj.set("textAlign", "center");
				backcanvas.renderAll();
			}
			if (backAlignment === "center") {
				$("#backbtncenteralign").removeClass("active");
				backActiveTextObj.set("textAlign", "none");
				backcanvas.renderAll();
			}
		}
	});

	var max_file_size = 5242880; //allowed file size. (1 MB = 1048576)
	var allowed_file_types = [
		"image/png",
		"image/gif",
		"image/jpeg",
		"image/jpg",
		"image/pjpeg"
	]; //allowed file types
	var result_output = "#output"; //ID of an element for response output
	var my_form_id = "#upload_form_back"; //ID of an element for response output
	var total_files_allowed = 3; //Number files allowed to upload
	$(my_form_id).on("submit", function(event) {
		event.preventDefault();
		var proceed = true; //set proceed flag
		var error = []; //errors
		var total_files_size = 0;

		if (!window.File && window.FileReader && window.FileList && window.Blob) {
			//if browser doesn't supports File API
			error.push("Your browser does not support new File API! Please upgrade."); //push error text
		} else {
			var total_selected_files = this.elements["__files[]"].files.length; //number of files

			//limit number of files allowed
			if (total_selected_files > total_files_allowed) {
				error.push(
					"You have selected " +
						total_selected_files +
						" file(s), " +
						total_files_allowed +
						" is maximum!"
				); //push error text
				proceed = false; //set proceed flag to false
			}
			//iterate files in file input field
			$(this.elements["__files[]"].files).each(function(i, ifile) {
				if (ifile.value !== "") {
					//continue only if file(s) are selected
					if (allowed_file_types.indexOf(ifile.type) === -1) {
						//check unsupported file
						error.push("<b>" + ifile.name + "</b> is unsupported file type!"); //push error text
						proceed = false; //set proceed flag to false
					}

					total_files_size = total_files_size + ifile.size; //add file size to total size
				}
			});

			//if total file size is greater than max file size
			if (total_files_size > max_file_size) {
				error.push(
					"You have " +
						total_selected_files +
						" file(s) with total size " +
						total_files_size +
						", Allowed size is " +
						max_file_size +
						", Try smaller file!"
				); //push error text
				proceed = false; //set proceed flag to false
			}

			var submit_btn = $(this).find("input[type=submit]"); //form submit button
			//if everything looks good, proceed with jQuery Ajax
			if (proceed) {
				submit_btn.val("Please Wait...").prop("disabled", true); //disable submit button
				var form_data = new FormData(this); //Creates new FormData object
				var post_url = $(this).attr("action"); //get action URL of form

                //filenane for back
                var fileName = $('#ctlfiles_2').val().split('\\').pop();
        $('.maintile_uploadfile_2').append('<div class="tile-progressbar"> <span class="pt_counter_2" data-fill="23.2%" style="width: 23.2%;"></span>    			</div>    			<div class="tile-footer">                     	<h4>    					<span class="pct-counter p_counter_2">Processing...</span>     				</h4>    				<span>'+fileName+' </span>    			</div>');


                $('#divloadingmodal_generic').modal('show');
				//jQuery Ajax to Post form data
                //compressing
				$.ajax({
					url: post_url,
					type: "POST",
					data: form_data,
					contentType: false,
					cache: false,
					processData: false,
					mimeType: "multipart/form-data"
				}).done(function(res_o) {
                    $('#divloadingmodal_generic').modal('hide');
                    var res = jQuery.parseJSON(res_o);

                    //fileupload
                    $('.p_counter_2').html("Uploaded");
                    $('.pt_counter_2').data('fill',100);
                    $('.pt_counter_2').animate({
                        width:'100%'
                    });
					$(my_form_id)[0].reset(); //reset form
                    $(result_output).html("<img  alt='file' data-original-width='"+res.original_dimentions.width+"' data-original-height='"+res.original_dimentions.height+"' src='"+res.filename+"' />"); //output response from server


					var imageToAdd = $("#output img").first().attr("src");
					$("#output img").eq(0).hide();
					$("#output img").eq(1).hide();
					$("#output img").eq(2).hide();
					$("#output img").eq(3).hide();
					var imageInstance = new fabric.Image.fromURL(
						imageToAdd,
						function(upImg) {
							upImg.left = 100;
							upImg.top = 100;
							upImg.scale(1);
							backcanvas.add(upImg);
							backcanvas.bringToFront(upImg);
							backcanvas.renderAll();
						},
						{ crossOrigin: "Anonymous" }
					);
					submit_btn.val("Upload").prop("disabled", false); //enable submit button once ajax is done
				});
			}
		}

		$(result_output).html(""); //reset output
		$(error).each(function(i) {
			//output any error to output element
			$(result_output).append('<div class="error">' + error[i] + "</div>");
		});
	});
	// var imageLoaderBackFront = document.getElementById(
	// 	"user-image-back-frontback"
	// );
	// imageLoaderBackFront.addEventListener("change", handleImageBackFront, false);

	// function handleImageBackFront(ef) {
	// 	var readerBackFront = new FileReader();
	// 	readerBackFront.onload = function(backFrontEvt) {
	// 		$(".droppable-back-frontback img").attr(
	// 			"src",
	// 			backFrontEvt.target.result
	// 		);
	// 		var imageToAddBackFront = $(".droppable-back-frontback img").attr("src");
	// 		var imageInstanceBackFront = new fabric.Image
	// 			.fromURL(
	// 			imageToAddBackFront,
	// 			function(upImgBackFront) {
	// 				upImgBackFront.scale(0.5);
	// 				backcanvas.add(upImgBackFront);
	// 				backcanvas.bringToFront(upImgBackFront);
	// 				backcanvas.renderAll();
	// 			},
	// 			{ crossOrigin: "Anonymous" }
	// 		);
	// 	};
	// 	readerBackFront.readAsDataURL(ef.target.files[0]);
	// }

	// var imageLoaderBackgroundBack = document.getElementById(
	// 	"user-image-back-backback"
	// );
	// imageLoaderBackgroundBack.addEventListener(
	// 	"change",
	// 	handleImageBackgroundBack,
	// 	false
	// );

	function handleImageBackgroundBack(eb) {
		var readerBackgroundBack = new FileReader();
		readerBackgroundBack.onload = function(backBackEvt) {
			$(".droppable-back-backback img").attr("src", backBackEvt.target.result);
			var backgroundImageToAddBack = $(".droppable-back-backback img").attr(
				"src"
			);
			var imageInstanceBack = new fabric.Image
				.fromURL(
				backgroundImageToAddBack,
				function(upBackImgBack) {
					backcanvas.setBackgroundImage(upBackImgBack);
					backcanvas.renderAll();
				},
				{ crossOrigin: "Anonymous" }
			);
		};
		readerBackgroundBack.readAsDataURL(eb.target.files[0]);
	}

	function doint(jsondefault, virgin) {
		$("#back-editable-field-wrapper").html("");
		//$("#hidden-counter-back").val() = 0;
		$("#div_backcanvas-product-front").html(
			'<canvas id="canvas-product-back"></canvas>'
		);

		$id_of_fileoutput_back = "";
		$id_of_fileoutput_front = "";
		//Find all hidden texts
		$('input[type="text"]').each(function() {
			if ($(this).attr("placeholder") == "fileoutput_back") {
				$id_of_fileoutput_back = $(this).attr("id");
			} //endif
			if ($(this).attr("placeholder") == "fileoutput_front") {
				$id_of_fileoutput_front = $(this).attr("id");
			} //endif
		});



		//Check if the data is already saved
		//var myjson_back = $( "#input-option229" ).val();
		// var jsondefault = $('#input-option231').val();
		var myjson_back = "";
		myjson_back = $("#input-option229").val();

		$(".backsidetemplates > .row").empty();
		//Get the backside template from jsondefault
        jsondefault = 1; //Hardcoded for now
		//$.each(backside.success.backsides, function(key, val) {


        //to do backside.success.backsides.length
        for(y=1;y<=1;y++){

            key = y;
            val = backside.success.backsides [key];

			if (!jsondefault) {
				jsondefault = 1; //just use default one
			}



			var price = 0;
			if (Math.round(val.price * 100) / 100 != 0) {
				price = (Math.round(val.price * 100) / 100).toFixed(2);
			} //endif

			if (!price || price === "0.00" || price == 0) {
				price = "";
			} else {
				price = "$" + price;
			}
			$(".backsidetemplates > .row").append(
				'<div class="col-md-4 no-pad"><a href="#" id="loadbacktemplate' +
					val.backside_id +
					'"><img style="width:100px;height:100px;" src="/image/backside/' +
					val.backside_id +
					'.png" alt="' +
					val.name +
					'" class="img-thumbnail" /> <p>' +
					price +
					"</p></a></div>"
			);
			$("#loadbacktemplate" + val.backside_id).click(function(e) {
				e.preventDefault();
				loadbacktemplate(val.backside_id);
				//return false;
			});

			//<div class="img-rounded" >'+val.name+'</div>');

			if (key === jsondefault) {
				jsondefault = val.code;
			} //endif
		}

		if (jsondefault) {
			//Get from detault
			backcanvas.clear();
            backcanvas.loadFromJSON(
				jsondefault,
				backcanvas.renderAll.bind(backcanvas)
			);

		} else {
			//User has something entered in front
			backcanvas.clear();
			backcanvas.loadFromJSON(
				myjson_back,
				backcanvas.renderAll.bind(backcanvas)
			);
		}

		//File upload
		document
			.getElementById("backbtnfileupload")
			.addEventListener("change", function(e) {
				var file = e.target.files[0];
				var reader = new FileReader();
				reader.onload = function(f) {
					var data = f.target.result;
					fabric.Image.fromURL(
						data,
						function(img) {
							var oImg = img.set({ left: 0, top: 0, angle: 00 }).scale(0.9);
							backcanvas.add(oImg).renderAll();
							var a = backcanvas.setActiveObject(oImg);
							var dataURL = backcanvas.toDataURL({
								format: "png",
								quality: 1
							});
						},
						{ crossOrigin: "Anonymous" }
					);
				};
				reader.readAsDataURL(file);
			});

		var selectedObject;

		if (virgin) {
			//Add support for clearing the canvas
			$("#backfileuploadbtn").click(function() {
				$("#backbtnfileupload").click();
			});

			//Text fields
		} //endif - virgin

		//observeNumeric("padding");

		radios5 = document.getElementsByName("backfonttype"); // wijzig naar button
		for (var i = 0, max = radios5.length; i < max; i++) {
			radios5[i].onclick = function() {
				if (document.getElementById(this.id).checked == true) {
					if (this.id == "backtext-cmd-bold") {
						backcanvas.getActiveObject().set("fontWeight", "bold");
					}
					if (this.id == "backtext-cmd-italic") {
						backcanvas.getActiveObject().set("fontStyle", "italic");
					}
					if (this.id == "backtext-cmd-underline") {
						backcanvas.getActiveObject().set("textDecoration", "underline");
					}
					if (this.id == "backtext-cmd-linethrough") {
						backcanvas.getActiveObject().set("textDecoration", "line-through");
					}
					if (this.id == "backtext-cmd-overline") {
						backcanvas.getActiveObject().set("textDecoration", "overline");
					}
				} else {
					if (this.id == "backtext-cmd-bold") {
						backcanvas.getActiveObject().set("fontWeight", "");
					}
					if (this.id == "backtext-cmd-italic") {
						backcanvas.getActiveObject().set("fontStyle", "");
					}
					if (this.id == "backtext-cmd-underline") {
						backcanvas.getActiveObject().set("textDecoration", "");
					}
					if (this.id == "backtext-cmd-linethrough") {
						backcanvas.getActiveObject().set("textDecoration", "");
					}
					if (this.id == "backtext-cmd-overline") {
						backcanvas.getActiveObject().set("textDecoration", "");
					}
				}

				backcanvas.renderAll();
			};
		} //end for
	} //end doinit function

	function onObjectSelectedBack() {
		for (i in backcanvas.getObjects()) {
			if (backcanvas.item(i).active === true) {
				selectedObject.push({
					itemNum: i,
					svdObject: fabric.util.object.clone(backcanvas.item(i))
				});
			}

			var activeObjBack = backcanvas.getActiveObject();
			if (activeObjBack) {
				$(".sp-preview-inner").eq(3).css({
					backgroundColor: activeObjBack.get("fill")
				});
				$(".sp-preview-inner").eq(4).css({
					backgroundColor: activeObjBack.get("backgroundColor")
				});
				getFiltersBack();
				if (activeObjBack.get("type") === "textbox") {
					var fontSizeBack = activeObjBack.get("fontSize");

					if (activeObjBack.get("fill")) {
						$(".sp-preview-inner").eq(3).css({
							backgroundColor: activeObjBack.get("fill")
						});
					}

					if (activeObjBack.get("stroke")) {
						$(".sp-preview-inner").eq(3).css({
							backgroundColor: activeObjBack.get("fill")
						});
					}

					if (activeObjBack.get("fontWeight") === "bold") {
						$("#backbtnbold").addClass("active");
					} else {
						$("#backbtnbold").removeClass("active");
					}

					if (activeObjBack.get("fontStyle") === "italic") {
						$("#backbtnitalic").addClass("active");
					} else {
						$("#backbtnitalic").removeClass("active");
					}

					if (activeObjBack.get("textDecoration") === "underline") {
						$("#backbtnunderline").addClass("active");
					} else {
						$("#backbtnunderline").removeClass("active");
					}

					if (activeObjBack.get("textDecoration") === "line-through") {
						$("#backbtnstrikethrough").addClass("active");
					} else {
						$("#backbtnstrikethrough").removeClass("active");
					}

					if (activeObjBack.get("textAlign") === "left") {
						$("#backbtnleftalign").addClass("active");
					} else {
						$("#backbtnleftalign").removeClass("active");
					}

					if (activeObjBack.get("textAlign") === "center") {
						$("#backbtncenteralign").addClass("active");
					} else {
						$("#backbtncenteralign").removeClass("active");
					}

					if (activeObjBack.get("textAlign") === "right") {
						$("#backbtnrightalign").addClass("active");
					} else {
						$("#backbtnrightalign").removeClass("active");
					}

					$("#back-text-font-size").val(fontSizeBack);
				}

				if (activeObjBack.get("type") !== "image") {
					fabric.util
						.toArray(document.querySelectorAll("input[type='checkbox']"))
						.forEach(function(el) {
							el.disabled = true;
						});
				} else {
					fabric.util
						.toArray(document.querySelectorAll("input[type='checkbox']"))
						.forEach(function(el) {
							el.disabled = false;
						});
				}
                if (activeObjBack.get("type") === "image") {
                    $(".imagefilter_back").css("opacity", "1");
                } else {
                    $(".imagefilter_back").css("opacity", "0.1");
                }
			}
		}
	}

	function getFiltersBack() {
		var filters = [
			"grayscale-back",
			"invert-back",
			"remove-white",
			"sepia-back",
			"sepia2-back",
			"brightness",
			"contrast",
			"saturate",
			"noise",
			"gradient-transparency",
			"pixelate",
			"blur",
			"sharpen",
			"emboss",
			"tint",
			"multiply",
			"blend"
		];

		for (var i = 0; i < filters.length; i++) {
			if (backcanvas.getActiveObject().get("type") === "image") {
				$(filters[i]).checked = !!backcanvas.getActiveObject().filters[i];
			}
		}
	}

	document.getElementById("grayscale-back").onclick = function(e) {
		applyFilterBack(0, this.checked && new f.Grayscale());
	};

	document.getElementById("invert-back").onclick = function(e) {
		applyFilterBack(1, this.checked && new f.Invert());
	};

	document.getElementById("sepia-back").onclick = function(e) {
		applyFilterBack(3, this.checked && new f.Sepia());
	};

	document.getElementById("sepia2-back").onclick = function(e) {
		applyFilterBack(4, this.checked && new f.Sepia2());
	};

	function onObjectAddedBack() {
		var b = backcanvas.getObjects();

		if (backReplayFlag === false) {
			backUndoHist.push({
				action: "add",
				itemNums: [b.length - 1]
			});
			backRedoHist = [];
		}

		for (var i = 0; i < backcanvas.getObjects().length; i++) {
			// console.log(i);
			if (backcanvas.getObjects()[i].get("name") === "innerbleedlineback") {
				if (backcanvas.getObjects()[i].length > 1) {
					backcanvas.getObjects()[i - 1].remove(backcanvas.getObjects()[i - 1]);
					backcanvas.renderAll();
				}
			}

			if (backcanvas.getObjects()[i].get("name") === "outerbleedlineback") {
				if (backcanvas.getObjects()[i].length > 1) {
					backcanvas.getObjects()[i - 1].remove(backcanvas.getObjects()[i - 1]);
					backcanvas.renderAll();
				}
			}

			if (backcanvas.getObjects()[i].get("name") === "safetylineback") {
				if (backcanvas.getObjects()[i].length > 1) {
					backcanvas.getObjects()[i - 1].remove(backcanvas.getObjects()[i - 1]);
					backcanvas.renderAll();
				}
			}

			// if (backcanvas.getObjects()[i].get("type") === "i-text") {
			// 	backcanvas
			// 		.getObjects()
			// 		[i].set(
			// 			"stroke",
			// 			$(".sp-preview-inner").eq(3).css("background-color")
			// 		);
			// 	// backcanvas
			// 	// 	.getObjects()
			// 	// 	[i].set("fill", $(".sp-preview-inner").eq(3).css("background-color"));
			// }

			// if (backcanvas.getObjects()[i].get("type") === "textbox") {
			// 	backcanvas
			// 		.getObjects()
			// 		[i].set(
			// 			"stroke",
			// 			$(".sp-preview-inner").eq(3).css("background-color")
			// 		);
			// 	// backcanvas
			// 	// 	.getObjects()
			// 	// 	[i].set("fill", $(".sp-preview-inner").eq(3).css("background-color"));
			// }
		}

		// if (b[b.length - 1].get("type") === "textbox") {
		// 	b[b.length - 1].set(
		// 		"stroke",
		// 		$(".sp-preview-inner").eq(3).css("background-color")
		// 	);
		// 	backcanvas.renderAll();
		// }

		// if (b[b.length - 1].get("type") === "i-text") {
		// 	b[b.length - 1].set(
		// 		"stroke",
		// 		$(".sp-preview-inner").eq(3).css("background-color")
		// 	);
		// 	backcanvas.renderAll();
		// }

		if (b[b.length - 1].get("type") === "rect") {
			b[b.length - 1].set(
				"fill",
				$(".sp-preview-inner").eq(5).css("background-color")
			);
		}
		if (b[b.length - 1].get("type") === "circle") {
			b[b.length - 1].set(
				"fill",
				$(".sp-preview-inner").eq(5).css("background-color")
			);
		}
		if (b[b.length - 1].get("type") === "line") {
			b[b.length - 1].set(
				"stroke",
				$(".sp-preview-inner").eq(5).css("background-color")
			);
		}

		// b[b.length - 1].set(
		// 	"stroke",
		// 	$(".sp-preview-inner").eq(5).css("background-color")
		// );

		if (b[b.length - 1].get("name") === "safetylineback") {
			b[b.length - 1].set("stroke", "#0000ff");
		}

		if (b[b.length - 1].get("name") === "outerbleedback") {
			b[b.length - 1].set("stroke", "#ff0000");
		}

		if (b[b.length - 1].get("name") === "innerbleedback") {
			b[b.length - 1].set("stroke", "#00ff00");
		}

		if (b[b.length - 1].get("name") === "innerbleedbackFinal") {
			b[b.length - 1].set("stroke", "#000000");
		}

		// setTimeout(function() {
		// 	//backBleedGroup.bringToFront();
		// 	backBleedGroup2.bringToFront();
		// 	backBleedGroup3.bringToFront();
		// }, 1000);

		// backBleedGroup.bringToFront();
		// backBleedGroup2.bringToFront();
	}

	function onObjectModifiedBack() {
		if (backReplayFlag === false) {
			// i.e. user is modifying..
			var itemProps = [];
			for (i in selectedObject) {
				itemProps.push({
					itemNum: selectedObject[i].itemNum,
					left: selectedObject[i].svdObject.left,
					top: selectedObject[i].svdObject.top,
					scaleX: selectedObject[i].svdObject.scaleX,
					scaleY: selectedObject[i].svdObject.scaleY
				});
			}

			// get current object properties
			onObjectSelectedBack();

			backUndoHist.push({
				action: "modify",
				itemProps: itemProps
			});
			backRedoHist = [];
		}
	}

	function onObjectRemovedBack(object, styleName, value, txtCtl) {
		var txtIdBack = object.target.id;
		if (!txtIdBack) {
			backcanvas.renderAll();
		} else {
			txtCtl = document.getElementById(
				"back-editable-field-" + txtIdBack.toString()
			);
			var snippedCtlId = txtCtl
				.getAttribute("id")
				.substr(20, txtCtl.getAttribute("id").length);
			if (txtIdBack.toString() === snippedCtlId) {
				$(txtCtl).remove();
				y--;
				document.getElementById("hidden-counter-back").value = y;
			}
		}

		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = true;
			});
	}

	backcanvas.on("object:selected", onObjectSelectedBack);
	backcanvas.on("object:added", onObjectAddedBack);
	backcanvas.on("object:modified", onObjectModifiedBack);
	backcanvas.on("object:removed", onObjectRemovedBack);
	backcanvas.on("text:editing:exited", backSetTextStyle);
	backcanvas.on("selection:changed", backSetStyle);
	backcanvas.on("selection:cleared", onObjectSelectionClearedBack);
	backcanvas.on("object:moving", onObjectMovingBack);

	backcanvas.on("object:scaling", function(e) {
		var obj = e.target;
		var maxWidth = canvas.width;
		var maxHeight = canvas.height;
		var actualWidth = obj.scaleX * obj.width;
		var actualHeight = obj.scaleY * obj.height;

		if (!isNaN(maxWidth) && actualWidth >= maxWidth) {
			obj.set({ scaleX: maxWidth / obj.width });
		}

		if (!isNaN(maxHeight) && actualHeight >= maxHeight) {
			obj.set({ scaleY: maxHeight / obj.height });
		}
	});

	function onObjectMovingBack(e) {
		var obj = e.target;
		if (
			obj.getHeight() > obj.canvas.height ||
			obj.getWidth() > obj.canvas.width
		) {
			return;
		}
		obj.setCoords();

		if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
			obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
			obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
		}

		if (
			obj.getBoundingRect().top + obj.getBoundingRect().height >
				obj.canvas.height ||
			obj.getBoundingRect().left + obj.getBoundingRect().width >
				obj.canvas.width
		) {
			obj.top = Math.min(
				obj.top,
				obj.canvas.height -
					obj.getBoundingRect().height +
					obj.top -
					obj.getBoundingRect().top
			);
			obj.left = Math.min(
				obj.left,
				obj.canvas.width -
					obj.getBoundingRect().width +
					obj.left -
					obj.getBoundingRect().left
			);
		}
	}

	function onObjectSelectionClearedBack() {
        $(".imagefilter_back").css("opacity", "0.1");
		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = true;
			});
	}

	var backDefaultText = "Click Me On Canvas To Edit";
	var y = 0;

	setTimeout(function() {
		var addValsBack = document.getElementById("hidden-counter-back").value;
		var dynamicElBack = document.getElementById(
			"back-editable-field-" + addValsBack
		);
		if (addValsBack > 0) {
			$(
				"#back-editable-field-" + addValsBack
			).on("click", dynamicElBack, function(e) {
				//dynamicElBack.value = "";
				typingFuncBack(dynamicElBack);
			});

			$("#back-editable-field-wrapper").on("keyup", dynamicElBack, function(e) {
				addHandler(
					"text-font-size",
					function(obj) {
						backSetStyle(obj, "fontSize", 12, e.target);
					},
					"keyup"
				);
			});
		}
	}, 1000);

	$("#back-add-editable").click(function(e) {
		if (document.getElementById("hidden-counter-back").value === 0) {
			y = 0;
		} else {
			y = document.getElementById("hidden-counter-back").value;
		}
		if (y < 15) {
			y++;
			document.getElementById("hidden-counter-back").value = y;
			var inputToAddBack = document.createElement("input");
			var fieldValBack = y.toString();
			inputToAddBack.id = "back-editable-field-" + fieldValBack;
			inputToAddBack.className = "editable-field";
			inputToAddBack.value = backDefaultText;

			$("#back-editable-field-wrapper").append(inputToAddBack);

			$("#" + inputToAddBack.id).on("click", dynElemBack, function(e) {
				//dynElemBack.value = "";
				selectedObjectTBack(dynElemBack);
			});

			backcanvas.add(
				new fabric.Textbox(backDefaultText, {
					fontFamily: "Arial",
					fill: $(".sp-preview-inner").eq(3).css("background-color"),
					// stroke: $(".sp-preview-inner").eq(3).css("background-color"),
					left: 100,
					top: 100,
					id: y
				})
			);

			var dynElemBack = document.getElementById(
				"back-editable-field-" + fieldValBack
			);
			$("#back-editable-field-wrapper").on("keyup", dynElemBack, function(e) {
				backAddHandler(
					"text-font-size",
					function(obj) {
						backSetStyle(obj, "fontSize", 12, e.target);
					},
					"keyup"
				);
			});
		}
	});

	var typingFuncBack = function(elem) {
		var idToUseBack = parseInt(elem.id.substr(20, elem.id.length));
		backcanvas.getObjects().forEach(function(ob) {
			if (ob.get("type") === "textbox") {
				if (ob.id === idToUseBack) {
					backcanvas.setActiveObject(ob);
				}
			}
		});
	};

	var selectedObjectTBack = function(elem) {
		var idToUseBack = parseInt(elem.id.substr(20, elem.id.length));
		backcanvas.getObjects().forEach(function(ob) {
			if (ob.get("type") === "textbox") {
				if (ob.id === idToUseBack) {
					backcanvas.setActiveObject(ob);
					backcanvas.bringToFront(ob);
					return;
				}
			}
		});
	};

	function backSetTextStyle(object, styleName, value, txtCtl) {
		var txtId = object.target.id;
		txtCtl = document.getElementById("back-editable-field-" + txtId.toString());
		var snippedCtlId = txtCtl
			.getAttribute("id")
			.substr(20, txtCtl.getAttribute("id").length);
		if (txtId.toString() === snippedCtlId) {
			txtCtl.value = object.target.text;
		}
	}

	function backSetStyle(object, styleName, value, txtCtl) {
		var myIdBack = $(txtCtl).attr("id").substr(20, $(txtCtl).attr("id").length);
		var myIntIdBack = parseInt(myIdBack);
		var newValBack = $(txtCtl).val();
		if (myIntIdBack === object.id) {
			object.text = newValBack;
		}
		backcanvas.renderAll();
		return;
	}

	function getStyle(object, styleName) {
		return object.getSelectionStyles && object.isEditing
			? object.getSelectionStyles()[styleName]
			: object[styleName];
	}

	function backAddHandler(id, fn, eventName) {
		var el = this;
		if ((obj = backcanvas.getActiveObject())) {
			fn.call(el, obj);
			backcanvas.renderAll();
		}
	}

	$("#backclearbutton").click(function() {
		emptyCanvas();
		readdBleedlinesBack();
	});

	$("#backdeletebtn").click(function() {
		backcanvas.isDrawingMode = false;
		//actionReplayBack("remove");
		deleteObjects();
	});

	//Undo button
	$("#backundobutton").click(function() {
		actionReplayBack("undo");
	});

	//Redo button
	$("#backredobutton").click(function() {
		actionReplayBack("redo");
	});

	$("#opacityrangeback").on("input", function() {
		var activeShapeBack = backcanvas.getActiveObject();
		var opacityValBack = document.getElementById("opacityrangeback").value;
		if (activeShapeBack.get("type") === "rect") {
			activeShapeBack.set("opacity", opacityValBack);
			backcanvas.renderAll();
		}
		if (activeShapeBack.get("type") === "circle") {
			activeShapeBack.set("opacity", opacityValBack);
			backcanvas.renderAll();
		}
	});

	$("#opacityrangeback").on("change", function() {
		var activeShapeBack = backcanvas.getActiveObject();
		var opacityValBack = document.getElementById("opacityrangeback").value;
		if (activeShapeBack.get("type") === "rect") {
			activeShapeBack.set("opacity", opacityValBack);
			backcanvas.renderAll();
		}
		if (activeShapeBack.get("type") === "circle") {
			activeShapeBack.set("opacity", opacityValBack);
			backcanvas.renderAll();
		}
	});

	var f = fabric.Image.filters;

	function applyFilterBack(index, filter) {
		var obj = backcanvas.getActiveObject();
		obj.filters[index] = filter;
		obj.applyFilters(backcanvas.renderAll.bind(backcanvas));
        $('#divloadingmodal').modal('hide');
	}

	function applyFilterValueBack(index, prop, value) {
		var obj = backcanvas.getActiveObject();
		if (obj.filters[index]) {
			obj.filters[index][prop] = value;
			obj.applyFilters(backcanvas.renderAll.bind(backcanvas));
		}
	}

	var backUndoHist = [];
	var backRedoHist = [];
	var backReplayFlag = false; // set to true while undoing or redoing..
	var selectedObject = [];

	function actionReplayBack(replayType) {
		// global flag to prevent onObjectSelected method defaults
		// from applying to programmatic actions.
		backReplayFlag = true;
		if (replayType === "undo") {
			if (backUndoHist.length > 0) {
				var backStackBack = backUndoHist;
				var forwardStackBack = backRedoHist;
				replayBack(backStackBack, forwardStackBack);
			}
			// ----------
		} else if (replayType === "redo") {
			if (backRedoHist.length > 0) {
				var backStackBack = backRedoHist;
				var forwardStackBack = backUndoHist;
				replayBack(backStackBack, forwardStackBack);
			}
		}
		backReplayFlag = false;
		backcanvas.renderAll();
	}

	// ----------
	function replayBack(backStack, forwardStack) {
		var o = backStack[backStack.length - 1];
		var actionType = backStack[backStack.length - 1].action;
		var itemProps = [];
		var itemNums = [];
		var boolShow = true;

		// Not working:
		// 1. move / resize *multiple* objects

		if (actionType === "remove") {
			actionType = "add"; // invert actionType for add/remove
			boolShow = true;
		} else if (actionType === "add") {
			actionType = "remove";
			boolShow = false;
		} else {
			boolShow = true;
		}

		if (actionType === "add" || actionType === "remove") {
			for (i in o.itemNums) {
				backcanvas.item(o.itemNums[i]).set({
					selectable: boolShow,
					visible: boolShow
				});
				itemNums.push(o.itemNums[i]);
				backcanvas.item(o.itemNums[i]).setCoords();
			}
		} else {
			// actionType === "modify"

			for (i in o.itemProps) {
				// current itemProps, not those from the history
				itemProps.push({
					itemNum: o.itemProps[i].itemNum,
					left: backcanvas.item(o.itemProps[i].itemNum).left,
					top: backcanvas.item(o.itemProps[i].itemNum).top,
					scaleX: backcanvas.item(o.itemProps[i].itemNum).scaleX,
					scaleY: backcanvas.item(o.itemProps[i].itemNum).scaleY
				});

				backcanvas.item(o.itemProps[i].itemNum).set({
					left: o.itemProps[i].left,
					top: o.itemProps[i].top,
					scaleX: o.itemProps[i].scaleX,
					scaleY: o.itemProps[i].scaleY
				});

				itemNums.push(o.itemProps[i].itemNum);

				backcanvas.item(o.itemProps[i].itemNum).setCoords();
			}
		}

		forwardStack.push({
			action: actionType,
			itemNums: itemNums,
			itemProps: itemProps
		});

		backStack.splice(backStack.length - 1, 1);

		selectedObject = [];
		onObjectSelectedBack();
	}

	$("#sendSelectedObjectToFront-back").click(function() {
		var selectedObject = backcanvas.getActiveObject();
		if (selectedObject) {
			backcanvas.bringToFront(selectedObject);
		}
	});

	$("#sendSelectedObjectBack-back").click(function(e) {
		var selectedObject = backcanvas.getActiveObject();
		if (selectedObject) {
			backcanvas.sendToBack(selectedObject);
		}
	});


	/**
* Initially hide the other panels
**/
	$("div#back-text-panel").hide();
	$("div#back-image-panel").hide();
	$("div#back-options-panel").hide();

	$("#back-design").addClass("active");
	$("#back-design").css({
		"background-color": "#cdcdcd"
	});
	$("#backsetdesignactive").css({
		color: "#000"
	});
	/**
* Functions which check which control tab is active
* and apply the active class to the parent of the
* anchor tag which is responsible for setting the
* visibility of each tab
**/

	$("#backsetdesignactive").click(function() {
		if ($("#back-text").hasClass("active")) {
			$("#back-text").removeClass("active");
			$("#back-text").css({
				"background-color": "transparent"
			});
			$("#backsettextactive").css({
				color: "#000"
			});
		}

		if ($("#back-image").hasClass("active")) {
			$("#back-image").removeClass("active");
			$("#back-image").css({
				"background-color": "transparent"
			});
			$("#backsetimageactive").css({
				color: "#000"
			});
		}

		if ($("#back-options").hasClass("active")) {
			$("#back-options").removeClass("active");
			$("#back-options").css({
				"background-color": "transparent"
			});
			$("#backsetoptionsactive").css({
				color: "#000"
			});
		}

		$("#back-design").addClass("active");
		$("#back-design").css({
			"background-color": "#cdcdcd"
		});
		$("#backsetdesignactive").css({
			color: "#000"
		});
		$("div#back-design-panel").show();
		$("div#back-text-panel").hide();
		$("div#back-image-panel").hide();
		$("div#back-options-panel").hide();
	});

	$("#backsettextactive").click(function() {
		if ($("#back-design").hasClass("active")) {
			$("#back-design").removeClass("active");
			$("#back-design").css({
				"background-color": "transparent"
			});
			$("#backsetdesignactive").css({
				color: "#000"
			});
		}

		if ($("#back-image").hasClass("active")) {
			$("#back-image").removeClass("active");
			$("#back-image").css({
				"background-color": "transparent"
			});
			$("#backsetimageactive").css({
				color: "#000"
			});
		}

		if ($("#back-options").hasClass("active")) {
			$("#back-options").removeClass("active");
			$("#back-options").css({
				"background-color": "transparent"
			});
			$("#backsetoptionsactive").css({
				color: "#000"
			});
		}

		$("#back-text").addClass("active");
		$("#back-text").css({
			"background-color": "#cdcdcd"
		});
		$("#backsettextactive").css({
			color: "#000"
		});

		$("div#back-text-panel").show();
		$("div#back-design-panel").hide();
		$("div#back-image-panel").hide();
		$("div#back-options-panel").hide();
	});

	$("#backsetimageactive").click(function() {
		if ($("#back-design").hasClass("active")) {
			$("#back-design").removeClass("active");
			$("#back-design").css({
				"background-color": "transparent"
			});
			$("#backsetdesignactive").css({
				color: "#000"
			});
		}

		if ($("#back-text").hasClass("active")) {
			$("#back-text").removeClass("active");
			$("#back-text").css({
				"background-color": "transparent"
			});
			$("#backsettextactive").css({
				color: "#000"
			});
		}

		if ($("#back-options").hasClass("active")) {
			$("#back-options").removeClass("active");
			$("#back-options").css({
				"background-color": "transparent"
			});
			$("#backsetoptionsactive").css({
				color: "#000"
			});
		}

		$("#back-image").addClass("active");
		$("#back-image").css({
			"background-color": "#cdcdcd"
		});
		$("#backsetimageactive").css({
			color: "#000"
		});

		$("div#back-image-panel").show();

		$("div#back-design-panel").hide();
		$("div#back-text-panel").hide();
		$("div#back-options-panel").hide();
	});

	$("#backsetoptionsactive").click(function() {
		if ($("#back-design").hasClass("active")) {
			$("#back-design").removeClass("active");
			$("#back-design").css({
				"background-color": "transparent"
			});
			$("#backsetdesignactive").css({
				color: "#000"
			});
		}

		if ($("#back-text").hasClass("active")) {
			$("#back-text").removeClass("active");
			$("#back-text").css({
				"background-color": "transparent"
			});
			$("#backsettextactive").css({
				color: "#000"
			});
		}

		if ($("#back-image").hasClass("active")) {
			$("#back-image").removeClass("active");
			$("#back-image").css({
				"background-color": "transparent"
			});
			$("#backsetimageactive").css({
				color: "#000"
			});
		}

		$("#back-options").addClass("active");

		$("#back-options").css({
			"background-color": "#cdcdcd"
		});
		$("#backsetoptionsactive").css({
			color: "#000"
		});

		$("div#back-options-panel").show();
		$("div#back-design-panel").hide();
		$("div#back-text-panel").hide();
		$("div#back-image-panel").hide();
	});

	/**
* Functions here will add an object to the canvas (Square, Cirle or Triangle)
**/

	$(document).bind("keydown", "ctrl+m", undoKeyComboBack);
	$(document).bind("keydown", "ctrl+alt+c", copyComboBack);
	$(document).bind("keydown", "ctrl+alt+v", pasteComboBack);
	$(document).bind("keydown", "ctrl+;", redoKeyComboBack);
	$(document).bind("keydown", "del", deleteComboBack);
	$(document).bind("keydown", "backspace", backBackspace);
	$(document).bind("keydown", "alt+del", clearComboBack);
	$(document).bind("keydown", "alt+backspace", clearBackspaceComboBack);
	$(document).bind("keydown", "ctrl+shift+f", backSave);
	// $(document).bind('keydown', 'ctrl+alt+b', backBoldButton);
	// $(document).bind('keydown', 'ctrl+alt+u', backUnderlineButton);
	// $(document).bind('keydown', 'ctrl+alt+s', backStrikeButton);

	function copyComboBack() {
		// 	var objToCopy = canvas.getActiveObject();
		// 	var copied = fabric.util.object.clone(objToCopy);
		// 	copiedObject.push(copied);
	}

	function pasteComboBack() {
		// 	if (copiedObject.length > 0) {
		// 		for (var pasteable = 0; pasteable < copiedObject.length; pasteable++) {
		// 			if (copiedObject[pasteable].get("type") === "textbox") {
		// 				$("#hidden-counter").val(function(i, oldVal) {
		// 					return ++oldVal;
		// 				});
		// 				var newIn = document.createElement("input");
		// 				newIn.id =
		// 					"editable-field-" + document.getElementById("hidden-counter").value;
		// 				newIn.className = "editable-field";
		// 				newIn.value = defaultText;
		// 				$("#editable-field-wrapper").append(newIn);
		// 				// $("#editable-field-" + t).on("click", dynamicEl, function(e) {
		// 				// 	//dynamicEl.value = "";
		// 				// 	typingFunc(e.target);
		// 				// });
		// 				console.log(copiedObject[pasteable]);
		// 				copiedObject[pasteable].on("click", function(e) {
		// 					typingFunc(e.target);
		// 				});
		// 				copiedObject[pasteable].id = document.getElementById(
		// 					"hidden-counter"
		// 				).value;
		// 				canvas.add(copiedObject[pasteable]);
		// 				canvas.renderAll();
		// 				copiedObject.pop();
		// 			} else {
		// 				canvas.add(copiedObject[pasteable]);
		// 				canvas.renderAll();
		// 				copiedObject.pop();
		// 			}
		// 		}
		// 	}
	}

	function undoKeyComboBack() {
		actionReplayBack("undo");
	}

	function redoKeyComboBack() {
		actionReplayBack("redo");
	}

	function deleteComboBack() {
		deleteObjects();
	}

	function backBackspace() {
		deleteObjects();
	}

	function clearComboBack() {
		emptyCanvas();
	}

	function clearBackspaceComboBack() {
		emptyCanvas();
	}

	function backSave() {
		$("ul.setup-panel li:eq(2)").removeClass("disabled");
		$('ul.setup-panel li a[href="#step-3"]').trigger("click");

	}

	// function backBoldButton () {

	// }

	// function backUnderlineButton () {

	// }

	// function backStrikeButton () {

	// }



	// $("#backaddtriangle").click(function() {
	//   backcanvas.add(new fabric.Triangle({
	//     fill: '#000',
	//     top: 300,
	//     left: 400
	//   }));
	// });

	$("#backaddline").click(function() {
		backcanvas.add(
			new fabric.Line([10, 10, 100, 100], {
				fill: $(".sp-preview-inner").eq(5).css("background-color"),
				stroke: $(".sp-preview-inner").eq(5).css("background-color")
			})
		);
	});

	var trapezoid = [
		{ x: -100, y: -50 },
		{ x: 100, y: -50 },
		{ x: 150, y: 50 },
		{ x: -150, y: 50 }
	];
	var emerald = [
		{ x: 850, y: 75 },
		{ x: 958, y: 137.5 },
		{ x: 958, y: 262.5 },
		{ x: 850, y: 325 },
		{ x: 742, y: 262.5 },
		{ x: 742, y: 137.5 }
	];
	var star4 = [
		{ x: 0, y: 0 },
		{ x: 100, y: 50 },
		{ x: 200, y: 0 },
		{ x: 150, y: 100 },
		{ x: 200, y: 200 },
		{ x: 100, y: 150 },
		{ x: 0, y: 200 },
		{ x: 50, y: 100 },
		{ x: 0, y: 0 }
	];
	var star5 = [
		{ x: 350, y: 75 },
		{ x: 380, y: 160 },
		{ x: 470, y: 160 },
		{ x: 400, y: 215 },
		{ x: 423, y: 301 },
		{ x: 350, y: 250 },
		{ x: 277, y: 301 },
		{ x: 303, y: 215 },
		{ x: 231, y: 161 },
		{ x: 321, y: 161 }
	];

	var shapeBack = new Array(trapezoid, emerald, star4, star5);

	// $("#backaddpolygon").click(function() {
	//   var polygBack = new fabric.Polygon(shapeBack[1], {
	//     top: 180,
	//     left: 200,
	//     fill: '#000',
	//     stroke: '',
	//     strokeWidth: 2
	//   });
	//   backcanvas.add(polygBack);
	//   backcanvas.renderAll();
	// });

	// $("#backaddtrapezoid").click(function() {
	//   var trapezoidBack = new fabric.Polygon(shapeBack[0], {
	//     top: 180,
	//     left: 200,
	//     fill: '#000',
	//     stroke: '',
	//     strokeWidth: 2
	//   });
	//   backcanvas.add(trapezoidBack);
	//   backcanvas.renderAll();
	// });

	// $("#backaddheart").click(function() {
	//   var pathBack = new fabric.Path('M 272.70141,238.71731 \
	//     C 206.46141,238.71731 152.70146,292.4773 152.70146,358.71731  \
	//     C 152.70146,493.47282 288.63461,528.80461 381.26391,662.02535 \
	//     C 468.83815,529.62199 609.82641,489.17075 609.82641,358.71731 \
	//     C 609.82641,292.47731 556.06651,238.7173 489.82641,238.71731  \
	//     C 441.77851,238.71731 400.42481,267.08774 381.26391,307.90481 \
	//     C 362.10311,267.08773 320.74941,238.7173 272.70141,238.71731  \
	//     z ');
	//     var scaleBack = 100 / pathBack.width;
	//     pathBack.set({ left: 20, top: 0, scaleX: scaleBack, scaleY: scaleBack,  fill: '#ff0000', });
	//     backcanvas.add(pathBack);
	//     backcanvas.renderAll();
	// });

	// $("#backaddstar").click(function() {
	//   var starBack = new fabric.Polygon(shapeBack[3], {
	//     top: 180,
	//     left: 200,
	//     fill: '#ffff00',
	//     stroke: '#ffff00',
	//     strokeWidth: 2
	//   });
	//   backcanvas.add(starBack);
	//   backcanvas.renderAll();
	// });

	$("#backaddsquare").click(function() {
       // alert($('#palette').val());
		backcanvas.add(
			new fabric.Rect({
				fill: 'rgba(255,0,0,0.5)',
				width: 100,
				height: 100,
				top: 20,
				left: 20
			})
		);
		backcanvas.renderAll();


	});

	$("#backaddcircle").click(function() {
		backcanvas.add(
			new fabric.Circle({
				radius: 30,
				fill: "#000",
				top: 10,
				left: 10
			})
		);
        backcanvas.renderAll();
	});

	$("#palette-back").spectrum({
		color: "#eb882d",
		allowEmpty: true,
		showInput: true,
         flat: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdateBack = color.toHexString();
			var objBack = backcanvas.getActiveObject();
			if (objBack.get("type") !== "textbox") {
				objBack.set("fill", colToUpdate);
			}
			//obj.set("stroke", colToUpdate);
			// if (objBack.get("type") !== "textbox") {
			// 	objBack.set("stroke", colToUpdateBack);
			// 	objBack.set("fill", colToUpdateBack);
			// }
			// if (objBack.get("type") !== "i-text") {
			// 	objBack.set("stroke", colToUpdateBack);
			// 	objBack.set("fill", colToUpdateBack);
			// }
			backcanvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});


	function readdBleedlinesBack() {

		var whiteBackground = new fabric.Rect({
				fill: 'rgba(255,255,255,1)',
				width: bCw+1,
				height: bCh+1,
				top: -1,
				left: -1
        });

		var backBleedGroup_back = new fabric.Group(
			[whiteBackground],
			{ name: "whitebackground" }
		);
		backBleedGroup_back.selectable = false;
		backBleedGroup_back.evented = false;
		backcanvas.add(backBleedGroup_back);

	// 	var backbleedline1 = new fabric.Line([0, 0, 0, bCh], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#ff0000"
	// 	});
	// 	var backbleedline2 = new fabric.Line(
	// 		[bCw - 1, 0, bCw - 1, bCh],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#ff0000"
	// 		}
	// 	);
	// 	var backbleedline3 = new fabric.Line([0, 1, bCw, 1], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#ff0000"
	// 	});
	// 	var backbleedline4 = new fabric.Line(
	// 		[0, bCh - 1, bCw, bCh - 1],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#ff0000"
	// 		}
	// 	);
	 //
	// 	var backBleedGroup = new fabric.Group(
	// 		[backbleedline1, backbleedline2, backbleedline3, backbleedline4],
	// 		{ name: "outerbleedback" }
	// 	);
	// 	backBleedGroup.selectable = false;
	// 	backBleedGroup.evented = false;
	 //
	// 	// backcanvas.add(backBleedGroup);
	// 	// backBleedGroup.bringToFront();
	 //
	// 	/**
	//  * Second group of bleed lines
	//  **/
	 //
	// 	var backbleedline5 = new fabric.Line([bl, bl, bl, bCh - bl], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#00ff00"
	// 	});
	// 	var backbleedline6 = new fabric.Line(
	// 		[bCw - bl, bl, bCw - bl, bCh - bl],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#00ff00"
	// 		}
	// 	);
	// 	var backbleedline7 = new fabric.Line([bl, bl, bCw - bl, bl], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#00ff00"
	// 	});
	// 	var backbleedline8 = new fabric.Line(
	// 		[bl, bCh - bl, bCw - bl, bCh - bl],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#00ff00"
	// 		}
	// 	);
	 //
	// 	var backBleedGroup2 = new fabric.Group(
	// 		[backbleedline5, backbleedline6, backbleedline7, backbleedline8],
	// 		{ name: "innerbleedback" }
	// 	);
	// 	backBleedGroup2.selectable = false;
	// 	backBleedGroup2.evented = false;
	 //
	// 	backcanvas.add(backBleedGroup2);
	// 	backBleedGroup2.bringToFront();
	 //
	// 	/* Third Bleed Group */
	 //
	// 	var bleedline9 = new fabric.Line([sz, sz, sz, bCh - sz], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#0000ff"
	// 	});
	// 	var bleedline10 = new fabric.Line(
	// 		[
	// 			bCw - sz,
	// 			sz,
	// 			bCw - sz,
	// 			bCh - sz
	// 		],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#0000ff"
	// 		}
	// 	);
	// 	var bleedline11 = new fabric.Line([sz, sz, bCw - sz, sz], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#0000ff"
	// 	});
	// 	var bleedline12 = new fabric.Line(
	// 		[
	// 			sz,
	// 			bCh - sz,
	// 			bCw - sz,
	// 			bCh - sz
	// 		],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#0000ff"
	// 		}
	// 	);

		// var whiteBackground = new fabric.Rect({
		// 		fill: 'rgba(255,255,255,1)',
		// 		width: $("#ctl_canvas_width").html(),
		// 		height: $("#ctl_canvas_height").html(),
		// 		top: 0,
		// 		left: 0
    //     });

		// var backBleedGroup3 = new fabric.Group(
		// 	[bleedline9, bleedline10, bleedline11, bleedline12, ],
		// 	{ name: "safetylineback" }
		// );
		// backBleedGroup3.selectable = false;
		// backBleedGroup3.evented = false;
		//
		// backcanvas.add(backBleedGroup3);
		// alwaysOnTopBack(backBleedGroup3);


    //backcanvas.renderAll();
	} // readdBleedlinesBack end


//function firstbackLoad() {

	var backbleedline1 = new fabric.Line([0, 0, 0, bCh], {
		strokeDashArray: [5, 5],
		stroke: "#ff0000"
	});
	var backbleedline2 = new fabric.Line(
		[bCw - 1, 0, bCw - 1, bCh],
		{
			strokeDashArray: [5, 5],
			stroke: "#ff0000"
		}
	);
	var backbleedline3 = new fabric.Line([0, 0, bCw, 0], {
		strokeDashArray: [5, 5],
		stroke: "#ff0000"
	});
	var backbleedline4 = new fabric.Line(
		[0, bCh - 1, bCw, bCh - 1],
		{
			strokeDashArray: [5, 5],
			stroke: "#ff0000"
		}
	);

	var backBleedGroup = new fabric.Group(
		[backbleedline1, backbleedline2, backbleedline3, backbleedline4],
		{ name: "outerbleedback" }
	);
	backBleedGroup.selectable = false;
	backBleedGroup.evented = false;

	// backcanvas.add(backBleedGroup);
	// alwaysOnTopBack(backBleedGroup);

	/**
	 * Second group of bleed lines
	 **/

	var backbleedline5 = new fabric.Line([bl, bl, bl, bCh - bl], {
		strokeDashArray: [5, 5],
		stroke: "#00ff00"
	});
	var backbleedline6 = new fabric.Line(
		[bCw - bl, bl, bCw - bl, bCh - bl],
		{
			strokeDashArray: [5, 5],
			stroke: "#00ff00"
		}
	);
	var backbleedline7 = new fabric.Line([bl, bl, bCw - bl, bl], {
		strokeDashArray: [5, 5],
		stroke: "#00ff00"
	});
	var backbleedline8 = new fabric.Line(
		[bl, bCh - bl, bCw - bl, bCh - bl],
		{
			strokeDashArray: [5, 5],
			stroke: "#00ff00"
		}
	);

	var backBleedGroup2 = new fabric.Group(
		[backbleedline5, backbleedline6, backbleedline7, backbleedline8],
		{ name: "innerbleedback" }
	);
	backBleedGroup2.selectable = false;
	backBleedGroup2.evented = false;

	backcanvas.add(backBleedGroup2);
	alwaysOnTopBack(backBleedGroup2);

	/* Third Bleed Group */

	var bleedline9 = new fabric.Line([sz, sz, sz, bCh - sz], {
		strokeDashArray: [5, 5],
		stroke: "#0000ff"
	});
	var bleedline10 = new fabric.Line(
		[bCw - sz, sz, bCw - sz, bCh - sz],
		{
			strokeDashArray: [5, 5],
			stroke: "#0000ff"
		}
	);
	var bleedline11 = new fabric.Line([sz, sz, bCw - sz, sz], {
		strokeDashArray: [5, 5],
		stroke: "#0000ff"
	});
	var bleedline12 = new fabric.Line(
		[sz, bCh - sz, bCw - sz, bCh - sz],
		{
			strokeDashArray: [5, 5],
			stroke: "#0000ff"
		}
	);

	var backBleedGroup3 = new fabric.Group(
		[bleedline9, bleedline10, bleedline11, bleedline12],
		{ name: "safetylineback" }
	);
	backBleedGroup3.selectable = false;
	backBleedGroup3.evented = false;

	backcanvas.add(backBleedGroup3);
	alwaysOnTopBack(backBleedGroup3);

	function alwaysOnTopBack(obj) {
		obj.bringToFront();
	}
//}// firstbackLoad

var finalB1 = new fabric.Line([bl, 0, bl, bl], {
	strokeWidth: 2,
	stroke: "#000000"
});

var finalB11 = new fabric.Line([bl, bCh - bl, bl, bCh], {
	strokeWidth: 2,
	stroke: "#000000"
});

var finalB2 = new fabric.Line(
	[bCw - bl, 0 , bCw - bl, bl],
	{
		strokeWidth: 2,
		stroke: "#000000"
	}
);

var finalB21 = new fabric.Line(
	[bCw - bl, bCh - bl , bCw - bl, bCh],
	{
		strokeWidth: 2,
		stroke: "#000000"
	}
);

var finalB3 = new fabric.Line([0, bl, bl, bl], {
	strokeWidth: 2,
	stroke: "#000000"
});
var finalB31 = new fabric.Line([bCw - bl, bl, bCw, bl], {
	strokeWidth: 2,
	stroke: "#000000"
});

var finalB4 = new fabric.Line(
	[0, bCh - bl, bl, bCh - bl],
	{
		strokeWidth: 2,
		stroke: "#000000"
	}
);

var finalB41 = new fabric.Line(
	[bCw - bl, bCh - bl, bCw, bCh - bl],
	{
		//strokeDashArray: [5, 5],
		stroke: "#000000",
		strokeWidth: 2

	}
);

var finalBack = new fabric.Group(
	[finalB1, finalB11, finalB2, finalB21, finalB3, finalB31, finalB4, finalB41],
	{ name: "innerbleedbackFinal" }
);
finalBack.selectable = false;
finalBack.evented = false;

//firstbackLoad();

    //Not used
	function loadEmUp() {
        return;
		var backItems;
		//var backItems = backcanvas._iTextInstances;
		backcanvas.getObjects().forEach(function(txts) {
			if (txts.get("type") === "textbox") {
				backItems = txts.get("type") === "Textbox";
			}
		});
        if(!backItems){
            return;
        }

		var cntBack = 0;
		for (var k = 0; k < backItems.length; k++) {
			if (backItems[k].get("type") === "textbox") {
				cntBack++;
				document.getElementById("hidden-counter-back").value = cntBack;
				var inFieldBack = document.createElement("input");
				inFieldBack.id = "back-editable-field-" + cntBack;
				inFieldBack.className = "editable-field";
				inFieldBack.value = backItems[k].text;
				$("#back-editable-field-wrapper").append(inFieldBack);
			}
		}
	}

	setTimeout(function() {
		loadEmUp();
	}, 2000);
});
