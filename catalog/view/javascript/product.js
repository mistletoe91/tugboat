var canvas;
var fields = [];
var bleedGroup3;
var bleedGroup2;
var finalFront;
var backBleedGroup2;

$(document).ready(function() {
	$("#designer-wrapper").hide();
	// $("#json_stuff").hide();
	// $("#json_back_stuff").hide();

    //File upload
    $('#ctlfiles_1').change(function() {
        $('#ctlfiles_sbt_1').submit();
    });
    $('#ctlfiles_2').change(function() {
        $('#ctlfiles_sbt_2').submit();
    });
    $('#clsuploadfile_1').click(function(){
        $('#ctlfiles_1').click ();
        return false;
    });
    $('#clsuploadfile_2').click(function(){
        $('#ctlfiles_2').click ();
        return false;
    });

    //Filters
    $('#imggrayscale_1').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#grayscale').click (); }, 3000);
        }
        return false;
    });
    $('#imginvert_1').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#invert').click (); }, 3000);
        }
        return false;
    });
    $('#imgsepia_1').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#sepia').click (); }, 3000);
        }
        return false;
    });
    $('#imgsepia2_1').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#sepia2').click (); }, 3000);
        }
        return false;
    });
    $('#imggrayscale_2').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#grayscale-back').click (); }, 3000);
        }
        return false;
    });
    $('#imginvert_2').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#invert-back').click (); }, 3000);
        }
        return false;
    });
    $('#imgsepia_2').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#sepia-back').click (); }, 3000);
        }
        return false;
    });
    $('#imgsepia2_2').click(function(){
        if( $(this).css("opacity") === "1"){
            $('#divloadingmodal').modal('show');
            setTimeout(function(){ $('#sepia2-back').click (); }, 3000);
        }
        return false;
    });

	$(".placeholder_discount").on("change", function() {
		var arr = this.value.split("_");

		var raw_price = Number(arr[1].replace(/[^0-9\.]+/g, ""));
		var raw_qty = Number(arr[0].replace(/[^0-9\.]+/g, ""));

		$(".placeholder_finalprice").html(
			"$" + parseFloat(parseFloat(raw_price) * parseFloat(raw_qty)).toFixed(2)
		);
		$("#input-quantity").val(arr[0]);
		$(".placeholder_discount").val(this.value);
	});

	// setCanvasSize();

function setCanvasProperty(){
	canvas = new fabric.Canvas("canvas-product-front", {
		width: $("#ctl_canvas_width").html(),
		height: $("#ctl_canvas_height").html(),
		bleed: $("#ctl_canvas_bleed").html(),
		saveZoon: $("#ctl_canvas_savezoon").html(),
		preserveObjectStacking: true,
		selection: false

	});
}
  setCanvasProperty();

	const fCh = canvas.height;
  const fCw = canvas.width;
  const bl  = canvas.bleed ? parseInt(canvas.bleed) : 18.5;
  const sz  = canvas.saveZoon ? parseInt(canvas.saveZoon) : 37.50;

	fabric.Object.prototype.set({
		transparentCorners: false,
		cornerColor: "#C40005",
		borderColor: "#10009B"
	});

	// save();
	var prodFrontId,
		matches = document.body.className.match(
			/(^|\s)product-product-(\d+)(\s|$)/
		);
	if (matches) {
		prodFrontId = matches[2];
	}

	function loadFrontTemplate(id) {
		$.each(frontside.success.frontsides, function(key, val) {
			if (!jsondefault) {
				jsondefault = 1; //just use default one
			}

			var price = 0;
			if (Math.round(val.price * 100) / 100 != 0) {
				price = (Math.round(val.price * 100) / 100).toFixed(2);
			} //endif

			if (!price || price === "0.00" || price == 0) {
				price = "";
			} else {
				price = "$" + price;
			}
			// $(".frontsidetemplates > .row").append(val.query);

			$(".frontsidetemplates > .row").append(
				'<div class="col-md-4 no-pad"><a href="Flyers/' +
					val.keyword +
					'"><img src="/image/' +
					val.image +
					'" alt="' +
					val.model +
					'" class="img-thumbnail img-responsive" /></a></div>'
			);

			if (key === jsondefault) {
				jsondefault = val.code;
			} //endif
		});
	} //end loadfronttemplate

	$.ajax({
		type: "GET",
		url: "/index.php?route=api/getfrontsidetemplates/frontside",
		data: { product_id: prodFrontId },
		dataType: "json",
		success: function(data) {
			frontside = data;
            var tmpIs = false;
			$.each(frontside.success.frontsides, function(key, val) {
				if (
					data.success.frontsides[key].json_back_option ==
					'{"backside_not_available": true}'
				) {
					//console.log(data.success.frontsides[key].json_back_option);
					//$("#savebtn").trigger("click");

					$("#hide-step-2").hide();
                    tmpIs = true;
				}
				if (key === prodFrontId) {
					frontside.success.frontsides[key].code =
						data.success.frontsides[key].code;
				}
			});

      if(tmpIs){
					$("#savebtn").on("click", function() {
						$("#backsavebtn").trigger("click");
						$("#clsproof_back").siblings("h3:eq(1)").hide();
						$("#clsproof_back").hide();
					});
      }//endif

			//Check if data already saved
			// var jsondefault = $("#input-option230").val();
			var ctlId = "";
			var inputs = document.querySelectorAll("textarea");
			Array.prototype.forEach.call(inputs, function(i) {
				if (!String.prototype.startsWith) {
					String.prototype.startsWith = function(searchString, position) {
						position = position || 0;
						return this.substr(searchString, position) === position;
					};
				}
				if (i.name.startsWith("option[")) {
					if (i.getAttribute("placeholder") === "json_default_front") {
						ctlId = i.id;
					}
				}
			});

			var jsondefault = $("#" + ctlId).val();
			// doint (jsondefault, 1);
			loadFrontTemplate(prodFrontId);
		}
	});

	$(".clsbacksavebtn,.clsgetfinalproof").click(function() {
		var json = canvas.toJSON(["id", "name"]);
		//$( "#input-option227" ).val(JSON.stringify(json));
	});



	//Check if the data is already saved
	var ctlId = "";

	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.indexOf(searchString, position) === position;
	};

	var inputs = document.querySelectorAll("textarea");
	Array.prototype.forEach.call(inputs, function(i) {
		if (!String.prototype.startsWith) {
			String.prototype.startsWith = function(searchString, position) {
				position = position || 0;
				return this.substr(searchString, position) === position;
			};
		}
		if (i.name.startsWith("option[")) {
			if (i.getAttribute("placeholder") === "json_default_front") {
				ctlId = i.id;
			}
		}
	});

	var jsondefault = $("#" + ctlId).val();
	// var jsondefault = $("#input-option230").val();
	var myjson_front = "";
	//myjson_front = $( "#input-option227" ).val();

	if (myjson_front.replace(/\s/g, "") == "") {
		//Get from detault
		canvas.clear();
		canvas.loadFromJSON(jsondefault, canvas.renderAll.bind(canvas));
	} else {
		//User has something entered in front
		canvas.clear();
		canvas.loadFromJSON(myjson_front, canvas.renderAll.bind(canvas));
	}

    /*
    //Jaswinder
    //Create White Background
	var backWhite =		new fabric.Rect({
				fill: 'rgba(0,0,0,0)',
				width: $("#ctl_canvas_width").html(),
				height: $("#ctl_canvas_height").html(),
				top: 0,
				left: 0
			});
	backWhite.selectable = false;
    backWhite.evented = false;
    canvas.add(backWhite);
    //canvas.sendToBack(backWhite);
    canvas.renderAll();
    */

	/***
  * ---------------------RESPONSIVE CANVAS---------------------------
  * This function resizes the canvas so that it maintains aspect ratio
  * across all different mobile devices. First it takes the canvas sizing element
  * (in this case it's col-md-8) and creates a width offset. Then the width and height
  * properties also get offsets based on the canvas sizing element. Next, the ratio gets
  * calculated by getting the canvas width and dividing it by the height.
  * Finally, a check is performed to see if the width divided by height is greater
  * than the ratio set up earlier. If so, we need to set the width equal to the height
  * multiplied by the ratio. Otherwise, we set the height equal to the width divided by the ratio.
  * Zoom is also used here to ensure that nothing looks too small on a smaller device.
  * ***/

	function resize() {
		var canvasSizer = document.getElementById("resp-canvas-elem");
		var canvasScaleFactor = canvasSizer.offsetWidth / 525;
		var width = canvasSizer.offsetWidth;
		var height = canvasSizer.offsetHeight;
		var ratio = canvas.getWidth() / canvas.getHeight();
		if (width / height > ratio) {
			width = height * ratio;
		} else {
			height = width / ratio;
		}
		var scale = width / canvas.getWidth();
		var zoom = canvas.getZoom();
		zoom *= scale;
		canvas.setDimensions({ width: width, height: height });
		canvas.setViewportTransform([zoom, 0, 0, zoom, 0, 0]);

	}

	//window.addEventListener('load', resizeBoth, false);
	window.addEventListener("load", resize, false);

	//window.addEventListener('load', backResize, false);

	//File upload
	document
		.getElementById("btnfileupload")
		.addEventListener("change", function(e) {
			var file = e.target.files[0];
			var reader = new FileReader();
			reader.onload = function(f) {
				var data = f.target.result;
				fabric.Image.fromURL(
					data,
					function(img) {
						var oImg = img.set({ left: 0, top: 0, angle: 0 }).scale(0.9);
						canvas.add(oImg).renderAll();
						var a = canvas.setActiveObject(oImg);
						var dataURL = canvas.toDataURL({
							format: "png",
							quality: 1
						});
					},
					{ crossOrigin: "Anonymous" }
				);
			};
			reader.readAsDataURL(file);
		});



	$("#fileuploadbtn").click(function() {
		$("#btnfileupload").click();
	});

	$("ul.setup-panel li:eq(0)").on("click", function() {
		canvas.remove(bleedGroup3);
		canvas.remove(bleedGroup2);
		canvas.remove(finalFront);

		setTimeout(function() {
			bleedGroup3.selectable = false;
			bleedGroup3.evented = false;
			canvas.add(bleedGroup3);
			canvas.add(bleedGroup2);
		}, 50);
	});

	$("ul.setup-panel li:eq(1)").on("click", function() {
		canvas.remove(bleedGroup3);
		canvas.deactivateAll().renderAll();
	});

	$("ul.setup-panel li:eq(2)").on("click", function() {
		canvas.remove(bleedGroup3);
		canvas.deactivateAll().renderAll();
	});

	function returnBleedGroup() {
		bleedGroup3.evented = false;
		bleedGroup3.selectable = false;
		canvas.add(bleedGroup3);
	}

    function removeAllBleedLinesForJSON (jsonOBJ){
            try {
                //console.log ("->"+jsonOBJ.name);
                if (typeof jsonOBJ != 'undefined' && jsonOBJ.type === "group" && jsonOBJ.hasOwnProperty("name")) {
                    if((jsonOBJ.name === "outerbleed") || (jsonOBJ.name === "innerbleed") || (jsonOBJ.name === "safetyline") || (jsonOBJ.name === "whitebackground") || (jsonOBJ.name === "outerbleedback") || (jsonOBJ.name === "innerbleedback") || (jsonOBJ.name === "safetylineback") || (jsonOBJ.name === "whitebackgroundback"))
                    {
                           // console.log ("-->"+jsonOBJ.name);
                            return true;
                    }
                }//endif
            }catch(e){return false;}
            return false;
    }

	$("#savebtn").click(function(e) {
		var json = canvas.toJSON(["id", "name"]);
		$("ul.setup-panel li:eq(1)").removeClass("disabled");
		$('ul.setup-panel li a[href="#step-2"]').trigger("click");
		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = true;
			});

		canvas.remove(bleedGroup3);

        //console.log ("BEFORE");
        //console.log (json);
        for($x=0;$x<=json.objects.length;$x++){
           // console.log (json.objects[$x] );
            if(removeAllBleedLinesForJSON (json.objects[$x])){
                //console.log ("removed:"+json.objects[$x].name);
                delete json.objects[$x];
            }
        }//end for
        //console.log ("AFTER");
        //console.log (json);
		$("#json_stuff").val(JSON.stringify(json));
	});

	// select all objects
	function deleteObjects() {
		var activeObject = canvas.getActiveObject(),
			activeGroup = canvas.getActiveGroup();
		if (activeObject) {
			if (confirm("Are you sure? This action can't be undone")) {
				canvas.remove(activeObject);
			}
		} else if (activeGroup) {
			if (confirm("Are you sure? This action can't be undone")) {
				var objectsInGroup = activeGroup.getObjects();
				canvas.discardActiveGroup();
				objectsInGroup.forEach(function(object) {
					canvas.remove(object);
				});
			}
		}
	}

	//Start over from scratch with a blank canvas after clicking on the clear canvas button (Erase)
	function emptyCanvas() {
		/* Possible refactor needed: Map over the objects currently on the canvas
    /* and assign them to a new group that can be set active all at once so
    /* that when you click the clear button the entire canvas gets cleared
    /* for anyone who might want to start over from scratch on their design
    */
		canvas.selection = true;
		if (confirm("Reset Canvas to blank? This action can't be undone")) {
			var objs = canvas.getObjects().map(function(o) {
				return o.set("active", true);
			});
			var group = new fabric.Group(objs, {
				originX: "center",
				originY: "center"
			});
			canvas._activeObject = null;
			canvas.setActiveGroup(group.setCoords()).renderAll();

			//actually empty the canvas here
			var oldCanvas = canvas.toJSON(["id", "name"]);
			canvas.clear();
		}

		if ($("#hidden-counter").val() > 0) {
			$("#hidden-counter").val(0);
			$("#editable-field-wrapper").html("");
		}
	}



	var undoHist = [];
	var redoHist = [];
	var replayFlag = false; // set to true while undoing or redoing..
	var selectedObject = [];



	//Add support for clearing the canvas
	$("#clearbutton").click(function() {
		//canvas.selected = true;
		emptyCanvas();
		readdBleedlines();
	});

	//Undo button
	$("#undobutton").click(function() {
		actionReplay("undo");
		// replay(undo, redo, '#redobutton', this);
	});

	//Redo button
	$("#redobutton").click(function() {
		actionReplay("redo");
		// replay(redo, undo, '#undobutton', this);
	});

	$("#sendSelectedObjectToFront").click(function() {
		var selectedObject = canvas.getActiveObject();
		if (selectedObject) {
			canvas.bringToFront(selectedObject);
		}
	});

	$("#sendSelectedObjectBack").click(function(e) {
		var selectedObject = canvas.getActiveObject();
		if (selectedObject) {
			canvas.sendToBack(selectedObject);
		}
	});

	$("#deletebtn").click(function() {
		canvas.isDrawingMode = false;
		deleteObjects();
	});

	$("#btnleftalign").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "left") {
				$("#btnleftalign").addClass("active");
				activeTextObj.set("textAlign", "left");
				canvas.renderAll();
			}
			if (alignment === "left") {
				$("#btnleftalign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "left") {
				$("#btncenteralign").addClass("active");
				activeTextObj.set("textAlign", "left");
				canvas.renderAll();
			}
			if (alignment === "left") {
				$("#btncenteralign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}
	});

	$("#btnrightalign").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "right") {
				$("#btnrightalign").addClass("active");
				activeTextObj.set("textAlign", "right");
				canvas.renderAll();
			}
			if (alignment === "right") {
				$("#btnrightalign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "right") {
				$("#btnrightalign").addClass("active");
				activeTextObj.set("textAlign", "right");
				canvas.renderAll();
			}
			if (alignment === "right") {
				$("#btnrightalign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}
	});

	$("#btncenteralign").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "center") {
				$("#btncenteralign").addClass("active");
				activeTextObj.set("textAlign", "center");
				canvas.renderAll();
			}
			if (alignment === "center") {
				$("#btncenteralign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			var alignment = activeTextObj.getTextAlign();
			if (alignment !== "center") {
				$("#btncenteralign").addClass("active");
				activeTextObj.set("textAlign", "center");
				canvas.renderAll();
			}
			if (alignment === "center") {
				$("#btncenteralign").removeClass("active");
				activeTextObj.set("textAlign", "none");
				canvas.renderAll();
			}
		}
	});

	$("#btnbold").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			if (activeTextObj.get("fontWeight", "bold")) {
				$("#btnbold").removeClass("active");
				activeTextObj.set("fontWeight", "");
				canvas.renderAll();
			} else {
				$("#btnbold").addClass("active");
				activeTextObj.set("fontWeight", "bold");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			if (activeTextObj.get("fontWeight", "bold")) {
				$("#btnitalic").removeClass("active");
				activeTextObj.set("fontWeight", "");
				canvas.renderAll();
			} else {
				$("#btnitalic").addClass("active");
				activeTextObj.set("fontWeight", "bold");
				canvas.renderAll();
			}
		}
	});

	$("#btnitalic").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			if (activeTextObj.get("fontStyle", "italic")) {
				$("#btnitalic").removeClass("active");
				activeTextObj.set("fontStyle", "");
				canvas.renderAll();
			} else {
				$("#btnitalic").addClass("active");
				activeTextObj.set("fontStyle", "italic");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			if (activeTextObj.get("fontStyle", "italic")) {
				$("#btnitalic").removeClass("active");
				activeTextObj.set("fontStyle", "");
				canvas.renderAll();
			} else {
				$("#btnitalic").addClass("active");
				activeTextObj.set("fontStyle", "italic");
				canvas.renderAll();
			}
		}
	});

	$("#btnunderline").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			if (activeTextObj.get("textDecoration", "underline")) {
				$("#btnunderline").removeClass("active");
				activeTextObj.set("textDecoration", "");
				canvas.renderAll();
			} else {
				$("#btnunderline").addClass("active");
				activeTextObj.set("textDecoration", "underline");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			if (activeTextObj.get("textDecoration", "underline")) {
				$("#btnunderline").removeClass("active");
				activeTextObj.set("textDecoration", "");
				canvas.renderAll();
			} else {
				$("#btnunderline").addClass("active");
				activeTextObj.set("textDecoration", "underline");
				canvas.renderAll();
			}
		}
	});
	$("#btnstrikethrough").click(function() {
		var activeTextObj = canvas.getActiveObject();
		if (activeTextObj.get("type") === "textbox") {
			if (activeTextObj.get("textDecoration", "line-through")) {
				$("#btnstrikethrough").removeClass("active");
				activeTextObj.set("textDecoration", "");
				canvas.renderAll();
			} else {
				$("#btnstrikethrough").addClass("active");
				activeTextObj.set("textDecoration", "line-through");
				canvas.renderAll();
			}
		}

		if (activeTextObj.get("type") === "i-text") {
			if (activeTextObj.get("textDecoration", "line-through")) {
				$("#btnstrikethrough").removeClass("active");
				activeTextObj.set("textDecoration", "");
				canvas.renderAll();
			} else {
				$("#btnstrikethrough").addClass("active");
				activeTextObj.set("textDecoration", "line-through");
				canvas.renderAll();
			}
		}
	});

	var f = fabric.Image.filters;

	function applyFilter(index, filter) {
		var obj = canvas.getActiveObject();
		obj.filters[index] = filter;
		obj.applyFilters(canvas.renderAll.bind(canvas));
        $('#divloadingmodal').modal('hide');
	}

	function applyFilterValue(index, prop, value) {
		var obj = canvas.getActiveObject();
		if (obj.filters[index]) {
			obj.filters[index][prop] = value;
			obj.applyFilters(canvas.renderAll.bind(canvas));
		}
	}

	function onObjectSelected() {
		for (i in canvas.getObjects()) {
			if (canvas.item(i).active === true) {
				selectedObject.push({
					itemNum: i,
					svdObject: fabric.util.object.clone(canvas.item(i))
				});
			}

			var activeObj = canvas.getActiveObject();
			if (activeObj) {
				$(".sp-preview-inner").eq(0).css({
					backgroundColor: activeObj.get("fill")
				});
				$(".sp-preview-inner").eq(1).css({
					backgroundColor: activeObj.get("backgroundColor")
				});
				$(".sp-preview-inner").eq(1).css({
					backgroundColor: activeObj.get("backgroundColor")
				});
				getFilters();


				if (activeObj.get("type") === "textbox") {
					var fontSize = activeObj.get("fontSize");

					if (activeObj.get("fill")) {
						$(".sp-preview-inner").eq(0).css({
							backgroundColor: activeObj.get("fill")
						});
					}

					if (activeObj.get("fontWeight") === "bold") {
						$("#btnbold").addClass("active");
					} else {
						$("#btnbold").removeClass("active");
					}

					if (activeObj.get("fontStyle") === "italic") {
						$("#btnitalic").addClass("active");
					} else {
						$("#btnitalic").removeClass("active");
					}

					if (activeObj.get("textDecoration") === "underline") {
						$("#btnunderline").addClass("active");
					} else {
						$("#btnunderline").removeClass("active");
					}

					if (activeObj.get("textDecoration") === "line-through") {
						$("#btnstrikethrough").addClass("active");
					} else {
						$("#btnstrikethrough").removeClass("active");
					}

					if (activeObj.get("textAlign") === "left") {
						$("#btnleftalign").addClass("active");
					} else {
						$("#btnleftalign").removeClass("active");
					}

					if (activeObj.get("textAlign") === "center") {
						$("#btncenteralign").addClass("active");
					} else {
						$("#btncenteralign").removeClass("active");
					}

					if (activeObj.get("textAlign") === "right") {
						$("#btnrightalign").addClass("active");
					} else {
						$("#btnrightalign").removeClass("active");
					}

					$("#text-font-size").val(fontSize);
				}

				if (activeObj.get("type") === "i-text") {
					var fontSize = activeObj.get("fontSize");

					if (activeObj.get("fill")) {
						$(".sp-preview-inner").eq(0).css({
							backgroundColor: activeObj.get("fill")
						});
					}

					if (activeObj.get("fontWeight") === "bold") {
						$("#btnbold").addClass("active");
					} else {
						$("#btnbold").removeClass("active");
					}

					if (activeObj.get("fontStyle") === "italic") {
						$("#btnitalic").addClass("active");
					} else {
						$("#btnitalic").removeClass("active");
					}

					if (activeObj.get("textDecoration") === "underline") {
						$("#btnunderline").addClass("active");
					} else {
						$("#btnunderline").removeClass("active");
					}

					if (activeObj.get("textDecoration") === "line-through") {
						$("#btnstrikethrough").addClass("active");
					} else {
						$("#btnstrikethrough").removeClass("active");
					}

					if (activeObj.get("textAlign") === "left") {
						$("#btnleftalign").addClass("active");
					} else {
						$("#btnleftalign").removeClass("active");
					}

					if (activeObj.get("textAlign") === "center") {
						$("#btncenteralign").addClass("active");
					} else {
						$("#btncenteralign").removeClass("active");
					}

					if (activeObj.get("textAlign") === "right") {
						$("#btnrightalign").addClass("active");
					} else {
						$("#btnrightalign").removeClass("active");
					}

					$("#text-font-size").val(fontSize);
				}

				if (activeObj.get("type") !== "image") {
					fabric.util
						.toArray(document.querySelectorAll("input[type='checkbox']"))
						.forEach(function(el) {
							el.disabled = true;
						});
				} else {
					fabric.util
						.toArray(document.querySelectorAll("input[type='checkbox']"))
						.forEach(function(el) {
							el.disabled = false;
						});
				}

                if (activeObj.get("type") === "image") {
                    $(".imagefilter").css("opacity", "1");
                } else {
                    $(".imagefilter").css("opacity", "0.1");
                }
			}
		}
	}

	function getFilters() {
		var filters = [
			"grayscale",
			"invert",
			"remove-white",
			"sepia",
			"sepia2",
			"brightness",
			"contrast",
			"saturate",
			"noise",
			"gradient-transparency",
			"pixelate",
			"blur",
			"sharpen",
			"emboss",
			"tint",
			"multiply",
			"blend"
		];

		for (var i = 0; i < filters.length; i++) {
			if (canvas.getActiveObject().get("type") === "image") {
				$(filters[i]).checked = !!canvas.getActiveObject().filters[i];
			}
		}
	}

	document.getElementById("grayscale").onclick = function(e) {
		applyFilter(0, this.checked && new f.Grayscale());
	};

	document.getElementById("invert").onclick = function(e) {
		applyFilter(1, this.checked && new f.Invert());
	};

	document.getElementById("sepia").onclick = function(e) {
		applyFilter(3, this.checked && new f.Sepia());
	};

	document.getElementById("sepia2").onclick = function(e) {
		applyFilter(4, this.checked && new f.Sepia2());
	};

	function onObjectAdded() {
		var o = canvas.getObjects();
		//save();
		if (replayFlag === false) {
			undoHist.push({
				action: "add",
				itemNums: [o.length - 1]
			});
			redoHist = [];
		}

		for (var i = 0; i < canvas.getObjects().length; i++) {
			// console.log(i);
			if (canvas.getObjects()[i].get("name") === "innerbleedline") {
				if (canvas.getObjects()[i].length > 1) {
					canvas.getObjects()[i - 1].remove(canvas.getObjects()[i - 1]);
					canvas.renderAll();
				}
			}

			if (canvas.getObjects()[i].get("name") === "outerbleedline") {
				if (canvas.getObjects()[i].length > 1) {
					canvas.getObjects()[i - 1].remove(canvas.getObjects()[i - 1]);
					canvas.renderAll();
				}
			}

			if (canvas.getObjects()[i].get("name") === "safetyline") {
				if (canvas.getObjects()[i].length > 1) {
					var safetyObj = canvas.getObjects()[i - 1];
					canvas.getObjects().remove(safetyObj);
					canvas.deactivateAll().renderAll();
				}
			}

			// if (canvas.getObjects()[i].get("type") === "i-text") {
			// 	canvas
			// 		.getObjects()
			// 		[i].set(
			// 			"stroke",
			// 			$(".sp-preview-inner").eq(0).css({ backgroundColor })
			// 		);
			// 	canvas.renderAll();
			// }

			// if (canvas.getObjects()[i].get("type") === "textbox") {
			// 	canvas
			// 		.getObjects()
			// 		[i].set(
			// 			"stroke",
			// 			$(".sp-preview-inner").eq(0).css({ backgroundColor })
			// 		);
			// }
		}

		// if (o[o.length - 1].get("type") === "textbox") {
		// 	o[o.length - 1].set(
		// 		"stroke",
		// 		// $(".sp-preview-inner:eq(0)").css("background-color")
		// 		$(".sp-preview-inner").eq(0).css()
		// 	);
		// 	canvas.renderAll();
		// }

		// if (o[o.length - 1].get("type") === "i-text") {
		// 	o[o.length - 1].set(
		// 		"stroke",
		// 		$(".sp-preview-inner").eq(0).css()
		// 	);
		// 	canvas.renderAll();
		// }

		if (o[o.length - 1].get("type") === "rect") {
			o[o.length - 1].set(
				"fill",
				$(".sp-preview-inner").eq(2).css("background-color")
			);
		}
		if (o[o.length - 1].get("type") === "circle") {
			o[o.length - 1].set(
				"fill",
				$(".sp-preview-inner").eq(2).css("background-color")
			);
		}
		if (o[o.length - 1].get("type") === "line") {
			o[o.length - 1].set(
				"stroke",
				$(".sp-preview-inner").eq(2).css("background-color")
			);
		}
		// o[o.length - 1].set(
		// 	"stroke",
		// 	$(".sp-preview-inner").eq(2).css("background-color")
		// );

		if (o[o.length - 1].get("name") === "safetyline") {
			o[o.length - 1].set("stroke", "#0000ff");
		}

		if (o[o.length - 1].get("name") === "outerbleed") {
			o[o.length - 1].set("stroke", "#ff0000");
		}

		if (o[o.length - 1].get("name") === "innerbleed") {
			o[o.length - 1].set("stroke", "#00ff00");
		}
	}

	function onObjectModified(e) {
		// save();
		if (replayFlag === false) {
			// i.e. user is modifying..
			var itemProps = [];
			for (i in selectedObject) {
				itemProps.push({
					itemNum: selectedObject[i].itemNum,
					left: selectedObject[i].svdObject.left,
					top: selectedObject[i].svdObject.top,
					scaleX: selectedObject[i].svdObject.scaleX,
					scaleY: selectedObject[i].svdObject.scaleY
				});
			}

			// get current object properties
			onObjectSelected();

			undoHist.push({
				action: "modify",
				itemProps: itemProps
			});
			redoHist = [];
		}

		// var activeObj = canvas.getActiveObject();
		// console.log("OBJECT: ", activeObj);
		// console.log("WIDTH : HEIGHT", activeObj.width + " : " + activeObj.height);
		//  console.log("SCALEX : SCALEY", activeObj.scaleX + " : " + activeObj.scaleY);
	}

	function onObjectSelectionCleared() {
		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = true;
			});
		// save();
	}

	function onObjectRemoved(object, styleName, value, txtCtl) {
		var txtId = object.target.id;
		if (!txtId) {
			canvas.renderAll();
		} else {
			txtCtl = document.getElementById("editable-field-" + txtId.toString());
			var snippedCtlId = txtCtl
				.getAttribute("id")
				.substr(15, txtCtl.getAttribute("id").length);
			if (txtId.toString() === snippedCtlId) {
				$(txtCtl).remove();
				x--;
				document.getElementById("hidden-counter").value = x;
			}
		}

		if (object.target.get("type") === "image") {
			if ($("uploaded-image").attr("src") !== "") {
				$("#uploaded-image").attr("src", "");
			}
		}
		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = true;
			});
		// save();
	}

	function actionReplay(replayType) {
		// global flag to prevent onObjectSelected method defaults
		// from applying to programmatic actions.
		replayFlag = true;
		if (replayType === "undo") {
			if (undoHist.length > 0) {
				var backStack = undoHist;
				var forwardStack = redoHist;
				replay(backStack, forwardStack);
			}
			// ----------
		} else if (replayType === "redo") {
			if (redoHist.length > 0) {
				var backStack = redoHist;
				var forwardStack = undoHist;
				replay(backStack, forwardStack);
			}
		}
		replayFlag = false;
		canvas.renderAll();
	}

	function replay(backStack, forwardStack) {
		var o = backStack[backStack.length - 1];
		var actionType = backStack[backStack.length - 1].action;
		var itemProps = [];
		var itemNums = [];
		var boolShow = true;

		// Not working:
		// 1. move / resize *multiple* objects

		if (actionType === "remove") {
			actionType = "add"; // invert actionType for add/remove
			boolShow = true;
		} else if (actionType === "add") {
			actionType = "remove";
			boolShow = false;
		} else {
			boolShow = true;
		}

		if (actionType === "add" || actionType === "remove") {
			for (i in o.itemNums) {
				canvas.item(o.itemNums[i]).set({
					selectable: boolShow,
					visible: boolShow
				});
				itemNums.push(o.itemNums[i]);
				canvas.item(o.itemNums[i]).setCoords();
			}
		} else {
			// actionType === "modify"

			for (i in o.itemProps) {
				// current itemProps, not those from the history
				itemProps.push({
					itemNum: o.itemProps[i].itemNum,
					left: canvas.item(o.itemProps[i].itemNum).left,
					top: canvas.item(o.itemProps[i].itemNum).top,
					scaleX: canvas.item(o.itemProps[i].itemNum).scaleX,
					scaleY: canvas.item(o.itemProps[i].itemNum).scaleY
				});

				canvas.item(o.itemProps[i].itemNum).set({
					left: o.itemProps[i].left,
					top: o.itemProps[i].top,
					scaleX: o.itemProps[i].scaleX,
					scaleY: o.itemProps[i].scaleY
				});

				itemNums.push(o.itemProps[i].itemNum);

				canvas.item(o.itemProps[i].itemNum).setCoords();
			}
		}

		forwardStack.push({
			action: actionType,
			itemNums: itemNums,
			itemProps: itemProps
		});

		backStack.splice(backStack.length - 1, 1);

		selectedObject = [];
		onObjectSelected();
	}

	canvas.on("object:selected", onObjectSelected);
	//canvas.on('selection:cleared', onObjectSelectionCleared);
	canvas.on("object:added", onObjectAdded);
	canvas.on("object:modified", onObjectModified);
	canvas.on("object:removed", onObjectRemoved);
	canvas.on("text:editing:exited", setTextStyle);
	canvas.on("selection:changed", setStyle);
	canvas.on("object:moving", onObjectMoving);


    canvas.on("selection:cleared", function(e) {
        $(".imagefilter").css("opacity", "0.1");
    });

	canvas.on("object:scaling", function(e) {
		var obj = e.target;
		var maxWidth = canvas.width;
		var maxHeight = canvas.height;
		var actualWidth = obj.scaleX * obj.width;
		var actualHeight = obj.scaleY * obj.height;

		if (!isNaN(maxWidth) && actualWidth >= maxWidth) {
			obj.set({ scaleX: maxWidth / obj.width });
		}

		if (!isNaN(maxHeight) && actualHeight >= maxHeight) {
			obj.set({ scaleY: maxHeight / obj.height });
		}

	});

	function onObjectMoving(e) {
		var obj = e.target;
		if (
			obj.getHeight() > obj.canvas.height ||
			obj.getWidth() > obj.canvas.width
		) {
			return;
		}
		obj.setCoords();

		if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
			obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
			obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
		}

		if (
			obj.getBoundingRect().top + obj.getBoundingRect().height >
				obj.canvas.height ||
			obj.getBoundingRect().left + obj.getBoundingRect().width >
				obj.canvas.width
		) {
			obj.top = Math.min(
				obj.top,
				obj.canvas.height -
					obj.getBoundingRect().height +
					obj.top -
					obj.getBoundingRect().top
			);
			obj.left = Math.min(
				obj.left,
				obj.canvas.width -
					obj.getBoundingRect().width +
					obj.left -
					obj.getBoundingRect().left
			);
		}
	}

	function observeBoolean(property) {
		document.getElementById(property).onclick = function() {
			canvas.getActiveObject()[property] = this.checked;
			canvas.renderAll();
		};
	}

	function observeOptionsList(property) {
		var list = document.querySelectorAll("#" + property + ' [type="checkbox"]');
		for (var i = 0, len = list.length; i < len; i++) {
			list[i].onchange = function() {
				canvas.getActiveObject()[property](this.name, this.checked);
				canvas.renderAll();
			};
		}
	}

	//Text fields
	$("#textcolor").spectrum({
		color: "#000",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdate = color.toHexString();
			var obj = canvas.getActiveObject();
			// if (obj.get("type") === "textbox") {
			// 	obj.set("stroke", colToUpdate);
			// }
			// if (obj.get("type") === "i-text") {
			// 	obj.set("stroke", colToUpdate);
			// }
			obj.set("fill", colToUpdate);
			canvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {
			// var colToUpdate = color.toHexString();
			// var obj = canvas.getActiveObject();
			// obj.set('fill', colToUpdate);
			// canvas.renderAll();
		},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});

	$("#textbgcolor").spectrum({
		color: "#000",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdate = color.toHexString();
			var obj = canvas.getActiveObject();
			if (obj.get("type") === "i-text") {
				obj.set("fill", colToUpdate);
			}

			if (obj.get("type") === "textbox") {
				obj.set("fill", colToUpdate);
			}
			obj.setBackgroundColor(colToUpdate);
			canvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {
			// var colToUpdate = color.toHexString();
			// var obj = canvas.getActiveObject();
			// obj.setBackgroundColor(colToUpdate);
			// canvas.renderAll();
		},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});

	$("#opacityrange").on("input", function() {
		var activeShape = canvas.getActiveObject();
		var opacityVal = document.getElementById("opacityrange").value;
		if (activeShape.get("type") === "circle") {
			activeShape.set("opacity", opacityVal);
			canvas.renderAll();
		}
		if (activeShape.get("type") === "rect") {
			activeShape.set("opacity", opacityVal);
			canvas.renderAll();
		}
		//console.log(document.getElementById("opacityrange").value);
	});

	$("#opacityrange").on("change", function() {
		var activeShape = canvas.getActiveObject();
		var opacityVal = document.getElementById("opacityrange").value;
		if (activeShape.get("type") === "circle") {
			activeShape.set("opacity", opacityVal);
			canvas.renderAll();
		}
		if (activeShape.get("type") === "rect") {
			activeShape.set("opacity", opacityVal);
			canvas.renderAll();
		}
		//console.log(document.getElementById("opacityrange").value);
	});

	document.getElementById("text-lines-bg-color").onchange = function() {
		canvas.getActiveObject().setTextBackgroundColor(this.value);
		canvas.renderAll();
	};

	// document.getElementById("text-stroke-color").onchange = function() {
	// 	canvas.getActiveObject().setStroke(this.value);
	// 	canvas.renderAll();
	// };

	// document.getElementById("text-stroke-width").onchange = function() {
	// 	canvas.getActiveObject().setStrokeWidth(this.value);
	// 	canvas.renderAll();
	// };

	document.getElementById("font-family").onchange = function() {
		canvas.getActiveObject().setFontFamily(this.value);
		canvas.renderAll();
	};

	document.getElementById("text-font-size").onchange = function() {
		canvas.getActiveObject().setFontSize(this.value);
		canvas.renderAll();
	};

	document.getElementById("text-line-height").onchange = function() {
		canvas.getActiveObject().setLineHeight(this.value);
		canvas.renderAll();
	};

	document.getElementById("text-align").onchange = function() {
		canvas.getActiveObject().setTextAlign(this.value);
		canvas.renderAll();
	};

	/**
 * Set canvas object active based on clicked textbox
 **/
	function setActiveObj(object, txtCtl) {
		var myId = $(txtCtl).attr("id").substr(15, $(txtCtl).attr("id").length);
		var myIntId = parseInt(myId);
		var newVal = $(txtCtl).val();
		if (myIntId == object.id) {
			canvas.setActiveObject(object);
		}
		canvas.renderAll();
		//  return;
	}

	/**
* This section deals with limiting the amount of text a user can
* have on their design to a max of 10 items. We also set the default font,
* text, and have an initial text field set for when the design loads
**/

	var defaultText = "Edit Me!";
	var x = 0;

	setTimeout(function() {
		var addVals = document.getElementById("hidden-counter").value;
		var dynamicEl = document.getElementById("editable-field-" + addVals);
		if (addVals > 0) {
			$("#editable-field-" + addVals).on("click", dynamicEl, function(e) {
				//dynamicEl.value = "";
				typingFunc(e.target);
			});

			for (var t = 0; t < addVals; t++) {
				$("#editable-field-" + t).on("click", dynamicEl, function(e) {
					//dynamicEl.value = "";
					typingFunc(e.target);
				});
			}

			$("#editable-field-wrapper").on("keyup", dynamicEl, function(e) {
				addHandler(
					"text-font-size",
					function(obj) {
						// console.log(e.target);
						setStyle(obj, "fontSize", 12, e.target);
					},
					"keyup"
				);
			});
		}
	}, 1000);

	$("#add-editable").click(function(e) {
		if (document.getElementById("hidden-counter").value === 0) {
			x = 0;
		} else {
			x = document.getElementById("hidden-counter").value;
		}
		if (x < 15) {
			x++;
			document.getElementById("hidden-counter").value = x;
			var inputToAdd = document.createElement("input");
			var fieldVal = x.toString();
			inputToAdd.id = "editable-field-" + fieldVal;
			inputToAdd.className = "editable-field";
			inputToAdd.value = defaultText;

			//console.log (dynElem);

			$("#editable-field-wrapper").append(inputToAdd);

			//forgot exact syntax to add event dynamically. lets try this first
			$("#" + inputToAdd.id).on("click", dynElem, function(e) {
				//dynElem.value = "";
				// console.log(e.target);
				// selectedObjectT(dynElem);
				selectedObjectT(e.target);
			});

			canvas.add(
				new fabric.Textbox(
					document.getElementById("editable-field-" + fieldVal).value,
					{
						fontFamily: "Arial",
						fill: $(".sp-preview-inner").first().css("background-color"),
						// stroke: "#000",
						left: 100,
						top: 100,
						id: x
					}
				)
			);

			var dynElem = document.getElementById("editable-field-" + fieldVal);

			//
			$("#editable-field-wrapper").on("keyup", dynElem, function(e) {
				addHandler(
					"text-font-size",
					function(obj) {
						setStyle(obj, "fontSize", 12, e.target);
					},
					"keyup"
				);
			});
		}
	});

	var typingFunc = function(elem) {
		var idToUse = parseInt(elem.id.substr(15, elem.id.length));
		canvas.getObjects().forEach(function(ob) {
			if (ob.get("type") === "textbox") {
				if (ob.id === idToUse) {
					canvas.setActiveObject(ob);
				}
			}
		});
	};

	var selectedObjectT = function(elem) {
		var idToUse = parseInt(elem.id.substr(15, elem.id.length));
		canvas.getObjects().forEach(function(ob) {
			if (ob.get("type") === "textbox") {
				if (ob.id === idToUse) {
					canvas.setActiveObject(ob);
					canvas.bringToFront(ob);
					//return;
				}
			}
		});
	};

	function setTextStyle(object, styleName, value, txtCtl) {
		var txtId = object.target.id;
		txtCtl = document.getElementById("editable-field-" + txtId.toString());
		var snippedCtlId = txtCtl
			.getAttribute("id")
			.substr(15, txtCtl.getAttribute("id").length);
		if (txtId.toString() === snippedCtlId) {
			txtCtl.value = object.target.text;
		}
		// save();
	}

	function setStyle(object, styleName, value, txtCtl) {
		// console.log("?!?!?!?!?!!?!?: : ", txtCtl);
		var myId = $(txtCtl).attr("id").substr(15, $(txtCtl).attr("id").length);
		var myIntId = parseInt(myId);
		var newVal = $(txtCtl).val();
		if (myIntId == object.id) {
			object.text = newVal;
		}
		canvas.renderAll();
		// save();
	}

	function getStyle(object, styleName) {
		return object.getSelectionStyles && object.isEditing
			? object.getSelectionStyles()[styleName]
			: object[styleName];
	}

	function addHandler(id, fn, eventName) {
		var el = this;
		if ((obj = canvas.getActiveObject())) {
			fn.call(el, obj);
			canvas.renderAll();
		}
	}

	/**
 * Color Palette
 **/

	$("#palette").spectrum({
		color: "#eb882d",
        flat: true,
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		move: function(color) {
			var colToUpdate = color.toHexString();
			var obj = canvas.getActiveObject();
			if (obj.get("type") !== "textbox") {
				obj.set("fill", colToUpdate);
				// obj.set("stroke", colToUpdate);
			}
			if (obj.get("type") !== "i-text") {
				obj.set("fill", colToUpdate);
				// obj.set("stroke", colToUpdate);
			}

			canvas.renderAll();
		},
		show: function() {},
		beforeShow: function() {},
		hide: function() {},
		change: function(color) {},
		palette: [
			[
				"rgb(0, 0, 0)",
				"rgb(67, 67, 67)",
				"rgb(102, 102, 102)",
				"rgb(204, 204, 204)",
				"rgb(217, 217, 217)",
				"rgb(255, 255, 255)"
			],
			[
				"rgb(152, 0, 0)",
				"rgb(255, 0, 0)",
				"rgb(255, 153, 0)",
				"rgb(255, 255, 0)",
				"rgb(0, 255, 0)",
				"rgb(0, 255, 255)",
				"rgb(74, 134, 232)",
				"rgb(0, 0, 255)",
				"rgb(153, 0, 255)",
				"rgb(255, 0, 255)"
			],
			[
				"rgb(230, 184, 175)",
				"rgb(244, 204, 204)",
				"rgb(252, 229, 205)",
				"rgb(255, 242, 204)",
				"rgb(217, 234, 211)",
				"rgb(208, 224, 227)",
				"rgb(201, 218, 248)",
				"rgb(207, 226, 243)",
				"rgb(217, 210, 233)",
				"rgb(234, 209, 220)",
				"rgb(221, 126, 107)",
				"rgb(234, 153, 153)",
				"rgb(249, 203, 156)",
				"rgb(255, 229, 153)",
				"rgb(182, 215, 168)",
				"rgb(162, 196, 201)",
				"rgb(164, 194, 244)",
				"rgb(159, 197, 232)",
				"rgb(180, 167, 214)",
				"rgb(213, 166, 189)",
				"rgb(204, 65, 37)",
				"rgb(224, 102, 102)",
				"rgb(246, 178, 107)",
				"rgb(255, 217, 102)",
				"rgb(147, 196, 125)",
				"rgb(118, 165, 175)",
				"rgb(109, 158, 235)",
				"rgb(111, 168, 220)",
				"rgb(142, 124, 195)",
				"rgb(194, 123, 160)",
				"rgb(166, 28, 0)",
				"rgb(204, 0, 0)",
				"rgb(230, 145, 56)",
				"rgb(241, 194, 50)",
				"rgb(106, 168, 79)",
				"rgb(69, 129, 142)",
				"rgb(60, 120, 216)",
				"rgb(61, 133, 198)",
				"rgb(103, 78, 167)",
				"rgb(166, 77, 121)",
				"rgb(91, 15, 0)",
				"rgb(102, 0, 0)",
				"rgb(120, 63, 4)",
				"rgb(127, 96, 0)",
				"rgb(39, 78, 19)",
				"rgb(12, 52, 61)",
				"rgb(28, 69, 135)",
				"rgb(7, 55, 99)",
				"rgb(32, 18, 77)",
				"rgb(76, 17, 48)"
			]
		]
	});

	/**
* Initially hide the other panels
**/
	$("div#text-panel").hide();
	$("div#image-panel").hide();
	$("div#options-panel").hide();
	$("#design").addClass("active");
	$("#design").css({
		"background-color": "#cdcdcd"
	});
	$("#setdesignactive").css({
		color: "#000"
	});
	/**
* Functions which check which control tab is active
* and apply the active class to the parent of the
* anchor tag which is responsible for setting the
* visibility of each tab
**/

	$("#setdesignactive").click(function() {
		if ($("#text").hasClass("active")) {
			$("#text").removeClass("active");
			$("#text").css({
				"background-color": "transparent"
			});
			$("#settextactive").css({
				color: "#000"
			});
		}

		if ($("#image").hasClass("active")) {
			$("#image").removeClass("active");
			$("#image").css({
				"background-color": "transparent"
			});
			$("#setimageactive").css({
				color: "#000"
			});
		}

		if ($("#options").hasClass("active")) {
			$("#options").removeClass("active");
			$("#options").css({
				"background-color": "transparent"
			});
			$("#setoptionsactive").css({
				color: "#000"
			});
		}

		$("#design").addClass("active");
		$("#design").css({
			"background-color": "#cdcdcd"
		});
		$("#setdesignactive").css({
			color: "#000"
		});
		$("div#design-panel").show();
		$("div#text-panel").hide();
		$("div#image-panel").hide();
		$("div#options-panel").hide();
	});

	$("#settextactive").click(function() {
		if ($("#design").hasClass("active")) {
			$("#design").removeClass("active");
			$("#design").css({
				"background-color": "transparent"
			});
			$("#setdesignactive").css({
				color: "#000"
			});
		}

		if ($("#image").hasClass("active")) {
			$("#image").removeClass("active");
			$("#image").css({
				"background-color": "transparent"
			});
			$("#setimageactive").css({
				color: "#000"
			});
		}

		if ($("#options").hasClass("active")) {
			$("#options").removeClass("active");
			$("#options").css({
				"background-color": "transparent"
			});
			$("#setoptionsactive").css({
				color: "#000"
			});
		}

		$("#text").addClass("active");
		$("#text").css({
			"background-color": "#cdcdcd"
		});
		$("#settextactive").css({
			color: "#000"
		});

		$("div#text-panel").show();
		$("div#design-panel").hide();
		$("div#image-panel").hide();
		$("div#options-panel").hide();
	});

	$("#setimageactive").click(function() {
		if ($("#design").hasClass("active")) {
			$("#design").removeClass("active");
			$("#design").css({
				"background-color": "transparent"
			});
			$("#setdesignactive").css({
				color: "#000"
			});
		}

		if ($("#text").hasClass("active")) {
			$("#text").removeClass("active");
			$("#text").css({
				"background-color": "transparent"
			});
			$("#settextactive").css({
				color: "#000"
			});
		}

		if ($("#options").hasClass("active")) {
			$("#options").removeClass("active");
			$("#options").css({
				"background-color": "transparent"
			});
			$("#setoptionsactive").css({
				color: "#000"
			});
		}

		$("#image").addClass("active");
		$("#image").css({
			"background-color": "#cdcdcd"
		});
		$("#setimageactive").css({
			color: "#000"
		});

		$("div#image-panel").show();

		$("div#design-panel").hide();
		$("div#text-panel").hide();
		$("div#options-panel").hide();
	});

	$("#setoptionsactive").click(function() {
		if ($("#design").hasClass("active")) {
			$("#design").removeClass("active");
			$("#design").css({
				"background-color": "transparent"
			});
			$("#setdesignactive").css({
				color: "#000"
			});
		}

		if ($("#text").hasClass("active")) {
			$("#text").removeClass("active");
			$("#text").css({
				"background-color": "transparent"
			});
			$("#settextactive").css({
				color: "#000"
			});
		}

		if ($("#image").hasClass("active")) {
			$("#image").removeClass("active");
			$("#image").css({
				"background-color": "transparent"
			});
			$("#setimageactive").css({
				color: "#000"
			});
		}

		$("#options").addClass("active");

		$("#options").css({
			"background-color": "#cdcdcd"
		});
		$("#setoptionsactive").css({
			color: "#000"
		});

		$("div#options-panel").show();
		$("div#design-panel").hide();
		$("div#text-panel").hide();
		$("div#image-panel").hide();
	});

	/**
* Functions here will add an object to the canvas (Square, Cirle or Triangle)
**/

	$("#addsquare").click(function() {
       // alert($('#palette').val());
		canvas.add(
			new fabric.Rect({
				fill: 'rgba(255,0,0,0.5)',
				width: 100,
				height: 100,
				top: 20,
				left: 20
			})
		);
		canvas.renderAll();


	});

	$("#addcircle").click(function() {
		canvas.add(
			new fabric.Circle({
				radius: 30,
				fill: 'rgba(255,0,0,0.5)',
				top: 10,
				left: 10
			})
		);
		canvas.renderAll();
	});


	$("#addline").click(function() {
		canvas.add(
			new fabric.Line([10, 10, 100, 100], {
				fill: "black",
				stroke: "black"
			})
		);
		canvas.renderAll();
	});

	var trapezoid = [
		{ x: -100, y: -50 },
		{ x: 100, y: -50 },
		{ x: 150, y: 50 },
		{ x: -150, y: 50 }
	];
	var emerald = [
		{ x: 850, y: 75 },
		{ x: 958, y: 137.5 },
		{ x: 958, y: 262.5 },
		{ x: 850, y: 325 },
		{ x: 742, y: 262.5 },
		{ x: 742, y: 137.5 }
	];
	var star4 = [
		{ x: 0, y: 0 },
		{ x: 100, y: 50 },
		{ x: 200, y: 0 },
		{ x: 150, y: 100 },
		{ x: 200, y: 200 },
		{ x: 100, y: 150 },
		{ x: 0, y: 200 },
		{ x: 50, y: 100 },
		{ x: 0, y: 0 }
	];
	var star5 = [
		{ x: 350, y: 75 },
		{ x: 380, y: 160 },
		{ x: 470, y: 160 },
		{ x: 400, y: 215 },
		{ x: 423, y: 301 },
		{ x: 350, y: 250 },
		{ x: 277, y: 301 },
		{ x: 303, y: 215 },
		{ x: 231, y: 161 },
		{ x: 321, y: 161 }
	];

	var shape = new Array(trapezoid, emerald, star4, star5);

	// $("#addpolygon").click(function() {
	//   var polyg = new fabric.Polygon(shape[1], {
	//     top: 180,
	//     left: 200,
	//     fill: 'black',
	//     stroke: '',
	//     strokeWidth: 2
	//   });
	//   canvas.add(polyg);
	//   canvas.renderAll();
	// });

	// $("#addtrapezoid").click(function() {
	//   var trapez = new fabric.Polygon(shape[0], {
	//     top: 180,
	//     left: 200,
	//     fill: 'black',
	//     stroke: '',
	//     strokeWidth: 2
	//   });
	//   canvas.add(trapez);
	//   canvas.renderAll();
	// });

	// $("#addheart").click(function() {
	//   var path = new fabric.Path('M 272.70141,238.71731 \
	//     C 206.46141,238.71731 152.70146,292.4773 152.70146,358.71731  \
	//     C 152.70146,493.47282 288.63461,528.80461 381.26391,662.02535 \
	//     C 468.83815,529.62199 609.82641,489.17075 609.82641,358.71731 \
	//     C 609.82641,292.47731 556.06651,238.7173 489.82641,238.71731  \
	//     C 441.77851,238.71731 400.42481,267.08774 381.26391,307.90481 \
	//     C 362.10311,267.08773 320.74941,238.7173 272.70141,238.71731  \
	//     z ');
	//     var scale = 100 / path.width;
	//     path.set({ left: 20, top: 0, scaleX: scale, scaleY: scale,  fill: 'red', });
	//     canvas.add(path);
	//     canvas.renderAll();
	// });

	// $("#addstar").click(function() {
	//   var star = new fabric.Polygon(shape[3], {
	//     top: 180,
	//     left: 200,
	//     fill: '#ffff00',
	//     stroke: '#ffff00',
	//     strokeWidth: 2
	//   });
	//   canvas.add(star);
	//   canvas.renderAll();
	// });

	/**
* Handler for images that need to be appended to the canvas
**/
	var max_file_size = 5242880; //allowed file size. (1 MB = 1048576)
	var allowed_file_types = [
		"image/png",
		"image/gif",
		"image/jpeg",
		"image/jpg",
		"image/pjpeg"
	]; //allowed file types
	var result_output = "#output"; //ID of an element for response output
	var my_form_id = "#upload_form"; //ID of an element for response output
	var total_files_allowed = 3; //Number files allowed to upload
	$(my_form_id).on("submit", function(event) {
		event.preventDefault();
		var proceed = true; //set proceed flag
		var error = []; //errors
		var total_files_size = 0;

		if (!window.File && window.FileReader && window.FileList && window.Blob) {
			//if browser doesn't supports File API
			error.push("Your browser does not support new File API! Please upgrade."); //push error text
		} else {
			var total_selected_files = this.elements["__files[]"].files.length; //number of files

			//limit number of files allowed
			if (total_selected_files > total_files_allowed) {
				error.push(
					"You have selected " +
						total_selected_files +
						" file(s), " +
						total_files_allowed +
						" is maximum!"
				); //push error text
				proceed = false; //set proceed flag to false
			}
			//iterate files in file input field
			$(this.elements["__files[]"].files).each(function(i, ifile) {
				if (ifile.value !== "") {
					//continue only if file(s) are selected
					if (allowed_file_types.indexOf(ifile.type) === -1) {
						//check unsupported file
						error.push("<b>" + ifile.name + "</b> is unsupported file type!"); //push error text
						proceed = false; //set proceed flag to false
					}

					total_files_size = total_files_size + ifile.size; //add file size to total size
				}
			});

			//if total file size is greater than max file size
			if (total_files_size > max_file_size) {
				error.push(
					"You have " +
						total_selected_files +
						" file(s) with total size " +
						total_files_size +
						", Allowed size is " +
						max_file_size +
						", Try smaller file!"
				); //push error text
				proceed = false; //set proceed flag to false
			}

			var submit_btn = $(this).find("input[type=submit]"); //form submit button

			//if everything looks good, proceed with jQuery Ajax
			if (proceed) {
				submit_btn.val("Please Wait...").prop("disabled", true); //disable submit button
				var form_data = new FormData(this); //Creates new FormData object
				var post_url = $(this).attr("action"); //get action URL of form

                var fileName = $('#ctlfiles_1').val().split('\\').pop();
        $('.maintile_uploadfile_1').append('<div class="tile-progressbar"> <span class="pt_counter_1" data-fill="23.2%" style="width: 23.2%;"></span>    			</div>    			<div class="tile-footer">                     	<h4>    					<span class="pct-counter p_counter_1">Processing...</span>     				</h4>    				<span>'+fileName+' </span>    			</div>');


                $('#divloadingmodal_generic').modal('show');

				//compress
				$.ajax({
					url: post_url,
					type: "POST",
					data: form_data,
					contentType: false,
					cache: false,
					processData: false,
					mimeType: "multipart/form-data"
				}).done(function(res_o) {
                    $('#divloadingmodal_generic').modal('hide');
                    var res = jQuery.parseJSON(res_o);


                    //fileupload
                    $('.p_counter_1').html("Uploaded");
                    $('.pt_counter_1').data('fill',100);
                    $('.pt_counter_1').animate({
                        width:'100%'
                    });

					//
					$(my_form_id)[0].reset(); //reset form
                    $(result_output).html("<img  alt='file' data-original-width='"+res.original_dimentions.width+"' data-original-height='"+res.original_dimentions.height+"' src='"+res.filename+"' />"); //output response from server
					$("#output img").eq(0).hide();
					$("#output img").eq(1).hide();
					var imageToAdd = $("#output img").first().attr("src");
					var imageInstance = new fabric.Image.fromURL(
						imageToAdd,
						function(upImg) {
                            upImg.set({
                                original_width : res.original_dimentions.width,
                                original_height : res.original_dimentions.height
                            });
							upImg.left = 100;
							upImg.top = 100;
							upImg.scale(1);
							canvas.add(upImg);
							canvas.bringToFront(upImg);
							canvas.renderAll();
						},
						{ crossOrigin: "Anonymous" }
					);
					submit_btn.val("Upload").prop("disabled", false); //enable submit button once ajax is done
				});
			}
		}

		$(result_output).html(""); //reset output
		$(error).each(function(i) {
			//output any error to output element
			$(result_output).append('<div class="error">' + error[i] + "</div>");
		});
	});

	// var imageLoader = document.getElementById("user-image");
	// imageLoader.addEventListener("change", handleImage, false);

	// function handleImage(e) {
	// 	var reader = new FileReader();
	// 	var file = e.target.files[0];
	// 	reader.onload = function(evt) {
	// 		$.ajax({
	// 			url: "compressimg.php",
	// 			type: "POST",
	// 			data: $(".droppable img").attr("src", evt.target.result),
	// 			mimeType: "multipart/form-data"
	// 		}).done(function(res) {
	// 			console.log(res);
	// 		});
	// 		// $(".droppable img").attr("src", evt.target.result);
	// 		// var imageToAdd = $(".droppable img").attr("src");
	// 		// imageInstance = new fabric.Image.fromURL(
	// 		// 	imageToAdd,
	// 		// 	function(upImg) {
	// 		// 		upImg.left = 100;
	// 		// 		upImg.top = 100;
	// 		// 		upImg.scale(0.5);
	// 		// 		canvas.add(upImg);
	// 		// 		canvas.bringToFront(upImg);
	// 		// 		canvas.renderAll();
	// 		// 	},
	// 		// 	{ crossOrigin: "Anonymous" }
	// 		// );
	// 	};

	// 	if (file) {
	// 		reader.readAsDataURL(file);
	// 	}
	// }

	// var imageLoaderBackground = document.getElementById("user-image-background");
	// imageLoaderBackground.addEventListener(
	// 	"change",
	// 	handleImageBackground,
	// 	false
	// );

	function handleImageBackground(e) {
		var readerBackground = new FileReader();
		readerBackground.onload = function(evt) {
			$(".droppable-background img").attr("src", evt.target.result);
			var backgroundImageToAdd = $(".droppable-background img").attr("src");
			var imageInstance = new fabric.Image
				.fromURL(
				backgroundImageToAdd,
				function(upBackImg) {
					canvas.setBackgroundImage(upBackImg);
					canvas.renderAll();
				},
				{ crossOrigin: "Anonymous" }
			);
		};
		readerBackground.readAsDataURL(e.target.files[0]);
	}

	$(".nav-tabs > li a[title]").tooltip();

	//Wizard
	$('a[data-toggle="tab"]').on("show.bs.tab", function(e) {
		var $target = $(e.target);

		if ($target.parent().hasClass("disabled")) {
			return false;
		}
	});

	$(".next-step").click(function(e) {
		var $active = $(".wizard .nav-tabs li.active");
		$active.next().removeClass("disabled");
		nextTab($active);
	});
	$(".prev-step").click(function(e) {
		var $active = $(".wizard .nav-tabs li.active");
		prevTab($active);
	});

	/**
* Shortcut key bindings and functions for the editor
**/

	$(document).bind("keydown", "ctrl+z", undoKeyCombo);
	$(document).bind("keydown", "ctrl+c", copyObj);
	$(document).bind("keydown", "ctrl+v", pasteObj);
	$(document).bind("keydown", "ctrl+y", redoKeyCombo);
	$(document).bind("keydown", "del", deleteCombo);
	$(document).bind("keydown", "backspace", frontBackspace);
	$(document).bind("keydown", "alt+del", clearCombo);
	$(document).bind("keydown", "alt+backspace", clearBackspaceCombo);
	$(document).bind("keydown", "ctrl+shift+s", frontSave);

	var copiedObject = [];
	function copyObj() {
		var objToCopy = canvas.getActiveObject();
		var copied = fabric.util.object.clone(objToCopy);
		copiedObject.push(copied);
	}

	function pasteObj() {
		if (copiedObject.length > 0) {
			for (var pasteable = 0; pasteable < copiedObject.length; pasteable++) {
				if (copiedObject[pasteable].get("type") === "textbox") {
					$("#hidden-counter").val(function(i, oldVal) {
						return ++oldVal;
					});
					var newIn = document.createElement("input");
					newIn.id =
						"editable-field-" + document.getElementById("hidden-counter").value;
					newIn.className = "editable-field";
					newIn.value = defaultText;
					$("#editable-field-wrapper").append(newIn);
					// $("#editable-field-" + t).on("click", dynamicEl, function(e) {
					// 	//dynamicEl.value = "";
					// 	typingFunc(e.target);
					// });
					// console.log(copiedObject[pasteable]);
					copiedObject[pasteable].on("click", function(e) {
						typingFunc(e.target);
					});
					copiedObject[pasteable].id = document.getElementById(
						"hidden-counter"
					).value;
					canvas.add(copiedObject[pasteable]);
					canvas.renderAll();
					copiedObject.pop();
				} else {
					canvas.add(copiedObject[pasteable]);
					canvas.renderAll();
					copiedObject.pop();
				}
			}
		}
	}

	function undoKeyCombo() {
		actionReplay("undo");
		// replay(undo, redo, '#redobutton', this);
	}

	function redoKeyCombo() {
		actionReplay("redo");
		// replay(redo, undo, '#undobutton', this);
	}

	function deleteCombo() {
		deleteObjects();
	}

	function frontBackspace() {
		deleteObjects();
	}

	function clearCombo() {
		emptyCanvas();
	}

	function clearBackspaceCombo() {
		emptyCanvas();
	}

	function frontSave() {
		$("ul.setup-panel li:eq(1)").removeClass("disabled");
		$('ul.setup-panel li a[href="#step-2"]').trigger("click");
		fabric.util
			.toArray(document.querySelectorAll("input[type='checkbox']"))
			.forEach(function(el) {
				el.disabled = false;
			});
	}

	$("ul.setup-panel li a[href='#step-3']").on("click", function() {
		canvas.remove(bleedGroup3);
		canvas.remove(bleedGroup2);
		canvas.remove(backBleedGroup2);
		canvas.add(finalFront);
		finalFront.bringToFront();

	});


 function readdBleedlines() {

		var whiteBackground = new fabric.Rect({
				fill: 'rgba(255,255,255,1)',
				width: fCw+1,
				height: fCh+1,
				top: -1,
				left: -1
        });

		var backBleedGroup_back = new fabric.Group(
			[whiteBackground],
			{ name: "whitebackground" }
		);
		backBleedGroup_back.selectable = false;
		backBleedGroup_back.evented = false;
		canvas.add(backBleedGroup_back);

		canvas.add(bleedGroup3);
    bleedGroup3.bringToFront();

		canvas.add(bleedGroup2);
    bleedGroup2.bringToFront();

	// 	var backbleedline1 = new fabric.Line([0, 0, 0, fCh], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#ff0000"
	// 	});
	// 	var backbleedline2 = new fabric.Line(
	// 		[fCw - 1, 0, fCw - 1, fCh],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#ff0000"
	// 		}
	// 	);
	// 	var backbleedline3 = new fabric.Line([0, 0, fCw, 0], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#ff0000"
	// 	});
	// 	var backbleedline4 = new fabric.Line(
	// 		[0, fCh - 1, fCw, fCh - 1],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#ff0000"
	// 		}
	// 	);
	 //
	// 	var backBleedGroup = new fabric.Group(
	// 		[backbleedline1, backbleedline2, backbleedline3, backbleedline4],
	// 		{ name: "outerbleedback" }
	// 	);
	// 	backBleedGroup.selectable = false;
	// 	backBleedGroup.evented = false;
	 //
	// 	// canvas.add(backBleedGroup);
	// 	// backBleedGroup.bringToFront();
	 //
	// 	/**
	//  * Second group of bleed lines
	//  **/
	 //
	// 	var backbleedline5 = new fabric.Line([bl, bl, bl, fCh - bl], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#00ff00"
	// 	});
	// 	var backbleedline6 = new fabric.Line(
	// 		[fCw - bl, 0 + bl, fCw - bl, fCh - bl],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#00ff00"
	// 		}
	// 	);
	// 	var backbleedline7 = new fabric.Line([bl, bl, fCw - bl, bl], {
	// 		strokeDashArray: [5, 5],
	// 		stroke: "#00ff00"
	// 	});
	// 	var backbleedline8 = new fabric.Line(
	// 		[bl, fCh - bl, fCw - bl, fCh - bl],
	// 		{
	// 			strokeDashArray: [5, 5],
	// 			stroke: "#00ff00"
	// 		}
	// 	);
	 //
	// 	var backBleedGroup2 = new fabric.Group(
	// 		[backbleedline5, backbleedline6, backbleedline7, backbleedline8],
	// 		{ name: "innerbleedback" }
	// 	);
	// 	backBleedGroup2.selectable = false;
	// 	backBleedGroup2.evented = false;
	 //
	// 	canvas.add(backBleedGroup2);
	// 	backBleedGroup2.bringToFront();

		/* Third Bleed Group */

		// var bleedline9 = new fabric.Line([sz, sz, sz,fCh - sz], {
		// 	strokeDashArray: [5, 5],
		// 	stroke: "#0000ff"
		// });
		// var bleedline10 = new fabric.Line(
		// 	[
		// 		fCw - sz,
		// 		sz,
		// 		fCw - sz,
		// 		fCh - sz
		// 	],
		// 	{
		// 		strokeDashArray: [5, 5],
		// 		stroke: "#0000ff"
		// 	}
		// );
		// var bleedline11 = new fabric.Line([sz, sz, fCw - sz, sz], {
		// 	strokeDashArray: [5, 5],
		// 	stroke: "#0000ff"
		// });
		// var bleedline12 = new fabric.Line(
		// 	[
		// 		sz,
		// 		fCh - sz,
		// 		fCw - sz,
		// 		fCh - sz
		// 	],
		// 	{
		// 		strokeDashArray: [5, 5],
		// 		stroke: "#0000ff"
		// 	}
		// );
		// bleedGroup3 = new fabric.Group(
		// 	[bleedline9, bleedline10, bleedline11, bleedline12 ],
		// 	{ name: "safetylineback" }
		// );
		// bleedGroup3.selectable = false;
		// bleedGroup3.evented = false;



    //canvas.renderAll() backBleedGroup3;
	} //readdBleedlines end

//firstLoad start

  //function finalBleedMark(){
		// const fCh = canvas.height;
	  // const fCw = canvas.width;
	  // const bl  = canvas.bleed ? parseInt(canvas.bleed) : 18.5;
	  // const sz  = canvas.saveZoon ? parseInt(canvas.saveZoon) : 37.50;

		var final1 = new fabric.Line([bl, 0, bl, bl], {
			strokeWidth: 2,
			stroke: "#000000"
		});

		var final11 = new fabric.Line([bl, fCh - bl, bl, fCh], {
			strokeWidth: 2,
			stroke: "#000000"
		});

		var final2 = new fabric.Line(
			[fCw - bl, 0 , fCw - bl, bl],
			{
				strokeWidth: 2,
				stroke: "#000000"
			}
		);

		var final21 = new fabric.Line(
			[fCw - bl, fCh - bl , fCw - bl, fCh],
			{
				strokeWidth: 2,
				stroke: "#000000"
			}
		);

		var final3 = new fabric.Line([0, bl, bl, bl], {
			strokeWidth: 2,
			stroke: "#000000"
		});
		var final31 = new fabric.Line([fCw - bl, bl, fCw, bl], {
			strokeWidth: 2,
			stroke: "#000000"
		});

		var final4 = new fabric.Line(
			[0, fCh - bl, bl, fCh - bl],
			{
				strokeWidth: 2,
				stroke: "#000000"
			}
		);

		var final41 = new fabric.Line(
			[fCw - bl, fCh - bl, fCw, fCh - bl],
			{
				strokeWidth: 2,
				stroke: "#000000"
			}
		);

		var finalFront = new fabric.Group(
			[final1, final11, final2, final21, final3, final31, final4, final41],
			{ name: "innerbleedback" }
		);
		finalFront.selectable = false;
		finalFront.evented = false;


	//}

	function firstLoad(){


	var bleedline1 = new fabric.Line([0, 0, 0, fCh], {
		strokeDashArray: [5, 5],
		stroke: "#ff0000"
	});
	var bleedline2 = new fabric.Line(
		[fCw - 1, 0, fCw - 1, fCh],
		{
			strokeDashArray: [5, 5],
			stroke: "#ff0000"
		}
	);
	var bleedline3 = new fabric.Line([0, 0, fCw, 0], {
		strokeDashArray: [5, 5],
		stroke: "#ff0000"
	});
	var bleedline4 = new fabric.Line(
		[0, fCh - 1, fCw, fCh - 1],
		{
			strokeDashArray: [5, 5],
			stroke: "#ff0000"
		}
	);

	var bleedGroup = new fabric.Group(
		[bleedline1, bleedline2, bleedline3, bleedline4],
		{ name: "outerbleed" }
	);
	bleedGroup.selectable = false;
	bleedGroup.evented = false;

	//canvas.add(bleedGroup);

	//alwaysOnTop(bleedGroup);

  //bleedGroup.bringToFront();



	/**
	 * Second group of bleed lines
	 **/

	var bleedline5 = new fabric.Line([bl, bl, bl, fCh - bl], {
		strokeDashArray: [5, 5],
		stroke: "#00ff00"
	});
	var bleedline6 = new fabric.Line(
		[fCw - bl, 0 + bl, fCw - bl, fCh -bl],
		{
			strokeDashArray: [5, 5],
			stroke: "#00ff00"
		}
	);
	var bleedline7 = new fabric.Line([0 + bl, bl, fCw - bl, bl], {
		strokeDashArray: [5, 5],
		stroke: "#00ff00"
	});
	var bleedline8 = new fabric.Line(
		[0 + bl, fCh - bl, fCw -bl, fCh - bl],
		{
			strokeDashArray: [5, 5],
			stroke: "#00ff00"
		}
	);

	bleedGroup2 = new fabric.Group(
		[bleedline5, bleedline6, bleedline7, bleedline8],
		{ name: "innerbleed" }
	);
	bleedGroup2.selectable = false;
	bleedGroup2.evented = false;

	canvas.add(bleedGroup2);
	alwaysOnTop(bleedGroup2);

  //bleedGroup2.bringToFront();
	/* Third Bleed Group */


	var bleedline9 = new fabric.Line([sz, sz, sz, fCh - sz], {
		strokeDashArray: [5, 5],
		stroke: "#0000ff"
	});
	var bleedline10 = new fabric.Line(
		[fCw - sz, sz, fCw - sz, fCh - sz],
		{
			strokeDashArray: [5, 5],
			stroke: "#0000ff"
		}
	);
	var bleedline11 = new fabric.Line([sz, sz, fCw - sz, sz], {
		strokeDashArray: [5, 5],
		stroke: "#0000ff"
	});
	var bleedline12 = new fabric.Line(
		[sz, fCh - sz, fCw - sz, fCh - sz],
		{
			strokeDashArray: [5, 5],
			stroke: "#0000ff"
		}
	);



	bleedGroup3 = new fabric.Group(
		[bleedline9, bleedline10, bleedline11, bleedline12],
		{ name: "safetyline" }
	);
	bleedGroup3.selectable = false;
	bleedGroup3.evented = false;

	canvas.add(bleedGroup3);
	alwaysOnTop(bleedGroup3);

	function alwaysOnTop(obj) {
		obj.bringToFront();
	}

}
// first load end
	firstLoad();




	$(window).on("load", function() {
		var iObjs = canvas.getObjects();
		var cnt = 0;
		for (var i = 0; i < iObjs.length; i++) {
			if (iObjs[i].get("type") === "textbox") {
				cnt++;
				document.getElementById("hidden-counter").value = cnt;
				var inField = document.createElement("input");
				inField.id = "editable-field-" + cnt;
				inField.className = "editable-field";
				inField.value = iObjs[i].text;
				$("#editable-field-wrapper").append(inField);
			}
		}

	});
 });

function nextTab(elem) {
	$(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
	$(elem).prev().find('a[data-toggle="tab"]').click();
}
