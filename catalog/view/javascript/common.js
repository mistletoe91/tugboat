function myTrim(x) {
	return x.replace(/^\s+|\s+$/gm, '');
}
function getURLVar(key) {
	const value = [];

	const query = String(document.location).split('?');

	if (query[1]) {
		const part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			const data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		}
			return '';
	}
}

const anchor = window.location.hash;
let sectionId = anchor.replace('#', '');

window.location.hash = '';

$(document).ready(() => {
	// Highlight any found errors
	$('.text-danger').each(function () {
		const element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#form-currency .currency-select').on('click', function (e) {
		e.preventDefault();

		$('#form-currency input[name=\'code\']').val($(this).attr('name'));

		$('#form-currency').submit();
	});

	// Language
	$('#form-language .language-select').on('click', function (e) {
		e.preventDefault();

		$('#form-language input[name=\'code\']').val($(this).attr('name'));

		$('#form-language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', () => {
		let url = `${$('base').attr('href')}index.php?route=product/search`;

		const value = $('header #search input[name=\'search\']').val();

		if (value) {
			url += `&search=${encodeURIComponent(value)}`;
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', (e) => {
		if (e.keyCode == 13) {
			$('header #search input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function () {
		const menu = $('#menu').offset();
		const dropdown = $(this).parent().offset();

		const i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', `-${i + 10}px`);
		}
	});

	// Product List
	$('#list-view').click(() => {
		$('#content .product-grid > .clearfix').remove();

		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
		$('#grid-view').removeClass('active');
		$('#list-view').addClass('active');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(() => {
		// What a shame bootstrap does not take into account dynamically loaded columns
		const cols = $('#column-right, #column-left').length;

		if (cols === 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols === 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		$('#list-view').removeClass('active');
		$('#grid-view').addClass('active');

		localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
		$('#list-view').addClass('active');
	} else {
		$('#grid-view').trigger('click');
		$('#grid-view').addClass('active');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', (e) => {
		if (e.keyCode === 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({ container: 'body' });

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(() => {
		$('[data-toggle=\'tooltip\']').tooltip({ container: 'body' });
	});

	// Register main page scroll to section animations
	const sectionClassNames = [
		'link-whyus',
		'link-enterprise',
		'link-products',
		'link-clients',
	];
	registerScrollEvents(sectionClassNames);

	// Prevent enterprise mockup nav elements from redirecting the page
	$('.ent-nav').click((ev) => {
		ev.preventDefault();
	});

	$('.ent-footer-link').click((ev) => {
		ev.preventDefault();
	});

	if (anchor) {
		scrollToSectionHeader(sectionId);
	}

	hideDimensionsText();

	// Initialize slick-carousel
	$('.clients-carousel').slick({
		lazyLoad: 'ondemand',
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
		centerMode: true,
		centerPadding: '10px',
		slidesToShow: 5,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					centerPadding: '0px',
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 768,
				settings: {
					centerPadding: '0px',
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 576,
				settings: {
					centerPadding: '130px',
					slidesToShow: 1,
				},
			},
			{
				breakpoint: 425,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '100px',
					slidesToShow: 1,
				},
			},
		],
	});
});

const hideDimensionsText = () => {
	const displaySVG = document.getElementById('displaySVG');
	if (displaySVG) {
		displaySVG.addEventListener('load', () => {
			const svg = displaySVG.contentDocument;
			const dimensionsTextArray = svg.getElementsByClassName('dimensionsText');
			if (window.innerWidth >= 768) {
				for (let i = 0; i < dimensionsTextArray.length; i += 1) {
					const element = dimensionsTextArray[i];
					element.style.fill = 'none';
				}
			}
		});
	}
};

const isMainPage = () => {
	let res = false;
	const isBaseURL = window.location.href.indexOf('?') >= 0;
	if (isBaseURL === false) {
		res = true;
		return res;
	}
	const isHomePage = getURLVar('route').indexOf('common/home') >= 0;
	if (isHomePage === true) {
		res = true;
		return res;
	}
	return res;
};

const registerScrollEvents = (elementClassNames) => {
	// hideFooterOnScroll();
	for (let i = 0; i < elementClassNames.length; i += 1) {
		const elementClassName = `.${elementClassNames[i]}`;
		$(elementClassName).click((ev) => {
			const bIsMainPage = isMainPage();
			if (bIsMainPage === true) {
				ev.preventDefault();
				sectionId = elementClassName.substr(6);
				sectionId = sectionId.charAt(0).toUpperCase() + sectionId.slice(1);
				sectionId = sectionId === 'Whyus' ? 'WhyUs' : sectionId;
				sectionId = `section${sectionId}`;
				scrollToSectionHeader(sectionId);
			}
		});
	}
};

const scrollToSectionHeader = (elementId) => {
	const element = $(`#${elementId}`);
	const elementOffset = element.offset();
	const offsetAmount = window.innerWidth >= 768 ? 160 : 120;	// Smaller offset for mobile site
	try {
		$(document).ready(() => {
			$('html,body').animate({ scrollTop: elementOffset.top - offsetAmount }, 1000, () => {
				sectionId = elementId.substr(7).toLowerCase();
				const animatedHeaderElement = $(`#header-${sectionId}`);
				animatedHeaderElement.mouseover();
			});
		})
	} catch (e) {
		console.log(e);
	}
};


// Cart add remove functions
const cart = {
	add(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: `product_id=${product_id}&quantity=${typeof (quantity) !== 'undefined' ? quantity : 1}`,
			dataType: 'json',
			beforeSend() {
				$('#cart > button').button('loading');
			},
			complete() {
				$('#cart > button').button('reset');
			},
			success(json) {
				$('.alert, .text-danger').remove();

				if (json.redirect) {
					location = json.redirect;
				}

				if (json.success) {
					// $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#contentmain').before(`<div class="alert green-alert"><i class="fa fa-check-circle"></i> ${json.success} <button type="button" class="close" data-dismiss="alert">&times;</button></div>`);

					// Need to set timeout otherwise it wont update the total
					setTimeout(() => {
						// $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					}, 100);

					$('html, body').animate({ scrollTop: 0 }, 'slow');

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
	update(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: `key=${key}&quantity=${typeof (quantity) !== 'undefined' ? quantity : 1}`,
			dataType: 'json',
			beforeSend() {
				$('#cart > button').button('loading');
			},
			complete() {
				$('#cart > button').button('reset');
			},
			success(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(() => {
					// $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
	remove(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: `key=${key}`,
			dataType: 'json',
			beforeSend() {
				$('#cart > button').button('loading');
			},
			complete() {
				$('#cart > button').button('reset');
			},
			success(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(() => {
					// $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
};

const voucher = {
	add() {

	},
	remove(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: `key=${key}`,
			dataType: 'json',
			beforeSend() {
				$('#cart > button').button('loading');
			},
			complete() {
				$('#cart > button').button('reset');
			},
			success(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(() => {
					// $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
};

const wishlist = {
	add(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: `product_id=${product_id}`,
			dataType: 'json',
			success(json) {
				$('.alert').remove();

				if (json.redirect) {
					location = json.redirect;
				}

				if (json.success) {
					$('#content').parent().before(`<div class="alert alert-success"><i class="fa fa-check-circle"></i> ${json.success} <button type="button" class="close" data-dismiss="alert">&times;</button></div>`);
				}

				$('#wishlist-total span').html(json.total);
				$('#wishlist-total').attr('title', json.total);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
	remove() {

	},
};

const compare = {
	add(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: `product_id=${product_id}`,
			dataType: 'json',
			success(json) {
				$('.alert').remove();

				if (json.success) {
					$('#content').parent().before(`<div class="alert alert-success"><i class="fa fa-check-circle"></i> ${json.success} <button type="button" class="close" data-dismiss="alert">&times;</button></div>`);

					$('#compare-total').html(json.total);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
			error(xhr, ajaxOptions, thrownError) {
				alert(`${thrownError}\r\n${xhr.statusText}\r\n${xhr.responseText}`);
			},
		});
	},
	remove() {

	},
};

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
	e.preventDefault();

	$('#modal-agree').remove();

	const element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success(data) {
			html = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += `        <h4 class="modal-title">${$(element).text()}</h4>`;
			html += '      </div>';
			html += `      <div class="modal-body">${data}</div>`;
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		},
	});
});

// Autocomplete */
(function ($) {
	$.fn.autocomplete = function (option) {
		return this.each(function () {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function () {
				this.request();
			});

			// Blur
			$(this).on('blur', function () {
				setTimeout((object) => {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function (event) {
				switch (event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function (event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			};

			// Show
			this.show = function () {
				const pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left,
				});

				$(this).siblings('ul.dropdown-menu').show();
			};

			// Hide
			this.hide = function () {
				$(this).siblings('ul.dropdown-menu').hide();
			};

			// Request
			this.request = function () {
				clearTimeout(this.timer);

				this.timer = setTimeout((object) => {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			};

			// Response
			this.response = function (json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i].value] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i].category) {
							html += `<li data-value="${json[i].value}"><a href="#">${json[i].label}</a></li>`;
						}
					}

					// Get all the ones with a categories
					const category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i].category) {
							if (!category[json[i].category]) {
								category[json[i].category] = new Array();
								category[json[i].category].name = json[i].category;
								category[json[i].category].item = new Array();
							}

							category[json[i].category].item.push(json[i]);
						}
					}

					for (i in category) {
						html += `<li class="dropdown-header">${category[i].name}</li>`;

						for (j = 0; j < category[i].item.length; j++) {
							html += `<li data-value="${category[i].item[j].value}"><a href="#">&nbsp;&nbsp;&nbsp;${category[i].item[j].label}</a></li>`;
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			};

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
		});
	};
}(window.jQuery));
