<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/register.css" rel="stylesheet"/>
<div class="container main-content">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php //} elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-xs-12 col-sm-6 col-md-5 col-lg-4'; ?>
    <?php } ?>
    <div id="content" class="container-form border-full <?php echo $class; ?>"><?php echo $content_top; ?>
      <form autocomplete="off" id="form-registration" name="registration-form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <fieldset id="account">
        <div class="form-group">
            <div class="col-sm-12">
                <h1>Create an Account</h1>
            </div>
            <div class="col-sm-12">
                <label id="label-firstname" class="control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            </div>
            <div class="col-sm-12">
              <input data-parsley-required="true" data-parsley-group="block-0" type="text" name="firstname" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label id="label-lastname" class="col-sm-12 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-12">
              <input data-parsley-required="true" data-parsley-group="block-0" type="text" name="lastname" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label id="label-email" class="col-sm-12 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-12">
              <input data-parsley-type-message="Must be a valid email." data-parsley-type="email" data-parsley-group="block-0" data-parsley-required="true" type="email" name="email" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>

        <fieldset id="password">
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-12">
              <input data-parsley-minlength="4" data-parsley-maxlength="20" data-parsley-minlength-message="Minimum 4 characters." data-parsley-maxlength-message="Maximum 20 characters." data-parsley-required="true" data-parsley-group="block-0" type="password" name="password" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-12">
              <input data-parsley-equalto="#input-password" data-parsley-equalto-message="Passwords must match." data-parsley-required="true" data-parsley-group="block-0" type="password" name="confirm" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
            </div>
          </div>
        </fieldset>

        <fieldset id="newsletter">
            <div class="form-group">
                <input id="c1" class="css-checkbox" data-parsley-required="false" data-parsley-group="block-0" type="checkbox" name="newsletter" checked="checked"/>
                <label class="css-label" for="c1"><span></span><?php echo $entry_newsletter; ?></label>
            </div>
        </fieldset>

        <fieldset id="textagree">
        <div class="form-group">
            <input id="c2" class="css-checkbox" data-parsley-required="true" data-parsley-group="block-0" type="checkbox" name="agree" />
            <label class="css-label" for="c2"><span></span><?php echo $text_agree; ?></label>
        </div>
        </fieldset>

        <fieldset id="textprintShop">
        <div class="form-group">
             <label class="css-label" for="c2"><span></span><?php echo $text_printShop; ?>
                <label class="radio-inline"><input type="radio" name="customer_group_id" value="3">Yes</label>
                <label class="radio-inline"><input type="radio" name="customer_group_id" value="1" checked>No</label>
             </label>
        </div>
        </fieldset>

        <fieldset id="address">
            <div class="form-group">
                <div class="col-sm-12">
                    <h1>Shipping Info</h1>
                </div>
            </div>
            <div class="form-group">
            <label class="col-sm-12 control-label" for="address-autocomplete">Address autocomplete</label>
            <div class="col-sm-12">
                <input type="text" name="address-autocomplete" placeholder="Please enter your address" id="autocomplete" class="form-control" />
            </div>
        </div>
            <div class="form-group">
                <label class="col-sm-12 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
                <div class="col-sm-12">
                    <input data-parsley-minlength-message="Minimum 3 characters." data-parsley-maxlength-message="Maximum 128 characters." data-parsley-minlength="3" data-parsley-maxlength="128" data-parsley-required="true" data-parsley-group="block-1" type="text" name="address_1" placeholder="<?php echo $entry_address_1_desc; ?>" id="input-address-1" class="form-control" />
                </div>
            </div>

        <div class="form-group">
            <label class="col-sm-12 control-label" for="input-address-2"></label>
            <div class="col-sm-12">
                <input data-parsley-group="block-1" type="text" name="address_2" placeholder="<?php echo $entry_address_2_desc; ?>" id="input-address-2" class="form-control" />
            </div>
        </div>
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-city"><?php echo $entry_city; ?></label>
            <div class="col-sm-12">
              <input data-parsley-required="true" data-parsley-group="block-1" type="text" name="city" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
            <div class="col-sm-12">
              <input data-parsley-pattern-message="Must be a valid Zip/Post code." data-parsley-required="true" data-parsley-group="block-1" type="text" name="postcode" data-parsley-pattern="(^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$)" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
            </div>
          </div>
            <div class="form-group">
                <label class="col-sm-12 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                <div class="col-sm-12">
                    <input type="text" data-parsley-minlength-message="Must be a valid phone number." data-parsley-maxlength-message="Must be a valid phone number." data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-minlength="10" data-parsley-maxlength="14" data-parsley-validation-threshold="1" data-parsley-trigger="focusin focusout" data-parsley-group="block-1" name="telephone" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                </div>
            </div>
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-country">Country</label>
            <div class="col-sm-12">
              <select data-parsley-required="true" data-parsley-group="block-1" name="country_id" id="input-country" class="form-control">
                <option></option>
                <option value="38">Canada</option>
                <option value="223">United States</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-12 control-label" for="input-zone">Province or State</label>
            <div class="col-sm-12">
              <select data-parsley-required="true" data-parsley-group="block-1" class="form-control" name="zone_id" id="input-zone">
                <option></option>
                <option class="option-province" value="602">Alberta</option>
                <option class="option-province"  value="603">British Columbia</option>
                <option class="option-province"  value="604">Manitoba</option>
                <option class="option-province"  value="605">New Brunswick</option>
                <option class="option-province"  value="606">Newfoundland and Labrador</option>
                <option class="option-province"  value="607">Northwest Territories</option>
                <option class="option-province"  value="608">Nova Scotia</option>
                <option class="option-province"  value="608">Nunavut</option>
                <option class="option-province"  value="610">Ontario</option>
                <option class="option-province"  value="611">Prince Edward Island</option>
                <option class="option-province"  value="612">Quebec</option>
                <option class="option-province"  value="613">Saskatchewan</option>
                <option class="option-province"  value="614">Yukon</option>
                <option class="option-state" value="3613">Alabama</option>
                <option class="option-state" value="3614">Alaska</option>
                <option class="option-state" value="3616">Arizona</option>
                <option class="option-state" value="3617">Arkansas</option>
                <option class="option-state" value="3624">California</option>
                <option class="option-state" value="3625">Colorado</option>
                <option class="option-state" value="3626">Connecticut</option>
                <option class="option-state" value="3627">Delaware</option>
                <option class="option-state" value="3630">Florida</option>
                <option class="option-state" value="3631">Georgia</option>
                <option class="option-state" value="3633">Hawaii</option>
                <option class="option-state" value="3634">Idaho</option>
                <option class="option-state" value="3635">Illinois</option>
                <option class="option-state" value="3613">Indiana</option>
                <option class="option-state" value="3637">Iowa</option>
                <option class="option-state" value="3638">Kansas</option>
                <option class="option-state" value="3639">Kentucky</option>
                <option class="option-state" value="3640">Louisiana</option>
                <option class="option-state" value="3641">Maine</option>
                <option class="option-state" value="3643">Maryland</option>
                <option class="option-state" value="3644">Massachusetts</option>
                <option class="option-state" value="3645">Michigan</option>
                <option class="option-state" value="3646">Minnesota</option>
                <option class="option-state" value="3647">Mississippi</option>
                <option class="option-state" value="3648">Missouri</option>
                <option class="option-state" value="3649">Montana</option>
                <option class="option-state" value="3650">Nebraska</option>
                <option class="option-state" value="3651">Nevada</option>
                <option class="option-state" value="3652">New Hampshire</option>
                <option class="option-state" value="3653">New Jersey</option>
                <option class="option-state" value="3654">New Mexico</option>
                <option class="option-state" value="3655">New York</option>
                <option class="option-state" value="3656">North Carolina</option>
                <option class="option-state" value="3657">North Dakota</option>
                <option class="option-state" value="3659">Ohio</option>
                <option class="option-state" value="3660">Oklahoma</option>
                <option class="option-state" value="3661">Oregon</option>
                <option class="option-state" value="3663">Pennsylvania</option>
                <option class="option-state" value="3665">Rhode Island</option>
                <option class="option-state" value="3666">South Carolina</option>
                <option class="option-state" value="3667">South Dakota</option>
                <option class="option-state" value="3668">Tennessee</option>
                <option class="option-state" value="3669">Texas</option>
                <option class="option-state" value="3670">Utah</option>
                <option class="option-state" value="3671">Vermont</option>
                <option class="option-state" value="3673">Virginia</option>
                <option class="option-state" value="3674">Washington</option>
                <option class="option-state" value="3675">West Virginia</option>
                <option class="option-state" value="3676">Wisconsin</option>
                <option class="option-state" value="3677">Wyoming</option>
              </select>
            </div>
          </div>
        </fieldset>

        <fieldset id="buttons">
            <div class="col-xs-12">
                <button id="next-section" class="btn btn-primary">Next</button>
                <button id="previous-section" class="btn btn-primary">Previous</button>

                <input id="submitFormButton" type="submit" value="<?php echo $button_continue; ?>" class="hidden btn btn-primary"/>
            </div>
        </fieldset>

        <div class="col-xs-12">
            <input id="captchaFormField" name="captcha" type="hidden" value="" />
        </div>
    </form>
    <div id="review" class="hidden col-sm-12">
        <h1>Review Your Info</h1>
        <div id="serverResponse"></div>
        <div id="captcha"><?php echo $captcha; ?></div>
        <div id="captchaError" class="captcha-required"></div>
        <button id="triggerFormSubmit" class="btn btn-primary">Submit</button>
        <button id="editForm" class="btn btn-primary">Edit</button>
    </div>
    </div>
<?php echo $content_bottom; ?></div>
<?php //echo $column_right; ?></div>
</div>

<script src="catalog/view/javascript/parsley.min.js"></script>
<script>

!function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(root.jQuery);
    }
}(this, function($) {
    'use strict';
	$.fn.typeWatch = function(o) {
		// The default input types that are supported
		var _supportedInputTypes =
			['TEXT', 'TEXTAREA', 'PASSWORD', 'TEL', 'SEARCH', 'URL', 'EMAIL', 'DATETIME', 'DATE', 'MONTH', 'WEEK', 'TIME', 'DATETIME-LOCAL', 'NUMBER', 'RANGE', 'DIV'];

		// Options
		var options = $.extend({
			wait: 750,
			callback: function() { },
			highlight: true,
			captureLength: 2,
			allowSubmit: false,
			inputTypes: _supportedInputTypes
		}, o);

		function checkElement(timer, override) {
			var value = timer.type === 'DIV'
				? jQuery(timer.el).html()
				: jQuery(timer.el).val();

			// If has capture length and has changed value
			// Or override and has capture length or allowSubmit option is true
			// Or capture length is zero and changed value
			if ((value.length >= options.captureLength && value != timer.text)
				|| (override && (value.length >= options.captureLength || options.allowSubmit))
				|| (value.length == 0 && timer.text))
			{
				timer.text = value;
				timer.cb.call(timer.el, value);
			}
		};

		function watchElement(elem) {
			var elementType = (elem.type || elem.nodeName).toUpperCase();
			if (jQuery.inArray(elementType, options.inputTypes) >= 0) {

				// Allocate timer element
				var timer = {
					timer: null,
					text: (elementType === 'DIV') ? jQuery(elem).html() : jQuery(elem).val(),
					cb: options.callback,
					el: elem,
					type: elementType,
					wait: options.wait
				};

				// Set focus action (highlight)
				if (options.highlight && elementType !== 'DIV')
					jQuery(elem).focus(function() { this.select(); });

				// Key watcher / clear and reset the timer
				var startWatch = function(evt) {
					var timerWait = timer.wait;
					var overrideBool = false;
					var evtElementType = elementType;

					// If enter key is pressed and not a TEXTAREA or DIV
					if (typeof evt.keyCode != 'undefined' && evt.keyCode == 13
						&& evtElementType !== 'TEXTAREA' && elementType !== 'DIV')
					{
						console.log('OVERRIDE');
						timerWait = 1;
						overrideBool = true;
					}

					var timerCallbackFx = function() {
						checkElement(timer, overrideBool)
					}

					// Clear timer
					clearTimeout(timer.timer);
					timer.timer = setTimeout(timerCallbackFx, timerWait);
				};

				jQuery(elem).on('keydown paste cut input', startWatch);
			}
		};

		// Watch each element
		return this.each(function() {
			watchElement(this);
		});
	};
});
</script>

<script>

    var geolocation = {};
        geolocation.formFields = {
            address_1 : $('#input-address-1'),
            address_2 : $('#input-address-2'),
            city : $('#input-city'),
            postcode: $('#input-postcode'),
            region : $('#input-zone'),
            country : $('#input-country')
        }
        geolocation.formatData = function (data) {
                                 var result = JSON.parse(data);
                                 var address = result.result.address_components.map(function (currentVal, index, array) {
                                     if (currentVal.types && currentVal.types[0] === "street_number") {
                                         return {name: 'address_1', value: currentVal.long_name};
                                     }
                                     if (currentVal.types && currentVal.types[0] === "route") {
                                         return {name: 'address_1', value: currentVal.long_name};
                                     }
                                     if (currentVal.types && currentVal.types[0] === "locality") {
                                         return {name: 'city', value: currentVal.long_name};
                                     }
                                     if (currentVal.types && currentVal.types[0] === "administrative_area_level_1") {
                                         return {name: 'region', value : currentVal.long_name};
                                     }
                                     if (currentVal.types && currentVal.types[0] === "country") {
                                         return {name: 'country', value : currentVal.long_name};
                                     }
                                     if (currentVal.types && (currentVal.types[0] === "postal_code" || currentVal.types[1] === "postal_code")) {
                                         return {name: 'postcode', value : currentVal.long_name};
                                     }
                                 });
                                 this.populateForm(address);
                                    var tempStr = ''
                                    var tempArray = [];
                                    for (var i = 0; i < address.length; i++) {
                                        if (address[i] !== undefined && address[i].name !== undefined && address[i].name === "address_1") {
                                            tempStr = tempStr + address[i].value + ' ';
                                        }
                                    }
                                    for (var i = 0; i < address.length; i++) {
                                        if (address[i] !== undefined && address[i].name !== undefined && address[i].name === "address_1") {
                                            delete address[i];
                                        }
                                    }
                                    tempArray = address.filter(function(e){ return e === 0 || e });
                                    tempArray.unshift({name: 'address_1', value: tempStr.trim()});
                                    this.populateForm(tempArray);
        }
        geolocation.populateForm = function (data) {
           for (var i = 0; i < data.length; i++) {
               if (data[i] !== undefined && data[i].name !== undefined && data[i].name === "address_1" && data[i].value !== undefined) {
                  this.formFields.address_1.attr('value', data[i].value);
               }
               if (data[i] !== undefined && data[i].name !== undefined && data[i].name === "city" && data[i].value !== undefined) {
                  this.formFields.city.attr('value', data[i].value);
               }
               if (data[i] !== undefined && data[i].name !== undefined && data[i].name === "postcode" && data[i].value !== undefined) {
                  this.formFields.postcode.attr('value', data[i].value);
               }
               if (data[i] !== undefined && data[i].name !== undefined && data[i].name === "country" && data[i].value !== undefined) {
                  $('#input-country option').each(function () {
                      $(this).removeAttr('selected');
                      var optionToFind = this.text.toLowerCase();
                      var optionToMatch = data[i].value.toLowerCase();
                      if (optionToFind === optionToMatch) {
                          $(this).attr('selected', 'selected');
                          $('#input-country').prop('value', $(this).attr('value'));
                          var selectedValue = $(this).attr('value');
                          if (selectedValue == 38) {
                            $('.option-province').css('display', 'block');
                            $('.option-state').css('display', 'none');
                          }

                          if (selectedValue == 223) {
                            $('.option-province').css('display', 'none');
                            $('.option-state').css('display', 'block');
                          }
                      }
                  })
               }
               if (data[i] !== undefined && data[i].name !== undefined && data[i].name === "region" && data[i].value !== undefined) {
                  $('#input-zone option').each(function () {
                      $(this).removeAttr('selected');
                      var optionToFind = this.text.toLowerCase();
                      var optionToMatch = data[i].value.toLowerCase();
                      if (optionToFind === optionToMatch) {
                          $(this).attr('selected', 'selected');
                          $('#input-zone').prop('value', $(this).attr('value'));
                      }
                  })
               }
           }
           $('#form-registration').parsley().validate({group: 'block-1'});
        }
        geolocation.inputElement = $('#autocomplete');
        geolocation.lat = '';
        geolocation.long = '';
        geolocation.findPlaceByID = function (placeID) {
            var url = '<?php echo $action; ?>';
            $.ajax({
                type: 'POST',
                url : '/index.php?route=api/register/reversePlaceIDSearch',
                data: {id: placeID},
                success: function (response) {
                    $('.autocomplete_container').remove();
                    if (response.length > 60 ) {
                        geolocation.formatData(response);
                    }
                    else {
                        alert('Error: could not find location. Please manually enter your information.');
                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        geolocation.displayAutocompleteResults = function (data) {
         $('.autocomplete_container').remove();
         var $div = $("<div>", {class: "autocomplete_container"});
         var $span = $("<span>", {class: "autocomplete_result", value: data.address, text: data.address});
         $span.attr("data-placeID", data.placeID);
         $span.on("click", function(e) {
             geolocation.findPlaceByID(data.placeID);
             $('#autocomplete').val(data.address).change();
             });
         $div.append($span);
         this.inputElement.parent().append($div);
      }
        geolocation.lookUpAddress = function (address) {
            var url = '<?php echo $action; ?>';
            $.ajax({
                type: 'POST',
                url : '/index.php?route=api/register/autocomplete',
                data: address,
                dataType: 'json',
                success: function (response) {
                    geolocation.displayAutocompleteResults(response);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

        var options = {
            callback: function (searchTerm) { geolocation.lookUpAddress(searchTerm); },
            wait: 200,
            allowSubmit: false,
            captureLength: 1
        }

$("#autocomplete").typeWatch( options );


</script>

<script type="text/javascript">


$(function() {

    $('#input-country').on('change', function (e) {
        e.preventDefault();
        selectedCountry = e.currentTarget.value;

        $('#input-zone option').each(function () {
                      $(this).removeAttr('selected');
                  })
        if (selectedCountry == 38) {
            $('.option-state').css('display', 'none');
            $('.option-province').css('display', 'block');
        }
        if (selectedCountry == 223) {
            $('.option-state').css('display', 'block');
            $('.option-province').css('display', 'none');
        }
    });

    function toggleDisplay(element) {

        if (element.hasClass('hidden') === true)
            element.removeClass('hidden');
        else
            element.addClass('hidden');
    }

    function getCountryNameByValue(i) {
        return $('#input-country option[value="' + i + '"]').html();
    }
    function getZoneNameByValue(i) {
        return $('#input-zone option[value="' + i + '"]').html();
    }

    var registrationForm = {};
    registrationForm.dataArray = [];
    registrationForm.currentSection = 0;
    registrationForm.readyState = false;
    registrationForm.sections = {
        account: $('#account'),
        password: $('#password'),
        address: $('#address'),
        newsletter: $('#newsletter'),
        TOS: $('#textagree'),
        printshop : $('#textprintShop'),
        captcha: $('#captcha'),
        review: $('#review')
    }
    registrationForm.labels = function (name) {
        switch(name) {
            case 'firstname':
                return 'First name:';
            case 'lastname':
                return 'Last name:';
            case 'email':
                return 'Email:';
            case 'password':
                return 'Password:';
            case 'address_1':
                return 'Street number and name, P.O. box, c/o:';
            case 'address_2':
                return 'Apartment, suite, unit, building, floor:';
            case 'city':
                return 'City:';
            case 'zone_id':
                return 'Province/State:';
            case 'postcode':
                return 'Postal Code:';
            case 'country_id':
                return 'Country:';
            case 'telephone':
                return 'Telephone';
            case 'fax':
                return 'Fax';
            default: return;
        }
    }
    registrationForm.navbuttons = {
        previous: $('#previous-section'),
        next: $('#next-section'),
        edit: $('#editForm'),
        submit: $('#triggerFormSubmit')
    }
    registrationForm.validateForm = function() {
        return $('#form-registration').parsley().validate();
    }
    registrationForm.validateGroup = function (blockNumber) {
        return $('#form-registration').parsley(
                { errorsWrapper:'<span class="parsley-input-error"></span>',
                errorTemplate: '<span>* Required</span>',
                errorsContainer: function (e) {
                    var el = $(e.element);
                    if (el.hasClass('css-checkbox'))
                        return;
                    var id = el.attr('id');
                    var label = $('label[for="'+ id +'"]');
                    return label;
                }
                }).validate({group: 'block-'+blockNumber});
    };
    registrationForm.setState = function (bool) {
        this.readyState = bool;
    }
    registrationForm.setCurrentSection = function (newSection) {
        this.updateView(this.currentSection, newSection);
        this.currentSection = newSection;
        this.renderReviewView();
    }
    registrationForm.updateView = function(currentSection, newSection) {

        if (newSection === 0) {
            this.sections.address.addClass('hidden');
            this.sections.review.addClass('hidden');
            this.navbuttons.edit.addClass('hidden');
            this.navbuttons.submit.addClass('hidden');
            this.navbuttons.previous.addClass('hidden');

            $('#form-registration').removeClass('hidden');
            this.sections.account.removeClass('hidden');
            this.sections.password.removeClass('hidden');
            this.sections.newsletter.removeClass('hidden');
            this.sections.TOS.removeClass('hidden');
            this.sections.printshop.removeClass('hidden');
            this.sections.captcha.addClass('hidden');
            this.navbuttons.next.removeClass('hidden');
            $('#serverResponse').empty();
        }

        if (newSection === 1) {
            $('#form-registration').removeClass('hidden');
            this.sections.review.addClass('hidden');
            this.navbuttons.edit.addClass('hidden');
            this.navbuttons.submit.addClass('hidden');
            this.sections.account.addClass('hidden');
            this.sections.password.addClass('hidden');
            this.sections.newsletter.addClass('hidden');
            this.sections.TOS.addClass('hidden');
            this.sections.printshop.addClass('hidden');
            this.sections.captcha.addClass('hidden');

            this.sections.address.removeClass('hidden');
            this.navbuttons.next.removeClass('hidden');
            this.navbuttons.previous.removeClass('hidden');
        }
        if (newSection === 2) {
            $('#form-registration').addClass('hidden');
            this.navbuttons.previous.addClass('hidden');
            this.navbuttons.next.addClass('hidden');

            this.sections.review.removeClass('hidden');
            this.navbuttons.edit.removeClass('hidden');
            this.navbuttons.submit.removeClass('hidden');
            this.sections.captcha.removeClass('hidden');
        }
    }

    registrationForm.renderReviewView = function () {

        if (this.currentSection === 2) {

            if ($("#formDataContainer").length > 0) {
                $("#formDataContainer").remove();
            }
            var $formDataContainer = $("<div>", {"class": "col-xs-12", "id": "formDataContainer"});
            var accountDataContainer = $("<div>");
            var addressDataContainer = $("<div>");
            var addressRow = $("<div>");
            var $addressLabel = $("<p>", {"class": "col-xs-4 form-review-heading", text:'Address'});



            accountDataContainer.addClass('review-section');
            addressDataContainer.addClass('review-section review-section-address');
            addressRow.addClass('row review-container-section-address');
            addressRow.append($addressLabel);
            addressDataContainer.append(addressRow);

            this.dataArray = $('#form-registration').serializeArray();

            var fullName = '';
            var reviewData = new Array();
            var reviewAddress = new Array();

            this.dataArray.forEach(function (val, index) {

                if (registrationForm.labels(val.name)) {

                    if (val.name == 'firstname')
                        fullName += val.value;
                    if (val.name == 'lastname')
                        fullName += ' ' + val.value;
                    if (val.name == 'email')
                        reviewData.push({'email' : val.value});
                    if (registrationForm.labels(val.name).match(/password/gi))
                        reviewData.push({'password' : val.value.replace(/./gi, '*')});
                    if (val.name == 'address_1')
                        reviewAddress.push({'name' : 'address_1', 'value': val.value});
                    if (val.name == 'address_2')
                        reviewAddress.push({'name' : 'address_2', 'value': val.value});
                    if (val.name == 'city')
                        reviewAddress.push({'name' : 'city', 'value': val.value});
                    if (val.name == 'zone_id')
                        reviewAddress.push({'name' : 'zone_id', 'value': val.value});
                    if (val.name == 'postcode')
                        reviewAddress.push({'name' : 'postcode', 'value': val.value});
                    if (val.name == 'country_id')
                        reviewAddress.push({'name' : 'country_id', 'value': val.value});
                    if (val.name == 'telephone')
                        reviewAddress.push({'name' : 'telephone', 'value': val.value});
                    // if (val.name == 'fax')
                    //     reviewAddress.push({'name' : 'fax', 'value': val.value});
                }
            });

            reviewData.unshift({'name' : fullName});
            reviewData.push(reviewAddress);
            reviewData.forEach(function (el, index) {

                if (el.name) {
                    var $row = $("<div>", {"class": "row"});
                    var $formEntry = $("<p>", {"class": "col-xs-8 form-review-data", text: el.name});
                    var $formEntryLabel = $("<p>", {"class": "col-xs-4 form-review-heading", text:'Name'});
                    $row.append($formEntryLabel);
                    $row.append($formEntry);
                    accountDataContainer.append($row);
                }
                if (el.email) {
                    var $row = $("<div>", {"class": "row"});
                    var $formEntry = $("<p>", {"class": "col-xs-8 form-review-data", text: el.email});
                    var $formEntryLabel = $("<p>", {"class": "col-xs-4 form-review-heading", text:'Email'});
                    $row.append($formEntryLabel);
                    $row.append($formEntry);
                    accountDataContainer.append($row);
                }
                if (el.password) {
                    var $row = $("<div>", {"class": "row"});
                    var $formEntry = $("<p>", {"class": "col-xs-8 form-review-data", text: el.password});
                    var $formEntryLabel = $("<p>", {"class": "col-xs-4 form-review-heading", text:'Password'});
                    $row.append($formEntryLabel);
                    $row.append($formEntry);
                    accountDataContainer.append($row);
                }
                if (el.length != undefined && el.length > 1) {
                    var sortedArray = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var tempDataArray = el.map(function (key) {
                        if (key.name == 'address_1')
                            sortedArray[0] = key.value;
                        if (key.name == 'address_2')
                            sortedArray[1] = key.value;
                        if (key.name == 'city')
                            sortedArray[2] = key.value;
                        if (key.name == 'zone_id')
                            sortedArray[3] = getZoneNameByValue(key.value);
                        if (key.name == 'postcode')
                            sortedArray[4] = key.value;
                        if (key.name == 'country_id')
                            sortedArray[5] = getCountryNameByValue(key.value);
                        if (key.name == 'telephone')
                            sortedArray[6] = key.value;
                    })
                    sortedArray.forEach(function (key, index) {
                        if (key != 0) {
                            if (index < 6) {
                                var $formEntry = $("<p>", {"class": "col-xs-8 form-review-data", text: key});
                                addressRow.append($formEntry)
                                addressDataContainer.append(addressRow);
                            }
                            if (index == 6) {
                                var $addressRow = $("<div>", {"class": "row"});
                                var $formEntryLabel = $("<p>", {"class": "col-xs-4 form-review-heading", text:'Telephone'});
                                var $formEntry = $("<p>", {"class": "col-xs-8 form-review-data", text: key});
                                $addressRow.append($formEntryLabel);
                                $addressRow.append($formEntry);
                                addressDataContainer.append($addressRow);
                            }
                        }
                    })

                }
            });
            $formDataContainer.append(accountDataContainer);
            $formDataContainer.append(addressDataContainer);
        }
            registrationForm.sections.captcha.before($formDataContainer);

    };


    registrationForm.setCurrentSection(0,0);



    registrationForm.postAJAX = function (formData, url) {

    $.ajax({
            type: 'POST',
            url : url + '/validate',
            data: formData,
            dataType: 'json',
            cache: false,
            processData: false,
            success: function (response) {
                if (response[0] === 'success') {
                    console.log(response)
                    $('#serverResponse').empty();
                    var $successMessage = $("<p>", {"class":"alert alert-success", text: "You have successfully registered. Redirecting to login."});
                    $('#serverResponse').append($successMessage);
                    if (response.redirect != undefined || null || false) {
                        setTimeout(function() {
                            $(location).attr('href', response.redirect);
                        }, 3000);
                    }

                }
                else {
                    grecaptcha.reset();
                    $('#serverResponse').empty();
                    $.each(response, function(key, value) {
                        var $error = $("<p>", {"class":"alert alert-danger", text:value + ' ' + key});
                        $('#serverResponse').append($error);
                    });
                }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

    }

    $('#previous-section').on('click', function (e) {
        e.preventDefault();
        registrationForm.setCurrentSection(registrationForm.currentSection - 1);
    });
    $('#next-section').on('click', function (e) {
        e.preventDefault();
        registrationForm.validateGroup(registrationForm.currentSection);
        if (registrationForm.validateGroup(registrationForm.currentSection) === true)
            registrationForm.setCurrentSection(registrationForm.currentSection + 1);
    });
    $('#editForm').on('click', function (e) {
        e.preventDefault();
        registrationForm.setCurrentSection(0);
    });
    $('#triggerFormSubmit').on('click', function (e) {
        e.preventDefault();
        if(!!$("input[id$='captchaFormField']").val()) {
          registrationForm.postAJAX($('#form-registration').serialize(), '/index.php?route=api/register/validate');
        }
        else {
            $('#captchaError').text("Captcha required.");
        }
    });
});



</script>


<?php echo $footer; ?>
