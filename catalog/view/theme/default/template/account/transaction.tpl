<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/transactions.css" rel="stylesheet"/>
<div class="container">
    <div class="row">
    <div id="content" class="main-content col-xs-12 col-sm-6 col-md-5 col-lg-4">
        <a title="Go back" href="<?php echo $continue; ?>" class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></a>
        <h1><?php echo $heading_title; ?></h1>
        <legend><?php echo $heading_title_info; ?></legend>
        <p><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td class="text-left"><?php echo $column_date_added; ?></td>
                        <td class="text-left"><?php echo $column_description; ?></td>
                        <td class="text-right"><?php echo $column_amount; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($transactions) { ?>
                    <?php foreach ($transactions  as $transaction) { ?>
                    <tr>
                        <td class="text-left"><?php echo $transaction['date_added']; ?></td>
                        <td class="text-left"><?php echo $transaction['description']; ?></td>
                        <td class="text-right"><?php echo $transaction['amount']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                        <td class="text-center" colspan="5"><?php echo $text_empty; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-12"><?php echo $results; ?></div>
            <div class="col-xs-12"><?php echo $pagination; ?></div>
        </div>
        <div class="container-buttons">
            <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
        </div>
    </div>
</div>
</div>
<?php echo $footer; ?>