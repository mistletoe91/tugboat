<?php echo $header; ?>



    <div class="container main-content">
        <div class="col-md-3">

            <?php if($categories) {

             echo $categories;

             } ?>

        </div> <!-- sun menu end -->

        <div class="col-md-9">
                    <div class="row printShopMainContant">
                        <div class="col-md-offset-6 col-md-6  printer-register text-center">
                            <h2>Don't have an account?</h2>
                            <p>Join our community of 8950 printers</p>
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-lg" id="registerName" placeholder="Full Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-lg" id="registerEmail" placeholder="E-mail Address">
                                </div>
                                <button type="submit" class="btn btn-danger btn-lg">Submit</button>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 vendor text-center">
                            <h5>Proud print solutions for the following industry-leading companies:</h5>
                            <img src="image/catalog/SocialProofMin.jpg" class="img-fluid">
                        </div>
                    </div>

        </div>
    </div>


<?php echo $footer; ?>