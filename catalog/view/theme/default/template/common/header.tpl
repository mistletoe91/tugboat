<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/slick/slick.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/slick/slick.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/slick/slick-theme.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/tether/css/tether.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/tether/js/tether.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<?php /*
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
*/ ?>

<link href="//fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,700,900" rel="stylesheet">


<link href="catalog/view/theme/default/stylesheet/common/header.css" rel="stylesheet">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/main.min.css">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

  <style>
    .dropdownPrintShop {
      position: relative;
      display: block;
      padding: 6px 12px;
      border-bottom:1px solid;

    }

    .dropdownPrintShop-content {
      display: none;
      position: absolute;
      float:right;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      padding: 10px;
      z-index: 1;
    }

    .dropdownPrintShop-content p {
      min-width: 150px;
    }

    .dropdownPrintShop:hover .dropdownPrintShop-content {
      display: inline-block;
    }

    .dropdownPrintShoptitile{
      background-color: rgb(0,87,150);
      border-radius: 5px;
      padding: 10px;
      color:#ffffff;
      font-size:1.4em;
      box-shadow: 0px 2px 4px 0px rgba(0,0.6,0,0.4);
      text-transform: uppercase;

    }

    .printShopMainContant{
      height: 450px;
      border-radius: 20px;
      margin: 0 0 1rem 10px;
      width: 100%!important;
      background: url(/image/new-welcome-page-min.jpg);
    }

    .printer-register{
      padding 4%;
      background-color: white;
      border-radius: 20px;
      border: solid #194273 3px;
      width: 48%;
    }



  </style>
</head>
<body class="<?php echo $class; ?> mainbody">

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
  <div id="mainNavContainer" class="container">
    <div id="mainNavRowOne">
      <a class="navbar-brand" href="/">
        <img src="/image/catalog/logo.png" alt="Your Store" class="hidden-xs img-logo">
        <img src="image/catalog/icon_tugboat_logo.svg" class=" visible-xs-inline img-brand">
        <span class="text-logo hidden-xs">TUGBOAT.cc</span>
      </a>
      <ul class="navbar-nav nav-login-cart visible-xs-inline">
        <?php if (!$logged) { ?>
        <li class="nav-item nav-login">
          <a class="nav-link header-login-link" href="<?php echo $login; ?>" aria-expanded="false">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_login_orange.svg">
            <span class="header-text-login">&nbsp;&nbsp;Login</span>
          </a>
        </li>
        <?php } else { ?>
        <li class="nav-item nav-login dropdown">
          <a id="mobileUserDropdownLink" class="nav-link dropdown-toggle text-center" href="#" id="navbarAccountDropdown" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <!-- <p class="header-category-text"><?php echo $customer_name; ?></p> -->
            <i class="fa fa-caret-down"></i>
          </a>
          <div id="mobileUserDropdown" class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a>
            <a class="dropdown-item" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a>
          </div>
        </li>
        <?php } ?>
        <li class="nav-item nav-cart">
          <?php echo $cart; ?>
        </li>
      </ul>
    </div>
    <div id="mainNavRowTwo" class="container collapse navbar-collapse show">
      <ul class="navbar-nav no-gutters">

        <li class="nav-item visible-xs">
          <a class="nav-link link-products" href="index.php?route=common/home#sectionProducts">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_products_outline_orange.svg">

            <p class="header-category-text">Products</p>
            <i class="fa fa-caret-down"></i>
          </a>
          <div id="productDropdown" class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo $landing_banners; ?>">Banners</a>
            <a class="dropdown-item" href="<?php echo $landing_businesscards; ?>">Business Cards</a>
            <a class="dropdown-item" href="<?php echo $landing_flyers; ?>">Flyers</a>
            <a class="dropdown-item" href="<?php echo $landing_postcards; ?>">Postcards</a>
            <a class="dropdown-item" href="<?php echo $landing_posters; ?>">Posters</a>
          </div>
        </li>

        <?php if(($userGroubI == 3) && (!$logged)) { ?>
        <li class="nav-item">
          <a class="nav-link" href="index.php?route=printshop/home">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_enterprise.svg">
            <p class="header-category-text">Print Shop</p>
          </a>
        </li>
        <?php } ?>

        <?php if (!$logged) { ?>
        <li class="nav-item">
          <a class="nav-link link-enterprise" href="index.php?route=common/home#sectionEnterprise">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_enterprise.svg">
            <p class="header-category-text">Enterprise</p>
          </a>
        </li>
        <li class="nav-item hidden-xs">
          <a class="nav-link link-whyus" href="index.php?route=common/home#sectionWhyUs">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_why-us_question-mark.svg">
            <p class="header-category-text">Why Us</p>
          </a>
        </li>
        <?php } ?>
        <?php if ($logged) { ?>
        <li class="nav-item visible-xs">
          <a class="nav-link link-enterprise" href="index.php?route=common/home#sectionEnterprise">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_enterprise.svg">
            <p class="header-category-text">Enterprise</p>
          </a>
        </li>
        <?php } ?>
        <li class="nav-item visible-xs">
          <a class="nav-link link-clients" href="index.php?route=common/home#sectionClients">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_clients_orange.svg">
            <p class="header-category-text">Clients</p>
          </a>
        </li>

        <?php if($userGroubI != 3) { ?>
        <li class="nav-item dropdown visible-sm visible-md visible-lg">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_products_outline_orange.svg">
            <p class="header-category-text">Products</p>
            <i class="fa fa-caret-down"></i>
          </a>
          <div id="productDropdown" class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo $landing_banners; ?>">Banners</a>
            <a class="dropdown-item" href="<?php echo $landing_businesscards; ?>">Business Cards</a>
            <a class="dropdown-item" href="<?php echo $landing_flyers; ?>">Flyers</a>
            <a class="dropdown-item" href="<?php echo $landing_postcards; ?>">Postcards</a>
            <a class="dropdown-item" href="<?php echo $landing_posters; ?>">Posters</a>
          </div>
        </li>
        <?php } ?>


        <li class="nav-item">
          <a class="nav-link link-contact" href="index.php?route=information/contact">
            <img class="img-header-icon" src="image/catalog/contact_envelope_orange.png">
            <p class="header-category-text">Contact Us</p>
          </a>
        </li>
        <?php if (!$logged) { ?>
        <li class="nav-item hidden-xs">
          <a class="nav-link header-login-link" href="<?php echo $login; ?>" aria-expanded="false">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_login_white.svg">
            <span class="header-text-login">
              Login </span>
          </a>
        </li>
        <?php } else { ?>
        <li class="nav-item dropdown hidden-xs">
          <a id="desktopUserDropdownLink" class="nav-link dropdown-toggle" href="#" id="navbarAccountDropdown" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <p class="header-category-text">
              <?php echo $customer_name; ?>
            </p>
            <i class="fa fa-caret-down"></i>
          </a>
          <div id="desktopUserDropdown" class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo $setting; ?>">
              <?php echo $text_setting; ?>
            </a>
            <a class="dropdown-item" href="<?php echo $logout; ?>">
              <?php echo $text_logout; ?>
            </a>
          </div>
        </li>
        <?php } ?>
        <li class="nav-item nav-cart hidden-xs">
          <?php echo $cart; ?>
        </li>
        <!-- <li class="col-sm-2 nav-item hidden-xs">
          <a class="nav-link" href="<?php echo $checkout; ?>" title="Checkout">
            <i class="fa fa-share"></i>
            <span class="hidden-xs hidden-sm hidden-md">
              Checkout </span>
          </a>
        </li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="toppaddingbecauseofnav hidden-xs"></div>
<div class="toppaddingbecauseofnav visible-xs"></div>

<?php if ($categories && 0) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>

<script>

$(function() {
    var isDropdownActive = false;

    $('#toggleDropDownLinks').click(function (event) {
        event.stopPropagation();
        $('#toggleDropDownLinks').toggleClass('active-header-dropdown-parent');
        $('#dropDownLinksToToggle').toggleClass('active-header-dropdown');
        (isDropdownActive) ? isDropdownActive = false : isDropdownActive = true;
    })
    $(document).click( function(){
        if (isDropdownActive) {
            $('#toggleDropDownLinks').toggleClass('active-header-dropdown-parent');
            $('#dropDownLinksToToggle').toggleClass('active-header-dropdown');
            isDropdownActive = false;
        }
    });
});

</script>
