<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/common/homepage.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pretty-checkbox/3.0.0/pretty-checkbox.min.css">
<script src="catalog/view/javascript/anime.min.js" type="text/javascript"></script>
<script type="text/javascript" src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js"></script>
<script src="catalog/view/javascript/homepage.js" type="text/javascript"></script>
<meta name="theme-color" content="#FFFFFF">

<div id="sectionLanding" class="container section text-center">
    <div class="container-section-headline">
        <h1 class="tugboat-blue"><strong>Printing in
            <a id="geo-location">Toronto</a>
        </strong></h1>
        <h3 class="tugboat-orange"><strong>has never been so easy!</strong></h3>
    </div>
    <div class="container-section-content">
        <div class="row no-gutters landing-timeline-img-row">
            <div class="col-xs-4">
                <img class="img-landing" src="image/catalog/step-1-icon.png">
            </div>
            <div class="col-xs-4">
                <img class="img-landing" src="image/catalog/step-2-icon.png">
            </div>
            <div class="col-xs-4">
                <img class="img-landing" src="image/catalog/step-3-icon.png">
            </div>
        </div>
        <div class="row no-gutters landing-timeline-text-row">
            <!-- <div class="col-xs-4">
            <div class="row no-gutters">
                <div class="col-xs-4"></div>
                <div class="col-xs-4 container-line-icon">
                    <span class="line-icon">1</span>
                </div>
                <div class="col-xs-4 landing-horizontal-line"></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="line-icon-text tugboat-blue">Convenient Ordering!</p>
                </div>
            </div> -->
            <div class="col-xs-4">
                <div class="row no-gutters">
                    <div class="container-line-icon">
                        <div class="line-icon">1</div>
                        <span class="landing-horizontal-line"></span>
                    </div>
                </div>
                <div class="row no-gutters">
                    <p class="line-icon-text tugboat-blue"><strong>Convenient <br/>Ordering!</strong></p>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row no-gutters">
                    <div class="container-line-icon">
                        <div class="line-icon">2</div>
                        <span class="landing-horizontal-line"></span>
                    </div>
                </div>
                <div class="row no-gutters">
                    <p id="local-fulfillment" class="line-icon-text tugboat-blue"><strong>Local <br/>Fulfillment!</strong></p>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row no-gutters">
                    <div class="container-line-icon">
                        <div class="line-icon">3</div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <p class="line-icon-text tugboat-blue"><strong>Right Time <br/>Delivery!</strong></p>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <img class="img-money-back" src="image/catalog/icon_money_back.svg">
    </div>
    </div>
    <div class="container-section-btn">
        <a class="btn btn-start link-products" href="index.php?route=common/home#sectionProducts">Get Started</a>
    </div>
</div>

<div id="sectionProducts" class="container-fluid section">
    <div class="container-section-header">
        <span class="products-line background-tugboat-blue"></span>
        <div id="container-header-products" class="section-header">
            <div id="header-products" class="container-background"></div>
            <img class="section-header-icon" src="image/catalog/tugboat_icons_products_outline_new.svg">
        </div>
        <span class="products-line background-tugboat-blue"></span>
    </div>
    <div class="container-section-headline text-center visible-xs">
        <h3>Products</h3>
    </div>
    <div class="container-section-headline text-center hidden-xs">
        <h1>Products</h1>
    </div>
    <div class="container-section-content text-center">
        <div class="col-xs-6 col-sm-2">
            <a id="containerAnimated2" class="product-link" href="index.php?route=productlanding/flyers">
                <div id="animated2" class="product-background"></div>
                <div class="container-product-icon">
                    <img class="product-icon" src="image/catalog/icon_flyers.svg">
                    <p class="product-icon-text">Flyers</p>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-2">
            <a id="containerAnimated3" class="product-link" href="index.php?route=productlanding/posters">
                <div id="animated3" class="product-background"></div>
                <div class="container-product-icon">
                    <img class="product-icon" id="iconPosters" src="image/catalog/icon_posters.svg">
                    <p class="product-icon-text">Posters</p>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-2">
            <a id="containerAnimated4" class="product-link" href="index.php?route=productlanding/banners">
                <div id="animated4" class="product-background"></div>
                <div class="container-product-icon">
                    <img class="product-icon" id="iconBanners" src="image/catalog/icon_banners.svg">
                    <p class="product-icon-text">Banners</p>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-2">
            <a id="containerAnimated5" class="product-link" href="index.php?route=productlanding/postcards">
                <div id="animated5" class="product-background"></div>
                <div class="container-product-icon">
                    <img class="product-icon" src="image/catalog/icon_postcards.svg">
                    <p class="product-icon-text">Postcards</p>
                </div>

            </a>
        </div>
        <div class="col-xs-12 col-sm-2">
            <a id="containerAnimated6" class="product-link" href="index.php?route=productlanding/businesscards">
                <div id="animated6" class="product-background"></div>
                <div class="container-product-icon">
                    <img class="product-icon" src="image/catalog/icon_business_cards.svg">
                    <p class="product-icon-text">Business <br/>Cards</p>
                </div>
            </a>
        </div>
    </div>
    <!-- <div class="container-section-content text-center hidden-xs">
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                    <a href="index.php?route=productlanding/flyers">
                        <img src="image/catalog/tugboat_icons_flyers.svg">
                        <div class="caption">
                            <h3>Flyers</h3>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id
                            dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <a href="index.php?route=productlanding/posters">
                        <img src="image/catalog/tugboat_icons_posters.svg">
                        <div class="caption">
                            <h3>Posters</h3>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.
                                Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <a href="index.php?route=productlanding/banners">
                        <img src="image/catalog/tugboat_icons_banners.svg">
                        <div class="caption">
                            <h3>Banners</h3>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.
                                Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="thumbnail">
                    <a href="index.php?route=productlanding/postcards">
                        <img src="image/catalog/tugboat_icons_postcards.svg">
                        <div class="caption">
                            <h3>Postcards</h3>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.
                                Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="thumbnail">
                    <a href="index.php?route=productlanding/businesscards">
                        <img src="image/catalog/tugboat_icons_business-cards.svg">
                        <div class="caption">
                            <h3>Business Cards</h3>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.
                                Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container-section-btn text-center">
        <!-- <a class="btn btn-start" href="index.php?route=information/contact">Anything else?</a> -->
        <a class="btn btn-start" href="#" data-toggle="modal" data-target="#productInquiryModal">Anything else?</a>
    </div>
</div>

<div id="sectionWhyUs" class="container-fluid section section-alternate hidden-xs">
    <div class="container-section-header">
        <div id="container-header-whyus" class="section-header">
            <div id="header-whyus" class="container-background"></div>
            <img class="section-header-icon" src="image/catalog/WhyUsIconNew.png">
        </div>
    </div>
    <div class="container-section-headline text-center">
        <h1>Why Us?</h1>
        <h3>There are plenty of reasons to choose Tugboat.</h3>
        <h3>Here are a few:</h3>
    </div>
    <div class="container-section-content">
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon1.png">
                <h5>Super Convenient!</h5>
                <p>Tugboat is the
                    <strong>only</strong> platform that lets you design and order from your neighbourhood print shop at the click
                    of a button.</p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon2.png">
                <h5>Hyper Local!</h5>
                <p>Using Tugboat's platform, you encourage businesses in your community. We believe in promoting local community
                    commerce.
                </p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon3.png">
                <h5>Better Pricing!</h5>
                <p>Tugboat is cheaper than all our online competition. Don't believe us? Feel free to compare our prices
                    with Vistaprint, Moo, etc.</p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon4.png">
                <h5>Timed Delivery!</h5>
                <p>Tugboat delivers products at a time that suits
                    <strong>you.</strong> No more running to the post office! Or you can pick-up your stuff for
                    <strong>free!</strong>
                </p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon5.png">
                <h5>Reduced Carbon</h5>
                <p>Tugboat's delivery will come from a printer that is at most 3KM away. Our competition's far-flung shipping
                    methods have a much larger carbon footprint!</p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon6.png">
                <h5>Serious Emergency?</h5>
                <p>Need something urgently for an important event in the evening? Give us a shout! We are the only online
                    shop that can help you.</p>
            </div>
        </span>
        <span class="content-box" href="#">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon7.png">
                <h5>International Network</h5>
                <p>Over 95% of print shops are independents. Tugboat’s independent print shop network is the biggest in
                    the world.
                </p>
            </div>
        </span>
        <span class="content-box">
            <div class="content-box-items text-center">
                <img class="" src="image/catalog/TBwhyUsIcon8.png">
                <h5>Convinced?</h5>
                <br>
                <br>
                <br>
                <a class="btn btn-start link-products" href="index.php?route=common/home#sectionProducts">Buy something!</a>
            </div>
        </span>
    </div>
</div>

<div class="flex-ordering">
    <div id="sectionEnterprise" class="container-fluid section section-alternate">
        <div class="container-section-header">
            <span class="products-line background-tugboat-blue visible-xs"></span>
            <div id="container-header-enterprise" class="section-header">
                <div id="header-enterprise" class="container-background"></div>
                <img class="section-header-icon" src="image/catalog/icon_header_enterprise.png">
            </div>
            <span class="products-line background-tugboat-blue visible-xs"></span>
        </div>
        <div class="container-section-headline text-center visible-xs">
            <h3>Enterprise</h3>
            <h4>Tugboat's platform is ideal for <br/>clients that want to:</h4>
        </div>
        <div class="container-section-headline text-center hidden-xs">
            <h1>Enterprise</h1>
            <h3>Tugboat's platform is ideal for clients that want to:</h3>
        </div>
        <div class="container-section-content">
            <div class="enterprise-grid container text-center visible-xs">
                <div class="row">
                    <div class="enterprise-grid-column col-xs-6">
                        <img src="image/catalog/money_back_enterprise_lg_transparent.png" alt="printing near me">
                        <p>Maintain a High Level <br/> of Quality Control</p>
                    </div>
                    <div class="enterprise-grid-column col-xs-6" id="automatePrintCol">
                        <img id="automatePrint" src="image/catalog/automate_print_enterprise_lg_transparent.png" alt="printing near me">
                        <p>Automate Print <br/>Coordination</p>
                    </div>
                    <div class="enterprise-grid-column col-xs-6">
                        <img src="image/catalog/local_vendors_enterprise_lg_transparent.png" alt="printing near me">
                        <p>Encourage Local <br/>Vendors</p>
                    </div>
                    <div class="enterprise-grid-column col-xs-6">
                        <img src="image/catalog/save_money_enterprise_lg_transparent.png" alt="printing near me">
                        <p>Save Money</p>
                    </div>
                </div>
            </div>
            <div class="enterprise-desktop hidden-xs">
                <div class="container enterprise-call-to-action text-center">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="image/catalog/money_back_enterprise_lg_transparent.png" alt="printing near me">
                            <h5>Maintain a high level of quality control</h5>
                        </div>
                        <div class="col-xs-3">
                            <img src="image/catalog/automate_print_enterprise_lg_transparent.png" alt="printing near me">
                            <h5>Automate Print Coordination</h5>
                        </div>
                        <div class="col-xs-3">
                            <img src="image/catalog/local_vendors_enterprise_lg_transparent.png" alt="printing near me">
                            <h5>Encourage local vendors</h5>
                        </div>
                        <div class="col-xs-3">
                            <img src="image/catalog/save_money_enterprise_lg_transparent.png" alt="printing near me">
                            <h5>Save money</h5>
                        </div>
                    </div>
                </div>
                <div class="container ent-container">
                    <div class="row no-gutters text-center">
                        <div class="col-xs-12">
                            <nav class="navbar navbar-default ent-nav">
                                <a class="navbar-brand" href="/">
                                    <img src="/image/catalog/logo.png" alt="Tugboat" class="img-logo">
                                </a>
                                <div class="collapse navbar-collapse" id="entNav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <p class="header-category-text">$CAD&nbsp;&nbsp;</p>
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <i class="fa fa-shopping-cart"></i>
                                                <p class="header-category-text">Cart</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <p class="header-category-text">Leslie - Marketing Manager&nbsp;&nbsp;</p>
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-xs-12 ent-content" id="panelOne">
                            <div class="row ent-header">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8 text-center">
                                    <img src="image/catalog/AppLabb_Icon.png" alt="print near me" width="60px">
                                    <span class="h3"> Welcome,
                                        <span class="tugboat-blue ent-link">Lesile - Marketing Manager</span>
                                    </span>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="row no-gutters ent-body">
                                <div class="col-md-1 col-lg-2"></div>
                                <div class="col-xs-12 col-md-10 col-lg-8 ent-body-content">
                                    <div class="row ent-categories-row text-center">
                                        <div class="col-xs-3 ent-category">MANAGE TEMPLATES</div>
                                        <div class="col-xs-3 ent-category">MANAGE ACCOUNTS</div>
                                        <div class="col-xs-3 ent-category">ORDER HISTORY</div>
                                        <div class="col-xs-3 ent-category">BUY SOMETHING</div>
                                    </div>
                                    <div class="row ent-app-row">
                                        <div class="col-xs-6 ent-account-manager">
                                            <h3>Your Account Manager</h3>
                                            <br>
                                            <h4>MARY READ</h4>
                                            <p>
                                                <i class="fa fa-envelope tugboat-blue"></i> Contact Mary Read</p>
                                        </div>
                                        <div class="col-xs-6 ent-postit">
                                            <h3>Post It</h3>
                                            <br>
                                            <h4>Make Notes Here</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-lg-2"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 ent-content hidden" id="panelTwo">
                            <div class="row ent-header">
                                <div class="col-xs-3"></div>
                                <div class="col-xs-6 input-group">
                                    <span class="input-group-addon tugboat-blue">
                                        <i class="fa fa-envelope"></i> SEND INVITATION TO: </span>
                                    <input type="text" class="form-control" value="josh@theapplabb.com">
                                </div>
                                <div class="col-xs-3"></div>
                            </div>
                            <div class="row no-gutters ent-body">
                                <div class="col-md-1 col-lg-2"></div>
                                <div class="col-xs-12 col-md-10 col-lg-8 ent-body-content">
                                    <div class="row ent-categories-row text-center">
                                        <div class="col-xs-4 ent-category">PRODUCT</div>
                                        <div class="col-xs-4 ent-category">DETAILS</div>
                                        <div class="col-xs-4 ent-category">EDITING OPTIONS</div>
                                    </div>
                                    <div class="row ent-app-row">
                                        <div class="col-xs-4">
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="pretty p-default">
                                                        <input type="checkbox">
                                                        <div class="state p-info">
                                                            <label>BUSINESS CARD</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="pretty p-default">
                                                        <input type="checkbox">
                                                        <div class="state p-info">
                                                            <label>FLYERS</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="pretty p-default">
                                                        <input type="checkbox">
                                                        <div class="state p-info">
                                                            <label>STICKERS</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="pretty p-default">
                                                        <input type="checkbox">
                                                        <div class="state p-info">
                                                            <label>BINDERS</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="pretty p-default">
                                                        <input type="checkbox">
                                                        <div class="state p-info">
                                                            <label>LETTERHEAD</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon">PAPER&nbsp;&nbsp;
                                                            <i class="fa fa-plus tugboat-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon">COATING&nbsp;&nbsp;
                                                            <i class="fa fa-plus tugboat-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon">SIZE&nbsp;&nbsp;
                                                            <i class="fa fa-plus tugboat-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon">QUANTITY&nbsp;&nbsp;
                                                            <i class="fa fa-plus tugboat-blue"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="ent-product-option-content">
                                                        <div class="pretty p-default">
                                                            <input type="checkbox">
                                                            <div class="state p-info">
                                                                <label>STANDARD</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                            <div class="ent-horizontal-divider"></div>
                                            <div class="row ent-product-option-row no-gutters">
                                                <div class="col-xs-3"></div>
                                                <div class="col-xs-6">
                                                    <div class="ent-product-option-content">
                                                        <div class="pretty p-default">
                                                            <input type="checkbox">
                                                            <div class="state p-info">
                                                                <label>CUSTOM</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-lg-2"></div>
                            </div>
                            <div class="row ent-bottom-btn text-center">
                                <button class="btn btn-start background-tugboat-blue">SEND</button>
                            </div>
                        </div>
                        <div class="col-xs-12 ent-content hidden" id="panelThree">
                            <div class="row ent-header">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8 text-center">
                                    <img src="image/catalog/AppLabb_Icon.png" alt="print near me" width="60px">
                                    <span class="h3"> Welcome,
                                        <span class="tugboat-blue ent-link">Josh - Business Development Manager</span>
                                    </span>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="row no-gutters ent-body">
                                <div class="col-md-1 col-lg-2"></div>
                                <div class="col-xs-12 col-md-10 col-lg-8 ent-body-content">
                                    <div class="row ent-menu-row text-center">
                                        <div class="col-xs-4 ent-menu-category">RECENT TEMPLATES&nbsp;&nbsp;
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                        <div class="col-xs-5 ent-menu-category">CATEGORY: BUSINESS CARDS&nbsp;&nbsp;
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                        <div class="col-xs-2"></div>
                                        <div class="col-xs-1"></div>
                                    </div>
                                    <div class="row ent-app-row">
                                        <div class="col-xs-2 ent-side-menu">
                                            <i class="fa fa-edit fa-2x tugboat-blue"></i>
                                            <span> EDIT</span>
                                        </div>
                                        <div class="col-xs-10 text-center">
                                            <img src="image/catalog/enterprise.png" width="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-lg-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters ent-footer text-center">
                        <div class="ent-footer-brand">
                            © TUGBOAT 2017
                        </div>
                        <div class="ent-footer-link">
                            <a href="#">FONTS</a>
                        </div>
                        <div class="ent-footer-link">
                            <a href="#">TERMS AND CONDITIONS</a>
                        </div>
                        <div class="ent-footer-link">
                            <a href="#">PRIVACY POLICY</a>
                        </div>
                        <div class="ent-footer-link">
                            <a href="#">SITEMAP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-pagination hidden-xs">
            <nav class="text-center" aria-label="..." id="pagination">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="prev page-link">
                            &laquo;
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="next page-link">
                            &raquo;
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="container-section-btn text-center">
            <a class="btn btn-start" href="#" data-toggle="modal" data-target="#businessInquiryModal">Schedule a Demo</a>
        </div>
    </div>

    <div id="sectionClients" class="container-fluid section">
        <div class="container-section-header">
            <span class="products-line background-tugboat-blue visible-xs"></span>
            <div id="container-header-clients" class="section-header">
                <div id="header-clients" class="container-background"></div>
                <img class="section-header-icon" src="image/catalog/icon_header_clients.png">
            </div>
            <span class="products-line background-tugboat-blue visible-xs"></span>
        </div>
        <div class="container-section-headline text-center hidden-xs">
            <h1>Clients</h1>
            <h3>Tugboat has many satisfied clients.</h3>
            <h3>Check them out!</h3>
        </div>
        <div class="container-section-headline text-center visible-xs">
            <h3>Clients</h3>
            <h4>Our clients love us because we are:</h4>
        </div>
        <div class="container-section-content text-center">
            <div class="whyus-grid container visible-xs">
                <div class="whyus-grid-row row no-gutters">
                    <div class="whyus-grid-col col-xs-3">
                        <img src="image/catalog/icon_hyper_local.png">
                        <p>Hyper <br/>Local!</p>
                    </div>
                    <div class="whyus-grid-col col-xs-3">
                        <img src="image/catalog/icon_better_priced.png">
                        <p>Better <br/>Priced!</p>
                    </div>
                    <div class="whyus-grid-col col-xs-3">
                        <img src="image/catalog/icon_globally_available.png">
                        <p>Globally <br/>Available</p>
                    </div>
                    <div class="whyus-grid-col col-xs-3">
                        <img src="image/catalog/icon_super_convenient.png">
                        <p>Super <br/>Convenient!</p>
                    </div>
                </div>
            </div>
            <!-- <div class="clients-grid container">
                <div class="row no-gutters text-center">
                    <div class="col-xs-6">
                        <img class="img-client" src="image/catalog/signature-logo.jpg">
                    </div>
                    <div class="col-xs-6">
                        <img class="img-client" src="image/catalog/fintros_full-logo.png">
                    </div>
                    <div class="col-xs-6">
                        <img class="img-client" src="image/catalog/aircare-logo-sm-grey.png">
                    </div>
                    <div class="col-xs-6">
                        <img class="img-client" src="image/catalog/jenie.png">
                    </div>
                    <div class="col-xs-12">
                        <img class="img-client" src="image/catalog/app_lab.jpg">
                    </div>
                </div>
            </div> -->
            <div class="container-section-headline visible-xs">
                <h4>Check out some of Tugboat's clients!</h4>
            </div>
            <div class="clients-carousel">
                <div>
                    <!-- <a href="http://www.altusgroup.com/">
                        <img class="img-client" src="image/catalog/altus_group_icon.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/altus_group_icon.png">
                </div>
                <div>
                    <!-- <a href="https://signatureretirementliving.com/">
                        <img class="img-client" src="image/catalog/signature-logo.jpg">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/signature-logo.jpg">
                </div>
                <div>
                    <!-- <a href="https://www.fintros.com/">
                        <img class="img-client" src="image/catalog/fintros_full-logo.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/fintros_full-logo.png">
                </div>
                <div>
                    <!-- <a href="https://play.google.com/store/apps/details?id=com.applabb.Giftjeenie">
                        <img class="img-client" src="image/catalog/jenie.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/jenie.png">
                </div>
                <div>
                    <!-- <a href="http://www.healflow.ca/">
                        <img class="img-client" src="image/catalog/healflow_icon.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/healflow_icon.png">
                </div>
                <div>
                    <!-- <a href="https://starkbim.com/">
                        <img class="img-client" src="image/catalog/starkbim_icon.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/starkbim_icon.png">
                </div>
                <div>
                    <!-- <a href="https://signatureretirementliving.com/">
                        <img class="img-client" src="image/catalog/healthsimple_icon.svg">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/healthsimple_icon.svg">
                </div>
                <div>
                    <!-- <a href="https://signatureretirementliving.com/">
                        <img class="img-client" src="image/catalog/orthoplus_icon.gif">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/orthoplus_icon.gif">
                </div>
                <div>
                    <!-- <a href="https://signatureretirementliving.com/">
                        <img class="img-client" src="image/catalog/workhaus_icon.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/workhaus_icon.png">
                </div>
                <div>
                    <!-- <a href="https://signatureretirementliving.com/">
                        <img class="img-client" src="image/catalog/infounders_logo.png">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/infounders_logo.png">
                </div>
                <div>
                    <!-- <a href="https://zighra.com">
                        <img class="img-client" src="image/catalog/zighra_icon.png">
                    </a> -->
                    <img class="img-client" src="image/catalog/zighra_icon.png">
                </div>
                <div>
                    <!-- <a href="http://theapplabb.com/">
                        <img class="img-client" src="image/catalog/app_lab.jpg">
                    </a> -->
                    <img class="img-client" data-lazy="image/catalog/app_lab.jpg">
                </div>
            </div>
        </div>
        <div class="container-section-btn text-center">
            <a class="btn btn-start link-products" href="index.php?route=common/home#sectionProducts">Join them!</a>
        </div>
    </div>
</div>

<div class="modal fade" id="productInquiryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog four-col">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-xs-3" for="email">Email:</label>
                    <div class="col-xs-9">
                        <input type="email" class="form-control" id="productEmail" placeholder="Enter email" name="email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="product">Product:</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="productProduct" placeholder="Enter product" name="product">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="quantity">Quantity:</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="productQuantity" placeholder="Enter quantity" name="quantity">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="finish">Finish:</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="productFinish" placeholder="Enter finish" name="finish">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="size">Size:</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="productSize" placeholder="Enter size" name="size">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-12" for="comment">Comment:</label>
                    <div class="col-xs-12">
                        <textarea class="form-control" name="comment" id="productComment" rows="4" cols="60"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-xs-9">
                        <button type="submit" class="btn btn-primary" id="sendProductEmail">Submit</button>
                        <button type="button" class="btn btn-secondary" id="productClose" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="businessInquiryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog four-col">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-xs-3 col-sm-3" for="name">Name:</label>
                    <div class="col-xs-12 col-sm-9">
                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3 col-sm-3" for="email">Email:</label>
                    <div class="col-xs-12 col-sm-9">
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3 col-sm-3" for="bus-name">Business name:</label>
                    <div class="col-xs-12 col-sm-9">
                        <input type="text" class="form-control" id="bus-name" placeholder="Enter business name" name="bus-name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3 col-sm-3" for="position">Position:</label>
                    <div class="col-xs-12 col-sm-9">
                        <input type="text" class="form-control" id="position" placeholder="Enter position" name="position">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3" for="position">Phone:</label>
                    <div class="col-xs-8 col-sm-7">
                        <input type="text" class="form-control" id="phone" placeholder="Enter phone" name="phone">
                    </div>
                    <div class="col-xs-4 col-sm-2">
                        <input type="text" class="form-control" id="ext" placeholder="ext" name="ext">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-xs-9">
                        <button type="submit" class="btn btn-primary" id="sendBusinessEmail">Submit</button>
                        <button type="button" class="btn btn-secondary" id="businessClose" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    const disableProductDropdown = () => {
        const productDropdownCaretElement = document.querySelector('#navbarDropdownMenuLink i');
        const productDropdownElement = document.getElementById('productDropdown');
        let isHidden = window.innerWidth < 768 ? true : false;
        if (isHidden) {
            productDropdownCaretElement.classList.add('hidden');
            productDropdownElement.classList.add('hidden');
        }
        window.addEventListener('resize', (ev) => {
            isHidden = productDropdownElement.classList.contains('hidden');
            if (window.innerWidth < 768){
                if (isHidden === false) {
                    productDropdownCaretElement.classList.add('hidden');
                    productDropdownElement.classList.add('hidden');
                }
            }
            else {
                productDropdownCaretElement.classList.remove('hidden');
                productDropdownElement.classList.remove('hidden');
            }
        });
    };
    $('#sendProductEmail').click((e) => {
        e.preventDefault();
        const userInput = {
            email: document.getElementById('productEmail').value,
            product: document.getElementById('productProduct').value,
            quantity: document.getElementById('productQuantity').value,
            finish: document.getElementById('productFinish').value,
            size: document.getElementById('productSize').value,
            comment: document.getElementById('productComment').value   
        };
        $.post("index.php?route=common/home/productInquiry", userInput, (result) => {
            $('#productClose').click();
        });
    });

    $('#sendBusinessEmail').click((e) => {
        e.preventDefault();
        const userInput = {
            name: document.getElementById('name').value,
            email: document.getElementById('email').value,
            businessName: document.getElementById('bus-name').value,
            position: document.getElementById('position').value,
            phone: document.getElementById('phone').value,
            ext: document.getElementById('ext').value
        }
        $.post("index.php?route=common/home/businessInquiry", userInput, (result) => {
            $('#businessClose').click();
        });
    });

    const hideFooterOnScroll = () => {
        const footerElement = document.getElementsByTagName('footer')[0];
        if (window.innerWidth < 768 && window.scrollY === 0) {
            footerElement.style.position = 'fixed';
        }
        window.addEventListener('scroll', (e) => {
            if (window.innerWidth < 768 && window.scrollY === 0) {
                footerElement.style.position = 'fixed';
            } else {
                footerElement.style.position = 'relative';
            }
        });
    };

    $(document).ready(() => {
        hideFooterOnScroll();
        disableProductDropdown();
    });

    $('#pagination ul li .prev').click(function () {

        const pOne = document.getElementById('panelOne');
        const pTwo = document.getElementById('panelTwo');
        const pThere = document.getElementById('panelThree');
        var panelOne = pOne.getAttribute("class");
        var panelTwo = pTwo.getAttribute("class");
        var panelThree = pThere.getAttribute("class");

        let panel1 = panelOne.match(/hidden/g);
        let panel2 = panelTwo.match(/hidden/g);
        let panel3 = panelThree.match(/hidden/g);

        if (!panel1) {
        } else if ((panel1) && (!panel2) && (panel3)) {
            pOne.setAttribute('class', 'col-xs-12 ent-content');
            pTwo.setAttribute('class', 'col-xs-12 ent-content hidden');
        } else if ((panel1) && (panel2) && (!panel3)) {
            pTwo.setAttribute('class', 'col-xs-12 ent-content');
            pThere.setAttribute('class', 'col-xs-12 ent-content hidden');
        }

    });

    $('#pagination ul li .next').click(function () {
        const pOne = document.getElementById('panelOne');
        const pTwo = document.getElementById('panelTwo');
        const pThere = document.getElementById('panelThree');

        var panelOne = pOne.getAttribute("class");
        var panelTwo = pTwo.getAttribute("class");
        var panelThree = pThere.getAttribute("class");

        let panel1 = panelOne.match(/hidden/g);
        let panel2 = panelTwo.match(/hidden/g);
        let panel3 = panelThree.match(/hidden/g);

        if (!panel1) {
            pOne.setAttribute('class', 'col-xs-12 ent-content hidden');
            pTwo.setAttribute('class', 'col-xs-12 ent-content');
        } else if ((!panel1) && (panel2) && (panel3)) {
            pOne.setAttribute('class', 'col col-xs-12 ent-content hidden');
            pTwo.setAttribute('class', 'col col-xs-12 ent-content');
        } else if ((panel1) && (!panel2) && (panel3)) {
            pTwo.setAttribute('class', 'col-xs-12 ent-content hidden');
            pThere.setAttribute('class', 'col-xs-12 ent-content');
        }
    });

    $(function () {
        var timelineBusinesscards = anime.timeline({ autoplay: false, loop: false });
        timelineBusinesscards.add({
            targets: '#animated6',
            duration: 1000,
            rotate: 45,
        });
        var timelinePostcards = anime.timeline({ autoplay: false, loop: false });
        timelinePostcards.add({
            targets: '#animated5',
            duration: 1000,
            rotate: 45,
        });
        var timelineBanners = anime.timeline({ autoplay: false, loop: false });
        timelineBanners.add({
            targets: '#animated4',
            duration: 1000,
            rotate: 45,
        });
        var timelinePosters = anime.timeline({ autoplay: false, loop: false });
        timelinePosters.add({
            targets: '#animated3',
            duration: 1000,
            rotate: 45,
        });
        var timelineFlyers = anime.timeline({ autoplay: false, loop: false });
        timelineFlyers.add({
            targets: '#animated2',
            duration: 1000,
            rotate: 45,
        });
        var timelineProductsHeader4 = anime.timeline({ autoplay: false, loop: false });
        timelineProductsHeader4.add({
            targets: '#header-enterprise',
            duration: 1000,
            rotate: 45,
        });
        var timelineProductsHeader3 = anime.timeline({ autoplay: false, loop: false });
        timelineProductsHeader3.add({
            targets: '#header-clients',
            duration: 1000,
            rotate: 45,
        });
        var timelineProductsHeader2 = anime.timeline({ autoplay: false, loop: false });
        timelineProductsHeader2.add({
            targets: '#header-whyus',
            duration: 1000,
            rotate: 45,
        });
        var timelineProductsHeader1 = anime.timeline({ autoplay: false, loop: false });
        timelineProductsHeader1.add({
            targets: '#header-products',
            duration: 1000,
            rotate: 45,
        });
        addAnimationEvent('#containerAnimated6', timelineBusinesscards);
        addAnimationEvent('#containerAnimated5', timelinePostcards);
        addAnimationEvent('#containerAnimated4', timelineBanners);
        addAnimationEvent('#containerAnimated3', timelinePosters);
        addAnimationEvent('#containerAnimated2', timelineFlyers);
        addAnimationEvent('#container-header-enterprise', timelineProductsHeader4);
        addAnimationEvent('#container-header-clients', timelineProductsHeader3);
        addAnimationEvent('#container-header-whyus', timelineProductsHeader2);
        addAnimationEvent('#container-header-products', timelineProductsHeader1);


        function addAnimationEvent(target, timeline) {
            $(target).mouseenter(function () {
                timeline.restart();
            }).mouseleave(function () {
                timeline.reverse();
            });
        }

    });
</script>

<?php echo $footer; ?>