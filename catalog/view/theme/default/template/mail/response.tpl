<!doctype html>
<html>

<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title></title>
  <?php echo $mail_style ?>
</head>

<body>
  <table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td>&nbsp;</td>
      <td class="container">
        <div class="content">
          <span class="preheader">Tugboat has recieved your inquiry!</span>
          <table class="main">
            <tr>
              <td class="brand-header" style="background-color: #F58B47; height: 50px;">
                <a href="https://tugboat.cc" style="display: flex; width: 100%; height: 100%">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALsAAAAgCAYAAABKMQnqAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4gIJEQ4gKJnIwgAAB/FJREFUeNrtnH+wlUUZxz/PvfweQAWH3yFEZJmShlOggDqEWkMKoqOhRDHKj0hLkBqmqUknLJWJ5JJ4mRwidaJBMDWMickKJCwDCgfSRKTSIEBSBIYLwrc/zvPWuvOec95zvZf7o/c7c+a+u/vdfZ+z++w+zz6750IGSLpG78b3yZGjhaEqI+/0KH1a3nU5Wquy58jxf6Psb0Xpg3nX5ciRI0eOHDmaFhZnSOoFXFWPtp43sy15l+ZoMZA0VfXDo3nv5WjOaJNltW/CiXclcGEFVdaa2e+D+pcCn/Dkm2ZWW+Q9M4CuntxoZuvKyDUAuAEYAfQCjgOvAc8Ay83s3yXq9gEmFSmuA/4GrDez/RX00zBgPPAxoBtwBNgJrAFWmtmxCvt9LPCRJDhhZg8W4XUDvlhB0wfNbGFzWtmnNZeVXdIDFcowO6p/T1C2o8R7dgW8eSV4bSXdJ6muhAxvSZojyYq0MTzD9zgqab6k6jL901vS02Xa2iXp0xX0eUdJ+6M2hhbhfrDC8Xmtua3szwNLorzhwHlBeh/wM0BB3jOt3L3rAPwcGF2G2hW4F9gFrKjn69oDs4HXgQXFFA34NdCnTFtnAU9J+pKZLc7w7huB7lHeTGBKq3NjzGwzMC3q2CnAQ0HWL8xs6imQ73FXmgSDgZuD9D3AgSC9rhFlWRIp+tvAUuAPwElgCPAFoKdP/JUZ273LlboTcLYrVTsvuyNN2SV18YkXKvqvgBrgJVfWccCtPnGqgBpJO8xsbRl5bk3Jmyhprpn9K8rfB3wtypvlfQCwAXgy6rNmv6pNiczRj5pIjjGRHO8vw28QN0bSiOi92yS9L4XXWVJtWlkJN+a8qPzeqPy0lDbujDgL09wmSSMlHQl4f5HUpoRsl5VwQb6ecYy2hXK9h7HuJ2mBt3dA0l8lPSHpJkkd4z2UpEX+/Q5IelHSKknXS2qf8PLrAtkQWrpjwNVm9o8Uq3jIzKallVWAuuD5kH/Cga0CQqu6BbjdzJQiz3rgG0HWh4BLSrz7tuB5D7A6SM+Q1PYULWoXA1uBrwCdgU3AYWAs8DDwuYA72rkz3Ypt8jEaBywHJuTKXhk+GTyvNrMdDdj25ZKuk3SjpG8DtwdltWZ2IuKf4xGgBA+kcELU+uAnGF1Ewc4CPhNkLQbmB+m+wLWnQNG7AasoXD68DRhgZmPM7AJgIPDDZK8oqbfvizq5ezvIuUOADwCPlNug5ogiMIEPmqykDYn5KXkngXnAnSllfaP0n0o1bmaHJL0chBL7lVjVqwPrssTM9kja6vuRhPOTRu7yW4AewMNmVhN9l78DtwSuyUzgDGCxmT0UcXcCk3I3pjJU8+6zh+MlJsbYyM8dWM93Vvmgp8XjLWVilMPJEvWR1An4fJC13Mz2+POiIH+YpI83cn+P8b+PlZjAdZHFzcJt9Sv70eC5ewle9yJ1MLOjkt7kf3f6BzewjEk0Bgq/ExjlvmkvYKmkg2a2KuDvjuqfC2wuMQE7uElP8HoKbTKFw6gEXSR91587pViAmxpxzPr731cbmNvqozEzI36fFE7/iDM1hfNkUP6GpK71XdnLRWOcsyYo3xCVVUvaF5Q/W+wAy/nxIeEVUblJ2l7BwdAxSX0bKxoj6WWve04G7m7n9s9kLj3UVeufoe9RGc/wU89aSYv9UllTYmOUnpPCmRWln0vh/Dh47gYsk9SuEeUOzw4GRpbmBIX4foKLgW8WGY+PUjiLSLCTwkFUiCuAD1cgW9soGtTQSCzXgAbm0sZNWA9P7/fQTX0xDJgRpO/3EFZTYYtv4M739JclHadwWCXgmijctgl4IaWdlRQOrEZ5ehzwnKT7gD+7+3E20WFcRvSTdNifTwcuc7kSvJJS5zsU7uYk8fxvSToXWAi86G7ZeGAu0CXRfWBWyj2Z8BDpn95GGq4CLvLn6ZLuDv3heiyMycq9x8zCyf0bYCRwNfB0kbrVPul/C1zgsq0rwwVJ6wKzs9XjuPVyYyQ9GN3vaN+UbozXuchlKYc6ScNLtNMjMLFZ8EqRA6HhFd4nmVxEnvPdpcqKuSltDJZ0ohQn4I7KKFcmN6bEfaZ+kg5LekfSpKjsTL+bdHMgf51/JkTcnpJqJN0QZs6OvsRXUwQr+98FJF0i6XjAebypffag3pVlFOONLJelJHWX9FQGxVotqWeRNipR9qVl/PFBkjaWaWOfpM8WqV8TLU49ynz/Pwb8zY2h7F52XXDZbruklZI2BHnTAu7kQO9ecO7GIG/if11AD+K/BJwZvG+xm7yjGZVpBvA9oENgMkea2YYGVvYL/T0Jrjez3Vn3E+5mXB74eLuAX3pM+UAFcowAJlK4INfTw5G7gd8Bq8zs2TLm+/4SzR92V+QJM9uYUZ5Pudsy1MfxkLs/azxe/XaaeadwIJO4OevN7K4y75kATA+ypprZqxFnWbDPeKzYlV5JidvxAzP7aZH9xhwKJ759gL3Ado/zLzezQ5Fe3OHuT28fi23Ao8AKMzsSz6ST0Yqw00/1qsq4COtTVpMFeXg+R7OFpOmRG5JgRQp3SBSOi01vdd6jOZq7wl/qK3qIa4PydpLmFZkUB0NfKkeOlqDwHXzTul/S0iC/o6S1RQ4ZFjWDmHqOHPVW+s7hlU5Jd0dKfkLSI5IG5b2VozUpfrWkvYGivyNpfN4zOVqjssf3R5blvZKjpaFNBby9FO5FQOH3jzlytCj8BxX42oyuHrkZAAAAAElFTkSuQmCC"
                   style="max-height: 187px; margin: auto;" />
                </a>
              </td>
            </tr>
            <tr>
              <td class="wrapper">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                      <p>Hey <strong><?php echo $name; ?></strong>!</p>
                      <p>We got your message. One of our crewmates will get back to you within 24 hours.</p>
                      <p>Here is a copy of what you sent us:</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="providedInfo">
                        <?php foreach ($userInput as $input) { ?>
                        <p><strong><?php echo $input[0]; ?>: </strong><?php echo $input[1]; ?></p>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <p>If any of the information above is wrong, please send us another message.</p>
                      <p>Thanks for choosing
                        <strong style="color: #F58B47">Tugboat.cc</strong>!</p>
                      <p>Talk Soon,</p>
                      <p>  Team Tugboat</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <div class="footer">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="content-block">
                  <span class="apple-link">Tugboat Inc, 439 University Ave, Toronto ON M5G 1Y8</span>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
</body>
</html>