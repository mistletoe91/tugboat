<!doctype html>
<html>

<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title></title>
  <?php echo $mail_style ?>
</head>

<body>
  <table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td>&nbsp;</td>
      <td class="container">
        <div class="content">
          <span class="preheader">New mail from the <?php echo $form_name; ?> form</span>
          <table class="main">
            <tr>
              <td class="brand-header" style="background-color: #F58B47; height: 50px;">
              </td>
            </tr>
            <tr>
              <td class="wrapper">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                      <p>New submission from the <strong><?php echo $form_name; ?></strong> form       </p>
                      <p>What they sent us:</p>
                      <div class="providedInfo">
                        <p><strong><?php echo $name; ?></strong></p>
                        <?php foreach ($userInput as $input) { ?>
                        <p><strong><?php echo $input[0]; ?>: </strong><?php echo $input[1]; ?></p>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
</body>
</html>