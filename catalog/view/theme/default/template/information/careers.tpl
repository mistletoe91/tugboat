<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/information/contact.css" rel="stylesheet" />
<div class="container">
  <div class="row">
    <div id="content" class="main-content col-sm-12">
      <?php echo $content_top; ?>
      <h1 style="text-align:center">
        <?php echo $heading_title; ?>
      </h1>
      <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <p>
                Great teams build great companies! Would you like to join our team and become a sailor? Send us message and we will get back
                to you.
              </p>
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal career-form">
                <fieldset>
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="form-group required">
                        <label class="col-xs-12 control-label" for="input-name">
                          <?php echo $entry_name; ?>
                        </label>
                        <div class="col-xs-12">
                          <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                          <?php if ($error_name) { ?>
                          <div class="text-danger">
                            <?php echo $error_name; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group required">
                        <label class="col-xs-12 control-label" for="input-email">
                          <?php echo $entry_email; ?>
                        </label>
                        <div class="col-xs-12">
                          <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                          <?php if ($error_email) { ?>
                          <div class="text-danger">
                            <?php echo $error_email; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-xs-12 control-label" for="input-enquiry">
                      <?php echo $text_roles; ?>
                    </label>
                    <div class="col-xs-12">
                      <select name="roles" class="form-control">
                        <option value="Sales">Sales</option>
                        <option value="Customer Service">Customer Service</option>
                        <option value="Operations">Operations</option>
                        <option value="Development">Development</option>
                        <option value="Other">Other</option>
                      </select>

                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-xs-12 control-label" for="input-enquiry">
                      <?php echo $text_resume; ?>
                    </label>
                    <div class="col-xs-12">
                      <input type="file" name="file" class="form-control" />
                    </div>
                  </div>
                </fieldset>
                <div class="buttons">
                  <div class="pull-left">
                    <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>