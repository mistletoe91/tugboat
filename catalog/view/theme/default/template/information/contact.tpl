<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/information/contact.css" rel="stylesheet" />
<div id="pageContainer" class="container-fluid">
  <!-- <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </ul> -->
  <div class="section">
    <div class="container-contact-form" class="main-content">
      <?php echo $content_top; ?>
      <div class="container-section-header">
        <span class="header-line background-tugboat-blue"></span>
        <div id="container-header-contact" class="section-header">
          <div id="header-contact" class="container-background"></div>
          <img class="section-header-icon" src="image/catalog/contact_envelope.png">
        </div>
        <span class="header-line background-tugboat-blue"></span>
      </div>
      <div class="container-section-headline text-center">
        <h3>Contact Us</h3>
      </div>
      <div class="container container-section-content">
        <div class="col-xs-12 visible-xs" id="mobileContactForm">
          <div class="contact-form-container">
            <div class="contact-form-blurb">
              <p>We are here to answer any questions. Reach out to us and we will respond within 24 hours.</p>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal contact-form">
              <fieldset>
                <div class="row">
                  <div class="col-xs-6">
                    <div class="form-group required">
                      <label class="col-xs-12 control-label" for="mobile-input-name">
                        <?php echo $entry_name; ?>
                      </label>
                      <div class="col-xs-12">
                        <input type="text" name="name" value="<?php echo $name; ?>" id="mobile-input-name" class="form-control" />
                        <?php if ($error_name) { ?>
                        <div class="text-danger">
                          <?php echo $error_name; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="form-group required">
                      <label class="col-xs-12 control-label" for="mobile-input-email">
                        <?php echo $entry_email; ?>
                      </label>
                      <div class="col-xs-12">
                        <input type="text" name="email" value="<?php echo $email; ?>" id="mobile-input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                        <div class="text-danger">
                          <?php echo $error_email; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>        
                <div class="form-group required">
                  <label class="col-xs-12 control-label" for="mobile-input-enquiry">
                    <?php echo $entry_enquiry; ?>
                  </label>
                  <div class="col-xs-12">
                    <textarea name="enquiry" rows="2" id="mobile-input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                    <?php if ($error_enquiry) { ?>
                    <div class="text-danger">
                      <?php echo $error_enquiry; ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <?php echo $captcha; ?>
              </fieldset>
              <div class="buttons">
                <input class="btn" type="submit" value="<?php echo $button_submit; ?>" />
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-5 hidden-xs" id="contactForm">
          <div class="contact-form-container">
            <div class="contact-form-blurb">
              <p>We are here to answer any questions. Reach out to us and we will respond within 24 hours.</p>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal contact-form">
              <fieldset>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group required">
                      <label class="col-xs-12 control-label" for="input-name">
                        <?php echo $entry_name; ?>
                      </label>
                      <div class="col-xs-12">
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                        <?php if ($error_name) { ?>
                        <div class="text-danger">
                          <?php echo $error_name; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="form-group required">
                      <label class="col-xs-12 control-label" for="input-email">
                        <?php echo $entry_email; ?>
                      </label>
                      <div class="col-xs-12">
                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                        <div class="text-danger">
                          <?php echo $error_email; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
        
                <div class="form-group required">
                  <label class="col-xs-12 control-label" for="input-enquiry">
                    <?php echo $entry_enquiry; ?>
                  </label>
                  <div class="col-xs-12">
                    <textarea name="enquiry" rows="8" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                    <?php if ($error_enquiry) { ?>
                    <div class="text-danger">
                      <?php echo $error_enquiry; ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <?php echo $captcha; ?>
              </fieldset>
              <div class="buttons">
                <input class="btn" type="submit" value="<?php echo $button_submit; ?>" />
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-3 visible-xs" id="mobileMapInfo">
          <!-- <h2><strong>Address:</strong></h2> -->
          <h2><strong>Contact:</strong></h2>
          <a href="tel:+1-866-206-6445">
            <h4>+1 (866) 206-6445</h4>
          </a>
          <h4>info@tugboat.cc</h4>
          <br/>
          <h4>780 - 439 University Ave.</h4>
          <h4>Toronto, ON. M5G 1Y8</h4>
        </div>
        <div class="col-sm-3 hidden-xs" id="mapInfo">
          <h1>Our Location</h1>
          <br/>
          <h2>Address:</h2>
          <h4>780 - 439 University Ave.</h4>
          <h4>Toronto, ON. M5G 1Y8</h4>
          <br/>
          <h2>Contact:</h2>
          <a href="tel:+1-866-206-6445"><h4>+1 (866) 206-6445</h4></a>
          <h4>info@tugboat.cc</h4>
        </div>
        <div class="col-xs-12 col-sm-12" id="map"></div>
        <script>
          function initMap() {
            var tugboatHQ = { lat: 43.654691, lng: -79.387529 };
            var icons = {
              tugboat: {
                icon: {
                  url: 'image/catalog/tugboat_marker_med.png'
                }
              }
            };
            var markerPos = { lat: tugboatHQ.lat - 0.0007, lng: tugboatHQ.lng + 0.0003 };
            var mobileOffset = { lat: tugboatHQ.lat - 0.0007, lng: tugboatHQ.lng - 0.0006 };
            var mobileZoom = 17;
            var zoom = window.innerWidth >= 768 ? 18 : 17;
            var mapCenter = window.innerWidth >= 768 ? markerPos : mobileOffset;
            var map = new google.maps.Map(document.getElementById('map'), {
              center: mapCenter,
              zoom: zoom,
              zoomControl: false,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              styles: [
                {
                  "featureType": "administrative.locality",
                  "elementType": "geometry",
                  "stylers": [
                    {
                      "color": "#3ca8b1"
                    },
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "administrative.locality",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#2b9096"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "administrative.locality",
                  "elementType": "labels",
                  "stylers": [
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "administrative.locality",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#73cfed"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "administrative.neighborhood",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#3078dc"
                    }
                  ]
                },
                {
                  "featureType": "administrative.neighborhood",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "color": "#73cfed"
                    }
                  ]
                },
                {
                  "featureType": "landscape.man_made",
                  "elementType": "geometry",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#3babcb"
                    }
                  ]
                },
                {
                  "featureType": "landscape.natural",
                  "elementType": "geometry",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#3babcb"
                    }
                  ]
                },
                {
                  "featureType": "landscape.natural.landcover",
                  "elementType": "all",
                  "stylers": [
                    {
                      "color": "#2678b4"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "color": "#4bb8d8"
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "lightness": "25"
                    },
                    {
                      "visibility": "simplified"
                    },
                    {
                      "color": "#73cfed"
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "elementType": "labels.text.stroke",
                  "stylers": [
                    {
                      "color": "#2e9ebf"
                    },
                    {
                      "weight": "0.80"
                    },
                    {
                      "visibility": "simplified"
                    },
                    {
                      "gamma": "2.66"
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "elementType": "labels.icon",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "poi.attraction",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "lightness": "75"
                    },
                    {
                      "saturation": "0"
                    },
                    {
                      "color": "#2e9ebf"
                    }
                  ]
                },
                {
                  "featureType": "poi.business",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "lightness": "0"
                    },
                    {
                      "saturation": "0"
                    },
                    {
                      "color": "#51cff4"
                    }
                  ]
                },
                {
                  "featureType": "road",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#2e9ebf"
                    }
                  ]
                },
                {
                  "featureType": "road",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "color": "#73cbed"
                    }
                  ]
                },
                {
                  "featureType": "road.highway",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#2e9ebf"
                    }
                  ]
                },
                {
                  "featureType": "road.highway",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "color": "#73cfed"
                    }
                  ]
                },
                {
                  "featureType": "road.highway.controlled_access",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#2e9ebf"
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#73cfed"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#2e9ebf"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#73cfed"
                    }
                  ]
                },
                {
                  "featureType": "transit",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "transit.station",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    },
                    {
                      "lightness": "25"
                    }
                  ]
                },
                {
                  "featureType": "transit.station.airport",
                  "elementType": "all",
                  "stylers": [
                    {
                      "visibility": "on"
                    },
                    {
                      "color": "#3babcb"
                    }
                  ]
                }
              ]
            });
            var marker = new google.maps.Marker({
              position: markerPos,
              map: map,
              icon: icons.tugboat.icon
            });

            var mapInfo = document.getElementById('mapInfo');
            var mobileMapInfo = document.getElementById('mobileMapInfo');
            var contactForm = document.getElementById('contactForm');
            map.controls[google.maps.ControlPosition.LEFT_TOP].push(contactForm);
            map.controls[google.maps.ControlPosition.RIGHT_TOP].push(mapInfo);
            if (window.innerWidth < 768) {
              map.controls[google.maps.ControlPosition.LEFT_TOP].push(mobileMapInfo);
            }
            google.maps.event.addDomListener(window, 'resize', function () {
              if (window.innerWidth >= 768) {
                map.setCenter(markerPos);
              }
              else {
                map.setCenter(mobileOffset);
              }
            });
          }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuu-2heM6ipqPyEphQwwOd-OXTI5ig1Ns&callback=initMap" async defer></script>
        <!-- <div class="col-xs-12 col-md-4">
          <h3>
            <?php echo $text_location; ?>
          </h3>
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <?php if ($image) { ?>
                <div class="col-xs-12 text-center">
                  <img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail"
                  />
                </div>
                <?php } ?>
                <div class="col-xs-12">
                  <br />
                  <strong>
                    <?php echo $store; ?>
                  </strong>
                  <br />
                  <address>
                    <?php echo $address; ?>
                  </address>
                  <?php if ($geocode) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank"
                    class="btn btn-info">
                    <i class="fa fa-map-marker"></i>
                    <?php echo $button_map; ?>
                  </a>
                  <?php } ?>
                </div>
                <div class="col-xs-12">
                  <strong>
                    <?php echo $text_telephone; ?>
                  </strong>
                  <br>
                  <?php echo $telephone; ?>
                  <br />
                  <br />
                  <?php if ($fax) { ?>
                  <strong>
                    <?php echo $text_fax; ?>
                  </strong>
                  <br>
                  <?php echo $fax; ?>
                  <?php } ?>
                </div>
                <div class="col-xs-12">
                  <?php if ($open) { ?>
                  <strong>
                    <?php echo $text_open; ?>
                  </strong>
                  <br />
                  <?php echo $open; ?>
                  <br />
                  <br />
                  <?php } ?>
                  <?php if ($comment) { ?>
                  <strong>
                    <?php echo $text_comment; ?>
                  </strong>
                  <br />
                  <?php echo $comment; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          <?php if ($locations) { ?>
          <h3>
            <?php echo $text_store; ?>
          </h3>
          <div class="panel-group" id="accordion">
            <?php foreach ($locations as $location) { ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse"
                    data-parent="#accordion">
                    <?php echo $location['name']; ?>
                    <i class="fa fa-caret-down"></i>
                  </a>
                </h4>
              </div>
              <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
                <div class="panel-body">
                  <div class="row">
                    <?php if ($location['image']) { ?>
                    <div class="col-xs-12">
                      <img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>"
                        class="img-thumbnail" />
                    </div>
                    <?php } ?>
                    <div class="col-xs-12">
                      <strong>
                        <?php echo $location['name']; ?>
                      </strong>
                      <br />
                      <address>
                        <?php echo $location['address']; ?>
                      </address>
                      <?php if ($location['geocode']) { ?>
                      <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                        target="_blank" class="btn btn-info">
                        <i class="fa fa-map-marker"></i>
                        <?php echo $button_map; ?>
                      </a>
                      <?php } ?>
                    </div>
                    <div class="col-xs-3">
                      <strong>
                        <?php echo $text_telephone; ?>
                      </strong>
                      <br>
                      <?php echo $location['telephone']; ?>
                      <br />
                      <br />
                      <?php if ($location['fax']) { ?>
                      <strong>
                        <?php echo $text_fax; ?>
                      </strong>
                      <br>
                      <?php echo $location['fax']; ?>
                      <?php } ?>
                    </div>
                    <div class="col-xs-12">
                      <?php if ($location['open']) { ?>
                      <strong>
                        <?php echo $text_open; ?>
                      </strong>
                      <br />
                      <?php echo $location['open']; ?>
                      <br />
                      <br />
                      <?php } ?>
                      <?php if ($location['comment']) { ?>
                      <strong>
                        <?php echo $text_comment; ?>
                      </strong>
                      <br />
                      <?php echo $location['comment']; ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <?php } ?>
        </div> -->
      </div>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>