<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/product/product_landing/flyers.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<script src="catalog/view/javascript/anime.min.js" type="text/javascript"></script>

<div class="section visible-xs">
    <div class="container-product-page">
        <div class="container-section-header">
            <span class="header-line background-tugboat-blue"></span>
            <div id="container-header-products" class="section-header">
                <div class="product-background text-center">
                    <div class="background-text">Flyers</div>
                </div>
            </div>
            <span class="header-line background-tugboat-blue"></span>
        </div>
        <div class="container-section-headline text-center">
            <h4>Shop by Size</h4>
        </div>
        <div class="container-section-content content text-center">
            <div class="container-product-image">
                <object class="product-image" type="image/svg+xml" data="image/catalog/display_flyers.svg"></object>
            </div>
            <div class="container-product-options">
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="#" data-toggle="modal" data-target="#priceListModal">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_price_list.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Price List</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="#" data-toggle="modal" data-target="#uploadSizeSelectModal">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_upload_design.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Upload your Design</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="index.php?route=information/contact">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_need_help.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Call us for Help</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <!-- <div class="row">
                    <p class="product-option-text">Price List</p>
                    <p class="product-option-text">Upload your Design</p>
                    <p class="product-option-text">Call us for Help</p>
                </div> -->
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>

<div class="container main-content hidden-xs">
    <h1>Flyers</h1>
    <div class="product-background-line">
        <h2>Shop by size</h2>
        <span class="line"></span>
    </div>
    <div class="container-row">
        <div class="container-column flex-column mClass">
            <p class="added-padding">8.5" x 11"</p>
            <p class="added-padding">8.5" x 5.5"</p>
        </div>
        <div class="container-column mClass">
            <object id="displaySVG" class="img-side" type="image/svg+xml" data="image/catalog/display_flyers.svg"></object>
        </div>
        <div class="row container">
            <!-- <img src="<?php echo $priceImage; ?>" alt="get price list" data-toggle="modal" data-target="#myModal" id="price"> -->
            <span data-toggle="modal" data-target="#priceListModal" id="price" style="color:#2ca3c4; font-weight:bold;">Price List</span>
        </div>
        <div class="line-break mClass"></div>
    </div>
    <div class="container-row">
        <div class="container-column-icon">
            <a href="#" data-toggle="modal" data-target="#templateSizeSelectModal">
                <!-- <a href="index.php?_route_=flyers"> -->
                <div class="container-img">
                    <img class="img-center first-img" src="image/catalog/tugboat_icons_templates_blue.svg" />
                </div>
                <p>Browse our templates</p>
            </a>
        </div>
        <div class="container-column-icon">
            <a href="#" data-toggle="modal" data-target="#uploadSizeSelectModal">
                <!-- <a href="Banners/upload-your-own-flyer"> -->
                <div class="container-img">
                    <img class="img-center second-img" src="image/catalog/tugboat_icons_design.svg" />
                </div>
                <p>Upload your design</p>
            </a>
        </div>
        <div class="container-column-icon">
            <a href="index.php?route=information/contact">
                <div class="container-img">
                    <img class="img-center third-img" src="image/catalog/tugboat_icons_help.svg" />
                </div>
                <p>Call us for help</p>
            </a>
        </div>
    </div>


</div>
<div class="modal fade" id="priceListModal" tabindex="-1" role="dialog">
    <div class="modal-dialog four-col">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <table class="table">
                <thead>
                    <tr>
                        <th>Quantity</th>
                        <th>8.5"x5.5"</th>
                        <th>8.5"x11"</th>
                        <th>Finish</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>250</td>
                        <td>$ 102.00</td>
                        <td>$ 129.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>$ 102.00</td>
                        <td>$ 129.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>$ 115.00</td>
                        <td>$ 156.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>1500</td>
                        <td>$ 129.00</td>
                        <td>$ 230.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>2000</td>
                        <td>$ 129.00</td>
                        <td>$ 230.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>2500</td>
                        <td>$ 149.00</td>
                        <td>$ 230.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>5000</td>
                        <td>$ 169.00</td>
                        <td>$ 304.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>10000</td>
                        <td>$ 297.00</td>
                        <td>$ 513.00</td>
                        <td>Glossy</td>
                    </tr>
                    <tr>
                        <td>20000</td>
                        <td>$ 513.00</td>
                        <td>$ 966.00</td>
                        <td>Glossy</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadDesignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div id="uploadDesignModalContent" class="modal-content">
        </div>
    </div>
</div>
<div class="modal fade size-select" id="templateSizeSelectModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <ul>
                <li> Select your size </li>
                <li>
                    <a href="index.php?_route_=flyers/flyers5.5"> 8.50' X 5.5' </a>
                </li>
                <li>
                    <a href="index.php?_route_=flyers/flyers11"> 8.50' X 11' </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="modal fade size-select" id="uploadSizeSelectModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <ul>
                <li> Select your size </li>
                <li>
                    <a class="hidden-xs" href="index.php?_route_=Banners/upload-your-own-flyer1"> 8.50' X 5.5' </a>
                    <a class="visible-xs" id="uploadSmall" href="index.php?_route_=Banners/upload-your-own-flyer1"> 8.50' X 5.5' </a>
                </li>
                <li>
                    <a class="hidden-xs" href="index.php?_route_=Banners/upload-your-own-flyer"> 8.50' X 11' </a>
                    <a class="visible-xs" id="uploadLarge" href="index.php?_route_=Banners/upload-your-own-flyer"> 8.50' X 11' </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>
    // let uploadSizeSelectModal = document.getElementById('uploadSizeSelectModal');
    // let uploadDesignModalContent = document.getElementById('uploadDesignModalContent');
    // let uploadSmallElement = document.getElementById('uploadSmall');
    // let uploadLargeElement = document.getElementById('uploadLarge');

    // uploadSmallElement.addEventListener('click', (ev) => {
    //     $(uploadSizeSelectModal).modal('hide');
    //     $(uploadDesignModalContent).load('index.php?_route_=Banners/upload-your-own-flyer1', () => {});
    // });

    // uploadLargeElement.addEventListener('click', (ev) => {
    //     $(uploadSizeSelectModal).modal('hide');
    //     $(uploadDesignModalContent).load('index.php?_route_=Banners/upload-your-own-flyer', () => {});
    // });
</script>

<?php echo $footer; ?>