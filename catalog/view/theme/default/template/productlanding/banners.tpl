<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/product/product_landing/banners.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<script src="catalog/view/javascript/anime.min.js" type="text/javascript"></script>

<div class="section visible-xs">
    <div class="container-product-page">
        <div class="container-section-header">
            <span class="header-line background-tugboat-blue"></span>
            <div id="container-header-products" class="section-header">
                <div class="product-background text-center">
                    <div class="background-text">Banners</div>
                </div>
            </div>
            <span class="header-line background-tugboat-blue"></span>
        </div>
        <div class="container-section-headline text-center">
            <h4>Shop by Size</h4>
        </div>
        <div class="container-section-content content text-center">
            <div class="container-product-image">
                <object class="product-image" type="image/svg+xml" data="image/catalog/display_banners.svg"></object>
            </div>
            <div class="container-product-options">
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="#" data-toggle="modal" data-target="#priceListModal">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_price_list.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Price List</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="/index.php?_route_=Banners/upload-your-own-banner">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_upload_design.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Upload your Design</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="index.php?route=information/contact">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_need_help.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Call us for Help</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <!-- <div class="row">
                    <p class="product-option-text">Price List</p>
                    <p class="product-option-text">Upload your Design</p>
                    <p class="product-option-text">Call us for Help</p>
                </div> -->
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<div class="container main-content hidden-xs">
    <h1>Banners</h1>
    <div class="product-background-line">
        <h2>Shop by size</h2>
        <span class="line"></span>

    </div>
    <div class="container-row">
        <div class="container-column mClass">
            <p>33.5" x 80"</p>
        </div>
        <div class="container-column mClass">
            <object id="displaySVG" class="img-side" type="image/svg+xml" data="image/catalog/display_banners.svg"></object>
        </div>
        <div class="row container">
            <!-- <img src="<?php echo $priceImage; ?>" alt="get price list" data-toggle="modal" data-target="#myModal" id="price"> -->
            <span data-toggle="modal" data-target="#priceListModal" id="price" style="color:#2ca3c4; font-weight:bold;">Price List</span>
        </div>
        <div class="line-break mClass"></div>
    </div>

    <div class="container-row">
        <div class="container-column-icon">
            <a href="index.php?_route_=Banners">
                <div class="container-img">
                    <img class="img-center first-img" src="image/catalog/tugboat_icons_templates_blue.svg" />
                </div>
                <p>Browse our templates</p>
            </a>
        </div>
        <div class="container-column-icon">
            <!-- <a id="touchBanner"> -->
            <a href="index.php?_route_=Banners/upload-your-own-banner">
                <div class="container-img">
                    <img class="img-center second-img" src="image/catalog/tugboat_icons_design.svg" />
                </div>
                <p>Upload your design</p>
            </a>
        </div>
        <div class="container-column-icon">
            <a href="index.php?route=information/contact">
                <div class="container-img">
                    <img class="img-center third-img" src="image/catalog/tugboat_icons_help.svg" />
                </div>
                <p>Call us for help</p>
            </a>
        </div>
    </div>
</div>
<div class="modal fade" id="priceListModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close text-center" data-dismiss="modal">
                <div class="close-text"><p>&times;</p></div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Quantity</th>
                        <th>33.5' x 80'</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>$ 142.00</td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>$ 284.00</td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>$ 426.00</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>$ 567.00</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>$ 709.00</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>$ 1418.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadDesignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var info = $(".c").html();
        if (info) {
            $("#bannerModal").toggleClass("hidden");
            $(".mClass").toggleClass("hidden");
        }

        $("#touchBanner").on('click', function (e) {
            e.preventDefault();
            $("#bannerModal").toggleClass("hidden");
            $(".mClass").toggleClass("hidden");
        });

        $(".bclose").on('click', function (e) {
            e.preventDefault();
            $("#bannerModal").toggleClass("hidden");
            $(".mClass").toggleClass("hidden");
        });



        var height = screen.height - 400;
        console.log(height);

        $('#price').on('toggle', function () {
            $('#myModal').modal(myModal);
        });
    });
</script>


<?php echo $footer; ?>