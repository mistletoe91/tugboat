<!doctype html>
<html>

<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title></title>
  <?php echo $mail_style ?>
</head>

<body>
  <table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
      <td>&nbsp;</td>
      <td class="container">
        <div class="content">
          <span class="preheader">Tugboat has recieved your inquiry!</span>
          <table class="main">
            <tr>
              <td class="brand-header" style="background-color: #F58B47; height: 50px;">
                <a href="https://tugboat.cc" style="display: flex; width: 100%; height: 100%">
                  <img src="image/icon_tugboat_logo_white_full.svg" style="height: 90%; width: 50%; margin: auto;" />
                </a>
              </td>
            </tr>
            <tr>
              <td class="wrapper">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                      <p>Hey <strong><?php echo $name; ?></strong>!</p>
                      <p>We got your message. One of our crewmates will get back to you within 24 hours.</p>
                      <p>Here is a copy of what you sent us:</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="providedInfo">
                        <?php foreach ($userInput as $input) { ?>
                        <p><strong><?php echo $input[0]; ?>: </strong><?php echo $input[1]; ?></p>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <p>If any of the information above is wrong, please send us another message.</p>
                      <p>Thanks for choosing
                        <strong style="color: #F58B47">Tugboat.cc</strong>!</p>
                      <p>Talk Soon,</p>
                      <p>  Team Tugboat</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <div class="footer">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="content-block">
                  <span class="apple-link">Tugboat Inc, 439 University Ave, Toronto ON M5G 1Y8</span>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
</body>
</html>