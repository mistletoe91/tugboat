<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/product/product_landing/businesscards.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<script src="catalog/view/javascript/anime.min.js" type="text/javascript"></script>

<div class="section visible-xs">
    <div class="container-product-page">
        <div class="container-section-header">
            <span class="header-line background-tugboat-blue"></span>
            <div id="container-header-products" class="section-header">
                <div class="product-background text-center">
                    <div class="background-text two-lines">Business Cards</div>
                </div>
            </div>
            <span class="header-line background-tugboat-blue"></span>
        </div>
        <div class="container-section-headline text-center">
            <h4>Shop by Size</h4>
        </div>
        <div class="container-section-content content text-center">
            <div class="container-product-image">
                <object class="product-image" type="image/svg+xml" data="image/catalog/display_business_cards.svg"></object>
            </div>
            <div class="container-product-options">
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="#" data-toggle="modal" data-target="#priceListModal">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_price_list.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Price List</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="/index.php?_route_=Banners/Upload-Your-Own-Business_Cards">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_upload_design.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Upload your Design</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <div class="product-col">
                    <a class="product-option" href="index.php?route=information/contact">
                        <div class="product-background">
                            <img class="product-option-icon" src="image/catalog/icon_need_help.svg" />
                        </div>
                    </a>
                    <p class="product-option-text">Call us for Help</p>
                </div>
                <span class="header-line background-tugboat-blue"></span>
                <!-- <div class="row">
                    <p class="product-option-text">Price List</p>
                    <p class="product-option-text">Upload your Design</p>
                    <p class="product-option-text">Call us for Help</p>
                </div> -->
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<div class="container main-content hidden-xs">
    <h1>Business Cards</h1>
    <div class="product-background-line">
        <h2>Shop by size</h2>
        <span class="line"></span>
    </div>
    <div class="container-row">
        <div class="container-column flex-column mClass">
            <p class="added-padding">3.5" x 2.0"</p>
            <p class="added-padding">2.0" x 3.5"</p>
        </div>
        <div class="container-column mClass">
            <object id="displaySVG" class="img-side" type="image/svg+xml" data="image/catalog/display_business_cards.svg"></object>
        </div>
        <div class="row container">
        <!-- <img src="<?php echo $priceImage; ?>" alt="get price list" data-toggle="modal" data-target="#myModal" id="price"> -->
        <span data-toggle="modal" data-target="#priceListModal" id="price" style="color:#2ca3c4; font-weight:bold;">Price List</span>
        <?php echo ($thanks) ? '<span class="alert  alert-success">'.$thanks.'</span>' : '';?>

        </div>
        <div class="line-break mClass"></div>
    </div>

    <div class="hidden" id="bannerModal" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close bclose" data-dismiss="modal" aria-label="Close"><span data-dismiss="modal" aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><?php echo $heading_title; ?></h4>
                </div>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="recipient-name" class="control-label"><?php echo $entry_name; ?>:</label>
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                        <?php if ($error_name) { ?>
                        <div class="text-danger c"><?php echo $error_name; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <label for="message-text" class="control-label"><?php echo $entry_email; ?>:</label>
                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                        <div class="text-danger c"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <label for="message-text" class="control-label"><?php echo $text_telephone; ?>:</label>
                        <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone" class="form-control" />
                        <?php if ($error_phone) { ?>
                        <div class="text-danger c"><?php echo $error_phone; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <label for="message-text" class="control-label"><?php echo $entry_pdf; ?>:</label>
                        <input type="file" name="filename" id="input-file" class="form-control" />
                        <?php if ($error_pdf) { ?>
                        <div class="text-danger c"><?php echo $error_pdf; ?></div>
                        <?php } ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bclose" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
                </form>
            </div>

        </div>
        <div class="line-break"></div>
    </div>

    <div class="container-row">
        <div class="container-column-icon">
            <a href="index.php?_route_=businesscards">
                <div class="container-img">
                    <img class="img-center first-img" src="image/catalog/tugboat_icons_templates_blue.svg"/>
                </div>
                <p>Browse our templates</p>
            </a>
        </div>
        <div class="container-column-icon">
            <!-- <a id="touchBCard"> -->
            <a href="index.php?_route_=Banners/Upload-Your-Own-Business_Cards">
                <div class="container-img">
                    <img class="img-center second-img" src="image/catalog/tugboat_icons_design.svg"/>
                </div>
                <p>Upload your design</p>
            </a>
        </div>
        <div class="container-column-icon">
            <a href="index.php?route=information/contact">
                <div class="container-img">
                    <img class="img-center third-img" src="image/catalog/tugboat_icons_help.svg"/>
                </div>
                <p>Call us for help</p>
            </a>
        </div>
    </div>

</div>
<div class="modal fade" id="priceListModal" tabindex="-1" role="dialog">
    <div class="modal-dialog four-col">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
            <table class="table">
                <thead>
                    <tr>
                        <th>Quantity</th>
                        <th>3.5" x 2.0"</th>
                        <th>2.0" x 3.5"</th>
                        <th>Finish</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>250</td>
                        <td>$ 21.00</td>
                        <td>$ 21.00</td>
                        <td>UV Coated</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>$ 25.00</td>
                        <td>$ 25.00</td>
                        <td>UV Coated</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>$ 34.00</td>
                        <td>$ 34.00</td>
                        <td>UV Coated</td>
                    </tr>
                    <tr>
                        <td>2500</td>
                        <td>$ 81.00</td>
                        <td>$ 81.00</td>
                        <td>UV Coated</td>
                    </tr>
                    <tr>
                        <td>5000</td>
                        <td>$ 127.00</td>
                        <td>$ 127.00</td>
                        <td>UV Coated</td>
                    </tr>
                    <tr>
                        <td>10000</td>
                        <td>$ 211.00</td>
                        <td>$ 211.00</td>
                        <td>UV Coated</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadDesignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-close text-center" data-dismiss="modal">
            <div class="close-text"><p>&times;</p></div>
        </div>
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
$('#price').on('toggle', function () {
    $('#myModal').modal(myModal);
});

$(document).ready(function() {
var info = $(".c").html();
if(info){
    $("#bannerModal").toggleClass("hidden");
    $(".mClass").toggleClass("hidden");
}

$("#touchBCard").on('click', function(e) {
    e.preventDefault();
    $("#bannerModal").toggleClass("hidden");
    $(".mClass").toggleClass("hidden");
});

$(".bclose").on('click', function(e) {
    e.preventDefault();
    $("#bannerModal").toggleClass("hidden");
    $(".mClass").toggleClass("hidden");
});

});

</script>


<?php echo $footer; ?>
