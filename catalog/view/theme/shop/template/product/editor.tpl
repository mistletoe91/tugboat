<script src="catalog/view/javascript/fabric.js" type="text/javascript"></script>
<script src="catalog/view/javascript/product.js" type="text/javascript"></script>
<script src="catalog/view/javascript/product_back.js" type="text/javascript"></script>
<script src="catalog/view/javascript/multistepform.js" type="text/javascript"></script>

<!-- Multi Step Form Start -->
<div class="container container-steps">
  <div class="row">
    <section>
      <div class="wizard">
        <div class="wizard-inner">
          <!-- <div class="connecting-line"></div> -->
          <ul class="nav nav-tabs setup-panel" role="tablist">
            <li role="presentation" id="front" class="active wizard-step">
              <a href="#step-1" data-toggle="tab" aria-controls="step1" role="tab">
                <div class="container-background-circle"></div>
                <div class="step-content">
                  <img class="step-content-image" src="image/catalog/tugboat_front-back-proof_icons_front.svg" />
                  <p class="step-content-text">Front</p>
                </div>
              </a>
            </li>

            <li role="presentation" id="back" class="disabled wizard-step">
              <span id="hide-step-2">
                <a class="clsgetfinalproof" href="#step-2" data-toggle="tab" aria-controls="step2" role="tab">
                  <div class="container-background-circle"></div>
                  <div class="step-content">
                    <img class="step-content-image" src="image/catalog/tugboat_front-back-proof_icons_back.svg" />
                    <p class="step-content-text">Back</p>
                  </div>
                </a>
              </span>
            </li>

            <li role="presentation" id="proof" class="disabled wizard-step">
              <a href="#step-3" data-toggle="tab" aria-controls="step3" role="tab">
                <div class="container-background-circle"></div>
                <div class="step-content">
                  <img class="step-content-image" src="image/catalog/proof.svg" />
                  <p class="step-content-text">Proof</p>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>
</div>

<div class="editor">
  <div class="row setup-content" id="step-1">
    <div class="col-xs-12">
      <div class="col-md-12   text-center">
        <div class="row">
          <nav class="menu-back sticky">
            <input type="file" class="hidden-ctrl" id="btnfileupload">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menubar" aria-expanded="false"
                  aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div id="menubar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                  <li id="design">
                    <a id="setdesignactive">Design
                      <i class="fa fa-picture-o"></i>
                    </a>
                  </li>
                  <li id="text">
                    <a id="settextactive">Text
                      <i class="fa fa-font"></i>
                    </a>
                  </li>
                  <li id="image">
                    <a id="setimageactive">Image
                      <i class="fa fa-picture-o"></i>
                    </a>
                  </li>
                  <li id="options">
                    <a id="setoptionsactive">More Options
                      <i class="fa fa-cogs"></i>
                    </a>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right nav-options">
                  <li>
                    <div class="btn-group">
                      <button type="button" class="btn btn-arrange dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Arrange
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li>
                          <a id="sendSelectedObjectBack">Send Backwards</a>
                        </li>
                        <li>
                          <a id="sendSelectedObjectToFront">Bring Forwards</a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <!-- <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="undobutton" data-original-title="Undo">Undo <i class="fa fa-2x fa-undo"></i></button></li>
                        <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="redobutton" data-original-title="Redo">Redo <i class="fa fa-2x fa-repeat"></i></button></li>
                        <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="clearbutton" data-original-title="Clear">Clear <i class="fa fa-2x fa-eraser"></i></button></li>
                        <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="deletebtn" data-original-title="Trash">Delete <i class="fa fa-2x fa-trash-o"></i></button></li> -->
                  <li>
                    <button class="btn" id="undobutton">Undo
                      <i class="fa fa-2x fa-undo"></i>
                    </button>
                  </li>
                  <li>
                    <button class="btn" id="redobutton">Redo
                      <i class="fa fa-2x fa-repeat"></i>
                    </button>
                  </li>
                  <li>
                    <button class="btn" id="clearbutton">Clear
                      <i class="fa fa-2x fa-eraser"></i>
                    </button>
                  </li>
                  <li>
                    <button class="btn" id="deletebtn">Delete
                      <i class="fa fa-2x fa-trash-o"></i>
                    </button>
                  </li>
                  <li class="container-btn-save">
                    <button class="btn btn-save" id="savebtn">
                      <i class="fa fa-floppy-o" aria-hidden="true"></i> Next</button>
                  </li>
                </ul>
              </div>
            </div>
          </nav>

        </div>

        <div class="row top-margin-20">
          <div class="col-md-3">
            <div id="design-panel" class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Templates</h4>
              </div>
              <div class="panel-body">
                <div class="template">
                  <div class="frontsidetemplates">
                    <h4>Choose from the following designs</h4>
                    <div class="row">

                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="text-panel" class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Text</h4>
              </div>
              <div class="panel-body">
                <div id="text-wrapper">
                  <div id="text-controls">

                    <div class="row">
                      <div class="col-md-7">
                        <select id="font-family" class="font-selection">
                          <option class="opt-arial" value="Arial">Arial</option>
                          <option class="opt-helvetica" value="helvetica">Helvetica</option>
                          <option class="opt-myriad" value="myriad pro">Myriad Pro</option>
                          <option class="opt-delicious" value="delicious">Delicious</option>
                          <option class="opt-verdana" value="verdana">Verdana</option>
                          <option class="opt-georgia" value="georgia">Georgia</option>
                          <option class="opt-courier" value="courier">Courier</option>
                          <option class="opt-comic" value="comic sans ms">Comic Sans MS</option>
                          <option class="opt-impact" value="impact">Impact</option>
                          <option class="opt-monaco" value="monaco">Monaco</option>
                          <option class="opt-optima" value="optima">Optima</option>
                          <option class="opt-hoefler" value="hoefler text">Hoefler Text</option>
                          <option class="opt-plaster" value="plaster">Plaster</option>
                          <option class="opt-engagement" value="engagement">Engagement</option>
                          <option class="opt-times" value="times new roman">Times New Roman</option>
                        </select>
                      </div>
                      <div class="col-md-5">
                        <select id="text-font-size" class="form-control">
                          <?php
                                for($i=1;$i<=120;$i++){
                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="btn-group" role="group">
                        <button type="button" id="btnleftalign" class="btn btn-default">
                          <i class="fa fa-align-left" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="btncenteralign" class="btn btn-default">
                          <i class="fa fa-align-center" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="btnrightalign" class="btn btn-default">
                          <i class="fa fa-align-right" aria-hidden="true"></i>
                        </button>
                        <!-- </div>

                            <div class="btn-group" role="group"> -->
                        <button type="button" id="btnbold" class="btn btn-default">
                          <i class="fa fa-bold" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="btnitalic" class="btn btn-default">
                          <i class="fa fa-italic" aria-hidden="true"></i>
                        </button>
                        <!-- </div>
                            <div class="btn-group" role="group"> -->
                        <button type="button" id="btnunderline" class="btn btn-default">
                          <i class="fa fa-underline" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="btnstrikethrough" class="btn btn-default">
                          <i class="fa fa-strikethrough" aria-hidden="true"></i>
                        </button>
                      </div>

                      <div class="btn-group" role="group">
                        <ul class="color-pickers">
                          <li>
                            <input type="text" id="textcolor" /> Color</li>
                          <li style="margin-left: 30px;">
                            <input type="text" id="textbgcolor" /> Fill</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="btn-group" role="group">
                        <button type="button" id="btnleftalign"  class="btn btn-default"><i class="fa fa-align-left" aria-hidden="true"></i></button>
                        <button type="button" id="btncenteralign" class="btn btn-default"><i class="fa fa-align-center" aria-hidden="true"></i></button>
                        <button type="button"  id="btnrightalign" class="btn btn-default"><i class="fa fa-align-right" aria-hidden="true"></i></button>
                      </div>

                      <div class="btn-group" role="group">
                      <button type="button" id="btnbold"  class="btn btn-default"><i class="fa fa-bold" aria-hidden="true"></i></button>
                      <button type="button" id="btnitalic"   class="btn btn-default"><i class="fa fa-italic" aria-hidden="true"></i></button>
                    </div>
                    <div class="btn-group" role="group">
                    <button type="button" id="btnunderline"   class="btn btn-default"><i class="fa fa-underline" aria-hidden="true"></i></button>
                    <button type="button" id="btnstrikethrough"   class="btn btn-default"><i class="fa fa-strikethrough" aria-hidden="true"></i></button>
                  </div>

                  <div class="btn-group" role="group">
                  <ul class="color-pickers">
                  <li><input type="text" id="textcolor" /> Color</li>
                  <li style="margin-left: 30px;"><input type="text" id="textbgcolor" /> Backcolor</li>
                </ul>
              </div>
            </div> -->

                  <div class="no-display">
                    <select id="text-align">
                      <option value="left">Left</option>
                      <option value="center">Center</option>
                      <option value="right">Right</option>
                      <option value="justify">Justify</option>
                    </select>

                    <li>
                      <input type="color" value="" class="no-display" id="text-lines-bg-color" size="10"> Background Text Color</li>
                    <li>
                      <input type="color" value="" class="no-display" id="text-stroke-color"> Stroke Color</li>

                    <input type="range" value="1" class="no-display" min="1" max="5" id="text-stroke-width">
                    <input type="range" value="1" class="no-display" min="0" max="10" step="0.1" id="text-line-height">
                  </div>

                  <?php /*
            <input type="color" value="" id="text-color" size="10">
            <div>
              <label for="text-bg-color">Background color:</label>
              <input type="color" value="" id="text-bg-color" size="10">
            </div>
            <div>
              <label for="text-lines-bg-color">Background text color:</label>
              <input type="color" value="" id="text-lines-bg-color" size="10">
            </div>
            <div>
              <label for="text-stroke-color">Stroke color:</label>
              <input type="color" value="" id="text-stroke-color">
            </div>
            <div>
              <label for="text-stroke-width">Stroke width:</label>
              <input type="range" value="1" min="1" max="5" id="text-stroke-width">
            </div>
            <div>
              <label for="text-font-size">Font size:</label>
              <input type="range" value="" min="1" max="120" step="1" id="text-font-size">
            </div>
            <div>
              <label for="text-line-height">Line height:</label>
              <input type="range" value="" min="0" max="10" step="0.1" id="text-line-height">
            </div>
            */
            ?>

                </div>
                <input id="hidden-counter" type="hidden" />
                <div id="editable-field-wrapper">

                </div>
                <button class="btn btn-add-editable" id="add-editable">Add Text Field</button>
              </div>
            </div>
            <div id="image-panel" class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Image Effects</h4>
              </div>
              <div class="panel-body">
                <div class="container mini-width">
                  <div class="row">
                    <div class="col-md-3 no-pad-full">
                      <div id="image-tools">



                        <div class="margin-bottom-20">
                          <img id="imggrayscale_1" class="imagefilter imgfilterMask" data-toggle="tooltip" data-placement="top" title="Grayscale" style="width:50px;margin-right:10px;"
                            src="/image/icons/tugboat_icons_website-50x50_greyscale.svg" />
                          <img id="imginvert_1" class="imagefilter imgfilterMask" data-toggle="tooltip" data-placement="top" title="Invert" style="width:50px;margin-right:10px;"
                            src="/image/icons/tugboat_icons_website-50x50_invert.svg" />
                          <img id="imgsepia_1" class="imagefilter imgfilterMask" data-toggle="tooltip" data-placement="top" title="Sepia" style="width:50px;margin-right:10px;"
                            src="/image/icons/tugboat_icons_website-50x50_greyscale.svg" />
                          <img id="imgsepia2_1" class="imagefilter imgfilterMask" data-toggle="tooltip" data-placement="top" title="Sepia2" style="width:50px;"
                            src="/image/icons/tugboat_icons_website-50x50_greyscale.svg" />
                        </div>
                        <div class="alert alert-info" role="alert">Please select image before clicking on effects</div>

                        <!-- <button class="btn btn-tools" id="crop-img">Crop <i class="fa fa-crop"></i></button>
                  <button disabled class="btn btn-tools" id="end-crop">Exit Crop <i class="fa fa-times"></i></button> -->
                        <div id="filter-wrapper" class="hide">
                          <label>
                            <span>Grayscale: </span>
                            <input type="checkbox" class="imgfilter" id="grayscale" />
                          </label>
                          <label>
                            <span>Invert: </span>
                            <input type="checkbox" class="imgfilter" id="invert" />
                          </label>
                          <label>
                            <span>Sepia: </span>
                            <input type="checkbox" class="imgfilter" id="sepia" />
                          </label>
                          <label>
                            <span>Sepia2: </span>
                            <input type="checkbox" class="imgfilter" id="sepia2" />
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-heading">
                <h4 class="panel-title">Upload Image</h4>
              </div>
              <div class="panel-body">
                <div class="container mini-width">
                  <div class="row">
                    <div class="col-md-3 no-pad-full">

                    </div>
                  </div>
                </div>
                <form class="form-img-controls" action="compressimg.php" method="post" enctype="multipart/form-data" id="upload_form">

                  <div class="margin-bottom-20">
                    <div class="tile-progress tile-red maintile_uploadfile_1">
                      <div class="tile-header">
                        <h1>
                          <a href="#" id="clsuploadfile_1" class="clsuploadfile">Click here to Upload Files</a>
                        </h1>
                      </div>

                    </div>
                  </div>

                  <input name="__files[]" id="ctlfiles_1" class="ctlfiles" type="file" multiple="multiple" style="display:none;" />
                  <input name="__submit__" id="ctlfiles_sbt_1" class="ctlfiles_sbt" type="submit" value="upload" style="display:none;" />
                </form>
                <div id="output"></div>
                <!--<div class="droppable" onclick="$('user-image').click()">
            <strong>Drag images or click here to add images</strong><br />
            <span><strong>Note: Preview images are not to scale</strong></span>
            <img id="uploaded-image" src="" />
            <input type="file" name="useradded_image" id="user-image" />
          </div>-->
                <br />
                <!--<h4>Set Background Image <i class="fa fa-5x fa-upload"></i></h4>
          <div class="droppable-background" onclick="$('user-image-background').click()">
            <strong>Drag images or click here set background image</strong><br />
            <span><strong>Note: Preview images are not to scale</strong></span>
            <img id="uploaded-image-background" src="" />
            <input type="file" name="useradded_image_background" id="user-image-background" />
          </div>-->
              </div>
            </div>

            <div id="options-panel" class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">Shapes</h4>
              </div>
              <div class="panel-body">
                <div class="margin-bottom-20">
                  <img id="addsquare" class="ctlshare" data-toggle="tooltip" data-placement="top" title="Add Square" style="width:50px;margin-right:10px;"
                    src="/image/icons/tugboat_icons_website-50x50-rectangle-orange.svg" />
                  <img id="addcircle" class="ctlshare" data-toggle="tooltip" data-placement="top" title="Add Circle" style="width:50px;margin-right:10px;"
                    src="/image/icons/tugboat_icons_website-50x50_full-circle-orange.svg" />
                </div>
              </div>
              <div class="panel-heading hide">
                <h4 class="panel-title">Transparency</h4>
              </div>
              <div class="panel-body hide">
                <div class="container container-options-shapes">
                  <div class="row range-ctrl-container">
                    <div class="col-md-12">
                      <input type="range" class="range-ctrl" id="opacityrange" value="0" min="0" max="1" step="0.1" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-heading">
                <h4 class="panel-title">Pick Color</h4>
              </div>
              <div class="panel-body">
                <div class="container container-options-shapes">
                  <div class="row">
                    <div id="palette-wrap">
                      <input type="text" id="palette" />
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-9">
            <div id="resp-canvas-elem">
              <canvas id="canvas-product-front"></canvas>
              <div id="overlaycanvas"></div>
              <div id="placeholdercanvas"></div>
            </div>
            <!-- <div id="rulerwrapper">
        <canvas id="rulerTop"></canvas>
        <canvas id="rulerBottom"></canvas>
      </div>-->
          </div>

        </div>


      </div>
    </div>
  </div>

</div>

<!-- Step2 -->
<div class="row setup-content" id="step-2">
  <div class="col-xs-12">
    <div class="col-md-12   text-center">

      <div class="row">
        <nav class="menu-back-back">
          <input type="file" class="hidden-ctrl" id="backbtnfileupload">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#backmenubar" aria-expanded="false"
                aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="backmenubar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-left">
                <li id="back-design">
                  <a id="backsetdesignactive">Design
                    <i class="fa fa-picture-o"></i>
                  </a>
                </li>
                <li id="back-text">
                  <a id="backsettextactive">Text
                    <i class="fa fa-font"></i>
                  </a>
                </li>
                <li id="back-image">
                  <a id="backsetimageactive">Image
                    <i class="fa fa-picture-o"></i>
                  </a>
                </li>
                <li id="back-options">
                  <a id="backsetoptionsactive">More Options
                    <i class="fa fa-cogs"></i>
                  </a>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right nav-options">
                <li>
                  <div class="btn-group">
                    <button type="button" class="btn btn-arrange dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Arrange
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li>
                        <a id="sendSelectedObjectBack-back">Send Backwards</a>
                      </li>
                      <li>
                        <a id="sendSelectedObjectToFront-back">Bring Forwards</a>
                      </li>
                    </ul>
                  </div>
                </li>
                <!-- <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="backundobutton" data-original-title="Undo">Undo <i class="fa fa-5x fa-undo"></i></button></li>
                    <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="backredobutton" data-original-title="Redo">Redo <i class="fa fa-5x fa-repeat"></i></button></li>
                    <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="backclearbutton" data-original-title="Clear">Clear <i class="fa fa-5x fa-eraser"></i></button></li>
                    <li><button data-toggle="tooltip" data-placement="top" title class="btn" id="backdeletebtn" data-original-title="Trash">Delete <i class="fa fa-5x fa-trash-o"></i></button></li>
                    <li><button class="btn btn-save" id="backsavebtn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Next</button></li> -->
                <li>
                  <button class="btn" id="backundobutton">Undo
                    <i class="fa fa-2x fa-undo"></i>
                  </button>
                </li>
                <li>
                  <button class="btn" id="backredobutton">Redo
                    <i class="fa fa-2x fa-repeat"></i>
                  </button>
                </li>
                <li>
                  <button class="btn" id="backclearbutton">Clear
                    <i class="fa fa-2x fa-eraser"></i>
                  </button>
                </li>
                <li>
                  <button class="btn" id="backdeletebtn">Delete
                    <i class="fa fa-2x fa-trash-o"></i>
                  </button>
                </li>
                <li>
                  <button class="btn btn-save" id="backsavebtn">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Next</button>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>

      <div class="row top-margin-20">
        <div class="col-md-3">
          <div id="back-design-panel" class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Templates</h4>
            </div>
            <div class="panel-body">
              <div class="template">
                <div class="backsidetemplates">
                  <h4>Choose from the following designs</h4>
                  <div class="row">

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="display:none">
            <div style="display: inline-block; ">
              <p>
                <label>
                  <code>padding</code>
                  <input id="backpadding" value="0" min="0" max="50" style="display: block" type="range">
                </label>
              </p>
            </div>
          </div>

          <div id="back-text-panel" class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Text</h4>
            </div>
            <div class="panel-body">
              <div id="text-wrapper">
                <div id="text-controls">

                  <div class="row">
                    <div class="col-md-7">
                      <select id="back-font-family" class="back-font-selection">
                        <option class="opt-arial" value="Arial">Arial</option>
                        <option class="opt-helvetica" value="helvetica">Helvetica</option>
                        <option class="opt-myriad" value="myriad pro">Myriad Pro</option>
                        <option class="opt-delicious" value="delicious">Delicious</option>
                        <option class="opt-verdana" value="verdana">Verdana</option>
                        <option class="opt-georgia" value="georgia">Georgia</option>
                        <option class="opt-courier" value="courier">Courier</option>
                        <option class="opt-comic" value="comic sans ms">Comic Sans MS</option>
                        <option class="opt-impact" value="impact">Impact</option>
                        <option class="opt-monaco" value="monaco">Monaco</option>
                        <option class="opt-optima" value="optima">Optima</option>
                        <option class="opt-hoefler" value="hoefler text">Hoefler Text</option>
                        <option class="opt-plaster" value="plaster">Plaster</option>
                        <option class="opt-engagement" value="engagement">Engagement</option>
                        <option class="opt-times" value="times new roman">Times New Roman</option>
                      </select>
                    </div>
                    <div class="col-md-5">
                      <select id="back-text-font-size" class="form-control">
                        <?php
                            for($i=1;$i<=120;$i++){
                              echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="btn-group" role="group">
                      <button type="button" id="backbtnleftalign" class="btn btn-default">
                        <i class="fa fa-align-left" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtncenteralign" class="btn btn-default">
                        <i class="fa fa-align-center" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtnrightalign" class="btn btn-default">
                        <i class="fa fa-align-right" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtnbold" class="btn btn-default">
                        <i class="fa fa-bold" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtnitalic" class="btn btn-default">
                        <i class="fa fa-italic" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtnunderline" class="btn btn-default">
                        <i class="fa fa-underline" aria-hidden="true"></i>
                      </button>
                      <button type="button" id="backbtnstrikethrough" class="btn btn-default">
                        <i class="fa fa-strikethrough" aria-hidden="true"></i>
                      </button>
                    </div>


                    <div class="btn-group" role="group">
                      <ul class="color-pickers">
                        <li>
                          <input type="text" id="textcolorback" /> Color</li>
                        <li style="margin-left: 30px;">
                          <input type="text" id="textbgcolorback" /> Fill</li>
                      </ul>
                    </div>

                  </div>

                </div>
                <!-- <div class="btn-group" role="group">
                    <button type="button" id="backbtnleftalign"  class="btn btn-default"><i class="fa fa-align-left" aria-hidden="true"></i></button>
                    <button type="button" id="backbtncenteralign" class="btn btn-default"><i class="fa fa-align-center" aria-hidden="true"></i></button>
                    <button type="button"  id="backbtnrightalign" class="btn btn-default"><i class="fa fa-align-right" aria-hidden="true"></i></button>
                  </div>

                  <div class="btn-group" role="group">
                  <button type="button" id="backbtnbold"  class="btn btn-default"><i class="fa fa-bold" aria-hidden="true"></i></button>
                  <button type="button" id="backbtnitalic"   class="btn btn-default"><i class="fa fa-italic" aria-hidden="true"></i></button>
                </div>
                <div class="btn-group" role="group">
                <button type="button" id="backbtnunderline"   class="btn btn-default"><i class="fa fa-underline" aria-hidden="true"></i></button>
                <button type="button" id="backbtnstrikethrough"   class="btn btn-default"><i class="fa fa-strikethrough" aria-hidden="true"></i></button>
              </div>


              <div class="btn-group" role="group">
              <ul class="color-pickers">
              <li><input type="text" id="textcolorback" /> Color</li>
              <li style="margin-left: 30px;"><input type="text" id="textbgcolorback" /> Backcolor</li>
            </ul>
          </div>

        </div> -->

                <div class="no-display">
                  <select id="backtext-align">
                    <option value="left">Left</option>
                    <option value="center">Center</option>
                    <option value="right">Right</option>
                    <option value="justify">Justify</option>
                  </select>

                  <li>
                    <input type="color" value="" class="no-display" id="back-text-lines-bg-color" size="10"> Background Text Color</li>
                  <li>
                    <input type="color" value="" class="no-display" id="back-text-stroke-color"> Stroke Color</li>

                  <input type="range" value="1" class="no-display" min="1" max="5" id="back-text-stroke-width">
                  <input type="range" value="1" class="no-display" min="0" max="10" step="0.1" id="back-text-line-height">
                </div>

                <?php /*
        <input type="color" value="" id="back-text-color" size="10">
        <div>
          <label for="text-bg-color">Background color:</label>
          <input type="color" value="" id="back-text-bg-color" size="10">
        </div>
        <div>
          <label for="text-lines-bg-color">Background text color:</label>
          <input type="color" value="" id="back-text-lines-bg-color" size="10">
        </div>
        <div>
          <label for="text-stroke-color">Stroke color:</label>
          <input type="color" value="" id="back-text-stroke-color">
        </div>
        <div>
          <label for="text-stroke-width">Stroke width:</label>
          <input type="range" value="1" min="1" max="5" id="back-text-stroke-width">
        </div>
        <div>
          <label for="text-font-size">Font size:</label>
          <input type="range" value="" min="1" max="120" step="1" id="back-text-font-size">
        </div>
        <div>
          <label for="text-line-height">Line height:</label>
          <input type="range" value="" min="0" max="10" step="0.1" id="back-text-line-height">
        </div>
        */
        ?>

              </div>

              <input type="hidden" id="hidden-counter-back" />
              <div id="back-editable-field-wrapper">


              </div>
              <button id="back-add-editable" class="btn btn-back-add-editable">Add Text Field</button>
            </div>
          </div>

          <div id="back-image-panel" class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Image Effects</h4>
            </div>
            <div class="panel-body">
              <div class="container mini-width">
                <div class="row">
                  <div class="col-md-3 no-pad-full">
                    <div id="image-tools">
                      <!-- <button class="btn btn-tools" id="crop-img-back">Crop <i class="fa fa-crop"></i></button>
                <button disabled class="btn btn-tools" id="end-crop-back">Exit Crop <i class="fa fa-times"></i></button> -->

                      <div class="margin-bottom-20">
                        <img id="imggrayscale_2" class="imagefilter_back imgfilterMask" data-toggle="tooltip" data-placement="left" title="Grayscale"
                          style="width:50px;margin-right:10px;" src="https://image.flaticon.com/icons/svg/61/61136.svg" />
                        <img id="imginvert_2" class="imagefilter_back imgfilterMask" data-toggle="tooltip" data-placement="left" title="Invert" style="width:50px;margin-right:10px;"
                          src="https://image.flaticon.com/icons/svg/61/61136.svg" />
                        <img id="imgsepia_2" class="imagefilter_back imgfilterMask" data-toggle="tooltip" data-placement="left" title="Sepia" style="width:50px;margin-right:10px;"
                          src="https://image.flaticon.com/icons/svg/61/61136.svg" />
                        <img id="imgsepia2_2" class="imagefilter_back imgfilterMask" data-toggle="tooltip" data-placement="left" title="Sepia2" style="width:50px;"
                          src="https://image.flaticon.com/icons/svg/61/61136.svg" />

                      </div>
                      <div class="alert alert-info" role="alert">Please select image before clicking on effects</div>

                      <div id="filter-wrapper-back" class="hide">
                        <label>
                          <span>Grayscale: </span>
                          <input type="checkbox" class="imgfilter" id="grayscale-back" />
                        </label>
                        <label>
                          <span>Invert: </span>
                          <input type="checkbox" class="imgfilter" id="invert-back" />
                        </label>
                        <label>
                          <span>Sepia: </span>
                          <input type="checkbox" class="imgfilter" id="sepia-back" />
                        </label>
                        <label>
                          <span>Sepia2: </span>
                          <input type="checkbox" class="imgfilter" id="sepia2-back" />
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel-heading">
              <h4 class="panel-title">Upload Image</h4>
            </div>
            <div class="panel-body">
              <div class="container mini-width">
                <div class="row">
                  <div class="col-md-3 no-pad-full">

                  </div>
                </div>
              </div>
              <form class="form-img-controls" action="compressimg.php" method="post" enctype="multipart/form-data" id="upload_form_back">
                <div class="margin-bottom-20">
                  <div class="tile-progress tile-red maintile_uploadfile_2">
                    <div class="tile-header">
                      <h1>
                        <a href="#" id="clsuploadfile_2" class="clsuploadfile">Click here to Upload Files</a>
                      </h1>
                    </div>

                  </div>
                </div>


                <input name="__files[]" id="ctlfiles_2" class="ctlfiles" type="file" multiple="multiple" style="display:none;" />
                <input name="__submit__" id="ctlfiles_sbt_2" class="ctlfiles_sbt" type="submit" value="upload" style="display:none;" />
              </form>
              <div class="hide" id="output"></div>
              <!--<div class="droppable-back-frontback" onclick="$('user-image-back-frontback').click()">
          <strong>Drag images or click here set background image</strong><br />
          <span><strong>Note: Preview images are not to scale</strong></span>
          <img id="uploaded-image-back-frontback" src="" />
          <input type="file" name="useradded_image-frontback" id="user-image-back-frontback" />
        </div>-->
              <br />
              <!--<h4>Set Background Image <i class="fa fa-5x fa-upload"></i></h4>
        <div class="droppable-back-backback" onclick="$('user-image-back-backback').click()">
          <strong>Drag images or click here set background image</strong><br />
          <span><strong>Note: Preview images are not to scale</strong></span>
          <img id="uploaded-image-back-backback" src="" />
          <input type="file" name="useradded_image-backback" id="user-image-back-backback" />
        </div>-->
            </div>
          </div>

          <div id="back-options-panel" class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Shapes</h4>
            </div>
            <div class="panel-body">
              <div class="margin-bottom-20">
                <img id="backaddsquare" class="ctlshare" data-toggle="tooltip" data-placement="top" title="Add Square" style="width:50px;margin-right:10px;"
                  src="/image/icons/tugboat_icons_website-50x50-rectangle-orange.svg" />
                <img id="backaddcircle" class="ctlshare" data-toggle="tooltip" data-placement="top" title="Add Circle" style="width:50px;margin-right:10px;"
                  src="/image/icons/tugboat_icons_website-50x50_full-circle-orange.svg" />
              </div>
            </div>
            <div class="panel-heading hide">
              <h4 class="panel-title">Transparency</h4>
            </div>
            <div class="panel-body hide">
              <div class="container container-options-shapes">
                <div class="row range-ctrl-container">
                  <div class="col-md-12">
                    <input type="range" class="range-ctrl" id="opacityrangeback" value="0" min="0" max="1" step="0.1" />
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-heading">
              <h4 class="panel-title">Pick Color</h4>
            </div>
            <div class="panel-body">
              <div class="container container-options-shapes">
                <div class="row">
                  <div id="palette-wrap-back">
                    <input type="text" id="palette-back" />
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-9">
          <div id="back-resp-canvas-elem">
            <canvas id="canvas-product-back"></canvas>
            <!-- <div id="div_backcanvas-product-front">
        <canvas id="canvas-product-back"></canvas>
      </div> -->
            <div id="backoverlaycanvas"></div>
            <div id="backplaceholdercanvas"></div>
          </div>
        </div>

      </div>


    </div>
  </div>
</div>


<div class="container">

  <div class="row setup-content" id="step-3">
    <div class="col-xs-12">
      <div class="col-md-12  text-center">
        <div class="row">
          <div class="col-md-8">
            <h3>Front Side</h3>
            <div id="clsproof_front">
              <img class="img-responsive" crossOrigin="Anonymous" src="/image/l.gif" alt="Loading..." />
            </div>

            <h3>Back Side</h3>
            <div id="clsproof_back">
              <img class="img-responsive" crossOrigin="Anonymous" src="/image/l.gif" alt="Loading..." />
            </div>

          </div>
          <div class="col-md-4">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <span style="color:#2ca3c4"> Select Option </span>
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php

                        $initialPriceToShow = '$';
                        foreach ($discounts as $discount) {
                          if($minimum ===  $discount['quantity']){

                            $qty = preg_replace("/[^0-9\.]/", '', $discount['quantity']);
                            $price = preg_replace("/[^0-9\.]/", '', $discount['price']);

                            $initialPriceToShow .= +number_format((float)$qty * $price, 2, '.', '');
                            break;
                          }#endif
                        }#end foreach

                        $price_to_show_at_entry = $price;
                        foreach ($discounts as $discount) {
                          if($minimum ===  $discount['quantity']){
                            $price_to_show_at_entry = $discount['price'];
                          }#endif
                        }#endforeach
                        ?>
                      <h1 class="placeholder_finalprice">
                        <?php echo $initialPriceToShow;?>
                      </h1>
                      <select class="placeholder_discount form-control bottom-margin-20">
                        <?php foreach ($discounts as $discount) { ?>
                        <option value="<?php echo $discount['quantity'].'_'.$discount['price']; ?>">
                          <?php echo $discount['price']; ?> -
                          <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
                        <?php } ?>
                      </select>

                      <!-- <p>By clicking following button you agree that the final product will be printed as the proof shown and <a>all other conditions listed here</a> and there are whatsoever no futher things ! Final Sale Forever !</p> -->
                      <p>Please check the proof to make sure there are no silly mistakes! All sales are final.</p>
                      <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block">
                        <?php echo $button_cart; ?>
                      </button>
                      <div id="downloadArea"></div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Panel End -->

          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="no-display" id="designer-wrapper">
  Frontside JSON
  <textarea rows="100" cols="100" id="json_stuff">

    </textarea>
  <br />
  <br /> Backside JSON
  <textarea rows="100" cols="100" id="json_back_stuff">

    </textarea>
</div>
<div class="container">

  <div class="row setup-content" id="step-4">
    <div class="col-xs-12">
      <div class="col-md-12 well text-center">
        <h1 class="text-center"> STEP 4</h1>

      </div>
    </div>
  </div>

</div>



<!-- Multi Step Form End -->

<div class="row no-display" id="divproof">
  <div class="col-md-12">
    Processing...
  </div>
</div>



<div class="row hide ">
  <?php echo $column_left; ?>
  <?php if ($column_left && $column_right) { ?>
  <?php $class = 'col-sm-6'; ?>
  <?php } elseif ($column_left || $column_right) { ?>
  <?php $class = 'col-sm-9'; ?>
  <?php } else { ?>
  <?php $class = 'col-sm-12'; ?>
  <?php } ?>
  <div id="content" class="<?php echo $class; ?>">
    <?php echo $content_top; ?>
    <div class="row">
      <?php if ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-8'; ?>
      <?php } ?>
      <div class="<?php echo $class; ?>">
        <?php if ($thumb || $images) { ?>
        <ul class="thumbnails">
          <?php if ($thumb) { ?>
          <li>
            <a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
              <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            </a>
          </li>
          <?php } ?>
          <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <li class="image-additional">
            <a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
              <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            </a>
          </li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php


                                ?>
          <?php } ?>
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tab-description" data-toggle="tab">
                <?php echo $tab_description; ?>
              </a>
            </li>
            <?php if ($attribute_groups) { ?>
            <li>
              <a href="#tab-specification" data-toggle="tab">
                <?php echo $tab_attribute; ?>
              </a>
            </li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li>
              <a href="#tab-review" data-toggle="tab">
                <?php echo $tab_review; ?>
              </a>
            </li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description">
              <?php echo $description; ?>
            </div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2">
                      <strong>
                        <?php echo $attribute_group['name']; ?>
                      </strong>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td>
                      <?php echo $attribute['name']; ?>
                    </td>
                    <td id="<?php echo 'ctl_' . strtolower($attribute['name'])?>">
                      <?php echo $attribute['text']; ?>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2>
                  <?php echo $text_write; ?>
                </h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name">
                      <?php echo $entry_name; ?>
                    </label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review">
                      <?php echo $entry_review; ?>
                    </label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block">
                      <?php echo $text_note; ?>
                    </div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label">
                      <?php echo $entry_rating; ?>
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" /> &nbsp;
                    <input type="radio" name="rating" value="2" /> &nbsp;
                    <input type="radio" name="rating" value="3" /> &nbsp;
                    <input type="radio" name="rating" value="4" /> &nbsp;
                    <input type="radio" name="rating" value="5" /> &nbsp;
                    <?php echo $entry_good; ?>
                  </div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary">
                      <?php echo $button_continue; ?>
                    </button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
          </div>
      </div>
      <?php if ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-4'; ?>
      <?php } ?>
      <div class="<?php echo $class; ?>">
        <?php /*
                                                      <div class="btn-group">
                                                        <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('
        <?php echo $product_id; ?>');">
        <i class="fa fa-heart"></i>
        </button>
        <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');">
          <i class="fa fa-exchange"></i>
        </button>
      </div>
      */ ?>
      <h1>
        <?php echo $heading_title; ?>
      </h1>
      <ul class="list-unstyled">
        <?php if ($manufacturer) { ?>
        <li>
          <?php echo $text_manufacturer; ?>
          <a href="<?php echo $manufacturers; ?>">
            <?php echo $manufacturer; ?>
          </a>
        </li>
        <?php } ?>
        <li>
          <?php echo $text_model; ?>
          <?php echo $model; ?>
        </li>
        <?php if ($reward && 0) { ?>
        <li>
          <?php echo $text_reward; ?>
          <?php echo $reward; ?>
        </li>
        <?php } ?>
        <li>
          <?php echo $text_stock; ?>
          <?php echo $stock; ?>
        </li>
      </ul>
      <?php if ($price) { ?>
      <ul class="list-unstyled">
        <?php if (!$special) { ?>
        <li>
          <h2>
            <?php echo $price; ?>
          </h2>
        </li>
        <?php } else { ?>
        <li>
          <span class="line-through">
            <?php echo $price; ?>
          </span>
        </li>
        <li>
          <h2>
            <?php echo $special; ?>
          </h2>
        </li>
        <?php } ?>
        <?php if ($tax) { ?>
        <li>
          <?php echo $text_tax; ?>
          <?php echo $tax; ?>
        </li>
        <?php } ?>
        <?php if ($points) { ?>
        <li>
          <?php echo $text_points; ?>
          <?php echo $points; ?>
        </li>
        <?php } ?>
        <?php if ($discounts) { ?>
        <li>
          <hr>
        </li>
        <?php foreach ($discounts as $discount) { ?>
        <li>
          <?php echo $discount['quantity']; ?>
          <?php echo $text_discount; ?>
          <?php echo $discount['price']; ?>
        </li>
        <?php } ?>
        <?php } ?>
      </ul>
      <?php } ?>
      <div id="product">
        <?php if ($options) { ?>
        <hr>
        <h3>
          <?php echo $text_option; ?>
        </h3>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>"
            class="form-control">
            <option value="">
              <?php echo $text_select; ?>
            </option>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>">
              <?php echo $option_value['name']; ?>
              <?php if ($option_value['price']) { ?> (
              <?php echo $option_value['price_prefix']; ?>
              <?php echo $option_value['price']; ?>)
              <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'radio') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label">
            <?php echo $option['name']; ?>
          </label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"
                />
                <?php if ($option_value['image']) { ?>
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                  class="img-thumbnail" />
                <?php } ?>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?> (
                <?php echo $option_value['price_prefix']; ?>
                <?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'checkbox') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label">
            <?php echo $option['name']; ?>
          </label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"
                />
                <?php if ($option_value['image']) { ?>
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                  class="img-thumbnail" />
                <?php } ?>
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?> (
                <?php echo $option_value['price_prefix']; ?>
                <?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
            id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>"
            id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
              <?php echo $option['value']; ?>
            </textarea>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label">
            <?php echo $option['name']; ?>
          </label>
          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>"
            class="btn btn-default btn-block">
            <i class="fa fa-upload"></i>
            <?php echo $button_upload; ?>
          </button>
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>"
          />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <div class="input-group date">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
              id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">
                <i class="fa fa-calendar"></i>
              </button>
            </span>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'datetime') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <div class="input-group datetime">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
              id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
              <button type="button" class="btn btn-default">
                <i class="fa fa-calendar"></i>
              </button>
            </span>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'time') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
            <?php echo $option['name']; ?>
          </label>
          <div class="input-group time">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
              id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
              <button type="button" class="btn btn-default">
                <i class="fa fa-calendar"></i>
              </button>
            </span>
          </div>
        </div>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php if ($recurrings) { ?>
        <hr>
        <h3>
          <?php echo $text_payment_recurring; ?>
        </h3>
        <div class="form-group required">
          <select name="recurring_id" class="form-control">
            <option value="">
              <?php echo $text_select; ?>
            </option>
            <?php foreach ($recurrings as $recurring) { ?>
            <option value="<?php echo $recurring['recurring_id']; ?>">
              <?php echo $recurring['name']; ?>
            </option>
            <?php } ?>
          </select>
          <div class="help-block" id="recurring-description"></div>
        </div>
        <?php } ?>
        <div class="form-group">
          <label class="control-label" for="input-quantity">
            <?php echo $entry_qty; ?>
          </label>
          <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
          <br />

        </div>
        <?php if ($minimum > 1) { ?>
        <div class="alert alert-info">
          <i class="fa fa-info-circle"></i>
          <?php echo $text_minimum; ?>
        </div>
        <?php } ?>
      </div>
      <?php if ($review_status) { ?>
      <div class="rating">
        <p>
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($rating < $i) { ?>
          <span class="fa fa-stack">
            <i class="fa fa-star-o fa-stack-1x"></i>
          </span>
          <?php } else { ?>
          <span class="fa fa-stack">
            <i class="fa fa-star fa-stack-1x"></i>
            <i class="fa fa-star-o fa-stack-1x"></i>
          </span>
          <?php } ?>
          <?php } ?>
          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">
            <?php echo $reviews; ?>
          </a> /
          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">
            <?php echo $text_write; ?>
          </a>
        </p>
        <hr>
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_pinterest_pinit"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
        <!-- AddThis Button END -->
      </div>
      <?php } ?>
    </div>
  </div>
  <?php if ($products) { ?>
  <h3>
    <?php echo $text_related; ?>
  </h3>
  <div class="row">
    <?php $i = 0; ?>
    <?php foreach ($products as $product) { ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-xs-8 col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-xs-6 col-md-4'; ?>
    <?php } else { ?>
    <?php $class = 'col-xs-6 col-sm-3'; ?>
    <?php } ?>
    <div class="<?php echo $class; ?>">
      <div class="product-thumb transition">
        <div class="image">
          <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"
              class="img-responsive" />
          </a>
        </div>
        <div class="caption">
          <h4>
            <a href="<?php echo $product['href']; ?>">
              <?php echo $product['name']; ?>
            </a>
          </h4>
          <p>
            <?php echo $product['description']; ?>
          </p>
          <?php if ($product['rating']) { ?>
          <div class="rating">
            <?php for ($j = 1; $j <= 5; $j++) { ?>
            <?php if ($product['rating'] < $j) { ?>
            <span class="fa fa-stack">
              <i class="fa fa-star-o fa-stack-1x"></i>
            </span>
            <?php } else { ?>
            <span class="fa fa-stack">
              <i class="fa fa-star fa-stack-1x"></i>
              <i class="fa fa-star-o fa-stack-1x"></i>
            </span>
            <?php } ?>
            <?php } ?>
          </div>
          <?php } ?>
          <?php if ($product['price']) { ?>
          <p class="price">
            <?php if (!$product['special']) { ?>
            <?php echo $product['price']; ?>
            <?php } else { ?>
            <span class="price-new">
              <?php echo $product['special']; ?>
            </span>
            <span class="price-old">
              <?php echo $product['price']; ?>
            </span>
            <?php } ?>
            <?php if ($product['tax']) { ?>
            <span class="price-tax">
              <?php echo $text_tax; ?>
              <?php echo $product['tax']; ?>
            </span>
            <?php } ?>
          </p>
          <?php } ?>
        </div>
        <div class="button-group">

          <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
            <i class="fa fa-heart"></i>
          </button>
          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
            <i class="fa fa-exchange"></i>
          </button>
        </div>
      </div>
    </div>
    <?php if (($column_left && $column_right) && (($i+1) % 2 == 0)) { ?>
    <div class="clearfix visible-md visible-sm"></div>
    <?php } elseif (($column_left || $column_right) && (($i+1) % 3 == 0)) { ?>
    <div class="clearfix visible-md"></div>
    <?php } elseif (($i+1) % 4 == 0) { ?>
    <div class="clearfix visible-md"></div>
    <?php } ?>
    <?php $i++; ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php if ($tags) { ?>
  <p>
    <?php echo $text_tags; ?>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>">
      <?php echo $tags[$i]['tag']; ?>
    </a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>">
      <?php echo $tags[$i]['tag']; ?>
    </a>
    <?php } ?>
    <?php } ?>
  </p>
  <?php } ?>
  <?php echo $content_bottom; ?>
</div>
<script src="catalog/view/javascript/select2.min.js"></script>
<script src="catalog/view/javascript/jquery.hotkeys.min.js"></script>
<script src="catalog/view/javascript/spectrum.min.js"></script>
<script>
  $(document).ready(function () {
    $(".font-selection").select2({
      templateResult: function (data, container) {
        if (data.element) {
          $(container).addClass($(data.element).attr("class"));
        }
        return data.text;
      }
    });
    $(".back-font-selection").select2({
      templateResult: function (data, container) {
        console.log(container);
        if (data.element) {
          $(container).addClass($(data.element).attr("class"));
        }
        return data.text;
      }
    });


  });
</script>


<!-- Modal -->
<div class="modal fade" id="divloadingmodal_generic" data-backdrop="static" tabindex="-1" data-keyboard="false" role="dialog"
  aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="container">
    <div class="row">
      <div class="maintext col-sm-6 col-sm-offset-3 text-center">
        <br>
        <br>
        <h1>Tugboat Sailing...</h1>
        <h4>Please relax while we perform the operation you requested.</h4>
        <h4>This may take a while</h4>
        <hr>
        <div class="alert alert-info" role="alert">
          <strong>It will be ! </strong> sooner than later ! </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="divloadingmodal" data-backdrop="static" tabindex="-1" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="container">
    <div class="row">
      <div class="maintext col-sm-6 col-sm-offset-3 text-center">
        <br>
        <br>
        <h1>Applying Filters</h1>
        <h4>Please relax while we apply filter to your uploaded images.</h4>
        <h4>This may take a while</h4>
        <hr>
        <div class="alert alert-info" role="alert">
          <strong>Heads up! </strong> Large image take long time when you apply filters </div>
      </div>
    </div>
  </div>
</div>