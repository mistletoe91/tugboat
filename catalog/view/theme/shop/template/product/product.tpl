<?php echo $header; ?>
<style>
  .product-header h2 {
    color: darkblue;
  }
  .property-kv:before {
    content: "";
    width: 95%;
    margin-left: 15px;
    margin-right: 15px;
    height: 1px;
    background-color: rgba(0, 0, 0, 0.25);
  }
  .property-details h3 {
    font-size: 16px;
    font-weight: bold;
    color: darkblue;
  }
  .property-kv {
  }
  .property-key {
    padding-top: 5px;
    padding-bottom: 5px;
    color: darkblue;
    font-size: 14px;
    font-weight: bold;
  }
  .property-value {
    font-size: 14px;
    padding-top: 5px;
    padding-bottom: 5px;
  }
  #product {
    border: 1px solid #DDDDDD;
    border-radius: 2.5px;
    background-color: #EFEFEF;
  }
  .control-label {
    font-size: 14px;
  }

</style>
<div class="container">
  <?php //echo $shop_header; ?>
  <div class="row">
    <?php echo $sidebar; ?>
    <div class="col-sm-10">
      <div class="row">
        <div class="col-sm-9">
          <div class="row product-header">
            <div class="col-sm-8">
              <h1>
                <?php echo ucfirst($catagory_title); ?>
              </h1>
              <h2>
                <?php echo $heading_title; ?>
              </h2>
              <!--p>14pt (Profit Maximizer)</p>
              <p> &#9733;&#9733;&#9733;&#9733;&#9734;</p-->
            </div>
            <div class="col-sm-4">
              <img class="img-responsive" src=<?php echo $thumb; ?> />
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <p><?php echo $description; ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="row property-details">
                <div class="col-xs-12">
                  <h3> <?php echo ucfirst($catagory_title); ?> Details</h3>
                </div>
              </div>
              <?php if ($attribute_groups) {  ?>
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <?php foreach($attribute_group['attribute'] as $attribute) { ?>
                      <div class="row property-kv">
                        <div class="col-xs-3 property-key">
                          <?php echo $attribute['name']; ?>
                        </div>
                        <div class="col-xs-9 property-value">
                          <?php echo html_entity_decode($attribute['text']); ?>
                        </div>
                      </div>
                    <?php } ?>
                <?php } ?>
              <?php } ?>
              <!-- <div class="row property-row">
                <div class="col-xs-4 property-key">
                  Paper Type
                </div>
                <div class="col-xs-8 property-value">
                  14PT Gloss (95 Bright C2S)
                </div>
              </div> -->
            </div>

            <div class="col-xs-12">
              <h3> Print Help Center </h3>
            </div>

            <div class="col-xs-12">
              <div class="property-details">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <h4 class="panel-title">
                        <a >
                          Download Setup Guide <br/>
                          Our guide will get your project started right.
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <ul>
                          <?php if(!empty($download_m)){
                              foreach($download_m AS $pdf){
                          ?>
                             <li><a href="<?php echo $pdf['link']; ?>"><?php echo $pdf['name']; ?></a></li>
                          <?php  } } ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <h4 class="panel-title">
                        <a >
                          File Orientation Guide <br/>
                          How to set up multi-page files properly.
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        <div class="col-sm-12">
                           File Orientation refers to the orientation of the artwork files submitted. Ensure that they are submitted to back up properly to produce the intended result.
                        </div>
                        <div class="col-sm-12">
                           <img src="<?php echo $fileOrientation; ?>" class="img-responsive" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree"  class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <h4 class="panel-title">
                        <a>
                          How To Set Up Your Files <br/>
                          Learn how to set up your files the right way.
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        General File Preparation Guidelines
                        <ul>
                          <li> Download our guides to ensure a more optimal print result. </li>
                          <li> Each job (including multiple paged projects) must be submitted as a single PDF file. Ensure that all pages are the same size. </li>
                          <li> Files must be submitted with proper orientation to ensure proper back up. </li>
                          <li> It is best to try to avoid using borders in your design. If a border is too close to the trim, the trim may be slightly off-center. </li>
                          <li> File must consist of 1/8" bleed and all important art and text must be within the safety margin. </li>
                          <li> Ensure that your PDF is high res and that all images are CMYK at 300 DPI. </li>
                          <li> Black type should have the following values: C0, M0, Y0, K100. </li>
                          <li> Embed or outline all fonts. </li>
                          <li> For best colour results, supply a CMYK only files. </li>
                          <li> See our Support Center for more details. </li>
                        </ul>

                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour"  class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <h4 class="panel-title">
                        <a>
                          Turnaround Schedule <br/>
                          Find out when your print project will be ready.
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        <div class="col-sm-6">
                           <img src="<?php echo $turnaround1; ?>" class="img-responsive" />
                        </div>
                        <div class="col-sm-6">
                           <img src="<?php echo $turnaround2; ?>" class="img-responsive" />
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- collapse end -->
              </div>
            </div>

          </div>
        </div>
        <div id="product" class="col-sm-3">
          <?php if ($options) { ?>
          <hr>
          <h3>
            <?php //echo $text_option; ?>
          </h3>
          <?php foreach ($options as $option) { ?>
          <?php if ($option['type'] == 'select') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>"
              class="form-control input-sm">
              <option value="">
                <?php echo $text_select; ?>
              </option>
              <?php foreach ($option['product_option_value'] as $option_value) { ?>
              <option value="<?php echo $option_value['product_option_value_id']; ?>">
                <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?> (
                <?php echo $option_value['price_prefix']; ?>
                <?php echo $option_value['price']; ?>)
                <?php } ?>
              </option>
              <?php } ?>
            </select>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'radio') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label">
              <?php echo $option['name']; ?>
            </label>
            <div id="input-option<?php echo $option['product_option_id']; ?>">
              <?php foreach ($option['product_option_value'] as $option_value) { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"
                  />
                  <?php if ($option_value['image']) { ?>
                  <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                    class="img-thumbnail" />
                  <?php } ?>
                  <?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?> (
                  <?php echo $option_value['price_prefix']; ?>
                  <?php echo $option_value['price']; ?>)
                  <?php } ?>
                </label>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'checkbox') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label">
              <?php echo $option['name']; ?>
            </label>
            <div id="input-option<?php echo $option['product_option_id']; ?>">
              <?php foreach ($option['product_option_value'] as $option_value) { ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"
                  />
                  <?php if ($option_value['image']) { ?>
                  <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                    class="img-thumbnail" />
                  <?php } ?>
                  <?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?> (
                  <?php echo $option_value['price_prefix']; ?>
                  <?php echo $option_value['price']; ?>)
                  <?php } ?>
                </label>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'text') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
              id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'textarea') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>"
              id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
              <?php echo $option['value']; ?>
            </textarea>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'file') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label">
              <?php echo $option['name']; ?>
            </label>
            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>"
              class="btn btn-default btn-block">
              <i class="fa fa-upload"></i>
              <?php echo $button_upload; ?>
            </button>
            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>"
            />
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'date') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <div class="input-group date">
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-calendar"></i>
                </button>
              </span>
            </div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'datetime') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <div class="input-group datetime">
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
                id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default">
                  <i class="fa fa-calendar"></i>
                </button>
              </span>
            </div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'time') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>">
              <?php echo $option['name']; ?>
            </label>
            <div class="input-group time">
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default">
                  <i class="fa fa-calendar"></i>
                </button>
              </span>
            </div>
          </div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
          <?php if ($recurrings) { ?>
          <hr>
          <h3>
            <?php echo $text_payment_recurring; ?>
          </h3>
          <div class="form-group required">
            <select name="recurring_id" class="form-control">
              <option value="">
                <?php echo $text_select; ?>
              </option>
              <?php foreach ($recurrings as $recurring) { ?>
              <option value="<?php echo $recurring['recurring_id']; ?>">
                <?php echo $recurring['name']; ?>
              </option>
              <?php } ?>
            </select>
            <div class="help-block" id="recurring-description"></div>
          </div>
          <?php } ?>
          <div class="form-group hidden">
            <label class="control-label" for="input-quantity">
              <?php echo $entry_qty; ?>
            </label>
            <?php if($this->data['model'] == "upload-your-own-banner"){ ?>
            <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
            <?php } else { ?>
            <!-- <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" /> -->
            <select class="form-control" name="quantity">
              <?php foreach ($discounts as $discount) { ?>
              <option value="<?php echo $discount['quantity']; ?>">
                <?php echo $discount['price']; ?> -
                <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
              <?php } ?>
            </select>
            <?php } ?>

            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
            <br />

          </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <?php
                                    $initialPriceToShow = '$';
                                    foreach ($discounts as $discount) {
                                      if($minimum ===  $discount['quantity']){
          
                                        $qty = preg_replace("/[^0-9\.]/", '', $discount['quantity']);
                                        $price = preg_replace("/[^0-9\.]/", '', $discount['price']);
          
                                        $initialPriceToShow .= +number_format((float)$qty * $price, 2, '.', '');
                                        break;
                                      }#endif
                                    }#end foreach
          
                                    $price_to_show_at_entry = $price;
                                    foreach ($discounts as $discount) {
                                      if($minimum ===  $discount['quantity']){
                                        $price_to_show_at_entry = $discount['price'];
                                      }#endif
                                    }#endforeach
                                    ?>
                <h1 class="placeholder_finalprice">
                  <?php echo $initialPriceToShow; ?>
                </h1>
                <select class=" hide placeholder_discount form-control bottom-margin-20">
                  <?php foreach ($discounts as $discount) { ?>
                  <option value="<?php echo $discount['quantity'].'_'.$discount['price']; ?>">
                    <?php echo $discount['price']; ?> -
                    <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
                  <?php } ?>
                </select>
                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block">
                  <?php echo $button_cart; ?>
                </button>
                <div id="downloadArea">

                </div>
            </div>
          </div>
          <?php if ($minimum > 1) { ?>
          <div class="hidden alert alert-info">
            <i class="fa fa-info-circle"></i>
            <?php echo $text_minimum; ?>
          </div>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <!--div class="col-md-4">
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
              <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
              <?php } ?>
              <li><?php //echo $text_model; ?> <?php //echo $model; ?></li>
              <?php if (!$reward && 0) { ?>
                <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
                <?php } ?>
                <li><?php //echo $text_stock; ?> <?php //echo $stock; ?></li>
              </ul>
              <?php if ($price) { ?>
                <ul class="list-unstyled">
                  <?php if (!$special) { ?>
                    <li>
                      <h2><?php echo $price; ?></h2>
                    </li>
                    <?php } else { ?>
                      <li><span class="line-through"><?php echo $price; ?></span></li>
                      <li>
                        <h2><?php echo $special; ?></h2>

                      </li>
                      <?php } ?>
                      <?php if ($tax) { ?>
                        <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
                        <?php } ?>
                        <?php if ($points) { ?>
                          <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                          <?php } ?>
                          <?php if ($discounts) { ?>
                            <li>
                              <hr>
                            </li>
                            <?php foreach ($discounts as $discount) { ?>
                              <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                              <?php } ?>
                              <?php } ?>
                            </ul>

            <?php } ?>
        </div-->

    <?php echo $column_right; ?>
  </div>
</div>
<script type="text/javascript">
  $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
    $.ajax({
      url: 'index.php?route=product/product/getRecurringDescription',
      type: 'post',
      data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
      dataType: 'json',
      beforeSend: function () {
        $('#recurring-description').html('');
      },
      success: function (json) {
        $('.alert, .text-danger').remove();

        if (json['success']) {
          $('#recurring-description').html(json['success']);
        }
      }
    });
  });
</script>
<script type="text/javascript">
  $('#button-cart').on('click', function () {
    $("input[type='checkbox']").attr('checked', 'checked');
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $(
        '#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'
      ),
      dataType: 'json',
      beforeSend: function () {
        $('#button-cart').button('loading');
      },
      complete: function () {
        $('#button-cart').button('reset');
      },
      success: function (json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');

        if (json['error']) {
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              var element = $('#input-option' + i.replace('_', '-'));

              if (element.parent().hasClass('input-group')) {
                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              } else {
                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              }
            }
          }

          if (json['error']['recurring']) {
            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] +
              '</div>');
          }

          // Highlight any found errors
          $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
          $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] +
            '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          //$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

          $('html, body').animate({
            scrollTop: 0
          }, 'slow');

          $('#cart > ul').load('index.php?route=common/cart/info ul li');
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script>
<script type="text/javascript">
  $('.date').datetimepicker({
    pickTime: false
  });

  $('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
  });

  $('.time').datetimepicker({
    pickDate: false
  });

  $('button[id^=\'button-upload\']').on('click', function () {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend(
      '<form enctype="multipart/form-data" id="form-upload" class="no-display"><input type="file" name="file" /></form>'
    );

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
      clearInterval(timer);
    }

    timer = setInterval(function () {
      if ($('#form-upload input[name=\'file\']').val() != '') {
        clearInterval(timer);

        $.ajax({
          url: 'index.php?route=tool/upload',
          type: 'post',
          dataType: 'json',
          data: new FormData($('#form-upload')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function () {
            $(node).button('loading');
          },
          complete: function () {
            $(node).button('reset');
          },
          success: function (json) {
            $('.text-danger').remove();

            if (json['error']) {
              $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
            }

            if (json['success']) {
              alert(json['success']);

              $(node).parent().find('input').val(json['code']);
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    }, 500);
  });
</script>
<script type="text/javascript">
  $('#review').delegate('.pagination a', 'click', function (e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
  });

  $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

  $('#button-review').on('click', function () {
    $.ajax({
      url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
      type: 'post',
      dataType: 'json',
      data: $("#form-review").serialize(),
      beforeSend: function () {
        $('#button-review').button('loading');
      },
      complete: function () {
        $('#button-review').button('reset');
      },
      success: function (json) {
        $('.alert-success, .alert-danger').remove();

        if (json['error']) {
          $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' +
            json['error'] + '</div>');
        }

        if (json['success']) {
          $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json[
            'success'] + '</div>');

          $('input[name=\'name\']').val('');
          $('textarea[name=\'text\']').val('');
          $('input[name=\'rating\']:checked').prop('checked', false);
        }
      }
    });
  });

  $(document).ready(function () {
    $('.thumbnails').magnificPopup({
      type: 'image',
      delegate: 'a',
      gallery: {
        enabled: true
      }
    });
  });
</script>
<?php echo $footer; ?>