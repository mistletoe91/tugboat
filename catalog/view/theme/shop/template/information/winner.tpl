<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/information/contact.css" rel="stylesheet"/>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach (!$breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </ul>
  <div class="row">
    <div id="content" class="main-content col-sm-12"><?php echo $content_top; ?>
      <h1 style="text-align:center"><?php echo $heading_title; ?></h1>
      <p style="text-align:center">
      We know how hard it is to run a business! Register now and let your friends at Tugboat.cc give you a hand.
     </p>


      <div class="row">

        <div class="col-sm-12 col-md-6">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">

                <div class="col-sm-12" style="text-align:center">
                  <h2>You can win! </h2>
                  <br/>
                  <br/>

                  <img class="product-icon" src="image/catalog/tugboat_icons_banners.svg" width="40px"/>

                  A roll up banner

                  <br/>
                  <br/>
                  <img class="product-icon" src="image/catalog/tugboat_icons_business-cards.svg" width="50px"/>

                  5000 business cards

                  <br/>
                  <br/>

                  5% off your next order
                 <br/>
                 <br/>
                  <b>Winners will be announced December 1st, just in time for holiday shopping.
                  Do not worry though, at Tugboat.cc #everybodyisawinner </b>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-12 col-md-6 ">
          <div class="panel panel-default">
          <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal winner-form">
            <fieldset>
              <legend><?php echo $text_contact; ?></legend>
              <div class="row">

                <div class="col-sm-12 col-md-6">
                  <div class="form-group required">
                    <label class="col-sm-12 control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <div class="col-sm-12">
                      <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                      <?php if ($error_name) { ?>
                      <div class="text-danger"><?php echo $error_name; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 col-md-6">
                  <div class="form-group required">
                    <label class="col-sm-12 control-label" for="input-email"><?php echo $entry_email; ?></label>
                    <div class="col-sm-12">
                      <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                      <?php if ($error_email) { ?>
                      <div class="text-danger"><?php echo $error_email; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group required">
                <label class="col-sm-12 control-label" for="input-enquiry"><?php echo $text_telephone; ?></label>
                <div class="col-sm-12">
                   <input type="text" name="phone" class="form-control" />
                </div>
              </div>

              <div class="form-group required">
                <label class="col-sm-12 control-label" for="input-enquiry"><?php echo $text_bus; ?></label>
                <div class="col-sm-12">
                    <input type="text" name="business" class="form-control" />

                </div>
              </div>
              <?php echo $captcha; ?>
            </fieldset>
            <div class="buttons">
              <div class="pull-left">
                <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
              </div>
                <h1><?php echo $success; ?></h1>
            </div>
          </form>
        </div>
        </div>
        </div>


      </div>


      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
