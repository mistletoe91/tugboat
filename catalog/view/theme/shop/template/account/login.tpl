<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/login.css" rel="stylesheet" />
<!--<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>-->
<div class="container main-content">
  <div class="row section hidden-xs">
    <?php echo $column_left; ?>
    <div id="content">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
          <div class="well">
            <h2>
              <?php echo $text_returning_customer; ?>
            </h2>
            <?php if ($success) { ?>
            <div class="alert alert-success">
              <?php echo $success; ?>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger">
              <?php echo $error_warning; ?>
            </div>
            <?php } ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email">
                  <?php echo $entry_email; ?>
                </label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email"
                  class="form-control" required minlength="1" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">
                  <?php echo $entry_password; ?>
                </label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password"
                  class="form-control" />
                <a class="link-forgot-password" href="<?php echo $forgotten; ?>">
                  <?php echo $text_forgotten; ?>
                </a>
              </div>
              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
            <div class="row registration-link-container">
              <div class="col-xs-12 link-separator">
                <span></span>
                <h3>New to Tugboat?</h3>
              </div>
              <a class="btn btn-secondary" href="<?php echo $register; ?>">
                <?php echo $text_register; ?>
              </a>
            </div>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="section visible-xs">
    <div class="container-login-form">
      <div class="row container-section-header">
        <span class="header-line background-tugboat-blue"></span>
        <div id="container-header-login" class="section-header">
          <div id="header-login" class="container-background"></div>
          <img class="section-header-icon" src="image/catalog/tugboat_icons_login_blue.svg">
        </div>
        <span class="header-line background-tugboat-blue"></span>
      </div>
      <div class="container-section-headline text-center">
        <h3>Login</h3>
      </div>
      <div class="container-section-content content">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
            <?php if ($success) { ?>
            <div class="alert alert-success">
              <?php echo $success; ?>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger">
              <?php echo $error_warning; ?>
            </div>
            <?php } ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="login-form">
              <div class="form-group">
                <label class="control-label" for="input-email">
                  <?php echo $entry_email; ?>
                </label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="mobile-input-email"
                  class="form-control" required minlength="1" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password">
                  <?php echo $entry_password; ?>
                </label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="mobile-input-password"
                  class="form-control" />
                <a class="link-forgot-password" href="<?php echo $forgotten; ?>">
                  <small><?php echo $text_forgotten; ?></small>
                </a>
              </div>
              <input id="loginBtn" type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
            <div class="registration-link-container">
              <div class="col-xs-12 link-separator">
                <span></span>
                <h3>New to Tugboat?</h3>
              </div>
              <a id="registerBtn" class="btn btn-secondary" href="<?php echo $register; ?>">
                <?php echo $text_register; ?>
              </a>
            </div>
          </div>
        </div>
        <?php echo $content_bottom; ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>