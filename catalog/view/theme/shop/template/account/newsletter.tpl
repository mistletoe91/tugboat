<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/newsletter.css" rel="stylesheet"/>
<div class="container">
    <div class="row">
        <div id="content" class="main-content col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <a title="Go back" href="<?php echo $back; ?>" class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></a>
            <h1><?php echo $heading_title; ?></h1>
            <!-- <legend><?php echo $heading_title_info; ?></legend> -->
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="form-group">
                        <label class="col-xs-12 control-label"><?php echo $entry_newsletter; ?></label>
                        <div class="col-xs-12">
                            <?php if ($newsletter) { ?>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="1" checked="checked" />
                                    <?php echo $text_yes; ?> 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="0" />
                                    <?php echo $text_no; ?>
                                </label>
                            <?php } else { ?>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="1" />
                                    <?php echo $text_yes; ?> 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="newsletter" value="0" checked="checked" />
                                    <?php echo $text_no; ?>
                                </label>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>
                <div class="container-buttons">
                    <input type="submit" value="Save" class="btn btn-primary" />
                    <!-- <a href="<?php echo $back; ?>" class="btn btn-secondary">Cancel</a> -->
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>