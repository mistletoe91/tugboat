<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/password.css" rel="stylesheet"/>
<div class="container">
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
    <div class="row">
        <div id="content" class="main-content col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <a title="Go back" href="<?php echo $back; ?>" class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></a>
            <h1><?php echo $heading_title; ?></h1>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <!-- <legend><?php echo $text_password; ?></legend> -->
                    <div class="form-group required">
                        <label class="col-xs-12 control-label" for="input-password"><?php echo $entry_password; ?></label>
                            <div class="col-xs-12">
                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                <?php if ($error_password) { ?>
                                <div class="text-danger"><?php echo $error_password; ?></div>
                                <?php } ?>
                            </div>
                    </div>
                        <div class="form-group required">
                        <label class="col-xs-12 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                        <div class="col-xs-12">
                            <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                            <?php if ($error_confirm) { ?>
                            <div class="text-danger"><?php echo $error_confirm; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>
                <div class="container-buttons">
                    <input type="submit" value="Save" class="btn btn-primary" />
                    <!-- <a href="<?php echo $back; ?>" class="btn btn-secondary"><?php echo $button_back; ?></a> -->
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>