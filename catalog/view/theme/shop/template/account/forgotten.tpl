<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/forgotten.css" rel="stylesheet"/>
<div class="container container-main-content">
    <?php $class = 'col-xs-12 col-sm-6 col-md-5 col-lg-4'; ?>
    <div id="content" class="<?php echo $class; ?> content"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p class="form-instructions"><?php echo $text_email; ?></p>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><?php echo $error_warning; ?></div>
        <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

          <div class="form-group">
            <label class="col-xs-12 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
          </div>
        <div class="buttons col-xs-12">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
            <div class="col-xs-12 link-separator">
                <span></span>
                <img class="img-icon-separator" src="image/catalog/logo-small.png"/>
            </div>
            <a href="<?php echo $back; ?>" class="btn btn-secondary"><?php echo $button_back; ?></a>
        </div>
      </form>
      <?php //echo $content_bottom; ?>
      </div>
      <?php //echo $column_right; ?>
</div>
<?php echo $footer; ?>
