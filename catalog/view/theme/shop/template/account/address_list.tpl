<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/address_list.css" rel="stylesheet"/>
<div class="container">
      <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
    <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
        <div class="alert alert-warning">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        </div>
    <?php } ?>
    <div class="row">
        <div id="content" class="main-content col-xs-12 col-sm-6 col-md-5 col-lg-4">
            <a title="Go back" href="<?php echo $back; ?>" class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></a>
            <h2><?php echo $text_address_book; ?></h2>
            <legend><?php echo $text_address_book_info; ?></legend>
            <?php if ($addresses) { ?>
                <div class="container_address-table">
                    <!-- <table class="address-table"> -->
                    <?php foreach ($addresses as $result) { ?>
                    <table class="address-table">
                        <tr class="address-table_row">
                            <td class="address-table_cell"><?php echo $result['address']; ?></td>
                        </tr>
                        <tr class="address-table_row">
                            <td class="address-table_cell">
                                <a href="<?php echo $result['update']; ?>" class="a-link-normal"><?php echo $button_edit; ?></a>
                                <span class="a-link-normal_line">|</span>
                                <a href="<?php echo $result['delete']; ?>" class="a-link-normal"><?php echo $button_delete; ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </table>
                </div>
            <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
            <?php } ?>
            <div class="container-buttons">
                <a href="<?php echo $add; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>