<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/account/account.css" rel="stylesheet"/>
<div class="container main-content">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
    <div class="row"><?php echo $column_left; ?>
    <?php $class = 'col-sm-12'; ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <ul class="container-account-buttons account-buttons-top">
                <li class="container-account-button">
                    <a class="account-button">
                        <div class="container-account-button-items">
                            <div class="container-account-icon">
                                <img class="account-icon" src="image/catalog/tugboat_vector-website-icons_settings.svg"/>
                            </div>
                            <div class="container-account-text">
                                <p class="text-heading">Settings</p>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>

            <ul class="container-account-buttons">
                <li class="container-account-button">
                    <!-- <a class="account-button" href="<?php echo $edit; ?>"> -->
                        <div class="container-account-button-items" data-toggle="modal" data-target="#information">
                            <div class="container-adjusted-height">
                                <div class="container-account-icon">
                                    <img class="account-icon information-icon" src="image/catalog/tugboat_vector-website-icons_information.png"/>
                                </div>
                                <div class="container-account-text">
                                    <p class="text-heading">Information</p>
                                </div>
                            </div>
                        </div>
                    <!-- </a> -->
                </li>
                <li class="container-account-button">
                    <!-- <a class="account-button" href="<?php echo $password; ?>"> -->
                        <div class="container-account-button-items" data-toggle="modal" data-target="#password">
                            <div class="container-adjusted-height">
                                <div class="container-account-icon">
                                    <img class="account-icon password-icon" src="image/catalog/tugboat_vector-website-icons_password.png"/>
                                </div>
                                <div class="container-account-text">
                                    <p class="text-heading">Password</p>
                                </div>
                            </div>
                        </div>
                    <!-- </a> -->
                </li>
                <li class="container-account-button">
                    <a class="account-button" href="<?php echo $address; ?>">
                       <!-- data-toggle="modal" data-target="#address" -->
                        <div class="container-account-button-items">
                            <div class="container-adjusted-height">
                                <div class="container-account-icon">
                                    <img class="account-icon addresses-icon" src="image/catalog/tugboat_vector-website-icons_address.png"/>
                                </div>
                                <div class="container-account-text">
                                    <p class="text-heading">Addresses</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="container-account-button">
                    <!-- <a class="account-button" href="<?php echo $transaction; ?>"> -->
                        <div class="container-account-button-items" data-toggle="modal" data-target="#transactions">
                            <div class="container-adjusted-height">
                                <div class="container-account-icon">
                                    <img class="account-icon transactions-icon" src="image/catalog/tugboat_vector-website-icons_transactions.png"/>
                                </div>
                                <div class="container-account-text">
                                    <p class="text-heading">Transactions</p>
                                </div>
                            </div>
                        </div>
                    <!-- </a> -->
                </li>
                <li class="container-account-button">
                    <!-- <a class="account-button" href="<?php echo $newsletter; ?>"> -->
                        <div class="container-account-button-items" data-toggle="modal" data-target="#newsletter">
                            <div class="container-adjusted-height">
                                <div class="container-account-icon">
                                    <img class="account-icon newsletter-icon" src="image/catalog/tugboat_vector-website-icons_newsletter.png"/>
                                </div>
                                <div class="container-account-text">
                                    <p class="text-heading">Newsletter</p>
                                </div>
                            </div>
                        </div>
                    <!-- </a> -->
                </li>
            </ul>

            <!-- Button trigger modal -->

            <!-- Modal information -->
            <div class="modal fade" id="information" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $text_your_details; ?></h4>

                  </div>
                  <div class="modal-body">

                    <!-- <h1><?php echo $heading_title; ?></h1> -->
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="formInfo">
                      <fieldset>
                        <!-- <legend class="sub-heading"><?php echo $text_your_details; ?></legend> -->
                        <div class="form-group required">
                          <label class="col-xs-12 control-label" for="input-firstname"><?php echo $entry_firstname; ?> </label>
                          <div class="col-xs-12">
                            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                            <?php if ($error_firstname) { ?>
                            <div class="text-danger"><?php echo $error_firstname; ?></div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group required">
                          <label class="col-xs-12 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                          <div class="col-xs-12">
                            <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                            <?php if ($error_lastname) { ?>
                            <div class="text-danger"><?php echo $error_lastname; ?></div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group required">
                          <label class="col-xs-12 control-label" for="input-email"><?php echo $entry_email; ?></label>
                          <div class="col-xs-12">
                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            <?php if ($error_email) { ?>
                            <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="form-group required">
                          <label class="col-xs-12 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                          <div class="col-xs-12">
                            <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                            <?php if ($error_telephone) { ?>
                            <div class="text-danger"><?php echo $error_telephone; ?></div>
                            <?php } ?>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveInformation">Save Information</button>
                    <span id="error"></span>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal password -->
            <div class="modal fade" id="password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Change your password</h4>
                  </div>
                  <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="changePassword">
                        <fieldset>
                            <!-- <legend><?php echo $text_password; ?></legend> -->
                            <div class="form-group required">
                                <label class="col-xs-12 control-label" for="input-password"><?php echo $entry_password; ?></label>
                                    <div class="col-xs-12">
                                        <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                        <?php if ($error_password) { ?>
                                        <div class="text-danger"><?php echo $error_password; ?></div>
                                        <?php } ?>
                                    </div>
                            </div>
                                <div class="form-group required">
                                <label class="col-xs-12 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                                <div class="col-xs-12">
                                    <input type="password" name="confirm" value="" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                                    <?php if ($error_confirm) { ?>
                                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePassword">Save changes</button>
                    <span id="pwdError"></span>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal information -->
            <div class="modal fade" id="address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Address</h4>
                  </div>
                  <div class="modal-body">

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal transaction -->
            <div class="modal fade" id="transactions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $heading_title; ?></h4>
                  </div>
                  <div class="modal-body">

                    <legend><?php echo $heading_title_info; ?></legend>
                    <p><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-left"><?php echo $column_date_added; ?></td>
                                    <td class="text-left"><?php echo $column_description; ?></td>
                                    <td class="text-right"><?php echo $column_amount; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($transactions) { ?>
                                <?php foreach ($transactions  as $transaction) { ?>
                                <tr>
                                    <td class="text-left"><?php echo $transaction['date_added']; ?></td>
                                    <td class="text-left"><?php echo $transaction['description']; ?></td>
                                    <td class="text-right"><?php echo $transaction['amount']; ?></td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="5"><?php echo $text_empty; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"><?php echo $results; ?></div>
                        <div class="col-xs-12"><?php echo $pagination; ?></div>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal information -->
            <div class="modal fade" id="newsletter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $heading_title; ?></h4>
                  </div>
                  <div class="modal-body">

                          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-xs-12 control-label"><?php echo $entry_newsletter; ?></label>
                                    <div class="col-xs-12">
                                        <?php if ($newsletter) { ?>
                                            <label class="radio-inline">
                                                <input type="radio" name="newsletter" value="1" checked="checked" />
                                                <?php echo $text_yes; ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="newsletter" value="0" />
                                                <?php echo $text_no; ?>
                                            </label>
                                        <?php } else { ?>
                                            <label class="radio-inline">
                                                <input type="radio" name="newsletter" value="1" />
                                                <?php echo $text_yes; ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="newsletter" value="0" checked="checked" />
                                                <?php echo $text_no; ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveNewsleter">Save </button>
                  </div>
                </div>
              </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">
  $("#saveInformation").click(function(e){
    e.preventDefault();
    var formInput = $("#formInfo").serializeArray();
    var json = {};

    $.each(formInput, function(){
      json[this.name] = this.value || '';
    });

    $.ajax({
      type: "POST",
      url: "/index.php?route=api/setting/edit",
      data: json ,
      dataType: "json",
      success: function(data) {
        if(data == 1) {
          location.reload();
        }else{
          var list =[];
          if(data.telephone){ list.push(data.telephone); }
          if(data.firstname){ list.push(data.firstname); }
          if(data.lastname) { list.push(data.lastname);  }
          if(data.email)    { list.push(data.email);     }
          if(data.warning)  { list.push(data.warning);   }

          $("#error").html(
            '<div class="alert alert-danger">'
            +
             list.toString()
            +
            '</div>'
          );
          //console.error(data);
        }
      }
    });
  });

  $("#savePassword").click(function(e){

    e.preventDefault();
    var formInput = $("#changePassword").serializeArray();
    var pwd = {};

    $.each(formInput, function(){
      pwd[this.name] = this.value || '';
    });

    $.ajax({
      type: "POST",
      url: "/index.php?route=api/setting/changePassword",
      data: pwd,
      dataType: 'json',
      success: function (data) {
        if(data == 1) {
          location.reload();
        }else{
          if(data.password){
            $("#pwdError").html(
              '<div class="alert alert-danger">' + data.password + '</div>'
            );
          }

          if(data.confirm){
            $("#pwdError").html(
              '<div class="alert alert-danger">' + data.confirm + '</div>'
            );
          }
        }
      }
    });

  });

  $("#saveNewsleter").click(function (e){
    e.preventDefault();

    var newsletter = $('input[type=radio]:checked').val();

    $.ajax({
      type : "POST",
      url : "/index.php?route=api/setting/newsLetter",
      data : {'newsletter' : newsletter},
      dataType : "json",
      success : function(data){
        if(data == 1) {
          location.reload();
        }else{
          console.error(data);
        }
      }
    });

  });
</script>
<?php echo $footer; ?>
