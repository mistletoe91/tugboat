<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#EB882D" />
  <title>
    <?php echo $title; ?>
  </title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>

  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/tether/css/tether.min.css" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/tether/js/tether.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <?php /*
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
*/ ?>

  <link href="//fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,700,900" rel="stylesheet">


  <link href="catalog/view/theme/default/stylesheet/common/header.css" rel="stylesheet">
  <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/main.min.css">
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>"
  />
  <?php } ?>
  <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>
</head>

<body class="<?php echo $class; ?> mainbody">
  <nav class="navbar fixed-top navbar-toggleable-xl navbar-expand navbar-light py-0 vcenter">
    <div class="navbar-collapse collapse" id="navbarSupportedContent">
      <a class="navbar-brand" href="#">
        <img src="<?php echo $logo; ?>" height="25" class="d-inline-block" title="<?php echo $name; ?>" alt="<?php echo $name; ?>">
        <span class="text-logo">tugboat.cc</span>
      </a>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown dropdown-height">
          <?php if ($logged) { ?>
            <a href="#" class="nav-link dropdown-toggle" id="navbarAccountDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span class="header-text-myaccount">
                Test Person<!-- <?php echo $customer_name; ?> -->
              </span>
              <span><i class="fa fa-caret"></i></span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="<?php echo $setting; ?>">
                <?php echo $text_setting; ?>
              </a>
              <a class="dropdown-item" href="<?php echo $logout; ?>">
                <?php echo $text_logout; ?>
              </a>
            </div>
          <?php } else { ?>
            <a href="<?php echo $login; ?>" class="header-login-link" aria-expanded="false">
              <i class="fa fa-sign-in" aria-hidden="true"></i>
              <span class="header-text-login">
                <?php echo $text_login;?>
              </span>
            </a>
            <div class="dropdown-menu"></div>
          <?php } ?>
        </li>
        <li class="nav-item">
          <?php echo $cart; ?>
        </li>
      </ul>
      <!-- <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link link-enterprise" href="index.php?route=common/home#sectionEnterprise">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_enterprise.svg">
            <span class="header-text-enterprise">Enterprise</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link link-whyus" href="index.php?route=common/home#sectionWhyUs">
            <img class="img-header-icon" src="image/catalog/tugboat_icons_why-us_question-mark.svg">
            <span class="header-text-whyus">Why Us</span>
          </a>
        </li>
      </ul> -->
    </div>
  </nav>
  <div class="toppaddingbecauseofnav"></div>

  <?php if ($categories && 0) { ?>
  <div class="container">
    <nav id="menu" class="navbar">
      <div class="navbar-header">
        <span id="category" class="visible-xs">
          <?php echo $text_category; ?>
        </span>
        <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown">
              <?php echo $category['name']; ?>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li>
                    <a href="<?php echo $child['href']; ?>">
                      <?php echo $child['name']; ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
              <a href="<?php echo $category['href']; ?>" class="see-all">
                <?php echo $text_all; ?>
                <?php echo $category['name']; ?>
              </a>
            </div>
          </li>
          <?php } else { ?>
          <li>
            <a href="<?php echo $category['href']; ?>">
              <?php echo $category['name']; ?>
            </a>
          </li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
  </div>
  <?php } ?>

  <script>

    $(function () {

      var isDropdownActive = false;

      $('#toggleDropDownLinks').click(function (event) {
        event.stopPropagation();
        $('#toggleDropDownLinks').toggleClass('active-header-dropdown-parent');
        $('#dropDownLinksToToggle').toggleClass('active-header-dropdown');
        (isDropdownActive) ? isDropdownActive = false : isDropdownActive = true;
      })
      $(document).click(function () {
        if (isDropdownActive) {
          $('#toggleDropDownLinks').toggleClass('active-header-dropdown-parent');
          $('#dropDownLinksToToggle').toggleClass('active-header-dropdown');
          isDropdownActive = false;
        }
      });

    });

  </script>