<div class="botpaddingbecauseoffooter hidden-xs"></div>
<footer>
    <div class="container hidden-xs text-center">
        <div class="row">
            <!--             <div class="col-sm-3">
                <h5>Information</h5>
                <ul class="list-unstyled">
                                    <li><a href="http://local.tugboat.cc/about_us">About Us</a></li>
                                    <li><a href="http://local.tugboat.cc/delivery">Delivery Information</a></li>
                                    <li><a href="http://local.tugboat.cc/privacy">Privacy Policy</a></li>
                                    <li><a href="http://local.tugboat.cc/terms">Terms &amp; Conditions</a></li>
                                </ul>
            </div>
         -->
            <div class="col-xs-12 footer-logo-container">
                <a href="index.php?route=common/home">
                    <img src="image/catalog/Tugboat-White-02.png">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="<?php echo $career; ?>">Careers</a>
            </div>
            <div class="col-xs-2">
                <a href="<?php echo $tnc; ?>">Terms &amp; Conditions</a>
            </div>
            <div class="col-xs-4">
                <a href="/">Tugboat.cc © 2017</a>
            </div>
            <div class="col-xs-2">
                <a href="<?php echo $pnp; ?>">Privacy Policy</a>
            </div>
            <div class="col-xs-2">
                <a href="<?php echo $contact; ?>">Contact</a>
            </div>
            <!-- <div class="col-sm-3">
        <h5>Customer Service</h5>
        <ul class="list-unstyled">
          <li><a href="http://local.tugboat.cc/index.php?route=information/contact">Contact Us</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=account/return/add">Returns</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=information/sitemap">Site Map</a></li>
        </ul>
      </div> -->
            <!-- <div class="col-sm-3">
        <h5>Extras</h5>
        <ul class="list-unstyled">
          <li><a href="http://local.tugboat.cc/index.php?route=product/manufacturer">Brands</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=account/voucher">Gift Certificates</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=affiliate/account">Affiliates</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=product/special">Specials</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5>My Account</h5>
        <ul class="list-unstyled">
          <li><a href="http://local.tugboat.cc/index.php?route=account/account">My Account</a></li>
          <li><a href="http://local.tugboat.cc/index.php?route=account/order">Order History</a></li>
                    <li><a href="http://local.tugboat.cc/index.php?route=account/newsletter">Newsletter</a></li>
        </ul>
      </div> -->
        </div>
    </div>
    <div class="mobile-footer-content visible-xs">
        <div class="row no-gutters text-center">
            <div class="col">
                <a href="<?php echo $career; ?>">Careers</a>
            </div>
            <div class="col">
                <a href="<?php echo $tnc; ?>">Terms &amp; Conditions</a>
            </div>
            <div class="col">
                <a href="/">Tugboat.cc © 2018</a>
            </div>
            <div class="col">
                <a href="<?php echo $pnp; ?>">Privacy Policy</a>
            </div>
            <div class="col">
                <a href="<?php echo $contact; ?>">Contact</a>
            </div>
        </div>
    </div>
    <!-- <p class="text-center no-bot"><img width="36.44" height="50" src="image/catalog/Tugboat-White-02.png" /><br /> Tugboat.cc &copy; 2017</p> -->

</footer>
</body></html>
