<?php echo $header; ?>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pretty-checkbox/3.0.0/pretty-checkbox.min.css">
<script src="catalog/view/javascript/anime.min.js" type="text/javascript"></script>
<script type="text/javascript" src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js"></script>
<script src="catalog/view/javascript/homepage.js" type="text/javascript"></script>
<!-- <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/animate.css">
<script src="catalog/view/javascript/wow.min.js" type="text/javascript"></script>
<script> new WOW().init(); </script> -->
<meta name="theme-color" content="#FFFFFF">

<div class="body">
    <div class="body-container container">
        <?php //echo $shop_header; ?>
        <div class="content-row">
            <div class="row">
                <?php echo $sidebar; ?>
                <div class="col-xs-12 col-sm-10 main-content">
                    <div class="raw call-to-action">
                        <div class="col-xs-6 col-xs-offset-6 printer-register text-center">
                            <h2>Don't have an account?</h2>
                            <p>Join our community of 8950 printers</p>
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-lg" id="registerName" placeholder="Full Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-lg" id="registerEmail" placeholder="E-mail Address">
                                </div>
                                <button type="submit" class="btn btn-danger btn-lg">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="vendors">
                        <div class="col-xs-12 vendor text-center">
                            <h5>Proud print solutions for the following industry-leading companies:</h5>
                            <img src="image/catalog/SocialProofMin.jpg" class="img-responsive">
                        </div>
                    </div>
                    <!--div class="row reviews">
                        <div class="col-xs-12 reviews-title">
                            <h5>Real Reviews From Real Customers</h5>
                        </div>
                        <div class="col-xs-12 reviews-content">
                            <div class="row">
                                <div class="col-xs-3 card">
                                    <div class="card-block">
                                        <h4 class="card-title">Review Title</h4>
                                        <p class="card-text">Review Text</p>
                                    </div>
                                </div>
                                <div class="col-xs-3 card">
                                    <div class="card-block">
                                        <h4 class="card-title">Review Title</h4>
                                        <p class="card-text">Review Text</p>
                                    </div>
                                </div>
                                <div class="col-xs-3 card">
                                    <div class="card-block">
                                        <h4 class="card-title">Review Title</h4>
                                        <p class="card-text">Review Text</p>
                                    </div>
                                </div>
                                <div class="col-xs-3 card">
                                    <div class="card-block">
                                        <h4 class="card-title">Review Title</h4>
                                        <p class="card-text">Review Text</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="footer-container container">
        <div class="row">
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>
                        Secure Payment Methods
                    </h5>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>Business Hours</h5>
                </div>
                <div class="footer-links">
                    <p>Monday to Friday</p>
                    <p>Customer Service: 8 AM to 5 PM EST</p>
                    <p>Local Pickup: 8 AM to 9 PM EST</p>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>Tugboat</h5>
                </div>
                <div class="footer-links">
                    <a href="#">
                        <p>About Tugboat</p>
                    </a>
                    <a href="#">
                        <p>Reviews</p>
                    </a>
                    <a href="#">
                        <p>Careers</p>
                    </a>
                    <a href="#">
                        <p>Terms of Service</p>
                    </a>
                    <a href="#">
                        <p>Privacy Policy</p>
                    </a>
                    <a href="#">
                        <p>Sitemap</p>
                    </a>
                    <a href="#">
                        <p>Contact Us</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>Our Services</h5>
                </div>
                <div class="footer-links">
                    <a href="#">
                        <p>Shipping Options</p>
                    </a>
                    <a href="#">
                        <p>Turnaround Options</p>
                    </a>
                    <a href="#">
                        <p>Direct Mail</p>
                    </a>
                    <a href="#">
                        <p>Custom Quotes</p>
                    </a>
                    <a href="#">
                        <p>Submit Custom Order</p>
                    </a>
                    <a href="#">
                        <p>Branded Websites</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>Resources</h5>
                </div>
                <div class="footer-links">
                    <a href="#">
                        <p>Support Center</p>
                    </a>
                    <a href="#">
                        <p>Artwork Setup Guides</p>
                    </a>
                    <a href="#">
                        <p>Free Delivery Eligibility</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="footer-category">
                    <h5>Connect</h5>
                </div>
                <div class="footer-links">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // const showTertiaryCategories = () => {
    //     const subCategories = document.getElementsByClassName('sub-category');
    //     for (let i = 0; i < subCategories.length; i += 1) {
    //         const subCategory = subCategories[i];
    //         subCategory.addEventListener('mouseover', ev => {
    //             const topSellers = document.getElementById('top-sellers');
    //             topSellers.style.display = 'block';
    //             topSellers.addEventListener('mouseover', ev => {
    //                 topSellers.style.display = 'block';
    //             });
    //             topSellers.addEventListener('mouseout', ev => {
    //                 topSellers.style.display = 'none';
    //             });
    //             subCategory.addEventListener('mouseout', ev => {
    //                 topSellers.style.display = 'none';
    //             });
    //         });
    //     }
    // }

    // $(document).ready(() => {
    //     showTertiaryCategories();
    // });
</script>

<?php echo $footer; ?>