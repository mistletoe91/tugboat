<div class="navigation-row">
  <div class="row">
    <div class="col-xs-2 brand-image my-auto">
      <img class="img-responsive" src="image/catalog/logo.jpg">
    </div>
    <div class="col-xs-7 search-bar my-auto">
      <input type="text" class="form-control" placeholder="Search here..."></input>
    </div>
    <div class="col-xs-1 my-auto">
      <i class="fa fa-shopping-cart"></i> Cart</div>
    <div class="col-xs-2 my-auto">
      <div class="row">
        <div class="col-xs-5">
          <button class="btn btn-sm btn-success">Register</button>
        </div>
        <div class="col-xs-5">
          <button class="btn btn-sm btn-primary">Login</button>
        </div>
      </div>
    </div>
  </div>
  <div class="row support-row">
    <div class="col-xs-7 col-xs-offset-2">
      <div class="row">
        <div class="col-xs-3">
          <i class="fa fa-envelope"></i>
          <span class="text-danger"> Support Ticket</span>
        </div>
        <div class="col-xs-3">
          <i class="fa fa-phone"></i> 1-800-123-4567
        </div>
        <div class="col-xs-2">
          <i class="fa fa-comment-o"></i> Live Chat
        </div>
        <div class="col-xs-3">
          <i class="fa fa-question"></i> Support Center
        </div>
      </div>
    </div>
  </div>
</div>