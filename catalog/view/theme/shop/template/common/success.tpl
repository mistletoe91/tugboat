<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/common/success.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,400,700,900" rel="stylesheet">
<div class="container success-page">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </ul>
  <div class="row">
    <div id="content" class="col-sm-12">
      <h1><?php echo $heading_title; ?></h1>
      <div class="alert alert-success">
          <?php echo $text_message; ?>
      </div>

      <div class="buttons">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-secondary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?>
    </div>
</div>
</div>
<?php echo $footer; ?>
