<style>
    .header {
    font-size: 12px;
    background-color: rgb(235, 235, 235);
    margin-bottom: 10px;
    }

    .call-to-action {
    margin-bottom: 16px;
    }

    .btn:hover {
    cursor: pointer;
    }

    .header-container {
    padding-top: 5px;
    padding-bottom: 5px;
    }

    .navigation-row {
    padding-top: 20px;
    padding-bottom: 20px;
    }

    .support-row {
    font-size: 14px;
    padding-left: 30px;
    }

    .support-row div:hover {
    cursor: pointer;
    }

    .sub-categories {
    margin-bottom: 15px;
    }

    .sidebar h5 {
    font-size: 16px;
    }

    .sub-category {
    color: black;
    }

    .sub-category:hover {
    color: lightblue;
    text-decoration: none;
    }

    .tertiary-categories {
        box-shadow: 1px 1px black;
        background-color: white;
        display: none;
        position: absolute;
        padding: 10px;
        margin-left: 50px;
    }

    .sidebar p {
    line-height: 25px;
    font-size: 12px;
    margin-bottom: 0px;
    text-indent: 5px;
    }

    .sub-category:not(:last-child) p {
    border-bottom: 1px solid rgba(0, 0, 0, 0.25);
    }
    .account-link:not(:last-child) {
    border-right: 1px solid rgba(0, 0, 0, 0.25);
    }

    .account-link:hover {
    color: lightblue;
    cursor: pointer;
    }

    .brand-image img {
    object-fit: contain;
    }

    .call-to-action {
    height: 375px;
    background-image: url("image/catalog/new-welcome-page-min.jpg");
    border-radius: 15px;
    }

    .vendor h5 {
    font-size: 15px;
    color: rgb(155, 155, 155);
    }

    .printer-register {
    margin-top: 15px;
    margin-bottom: 15px;
    right: 15px;
    background-color: white;
    border-radius: 15px;
    padding: 35px;
    border: 3px solid rgb(25, 66, 115);
    }

    .printer-register h2 {
    color: rgb(13, 89, 149);
    }

    .printer-register p {
    color: rgba(0, 0, 0, 0.5);
    font-size: 13px;
    }

    .printer-register .form-control {
    border: 3px solid rgba(0, 0, 0, 0.25);
    border-radius: 50px;
    }

    .printer-register .btn {
    border-radius: 50px;
    width: 100%;
    }

    .reviews-content {
    margin-top: 15px;
    min-height: 300px;
    }

    .reviews-title {
    padding-top: 5px;
    padding-bottom: 5px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.25);
    }

    .footer-container {
    border-top: 1px solid rgba(0, 0, 0, 0.25);
    padding-top: 5px;
    }

    .footer-category h5 {
    color: rgb(2, 75, 158);
    font-size: 18px;
    font-weight: 700;
    }

    .footer-links p {
    font-size: 14px;
    line-height: 19px;
    margin-bottom: 0px;
    text-decoration: none;
    }

    #nav ul{
       list-style-type: none;
       position:relative;
       left:0;
       background:#fff;
       padding:0px;
       border-radius: 5px;
    }

    #nav ul li{
        float:none;
        min-width:100%;
        line-height:115%;
        padding:5px 0px;
    }

    #nav ul li:not(:last-child) {
      border-bottom: 1px solid rgba(0, 0, 0, 0.25);
    }

    #nav ul ul{
        left:100%;
        display:none;
        position: absolute;
        z-index: 1;
        min-width: 200px;
        color: rgb(241,128,50) !important;
        border: 1px solid #ffffec;
        box-shadow: 2px 2px;

    }

    #nav ul ul ul{
      left:100%;
      display:none;
      position: absolute;
      z-index: 1;
      min-width: 220px;
      color: rgb(241,128,50) !important;
      border: 1px solid #ffffec;
      box-shadow: 2px 2px;

    }

    #nav ul ul li:not(:last-child) {
      border-bottom: none;

    }

    #nav ul a{
       line-height:150%;
       padding:10px 15px;

    }


    #nav ul li:hover > ul{
        display:inline-table;
    }

    #nav ul ul li:hover > ul{
      display:inline-table;
    }

    #nav ul li:hover{
      /*background-color:rgb(118,167,190);*/
      padding-right:10px;
      color: rgb(241,128,50);
      font-size:1.0em;
    }

    #nav ul li a:hover{
      /*background-color:rgb(118,167,190);*/
      padding-right:10px;
      color: rgb(241,128,50);
      font-size:1.0em;
    }

    .menu{
      display:block;
    }

    .category{
      background-color: rgb(0,87,150);
      border-radius: 5px;
      padding: 10px;
      color: #ffffff !important;
      font-size:0.75em;
      box-shadow: 0px 2px 4px 0px rgba(0,0.6,0,0.4);
      text-transform: uppercase;
    }

     @media(max-width : 768px){
        .category{
           display:none !important;
        }

       .bar1, .bar2, .bar3 {
         width: 35px;
         height: 5px;
         background-color: #333;
         margin: 6px 0;
         transition: 0.4s;
       }

       .change .bar1 {
         -webkit-transform: rotate(-45deg) translate(-9px, 6px);
         transform: rotate(-45deg) translate(-9px, 6px);
       }

       .change .bar2 {opacity: 0;}

       .change .bar3 {
         -webkit-transform: rotate(45deg) translate(-8px, -8px);
         transform: rotate(45deg) translate(-8px, -8px);
       }

       .menu{
         display:none;
       }

       #nav ul ul {
         left: 0%;
         position: relative;
       }

       #nav ul ul li:hover > ul{
         display:none;
       }
     }

</style>
<div class="raw col-sm-2 sidebar">
  <div class="category">
    Shop By Department
  </div>




  <div class="sub-categories" id="nav">

    <div onclick="myFunction(this)" class="container collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
    </div>

    <ul class="menu collapse" id="bs-example-navbar-collapse-1">

      <?php
         foreach ($catList AS $info) {
           echo $info;
         }
       ?>

    </ul>


  </div>
  <div class="category">
    Shop By Product
  </div>


</div>

<script>
    function myFunction(x) {
        x.classList.toggle("change");
    }
</script>