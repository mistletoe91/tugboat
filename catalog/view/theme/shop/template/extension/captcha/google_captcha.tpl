<fieldset>
  <div class="form-group required">
    <?php if (substr($route, 0, 9) == 'checkout/') { ?>
    <label class="control-label" for="input-payment-captcha"><?php echo $entry_captcha; ?></label>
    <div id="input-payment-captcha" class="g-recaptcha" data-callback="grecaptchaSuccesss" data-expired-callback="grecaptchaExpired" data-sitekey="<?php echo $site_key; ?>"></div>
    <?php if ($error_captcha) { ?>
    <div class="text-danger"><?php echo $error_captcha; ?></div>
    <?php } ?>
    <?php } else { ?>
    <div class="col-sm-10">
      <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>" data-callback="grecaptchaSuccess" data-expired-callback="grecaptchaExpired"></div>
      <?php if ($error_captcha) { ?>
      <div class="text-danger"><?php echo $error_captcha; ?></div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</fieldset>
<script>
        var grecaptchaSuccess = function (token) {
                                $("#captchaFormField").val(token);
                                $('#captchaError').text('');
                            };
        var grecaptchaExpired = function (response) {
                                grecaptcha.reset();
                                $("#captchaFormField").val('');
                             };
</script>
<script src="//www.google.com/recaptcha/api.js" type="text/javascript" async defer></script>