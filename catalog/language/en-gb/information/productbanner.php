<?php
// Heading
$_['heading_title']  = 'Select your size';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';
$_['entry_pdf']      = 'File (PDF*)';

// Email
$_['email_subject']  = 'My document';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_phone']    = 'Telephone number is must!';
$_['error_no_pdf']   = 'File must and to be in PDF or PSD format!';
$_['error_pdf']      = 'File must be in PDF or PSD format!';
