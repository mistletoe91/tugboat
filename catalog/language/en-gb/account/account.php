<?php
// Heading
$_['heading_title']      = 'Your Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'Your Account';
$_['text_my_orders']     = 'Your Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Information';
$_['text_edit_info']     = 'Edit name, email and phone';
$_['text_password']      = 'Security';
$_['text_password_info'] = 'Change your password';
$_['text_address']       = 'Addresses';
$_['text_address_info']  = 'Edit shipping information';
$_['text_credit_card']   = 'Manage Stored Credit Cards';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Transactions';
$_['text_transaction_info']   = 'View your orders';

$_['text_newsletter']    = 'Subscriptions';
$_['text_newsletter_info']    = 'Edit newsletter preferences';


$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';