<?php
// Heading
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_password'] = 'New password';
$_['entry_confirm']  = 'Reenter new password';

// Error
$_['error_password'] = 'Password must be between 4 to 20 characters!';
$_['error_confirm']  = 'Passwords do not match!';