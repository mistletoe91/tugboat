var express = require('express'),
    fs = require('fs'),
    gm = require('gm'),
    https = require('https'),
    exec = require('child_process').exec,
    sizeOf = require('image-size'),
    fabric = require('fabric').fabric, 
    bodyParser = require('body-parser'),
    port = 9002;
 
//Global var
var postData;

var privateKey  = fs.readFileSync('/home/ubuntu/tugboat.cc.keys/tugboat.cc.key', 'utf8');
var certificate = fs.readFileSync('/home/ubuntu/tugboat.cc.keys/79ecf19f01ab8dee.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

var app = express(credentials);

var allowCrossDomain = function (req, res, next) {
      
    
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Methods', 'POST,GET,  OPTIONS');
    
    res.setHeader('Access-Control-Allow-Headers', req.header.origin);
    res.header("Access-Control-Allow-Headers", "X-Requested-With");    
    
    res.header('Access-Control-Allow-Headers', request.headers["access-control-request-headers"]);
    //res.header('Access-Control-Allow-Headers', 'Content-Type');
    
    next();
}

function randomString(length) {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}


// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json());


app.options('/', function(req, res) {    
    res.send(200);
});

app.post('/', function(req, res) { 
    
    var outputFileName =  randomString(10); 
    
 
    postData = req.body;
    var maxW = req.body.width;
    var maxH = req.body.height;
for($i=0;$i<=postData.json.objects.length;$i++){
    if(postData.json.objects [$i]){
        if(postData.json.objects [$i].type =="image" ){
            var filename = postData.json.objects [$i].src .replace(/^.*[\\\/]/, '') ;            
            if(filename != ""){
                postData.json.objects [$i].src = postData.json.objects [$i].src.replace(filename, "original_"+filename);
                
                // obtain the size of an image
                
                //imageABSlocation = postData.json.objects [$i].src.replace('http:', '/var/www/html');
                imageABSlocation = postData.json.objects [$i].src.replace('https://www.tugboat.cc', '/var/www/html/tugboat.cc');
                //console.log (imageABSlocation);

                var dimensions = sizeOf(imageABSlocation);                

                postData.json.objects [$i].scaleX = postData.json.objects [$i].scaleX*(postData.json.objects [$i].width/dimensions.width);
                postData.json.objects [$i].scaleY = postData.json.objects [$i].scaleY*(postData.json.objects [$i].width/dimensions.width);
                postData.json.objects [$i].width = dimensions.width;
                postData.json.objects [$i].height = dimensions.height;
                 
                /*
                if(maxW<dimensions.width){
                    maxW = dimensions.width;
                }
                if(maxH<dimensions.height){
                    maxH = dimensions.height;
                } 
                */
                
                /*
                console.log ("------+++++++----");
                console.log ("scaleX   : "+postData.json.objects [$i].scaleX  );
                console.log ("scaleY   : "+postData.json.objects [$i].scaleY  );
                console.log ("width   : "+postData.json.objects [$i].width  );
                console.log ("height   : "+postData.json.objects [$i].height  );
                console.log ("------+++++++----");                
                */

            }
            //console.log ("after : filename"+postData.json.objects [$i].src);
        }
    }
}//end for
            
    
        
        var dpi = 300;
        var scaleFactor = dpi / 96; 
        //var canvas = fabric.createCanvasForNode(maxW, maxH);        
        var canvas = fabric.createCanvasForNode(Math.ceil(maxW * scaleFactor), Math.ceil(maxH * scaleFactor));        
     
        //Resize
        var ctx = canvas.getContext('2d');
        //canvas.width = Math.ceil(maxW * scaleFactor);
        //canvas.height = Math.ceil(maxH * scaleFactor);   
      
        ctx.scale(scaleFactor, scaleFactor);     
     
    canvas.loadFromJSON(req.body.json, function() {
        canvas.renderAll(); 
        
        var dataUrl = canvas.toDataURL('jpeg', 1);         
        data = dataUrl.replace(/^data:image\/png;base64,/, '');

        //console.log('> Saving jpg to file ...');
        var filePath = '/var/www/html/tugboat.cc/image/u/'+outputFileName+'_hi_res';
        fs.writeFile(filePath+'.jpg', data, 'base64', function(err) {
            if (err) {
                //console.log('! Error saving jpg: ' + err);
                res.json(200, { error: 'Error saving jpg: ' + err });
            } else {
                //console.log('> jpg file saved to: ' + filePath+".jpg");               
                 
                //create CMYK format a 
                $command = "convert -units PixelsPerInch "+filePath+".jpg -profile  /var/www/html/tugboat.cc/image/u/sRGB_v4_ICC_preference.icc -profile /var/www/html/tugboat.cc/image/u/USWebCoatedSWOP.icc -density 300 "+filePath+"_cmyk.jpg";
                exec($command, (err, stdout, stderr) => {
                  if (err) {
                    // node couldn't execute the command
                    return;
                  }
                  res.json(200, { success:  filePath+"_cmyk.jpg" });    

                  // the *entire* stdout and stderr (buffered)
                  //console.log(`stdout: ${stdout}`);
                  //console.log(`stderr: ${stderr}`);
                });
                
                
            }
        });
    });
});

var sslport = 9001;
 
 
var httpsServer = https.createServer(credentials, app); 



httpsServer.listen(sslport);

app.listen(port);
console.log('> Server listening on port ' + sslport);


function setDPI(canvas, dpi) {
    // Set up CSS size.
    canvas.style.width =  canvas.width + 'px';
    canvas.style.height =  canvas.height + 'px';

    // Get size information.
    var scaleFactor = dpi / 96;
    var width = parseFloat(canvas.style.width);
    var height = parseFloat(canvas.style.height);

    // Backup the canvas contents.
    var oldScale = canvas.width / width;
    var backupScale = scaleFactor / oldScale;
    var backup = canvas.cloneNode(false);
    backup.getContext('2d').drawImage(canvas, 0, 0);

    // Resize the canvas.
    var ctx = canvas.getContext('2d');
    canvas.width = Math.ceil(width * scaleFactor);
    canvas.height = Math.ceil(height * scaleFactor);

    // Redraw the canvas image and scale future draws.
    ctx.setTransform(backupScale, 0, 0, backupScale, 0, 0);
    ctx.drawImage(backup, 0, 0);
    ctx.setTransform(scaleFactor, 0, 0, scaleFactor, 0, 0);
    
    return ctx;
}

