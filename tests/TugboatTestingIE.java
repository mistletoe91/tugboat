package testingpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TugboatTestingIE {

	public static void main(String[] args) {
		System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		WebDriver explorer = new InternetExplorerDriver(capabilities);
		
		explorer.get("http://local.tugboat.cc/index.php?route=account/register");
		explorer.findElement(By.name("firstname")).sendKeys("Test");
		explorer.findElement(By.name("lastname")).sendKeys("Test");
		explorer.findElement(By.name("email")).sendKeys("Test@mailinator.com");
		explorer.findElement(By.name("telephone")).sendKeys("Test");
		explorer.findElement(By.name("fax")).sendKeys("Test");
		explorer.findElement(By.name("password")).sendKeys("aaaa");
		explorer.findElement(By.name("confirm")).sendKeys("aaaa");
		
		
		
		
		//explorer.quit();
		
	}

}
