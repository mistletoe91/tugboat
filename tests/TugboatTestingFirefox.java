package testingpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TugboatTestingFirefox {
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		WebDriver fireFox = new FirefoxDriver();
		
		fireFox.get("http://local.tugboat.cc/index.php?route=account/register");
		
		fireFox.findElement(By.xpath("//*[@id=\"input-firstname\"]")).sendKeys("Test");
		fireFox.findElement(By.xpath("//*[@id=\"input-lastname\"]")).sendKeys("World");
		fireFox.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys("asdfasdf@mailinator.com");
		fireFox.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys("aaaa");
		fireFox.findElement(By.xpath("//*[@id=\"input-confirm\"]")).sendKeys("aaaa");
		
		
		fireFox.findElement(By.xpath("//*[@id=\"next-section\"]")).click();
		
		fireFox.findElement(By.xpath("//*[@id=\"autocomplete\"]")).sendKeys("aaaaa");
		fireFox.findElement(By.xpath("//*[@id=\"input-address-2\"]")).sendKeys("905");
		fireFox.findElement(By.xpath("//*[@id=\"input-telephone\"]")).sendKeys("1111111111");
		
		fireFox.findElement(By.xpath("//*[@id=\"next-section\"]")).click();
		
		//fireFox.quit();
	}
}
