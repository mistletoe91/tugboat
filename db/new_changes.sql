
-- New SQL changes for checkout 
-- We Need to deploy in Prod, Dev, @Eric, @Ignat
-- After that we can delete the content of this file

DROP TABLE IF EXISTS `oc_currency`;
CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.60720003, 1, '2017-07-13 18:03:32'),
(2, 'US Dollar', 'USD', '$', '', '2', 0.78490001, 1, '2017-07-13 18:03:32'),
(3, 'Euro', 'EUR', '', '€', '2', 0.68930000, 1, '2017-07-13 18:03:32'),
(4, 'CAD', 'CAD', '$', '', '2', 1.00000000, 1, '2017-07-13 18:03:32');


--
-- Table structure for table `oc_customer_authentication`
--

DROP TABLE IF EXISTS `oc_customer_authentication`;
CREATE TABLE `oc_customer_authentication` (
  `customer_authentication_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider` varchar(55) NOT NULL,
  `identifier` varchar(200) NOT NULL,
  `web_site_url` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `birth_day` varchar(255) NOT NULL,
  `birth_month` varchar(255) NOT NULL,
  `birth_year` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Indexes for table `oc_customer_authentication`
--
ALTER TABLE `oc_customer_authentication`
  ADD PRIMARY KEY (`customer_authentication_id`),
  ADD UNIQUE KEY `identifier` (`identifier`,`provider`);


--
-- AUTO_INCREMENT for table `oc_customer_authentication`
--
ALTER TABLE `oc_customer_authentication`
  MODIFY `customer_authentication_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;



  --
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
 
 
--
-- Table structure for table `oc_dqc_setting`
--

DROP TABLE IF EXISTS `oc_dqc_setting`;
CREATE TABLE `oc_dqc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_dqc_setting`
--

INSERT INTO `oc_dqc_setting` (`setting_id`, `store_id`, `name`, `value`) VALUES
(1, 0, '07/13/2017 06:20:18 pm', '{"name":"07\\/13\\/2017 06:20:18 pm","general":{"clear_session":"0","login_refresh":"0","analytics_event":"0","update_mini_cart":"1","compress":"1","min_order":{"value":"0","text":{"1":"You must have a sum more then %s to make an order "}},"min_quantity":{"value":"0","text":{"1":"You must have a quantity more then %s to make an order "}},"config":"d_quickcheckout","social_login":"0"},"step":{"login":{"default_option":"guest","option":{"login":{"display":"1","title":{"1":"Login"}},"register":{"display":"1","title":{"1":"Register"}},"guest":{"display":"1","title":{"1":"Guest"}}},"icon":"fa fa-user","description":{"1":""},"column":"1","row":"1"},"payment_address":{"fields":{"firstname":{"sort_order":"0","value":"","mask":""},"lastname":{"sort_order":"1","value":"","mask":""},"email":{"sort_order":"2","value":"","mask":""},"email_confirm":{"sort_order":"3","value":"","mask":""},"telephone":{"sort_order":"4","value":"","mask":""},"fax":{"sort_order":"5","value":"","mask":""},"password":{"sort_order":"6"},"confirm":{"sort_order":"7"},"heading":{"sort_order":"8"},"company":{"sort_order":"9","value":"","mask":""},"customer_group_id":{"sort_order":"10"},"address_1":{"sort_order":"11","value":"","mask":""},"address_2":{"sort_order":"12","value":"","mask":""},"city":{"sort_order":"13","value":"","mask":""},"postcode":{"sort_order":"14","value":"","mask":""},"country_id":{"sort_order":"15","value":""},"zone_id":{"sort_order":"16","value":""},"newsletter":{"sort_order":"17","value":"1"},"shipping_address":{"sort_order":"18","value":"0"},"agree":{"sort_order":"19","value":"0"}},"icon":"fa fa-book","title":{"1":"Payment Address"},"description":{"1":""},"column":"1","row":"2"},"shipping_address":{"fields":{"firstname":{"sort_order":"1","value":"","mask":""},"lastname":{"sort_order":"2","value":"","mask":""},"company":{"sort_order":"3","value":"","mask":""},"address_1":{"sort_order":"4","value":"","mask":""},"address_2":{"sort_order":"5","value":"","mask":""},"city":{"sort_order":"6","value":"","mask":""},"postcode":{"sort_order":"7","value":"","mask":""},"country_id":{"sort_order":"8","value":""},"zone_id":{"sort_order":"9","value":""}},"icon":"fa fa-book","title":{"1":"Shipping Address"},"description":{"1":""},"column":"1","row":"3"},"shipping_method":{"display":"1","display_options":"1","display_title":"1","input_style":"radio","default_option":"free","icon":"fa fa-truck","title":{"1":"Shipping method"},"description":{"1":"Please select the preferred shipping method to use on this order."},"column":"2","row":"1"},"payment_method":{"display":"1","display_options":"1","display_images":"1","input_style":"radio","default_option":"cod","icon":"fa fa-credit-card","title":{"1":"Payment method"},"description":{"1":"Please select the preferred payment method to use on this order."},"column":"3","row":"1"},"payment":{"default_payment_popup":"0","payment_popups":{"cod":"0","free_checkout":"0","stripe":"0"},"column":"4","row":"2"},"cart":{"icon":"fa fa-shopping-cart","title":{"1":"Shopping cart"},"description":{"1":""},"column":"4","row":"2"},"confirm":{"fields":{"comment":{"sort_order":"0"},"agree":{"sort_order":"1","value":"0"}},"column":"4","row":"2"}},"design":{"login_style":"popup","theme":"default","block_style":"row","placeholder":"1","breadcrumb":"1","address_style":"radio","cart_image_size":{"width":"80","height":"80"},"max_width":"","bootstrap":"1","autocomplete":"1","only_quickcheckout":"0","telephone_countries":"","telephone_preferred_countries":"","telephone_validation":"1","column_width":{"1":"4","2":"4","3":"4","4":"8"},"custom_style":""},"account":{"guest":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"register":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"password":{"display":"1","require":"1"},"confirm":{"display":"1","require":"1"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"newsletter":{"display":"1","require":"0"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"logged":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}}}}'),
(2, 0, '07/13/2017 06:22:51 pm', '{"name":"07\\/13\\/2017 06:22:51 pm","general":{"clear_session":"0","login_refresh":"0","analytics_event":"0","update_mini_cart":"1","compress":"1","min_order":{"value":"0","text":{"1":"You must have a sum more then %s to make an order "}},"min_quantity":{"value":"0","text":{"1":"You must have a quantity more then %s to make an order "}},"config":"d_quickcheckout","social_login":"0"},"step":{"login":{"default_option":"guest","option":{"login":{"display":"1","title":{"1":"Login"}},"register":{"display":"1","title":{"1":"Register"}},"guest":{"display":"1","title":{"1":"Guest"}}},"icon":"fa fa-user","description":{"1":""},"column":"1","row":"1"},"payment_address":{"fields":{"firstname":{"sort_order":"0","value":"","mask":""},"lastname":{"sort_order":"1","value":"","mask":""},"email":{"sort_order":"2","value":"","mask":""},"email_confirm":{"sort_order":"3","value":"","mask":""},"telephone":{"sort_order":"4","value":"","mask":""},"fax":{"sort_order":"5","value":"","mask":""},"password":{"sort_order":"6"},"confirm":{"sort_order":"7"},"heading":{"sort_order":"8"},"company":{"sort_order":"9","value":"","mask":""},"customer_group_id":{"sort_order":"10"},"address_1":{"sort_order":"11","value":"","mask":""},"address_2":{"sort_order":"12","value":"","mask":""},"city":{"sort_order":"13","value":"","mask":""},"postcode":{"sort_order":"14","value":"","mask":""},"country_id":{"sort_order":"15","value":""},"zone_id":{"sort_order":"16","value":""},"newsletter":{"sort_order":"17","value":"1"},"shipping_address":{"sort_order":"18","value":"0"},"agree":{"sort_order":"19","value":"0"}},"icon":"fa fa-book","title":{"1":"Payment Address"},"description":{"1":""},"column":"1","row":"2"},"shipping_address":{"fields":{"firstname":{"sort_order":"1","value":"","mask":""},"lastname":{"sort_order":"2","value":"","mask":""},"company":{"sort_order":"3","value":"","mask":""},"address_1":{"sort_order":"4","value":"","mask":""},"address_2":{"sort_order":"5","value":"","mask":""},"city":{"sort_order":"6","value":"","mask":""},"postcode":{"sort_order":"7","value":"","mask":""},"country_id":{"sort_order":"8","value":""},"zone_id":{"sort_order":"9","value":""}},"icon":"fa fa-book","title":{"1":"Shipping Address"},"description":{"1":""},"column":"1","row":"3"},"shipping_method":{"display":"1","display_options":"1","display_title":"1","input_style":"radio","default_option":"free","icon":"fa fa-truck","title":{"1":"Shipping method"},"description":{"1":"Please select the preferred shipping method to use on this order."},"column":"2","row":"1"},"payment_method":{"display":"1","display_options":"1","display_images":"1","input_style":"radio","default_option":"cod","icon":"fa fa-credit-card","title":{"1":"Payment method"},"description":{"1":"Please select the preferred payment method to use on this order."},"column":"3","row":"1"},"payment":{"default_payment_popup":"0","payment_popups":{"cod":"0","free_checkout":"0","stripe":"0"},"column":"4","row":"2"},"cart":{"icon":"fa fa-shopping-cart","title":{"1":"Shopping cart"},"description":{"1":""},"column":"4","row":"2"},"confirm":{"fields":{"comment":{"sort_order":"0"},"agree":{"sort_order":"1","value":"0"}},"column":"4","row":"2"}},"design":{"login_style":"popup","theme":"blue","block_style":"row","placeholder":"1","breadcrumb":"1","address_style":"radio","cart_image_size":{"width":"80","height":"80"},"max_width":"","bootstrap":"1","autocomplete":"1","only_quickcheckout":"0","telephone_countries":"","telephone_preferred_countries":"","telephone_validation":"1","column_width":{"1":"4","2":"4","3":"4","4":"8"},"custom_style":""},"account":{"guest":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"register":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"password":{"display":"1","require":"1"},"confirm":{"display":"1","require":"1"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"newsletter":{"display":"1","require":"0"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"logged":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_dqc_statistic`
--

DROP TABLE IF EXISTS `oc_dqc_statistic`;
CREATE TABLE `oc_dqc_statistic` (
  `statistic_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `rating` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_dqc_statistic`
--

INSERT INTO `oc_dqc_statistic` (`statistic_id`, `setting_id`, `order_id`, `customer_id`, `data`, `rating`, `date_added`, `date_modified`) VALUES
(1, 1, 25, 0, '{"account":"guest","field":{"guest":{"payment_address":16,"shipping_address":7,"confirm":2},"register":{"payment_address":19,"shipping_address":7,"confirm":2},"logged":{"payment_address":9,"shipping_address":7,"confirm":2}},"click":{"cart":4,"login":4},"update":{"payment_method":1}}', 0, '2017-07-13 12:21:14', '2017-07-13 12:22:13');

--
-- Indexes for table `oc_dqc_setting`
--
ALTER TABLE `oc_dqc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_dqc_statistic`
--
ALTER TABLE `oc_dqc_statistic`
  ADD PRIMARY KEY (`statistic_id`);

--
-- AUTO_INCREMENT for table `oc_dqc_setting`
--
ALTER TABLE `oc_dqc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_dqc_statistic`
--
ALTER TABLE `oc_dqc_statistic`
  MODIFY `statistic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


 --
-- Table structure for table `oc_extension`
--

DROP TABLE IF EXISTS `oc_extension`;
CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(31, 'module', 'd_shopunity'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'theme_default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'payment', 'stripe'),
(30, 'shipping', 'free'),
(32, 'module', 'd_quickcheckout'),
(33, 'module', 'd_social_login');

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);


ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Table structure for table `oc_modification`
--

DROP TABLE IF EXISTS `oc_modification`;
CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Dumping data for table `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 'Local copy OCMOD by iSenseLabs', 'isensealabs_quickfix_ocmod', 'iSenseLabs', '1.3', 'http://isenselabs.com', '<modification>\r\n    <name>Local copy OCMOD by iSenseLabs</name>\r\n	<version>1.3</version>\r\n	<link>http://isenselabs.com</link>\r\n	<author>iSenseLabs</author>\r\n	<code>isensealabs_quickfix_ocmod</code>\r\n\r\n	<file path="admin/controller/extension/installer.php">\r\n		<operation error="skip">\r\n			<search ><![CDATA[''url''  => str_replace(''&amp;'', ''&'', $this->url->link(''extension/installer/ftp'', ''token='' . $this->session->data[''token''],]]></search>\r\n			<add position="replace"><![CDATA[''url''  => str_replace(''&amp;'', ''&'', $this->url->link(''extension/installer/localcopy'', ''token='' . $this->session->data[''token''],]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[public function unzip() {]]></search>\r\n			<add position="before"><![CDATA[			\r\n	public function localcopy() {\r\n		$this->load->language(''extension/installer'');\r\n\r\n		$json = array();\r\n\r\n		if (!$this->user->hasPermission(''modify'', ''extension/installer'')) {\r\n			$json[''error''] = $this->language->get(''error_permission'');\r\n		}\r\n\r\n		if (VERSION == ''2.0.0.0'') {\r\n		    $directory = DIR_DOWNLOAD  . str_replace(array(''../'', ''..\\\\'', ''..''), '''', $this->request->post[''path'']) . ''/upload/'';\r\n		} else {\r\n		    $directory = DIR_UPLOAD  . str_replace(array(''../'', ''..\\\\'', ''..''), '''', $this->request->post[''path'']) . ''/upload/'';\r\n		}\r\n\r\n		if (!is_dir($directory)) {\r\n			$json[''error''] = $this->language->get(''error_directory'');\r\n		}\r\n\r\n		if (!$json) {\r\n			// Get a list of files ready to upload\r\n			$files = array();\r\n\r\n			$path = array($directory . ''*'');\r\n\r\n			while (count($path) != 0) {\r\n				$next = array_shift($path);\r\n\r\n				foreach (glob($next) as $file) {\r\n					if (is_dir($file)) {\r\n						$path[] = $file . ''/*'';\r\n					}\r\n\r\n					$files[] = $file;\r\n				}\r\n			}\r\n\r\n			$root = dirname(DIR_APPLICATION).''/'';\r\n\r\n			foreach ($files as $file) {\r\n				// Upload everything in the upload directory\r\n				$destination = substr($file, strlen($directory));\r\n\r\n				// Update from newer OpenCart versions:\r\n				if (substr($destination, 0, 5) == ''admin'') {\r\n					$destination = DIR_APPLICATION . substr($destination, 5);\r\n				} else if (substr($destination, 0, 7) == ''catalog'') {\r\n					$destination = DIR_CATALOG . substr($destination, 7);\r\n				} else if (substr($destination, 0, 5) == ''image'') {\r\n					$destination = DIR_IMAGE . substr($destination, 5);\r\n				} else if (substr($destination, 0, 6) == ''system'') {\r\n					$destination = DIR_SYSTEM . substr($destination, 6);\r\n				} else {\r\n					$destination = $root.$destination;\r\n				}\r\n\r\n				if (is_dir($file)) {\r\n					if (!file_exists($destination)) {\r\n						if (!mkdir($destination)) {\r\n							$json[''error''] = sprintf($this->language->get(''error_ftp_directory''), $destination);\r\n						}\r\n					}\r\n				}\r\n\r\n				if (is_file($file)) {\r\n					if (!copy($file, $destination)) {\r\n						$json[''error''] = sprintf($this->language->get(''error_ftp_file''), $file);\r\n					}\r\n				}\r\n			}\r\n		}\r\n\r\n		$this->response->addHeader(''Content-Type: application/json'');\r\n		$this->response->setOutput(json_encode($json));\r\n	}]]></add>\r\n		</operation>\r\n	</file>	\r\n</modification>\r\n', 1, '2017-07-13 12:07:38'),
(2, 'Social Login', 'd_social_login', 'Bot', '4.3.4', '/', '<?xml version="1.0" encoding="UTF-8"?>\n<modification>\n    <name>Social Login</name>\n    <code>d_social_login</code>\n    <version>4.3.4</version>\n    <author>Bot</author>\n    	\n    <file path="admin/model/sale/customer.php">\n        <operation>\n            <search><![CDATA[$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = ''" . (int)$customer_id . "''");]]></search>\n            <add  position="after">\n                    <![CDATA[$this->db->query("DELETE FROM " . DB_PREFIX . "customer_authentication WHERE customer_id = ''" . (int)$customer_id . "''"); //vqmod_d_social_login.xml]]>\n            </add>\n        </operation>\n    </file>\n</modification>', 1, '2017-07-13 12:17:14'),
(3, 'd_shopunity', 'd_shopunity', 'Bot', '3.1.0', '/', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n    <name>d_shopunity</name>\n    <code>d_shopunity</code>\n    <description>This is a modification file. You can use it to </description>\n    <version>3.1.0</version>\n    <author>Bot</author>\n    \n    <file path="admin/controller/common/header.php">\n        <operation error="skip">\n            <search><![CDATA[public function index() {]]></search>\n            <ignoreif><![CDATA[$data[''d_shopunity''] = $this->url->link(''extension/d_shopunity/extension'', ''token=''.$this->request->get[''token''], ''SSL'');]]></ignoreif>\n            <add position="after"><![CDATA[\n            if(!empty($this->request->get[''token''])){\n                $data[''d_shopunity''] = $this->url->link(''extension/d_shopunity/extension'', ''token=''.$this->request->get[''token''], ''SSL'');\n            }\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/view/template/common/header.tpl">\n        <operation error="skip">\n            <search><![CDATA[<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i></a>]]></search>\n            <ignoreif><![CDATA[<li><a href="<?php echo $d_shopunity; ?>"><i class="fa fa-flask fa-lg"></i></a></li>]]></ignoreif>\n            <add position="before"><![CDATA[\n            <li><a href="<?php echo $d_shopunity; ?>" title="Shopunity"><i class="fa fa-flask fa-lg"></i></a></li>\n            ]]></add>\n        </operation>\n        <operation error="skip">\n            <search><![CDATA[<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><?php if($alerts > 0) { ?><span class="label label-danger pull-left"><?php echo $alerts; ?></span><?php } ?> <i class="fa fa-bell fa-lg"></i></a>]]></search>\n            <ignoreif><![CDATA[<li><a href="<?php echo $d_shopunity; ?>"><i class="fa fa-flask fa-lg"></i></a></li>]]></ignoreif>\n            <add position="before"><![CDATA[\n            <li><a href="<?php echo $d_shopunity; ?>" title="Shopunity"><i class="fa fa-flask fa-lg"></i></a></li>\n            ]]></add>\n        </operation>\n    </file>\n</modification>', 1, '2017-07-13 12:19:06'),
(4, 'd_opencart_patch', 'd_opencart_patch', 'Bot', '2.3.0.x', '/', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n    <name>d_opencart_patch</name>\n    <code>d_opencart_patch</code>\n    <description>Adds copmatibility fixes for old extensions on Opencarts 2.x</description>\n    <version>2.3.0.x</version>\n    \n    \n\n    <file path="admin/controller/extension/extension/dashboard.php">\n        <operation error="skip" info="fix for install and uninstall of dashboard extensions">\n            <search ><![CDATA[''dashboard_'' . $this->request->get[''extension'']]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 1]]></ignoreif>\n            <add position="replace"><![CDATA[$this->request->get[''extension'']]]></add>\n        </operation>\n        <operation error="skip" info="fix for install and uninstall of dashboard extensions">\n            <search ><![CDATA[$this->load->controller(''extension/dashboard/'' . $this->request->get[''extension''] . ''/uninstall'');]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add position="before"><![CDATA[\n                //d_shopunity_oc230_patch.xml 2\n                $this->load->model(''setting/setting'');\n                $this->model_setting_setting->deleteSetting(''dashboard_''.$this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for install and uninstall of dashboard extensions">\n            <search ><![CDATA[$this->model_extension_extension->uninstall(''dashboard'', $value);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 3]]></ignoreif>\n            <add position="after"><![CDATA[\n                //d_shopunity_oc230_patch.xml 3\n                $this->load->model(''setting/setting'');\n                $this->model_setting_setting->deleteSetting(''dashboard_''.$value);\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/controller/event/compatibility.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$route = $part[1] . ''/'' . $part[2];]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 1]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 1\n            unset($part[0]);\n            $route = implode(''/'', $part);\n            ]]></add>\n        </operation>\n        <!-- <operation error="skip" info="fix for 2.3.0.x">\n            <search position="replace" offset="2"><![CDATA[if (!is_file(DIR_LANGUAGE . $this->config->get(''config_language'') . ''/'' . $route . ''.php'') && is_file(DIR_LANGUAGE . $this->config->get(''config_language'') . ''/'' . $part[1] . ''/'' . $part[2] . ''.php'')) {]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add><![CDATA[\n            //d_shopunity_oc230_patch.xml 2\n            $directories = array($this->config->get(''config_admin_language''), $this->language->default, ''english'');\n            \n            foreach ($directories as $directory) {\n                if (!is_file(DIR_LANGUAGE . $directory . ''/'' . $route . ''.php'') && is_file(DIR_LANGUAGE . $directory . ''/'' . $part[1] . ''/'' . $part[2] . ''.php'')) {\n                    $route = $part[1] . ''/'' . $part[2];\n                    break;\n                }\n             }\n         ]]></add>\n        </operation> -->\n    </file>\n    <file path="admin/controller/extension/extension/feed.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/feed/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''feed/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''feed/'' . $this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/fraud.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/fraud/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''fraud/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''fraud/'' . $this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/module.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/module/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 1]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml 1\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''module/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''module/'' . $this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$data[''extensions''][] = array(]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 2\n            $data[''extensions''][$extension] = array(\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/payment.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/payment/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''payment/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''payment/'' . $this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/shipping.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/shipping/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''shipping/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''shipping/'' . $this->request->get[''extension'']);\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/theme.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/theme/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''theme/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''theme/'' . $this->request->get[''extension'']);\n\n            ]]></add>\n        </operation>\n    </file>\n    <file path="admin/controller/extension/extension/total.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''extension/total/'' . $this->request->get[''extension'']);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''access'', ''total/'' . $this->request->get[''extension'']);\n            $this->model_user_user_group->addPermission($this->user->getGroupId(), ''modify'', ''total/'' . $this->request->get[''extension'']);\n\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="catalog/controller/event/compatibility.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$route = $part[1] . ''/'' . $part[2];]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 1]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 1\n            unset($part[0]);\n            $route = implode(''/'', $part);\n            ]]></add>\n        </operation>\n        <!-- <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[if (!is_file(DIR_LANGUAGE . $this->config->get(''config_language'') . ''/'' . $route . ''.php'') && is_file(DIR_LANGUAGE . $this->config->get(''config_language'') . ''/'' . $part[1] . ''/'' . $part[2] . ''.php'')) {]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add position="replace" offset="2"><![CDATA[\n            //d_shopunity_oc230_patch.xml 2\n            $directories = array($this->config->get(''config_admin_language''), $this->language->default, ''english'');\n            \n            foreach ($directories as $directory) {\n                if (!is_file(DIR_LANGUAGE . $directory . ''/'' . $route . ''.php'') && is_file(DIR_LANGUAGE . $directory . ''/'' . $part[1] . ''/'' . $part[2] . ''.php'')) {\n                    $route = $part[1] . ''/'' . $part[2];\n                    break;\n                }\n             }\n         ]]></add>\n        </operation> -->\n    </file>\n\n    <file path="system/library/cart/user.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[return in_array($value, $this->permission[$key]);]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            $part = explode(''/'', $value);\n            unset($part[0]);\n            return (in_array($value, $this->permission[$key]) || in_array(implode(''/'', $part), $this->permission[$key])) ? true: false;\n            ]]></add>\n        </operation>\n    </file>\n    <!-- <file path="system/library/language.php">\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search><![CDATA[private $default = ''en-gb'';]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 1]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 1\n            public $default = ''en-gb'';\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search><![CDATA[$old_file = DIR_LANGUAGE . ''english/'' . str_replace(''extension/'', '''', $filename) . ''.php'';]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 2\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search><![CDATA[} elseif (is_file($old_file)) {]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 2]]></ignoreif>\n            <add position="replace" offset="1"><![CDATA[\n            //d_shopunity_oc230_patch.xml 2\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search><![CDATA[$old_file = DIR_LANGUAGE . $this->default . ''/'' . str_replace(''extension/'', '''', $filename) . ''.php'';]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 3]]></ignoreif>\n            <add  position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 3\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix for 2.3.0.x">\n            <search><![CDATA[$old_file = DIR_LANGUAGE . $this->directory . ''/'' . str_replace(''extension/'', '''', $filename) . ''.php'';]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml 4]]></ignoreif>\n            <add  position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml 4\n            ]]></add>\n        </operation>\n    </file> -->\n    <file path="system/config/catalog.php">\n         <operation error="skip" info="fix for 2.3.0.x">\n            <search ><![CDATA[$_[''action_event''] = array(]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml]]></ignoreif>\n            <add position="after"><![CDATA[\n            //d_shopunity_oc230_patch.xml\n            ''controller/extension/analytics/*/before'' => ''event/compatibility/controller'',\n            ''controller/extension/captcha/*/before''   => ''event/compatibility/controller'',\n            ''controller/extension/feed/*/before''      => ''event/compatibility/controller'',\n            ''controller/extension/fraud/*/before''     => ''event/compatibility/controller'',\n            ''controller/extension/module/*/before''    => ''event/compatibility/controller'',\n            ''controller/extension/payment/*/before''   => ''event/compatibility/controller'',\n            ''controller/extension/recurring/*/before'' => ''event/compatibility/controller'',\n            ''controller/extension/shipping/*/before''  => ''event/compatibility/controller'',\n            ''controller/extension/theme/*/before''     => ''event/compatibility/controller'',\n            ''controller/extension/total/*/before''     => ''event/compatibility/controller'',\n            ]]></add>\n        </operation>\n    </file>\n    \n    <file path="admin/controller/event/compatibility.php">\n         <operation error="skip" info="remove errror for 2.3.0.x">\n            <search ><![CDATA[class ControllerEventCompatibility extends Controller {]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml fix 1]]></ignoreif>\n            <add position="before"><![CDATA[\n            //d_shopunity_oc230_patch.xml fix 1\n            if (!class_exists(''ControllerEventCompatibility'')) {\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/controller/event/compatibility.php">\n         <operation error="skip" info="remove errror for 2.3.0.x">\n            <search ><![CDATA[public function view(&$route, &$data) {]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml fix 2]]></ignoreif>\n            <add position="after" offset="8"><![CDATA[\n            //d_shopunity_oc230_patch.xml fix 2\n}\n            ]]></add>\n        </operation>\n    </file>\n    \n    <file path="admin/model/localisation/language.php">\n         <operation error="skip" info="fix errror for 2.3.0.x">\n            <search ><![CDATA[$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image_description WHERE language_id = ''" . (int)$language_id . "''");]]></search>\n            <ignoreif><![CDATA[//d_shopunity_oc230_patch.xml fix]]></ignoreif>\n            <add position="replace"><![CDATA[\n            //d_shopunity_oc230_patch.xml fix\n            if(VERSION < ''2.3.0.0''){\n                $this->db->query("DELETE FROM " . DB_PREFIX . "banner_image_description WHERE language_id = ''" . (int)$language_id . "''");\n            }\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/view/template/extension/extension/analytics.tpl">\n         <operation error="skip" info="fix analytics tpl errror for 2.3.0.x">\n            <search index="3"><![CDATA[<?php } ?>]]></search>\n            <ignoreif><![CDATA[<!-- d_shopunity_oc230_patch.xml 1 -->]]></ignoreif>\n            <add position="replace"><![CDATA[\n            <!-- d_shopunity_oc230_patch.xml 1 -->\n            ]]></add>\n        </operation>\n        <operation error="skip" info="fix analytics tpl errror for 2.3.0.x">\n            <search index="1"><![CDATA[<?php } else { ?>]]></search>\n            <ignoreif><![CDATA[<!-- d_shopunity_oc230_patch.xml 2 -->]]></ignoreif>\n            <add position="before"><![CDATA[\n            <!-- d_shopunity_oc230_patch.xml 2 -->\n            <?php } ?>\n            ]]></add>\n        </operation>\n    </file>\n</modification>', 1, '2017-07-13 12:19:06'),
(5, 'Ajax Quick Checkout', 'd_quickcheckout', 'Bot', '6.4.1', '/', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n    <name>Ajax Quick Checkout</name>\n    <code>d_quickcheckout</code>\n    <version>6.4.1</version>\n    \n    \n\n    <file path="catalog/controller/checkout/checkout.php">\n        <operation>\n            <search ><![CDATA[if (file_exists(DIR_TEMPLATE . $this->config->get(''config_template'') . ''/template/checkout/checkout.tpl'')) {]]></search>\n            <add position="replace" offset="4"><![CDATA[\n            // a_vqmod_d_quickcheckout.xml\n            if($this->config->get(''d_quickcheckout_status'')){\n                $template = ''d_quickcheckout'';\n            }else{\n                $template = ''checkout'';\n            }\n            \n            if (file_exists(DIR_TEMPLATE . $this->config->get(''config_template'') . ''/template/checkout/''.$template.''.tpl'')) {\n                $template = $this->config->get(''config_template'') . ''/template/checkout/''.$template.''.tpl'';\n            } else {\n                $template = ''default/template/checkout/''.$template.''.tpl'';\n            }\n            $this->response->setOutput($this->load->view($template, $data));]]>\n            </add>\n        </operation>\n    </file>\n    <file path="catalog/controller/checkout/checkout.php">\n        <operation error="skip">\n            <search><![CDATA[$this->response->setOutput($this->load->view(''checkout/checkout'', $data));]]></search>\n            <add position="replace"><![CDATA[\n            // a_vqmod_d_quickcheckout.xml\n            if($this->config->get(''d_quickcheckout_status'')){\n                $template = ''d_quickcheckout'';\n            }else{\n                $template = ''checkout'';\n            }\n            \n            $this->response->setOutput($this->load->view(''checkout/''.$template, $data));]]></add>\n        </operation>\n    </file>\n    <file path="catalog/controller/checkout/checkout.php">\n        <operation>\n            <search ><![CDATA[$data[''header''] = $this->load->controller(''common/header'');]]></search>\n            <add position="before"><![CDATA[\n        if($this->config->get(''d_quickcheckout_status'')){\n            $data[''d_quickcheckout''] = $this->load->controller(''module/d_quickcheckout'');\n        }\n        ]]></add>\n        </operation>\n    </file>\n    <file path="system/library/cart/tax.php">\n        <operation>\n            <search ><![CDATA[final class Tax {]]></search>\n            <add position="after"><![CDATA[\n    // a_vqmod_d_quickcheckout.xml\n    public function clearRates(){\n        $this->tax_rates = array();\n    }\n            ]]></add>\n        </operation>\n    </file> \n    <file path="system/library/tax.php">\n        <operation>\n            <search ><![CDATA[final class Tax {]]></search>\n            <add position="after"><![CDATA[\n    // a_vqmod_d_quickcheckout.xml\n    public function clearRates(){\n        $this->tax_rates = array();\n    }\n            ]]></add>\n        </operation>\n    </file> \n</modification>', 1, '2017-07-13 12:20:54');


--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Table structure for table `oc_setting`
--

DROP TABLE IF EXISTS `oc_setting`;
CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(456, 0, 'config', 'config_mail_alert_email', '', 0),
(457, 0, 'config', 'config_maintenance', '0', 0),
(458, 0, 'config', 'config_seo_url', '1', 0),
(459, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai''hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(465, 0, 'config', 'config_file_max_size', '300000', 0),
(466, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(467, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(468, 0, 'config', 'config_error_display', '1', 0),
(469, 0, 'config', 'config_error_log', '1', 0),
(470, 0, 'config', 'config_error_filename', 'error.log', 0),
(4, 0, 'voucher', 'voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'voucher_status', '1', 0),
(460, 0, 'config', 'config_compression', '0', 0),
(461, 0, 'config', 'config_secure', '0', 0),
(462, 0, 'config', 'config_password', '1', 0),
(463, 0, 'config', 'config_shared', '0', 0),
(464, 0, 'config', 'config_encryption', '53E7DhiFefdYbuH7pfjqmUuE11lU29RwYj2CGqIeAu8NSeNgqNnSv6XW5al0xdakwPiPQWJZuf1S5c1TSbjr4TVlxzxdE77kofOmmLWiJe8L3WeXwHo2w4qDPiNwMIJMnMzbbzE8sG0mxO0Ly0Pz8sv9zDjeiy8iU0TFjGtdT6E6PnXZOTiJYytLd6DnEt6nM39qephQ5GQefgUjcawKYOHHHgHCLk2OylOnvK9w5DvRNDpX1vPLLJSx8HDgRlsOVTR7cXkvhZQoz3r8EuwSMWUHMewuy1Jyy0EFGL1Dq5skjEyOgiH6yvWoyx6d2UIWNc1Wk4AJ2egg27t3BUEAAv7iPbBy7UNYc7vHSDdMHNxeO6KWoMr3UZncj9NTkrY4idKXnzod5tLzK8W0Rk1Ke2Os5SYPoBnFCIl1FKfupB3CMlQvDcpbVQ1jDGdBWmOEPsF8X9p5vHeD9QXYfNqeStt9lI69JiyduNAJCP5gHGGnQgo5mWkLVCz86oGIb1m73jd24CvDwPTa5AIMHZsPXpxBuaPfeAZb0vWWfoNLRxknjMo7fv4x6nCSD1SddArJ6UY3fGJgCTHoYm628FbhYMmXAEilK1DA4Aa4zLzRutJDF6JGYclH3ciZg2mROLlj0KusdqT98IoSjtP4QD2fJmjuyVT3RJaBOIb1y14hyIv8FDGAM9T8t21hb91HXNBLdXpQJ9I3rePnei151Z4CMg0gKgicX9YK9xuGqo6eLEIOO03xLqc843aEhSrp2SN0gjTBm61gwvmqjjLcSWhH9PSc91jicOKKhJXyqjKtegQp0e4gLwf8BJmBFxIYYhsDBQUe6WNXn3Y2jrxqMPLX3GDKnBhP3c1L0YTSIce8xA9Dgoj7xnURgI1w4JL3iDbD7VKbDdzL0W0VBLhCIaqCo2iecehuxZ24Odn784mLYiLhZB1YJ6eUPnxqgnme70DInDLnM1me6ZB8yBSjvSZZhMfQPNwZHh750lisfRbBMl2fRkmJ4QHLoeNyi575ocZj', 0),
(454, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(455, 0, 'config', 'config_mail_alert', '["order"]', 1),
(453, 0, 'config', 'config_mail_smtp_port', '25', 0),
(452, 0, 'config', 'config_mail_smtp_password', '', 0),
(451, 0, 'config', 'config_mail_smtp_username', '', 0),
(450, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(449, 0, 'config', 'config_mail_parameter', '', 0),
(447, 0, 'config', 'config_ftp_status', '0', 0),
(448, 0, 'config', 'config_mail_protocol', 'mail', 0),
(446, 0, 'config', 'config_ftp_root', '', 0),
(445, 0, 'config', 'config_ftp_password', '', 0),
(444, 0, 'config', 'config_ftp_username', '', 0),
(443, 0, 'config', 'config_ftp_port', '21', 0),
(442, 0, 'config', 'config_ftp_hostname', 'dev.tugboat.cc', 0),
(441, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(439, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(440, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(438, 0, 'config', 'config_captcha', '', 0),
(437, 0, 'config', 'config_return_status_id', '2', 0),
(436, 0, 'config', 'config_return_id', '0', 0),
(435, 0, 'config', 'config_affiliate_id', '4', 0),
(434, 0, 'config', 'config_affiliate_commission', '5', 0),
(433, 0, 'config', 'config_affiliate_auto', '0', 0),
(432, 0, 'config', 'config_affiliate_approval', '0', 0),
(431, 0, 'config', 'config_stock_checkout', '1', 0),
(430, 0, 'config', 'config_stock_warning', '0', 0),
(429, 0, 'config', 'config_stock_display', '0', 0),
(428, 0, 'config', 'config_api_id', '1', 0),
(427, 0, 'config', 'config_fraud_status_id', '7', 0),
(426, 0, 'config', 'config_complete_status', '["5","3"]', 1),
(424, 0, 'config', 'config_order_status_id', '1', 0),
(425, 0, 'config', 'config_processing_status', '["5","1","2","12","3"]', 1),
(374, 0, 'free_checkout', 'free_checkout_status', '0', 0),
(97, 0, 'shipping', 'shipping_sort_order', '3', 0),
(98, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(99, 0, 'sub_total', 'sub_total_status', '1', 0),
(100, 0, 'tax', 'tax_status', '1', 0),
(101, 0, 'total', 'total_sort_order', '9', 0),
(102, 0, 'total', 'total_status', '1', 0),
(103, 0, 'tax', 'tax_sort_order', '5', 0),
(373, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0),
(474, 0, 'cod', 'cod_status', '1', 0),
(473, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(110, 0, 'shipping', 'shipping_status', '1', 0),
(111, 0, 'shipping', 'shipping_estimator', '1', 0),
(112, 0, 'coupon', 'coupon_sort_order', '4', 0),
(113, 0, 'coupon', 'coupon_status', '1', 0),
(379, 0, 'free', 'free_sort_order', '', 0),
(378, 0, 'free', 'free_status', '1', 0),
(377, 0, 'free', 'free_geo_zone_id', '0', 0),
(376, 0, 'free', 'free_total', '', 0),
(119, 0, 'credit', 'credit_sort_order', '7', 0),
(120, 0, 'credit', 'credit_status', '1', 0),
(477, 0, 'reward', 'reward_sort_order', '2', 0),
(476, 0, 'reward', 'reward_status', '0', 0),
(123, 0, 'category', 'category_status', '1', 0),
(124, 0, 'account', 'account_status', '1', 0),
(125, 0, 'affiliate', 'affiliate_status', '1', 0),
(126, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(127, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(132, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(134, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(138, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(146, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(147, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(148, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(149, 0, 'theme_default', 'theme_default_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(151, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(153, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(155, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(157, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(158, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(159, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(160, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(161, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(162, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(164, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(165, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(166, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(167, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(168, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(169, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(171, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(172, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(173, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(423, 0, 'config', 'config_checkout_id', '5', 0),
(422, 0, 'config', 'config_checkout_guest', '0', 0),
(421, 0, 'config', 'config_cart_weight', '1', 0),
(420, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(419, 0, 'config', 'config_account_id', '3', 0),
(418, 0, 'config', 'config_login_attempts', '5', 0),
(417, 0, 'config', 'config_customer_price', '0', 0),
(416, 0, 'config', 'config_customer_group_display', '["1"]', 1),
(415, 0, 'config', 'config_customer_group_id', '1', 0),
(414, 0, 'config', 'config_customer_search', '0', 0),
(413, 0, 'config', 'config_customer_activity', '0', 0),
(412, 0, 'config', 'config_customer_online', '0', 0),
(410, 0, 'config', 'config_tax_default', 'shipping', 0),
(411, 0, 'config', 'config_tax_customer', 'shipping', 0),
(409, 0, 'config', 'config_tax', '1', 0),
(408, 0, 'config', 'config_voucher_max', '1000', 0),
(407, 0, 'config', 'config_voucher_min', '1', 0),
(406, 0, 'config', 'config_review_guest', '1', 0),
(405, 0, 'config', 'config_review_status', '1', 0),
(404, 0, 'config', 'config_limit_admin', '20', 0),
(403, 0, 'config', 'config_product_count', '1', 0),
(402, 0, 'config', 'config_weight_class_id', '1', 0),
(401, 0, 'config', 'config_length_class_id', '1', 0),
(400, 0, 'config', 'config_currency_auto', '1', 0),
(399, 0, 'config', 'config_currency', 'CAD', 0),
(398, 0, 'config', 'config_admin_language', 'en-gb', 0),
(397, 0, 'config', 'config_language', 'en-gb', 0),
(393, 0, 'config', 'config_open', '', 0),
(394, 0, 'config', 'config_comment', '', 0),
(395, 0, 'config', 'config_country_id', '38', 0),
(396, 0, 'config', 'config_zone_id', '610', 0),
(392, 0, 'config', 'config_image', '', 0),
(391, 0, 'config', 'config_fax', '', 0),
(390, 0, 'config', 'config_telephone', '123456789', 0),
(389, 0, 'config', 'config_email', 'info@tugboat.cc', 0),
(388, 0, 'config', 'config_geocode', '', 0),
(387, 0, 'config', 'config_address', 'Address 1', 0),
(386, 0, 'config', 'config_owner', 'Your Name', 0),
(385, 0, 'config', 'config_name', 'Your Store', 0),
(384, 0, 'config', 'config_layout_id', '4', 0),
(383, 0, 'config', 'config_theme', 'theme_default', 0),
(382, 0, 'config', 'config_meta_keyword', '', 0),
(381, 0, 'config', 'config_meta_description', 'My Store', 0),
(380, 0, 'config', 'config_meta_title', 'Your Store', 0),
(359, 0, 'stripe', 'stripe_test_secret_key', 'sk_test_J3YGG1wf67gQscIpQPLVnTJZ', 0),
(360, 0, 'stripe', 'stripe_test_publishable_key', 'pk_test_DOwpKuLn2N2Xb4OBC6vNZEVE', 0),
(361, 0, 'stripe', 'stripe_live_secret_key', '', 0),
(362, 0, 'stripe', 'stripe_live_publishable_key', '', 0),
(363, 0, 'stripe', 'stripe_environment', 'test', 0),
(364, 0, 'stripe', 'stripe_status', '1', 0),
(365, 0, 'stripe', 'stripe_order_status_id', '7', 0),
(366, 0, 'stripe', 'stripe_currency', 'usd', 0),
(367, 0, 'stripe', 'stripe_store_cards', '0', 0),
(472, 0, 'cod', 'cod_order_status_id', '1', 0),
(471, 0, 'cod', 'cod_total', '0.01', 0),
(375, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(475, 0, 'cod', 'cod_sort_order', '5', 0),
(495, 0, 'd_quickcheckout', 'd_quickcheckout_debug_file', 'd_quickcheckout.log', 0),
(491, 0, 'd_quickcheckout', 'd_quickcheckout_status', '1', 0),
(492, 0, 'd_quickcheckout', 'd_quickcheckout_trigger', '#button-confirm, .button, .btn, .button_oc, input[type=submit]', 0),
(493, 0, 'd_quickcheckout', 'd_quickcheckout_debug', '0', 0),
(494, 0, 'd_quickcheckout', 'd_quickcheckout_setting', '{"name":"07\\/13\\/2017 06:20:18 pm","general":{"clear_session":"0","login_refresh":"0","analytics_event":"0","update_mini_cart":"1","compress":"1","min_order":{"value":"0","text":{"1":"You must have a sum more then %s to make an order "}},"min_quantity":{"value":"0","text":{"1":"You must have a quantity more then %s to make an order "}},"config":"d_quickcheckout","social_login":"0"},"step":{"login":{"default_option":"guest","option":{"login":{"display":"1","title":{"1":"Login"}},"register":{"display":"1","title":{"1":"Register"}},"guest":{"display":"1","title":{"1":"Guest"}}},"icon":"fa fa-user","description":{"1":""},"column":"1","row":"1"},"payment_address":{"fields":{"firstname":{"sort_order":"0","value":"","mask":""},"lastname":{"sort_order":"1","value":"","mask":""},"email":{"sort_order":"2","value":"","mask":""},"email_confirm":{"sort_order":"3","value":"","mask":""},"telephone":{"sort_order":"4","value":"","mask":""},"fax":{"sort_order":"5","value":"","mask":""},"password":{"sort_order":"6"},"confirm":{"sort_order":"7"},"heading":{"sort_order":"8"},"company":{"sort_order":"9","value":"","mask":""},"customer_group_id":{"sort_order":"10"},"address_1":{"sort_order":"11","value":"","mask":""},"address_2":{"sort_order":"12","value":"","mask":""},"city":{"sort_order":"13","value":"","mask":""},"postcode":{"sort_order":"14","value":"","mask":""},"country_id":{"sort_order":"15","value":""},"zone_id":{"sort_order":"16","value":""},"newsletter":{"sort_order":"17","value":"1"},"shipping_address":{"sort_order":"18","value":"0"},"agree":{"sort_order":"19","value":"0"}},"icon":"fa fa-book","title":{"1":"Payment Address"},"description":{"1":""},"column":"1","row":"2"},"shipping_address":{"fields":{"firstname":{"sort_order":"1","value":"","mask":""},"lastname":{"sort_order":"2","value":"","mask":""},"company":{"sort_order":"3","value":"","mask":""},"address_1":{"sort_order":"4","value":"","mask":""},"address_2":{"sort_order":"5","value":"","mask":""},"city":{"sort_order":"6","value":"","mask":""},"postcode":{"sort_order":"7","value":"","mask":""},"country_id":{"sort_order":"8","value":""},"zone_id":{"sort_order":"9","value":""}},"icon":"fa fa-book","title":{"1":"Shipping Address"},"description":{"1":""},"column":"1","row":"3"},"shipping_method":{"display":"1","display_options":"1","display_title":"1","input_style":"radio","default_option":"free","icon":"fa fa-truck","title":{"1":"Shipping method"},"description":{"1":"Please select the preferred shipping method to use on this order."},"column":"2","row":"1"},"payment_method":{"display":"1","display_options":"1","display_images":"1","input_style":"radio","default_option":"cod","icon":"fa fa-credit-card","title":{"1":"Payment method"},"description":{"1":"Please select the preferred payment method to use on this order."},"column":"3","row":"1"},"payment":{"default_payment_popup":"0","payment_popups":{"cod":"0","free_checkout":"0","stripe":"0"},"column":"4","row":"2"},"cart":{"icon":"fa fa-shopping-cart","title":{"1":"Shopping cart"},"description":{"1":""},"column":"4","row":"2"},"confirm":{"fields":{"comment":{"sort_order":"0"},"agree":{"sort_order":"1","value":"0"}},"column":"4","row":"2"}},"design":{"login_style":"popup","theme":"default","block_style":"row","placeholder":"1","breadcrumb":"1","address_style":"radio","cart_image_size":{"width":"80","height":"80"},"max_width":"","bootstrap":"1","autocomplete":"1","only_quickcheckout":"0","telephone_countries":"","telephone_preferred_countries":"","telephone_validation":"1","column_width":{"1":"4","2":"4","3":"4","4":"8"},"custom_style":""},"account":{"guest":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"register":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"email":{"display":"1","require":"1"},"email_confirm":{"display":"1","require":"1"},"telephone":{"display":"1","require":"0"},"fax":{"display":"1","require":"0"},"password":{"display":"1","require":"1"},"confirm":{"display":"1","require":"1"},"heading":{"display":"1"},"company":{"display":"1","require":"0"},"customer_group_id":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"newsletter":{"display":"1","require":"0"},"shipping_address":{"display":"1"},"agree":{"display":"1","require":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}},"logged":{"payment_address":{"display":"1","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"1","require":"1"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"0"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"},"shipping_address":{"display":"1"}}},"shipping_address":{"display":"1","require":"0","fields":{"firstname":{"display":"1","require":"1"},"lastname":{"display":"0","require":"0"},"company":{"display":"1","require":"0"},"address_1":{"display":"1","require":"1"},"address_2":{"display":"0","require":"0"},"city":{"display":"1","require":"1"},"postcode":{"display":"1","require":"1"},"country_id":{"display":"1","require":"1"},"zone_id":{"display":"1","require":"1"}}},"cart":{"display":"1","columns":{"image":"1","name":"1","model":"0","quantity":"1","price":"1","total":"1"},"option":{"coupon":{"display":"1"},"voucher":{"display":"1"},"reward":{"display":"1"}}},"confirm":{"fields":{"comment":{"display":"1","require":"0"},"agree":{"display":"1","require":"1"}}}}}}', 1),
(490, 0, 'd_quickcheckout', 'd_quickcheckout_setting_cycle', '{"1":"1","2":"0"}', 1);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=496;

--
-- Table structure for table `oc_user_group`
--

DROP TABLE IF EXISTS `oc_user_group`;
CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{"access":["catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/language","design\\/layout","design\\/menu","design\\/theme","design\\/translation","event\\/compatibility","event\\/theme","extension\\/analytics\\/google_analytics","extension\\/captcha\\/basic_captcha","extension\\/captcha\\/google_captcha","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/event","extension\\/extension","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/installer","extension\\/modification","extension\\/module\\/account","extension\\/module\\/affiliate","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/slideshow","extension\\/module\\/special","extension\\/module\\/store","extension\\/openbay","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/worldpay","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/store","extension\\/theme\\/theme_default","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/customer_search","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","setting\\/setting","setting\\/store","startup\\/compatibility","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission","extension\\/payment\\/stripe","extension\\/shipping\\/free","extension\\/module\\/d_shopunity","extension\\/d_shopunity","extension\\/d_shopunity\\/account","extension\\/d_shopunity\\/developer","extension\\/d_shopunity\\/extension","extension\\/d_shopunity\\/market","extension\\/d_shopunity\\/backup","extension\\/d_shopunity\\/order","extension\\/d_shopunity\\/invoice","extension\\/d_shopunity\\/transaction","extension\\/d_shopunity\\/setting","extension\\/d_shopunity\\/tester","extension\\/d_shopunity\\/dependency","extension\\/d_shopunity\\/filemanager","extension\\/module\\/d_quickcheckout","module\\/d_quickcheckout","extension\\/module\\/d_social_login","module\\/d_social_login"],"modify":["catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/language","design\\/layout","design\\/menu","design\\/theme","design\\/translation","event\\/compatibility","event\\/theme","extension\\/analytics\\/google_analytics","extension\\/captcha\\/basic_captcha","extension\\/captcha\\/google_captcha","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/event","extension\\/extension","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/installer","extension\\/modification","extension\\/module\\/account","extension\\/module\\/affiliate","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/slideshow","extension\\/module\\/special","extension\\/module\\/store","extension\\/openbay","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/worldpay","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/store","extension\\/theme\\/theme_default","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/customer_search","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","setting\\/setting","setting\\/store","startup\\/compatibility","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission","extension\\/payment\\/stripe","extension\\/shipping\\/free","extension\\/module\\/d_shopunity","extension\\/d_shopunity","extension\\/d_shopunity\\/account","extension\\/d_shopunity\\/developer","extension\\/d_shopunity\\/extension","extension\\/d_shopunity\\/market","extension\\/d_shopunity\\/backup","extension\\/d_shopunity\\/order","extension\\/d_shopunity\\/invoice","extension\\/d_shopunity\\/transaction","extension\\/d_shopunity\\/setting","extension\\/d_shopunity\\/tester","extension\\/d_shopunity\\/dependency","extension\\/d_shopunity\\/filemanager","extension\\/module\\/d_quickcheckout","module\\/d_quickcheckout","extension\\/module\\/d_social_login","module\\/d_social_login"]}'),
(10, 'Demonstration', '');


--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;



