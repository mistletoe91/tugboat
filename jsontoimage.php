<?php
ini_set('display_errors',1);
ini_set('display_startup_errors', 1);
error_reporting(1);

function convertJsonToImage($file = "test", $newFile = "newFileName"){
    try {

        $id   = $newFile;
        $path = "tmp/".$file.".json";
        $printData = json_decode(file_get_contents($path));

        echo '<pre>';
        //print_r($printData->objects);
        echo '</pre>';

        imagickNew($printData,$id);

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function imagickNew($printData,$id){
  try {
      $print = new Imagick();
      $print->setResolution(450, 450);
      $background = (empty($printData->objects[0]->background)) ? 'transparent' : $printData->objects[0]->background;
      $print->newImage($printData->objects[0]->width, $printData->objects[0]->height, new ImagickPixel($background));

      $print->setImageFormat('png32');
      $print->setImageUnits(imagick::RESOLUTION_PIXELSPERCENTIMETER);


      // Re-Scaling each Image/Text for Larger Canvas/Image
      foreach ($printData->objects as $i => $object) {
          if ($object->type == 'image') {
            addImage($object, $print, $printData);
          } elseif ($object->type == 'textbox') {
            addText($object, $print, $printData);
          } elseif ($object->type == 'group'){
            addLine($object, $print, $printData);
          }
      }

      try {
         // Saving High Quality Image in (300 dpi)
        $fileDir = dirname(__FILE__) ;

         if (!file_exists($fileDir) || !is_dir($fileDir)) {
            if (!mkdir($fileDir)) die("Could not create directory: {$fileDir}\n");
         }
         $saved = $print->writeimage($fileDir . "/" . $id . '.png');

         #    jas: uncomment following for testing
         #header('Content-type: image/png');
         #echo $print;

       } catch (Exception $e) {
           echo $e->getMessage();
       }

  } catch (Exception $e) {
     echo $e->getMessage();
  }
}

function addImage($object, $print, $printData) {

    $widthScale = ($printData->objects[0]->width / 576); //fabric canvas width
    $heightScale = ($printData->objects[0]->height / 384); //fabric canvas height
    $fileDir = dirname(__FILE__)."/tmp/" ;
    $src = new Imagick($fileDir . basename($object->src));

    $size = $src->getImageGeometry();

    $resizeWidth = ($object->width * $object->scaleX) * $widthScale;
    $resizeHeight = ($object->height * $object->scaleY) * $heightScale;
    $src->resizeImage($resizeWidth, $resizeHeight, Imagick::FILTER_LANCZOS, 1);
    $sizeAfterResize = $src->getImageGeometry();

    $src->rotateImage(new ImagickPixel('none'), $object->angle);
    $sizeAfterRotate = $src->getImageGeometry();

    // $left = $object->left * $widthScale;
    // $top = $object->top * $heightScale;
    $left = $object->left;
    $top  = $object->top;

    $print->compositeImage($src, Imagick::COMPOSITE_DEFAULT, $left, $top);
}

function addText($object, $print, $printData) {

    $draw = new ImagickDraw();
    $color = new ImagickPixel($object->fill);
    $background = new ImagickPixel('blue');

    $draw->setFontFamily($object->fontFamily); // Set Wiith Font Path
    $draw->setFontSize($object->fontSize/10);
    $draw->setFontWeight(($object->fontWeight == 'bold') ? 600 : 100 );
    $draw->setFontStyle(0);
    //$draw->setFillColor($color);
    $draw->setFillColor('red');
    $draw->setStrokeAntialias(true);
    $draw->setTextAntialias(true);

    // Position text at the bottom-right of the image
    //$draw->setGravity(Imagick::GRAVITY_SOUTHEAST);
    //$draw->setGravity(Imagick::GRAVITY_CENTER);

    $metrics = $print->queryFontMetrics($draw, $object->text);
    // $widthScale = round($printData->objects[0]->width / 384);
    // $heightScale = round($printData->objects[0]->height / 576);
    // $left = round(($object->left * $object->scaleX) * $widthScale);
    // $top = round(($object->top * $object->scaleY)* $heightScale);

    $draw->rotate(1);

    $draw->annotation($object->left, $object->top, $object->text);
    $print->drawImage($draw);

}

convertJsonToImage();
