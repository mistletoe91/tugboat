<?php
//Disabled error reporting for live but this should be in config somewhere
ini_set('display_errors',0);
ini_set('display_startup_errors', 0);
error_reporting(0);

// createImage.php
if(!trim($_POST["str"])){
  exit(1);
}

$data = base64_decode($_POST["str"]);

$nameImage = generatefilename ();
$appendname = '_back';
if($_POST["front"]){
	$appendname = '_front';
} else {
	$appendname = '_back';
} 

//$urlUploadImages = "image/u/".$nameImage.".png";
 $urlUploadImages_normal = "image/u/".$nameImage.".jpg";
 $urlUploadImages_cmyk = "image/u/".$nameImage."_cmyk.jpg";
 
file_put_contents($urlUploadImages_normal, $data);
//system("convert -units PixelsPerInch $urlUploadImages_normal -profile  sRGB_v4_ICC_preference.icc -profile USWebCoatedSWOP.icc -density 450 ".$urlUploadImages_cmyk."");
//file_put_contents($urlUploadImages_cmyk, $data);
echo '["'.$nameImage.'"]'; 

function generatefilename (){
	$random_string_length = 10;
	$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	$max = strlen($characters) - 1;
	for ($i = 0; $i < $random_string_length; $i++) {
		$string .= $characters[mt_rand(0, $max)];
	}
	return $string;
}