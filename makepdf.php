<?php

$showPdf = generatePDF();

function generatePDF() {
    $image1 = $_POST['imageOne'];
    $image2 = $_POST['imageTwo'];
    $pdf = new Imagick();
    $pdf->newImage($image1);
    $pdf->newImage($image2);
    $pdf->resetIterator();
    $combined = $pdf->appendImages(true);
    $combined->setFormat('pdf');
    return $combined;
}
echo '["'.$showPdf.'"]';