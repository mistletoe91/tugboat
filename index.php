<?php

if(strpos( $_SERVER['SERVER_NAME'], "local.tugboat.cc" )){
	#ini_set('display_errors', 1);
	ini_set('log_errors', 1);
	error_reporting(E_ALL);
} else {
	ini_set("display_errors", "0");
	error_reporting(0);
}

// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');
